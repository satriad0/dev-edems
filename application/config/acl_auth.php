<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['identity_field'] 	= 'username';
$config['remember_field'] 	= 'remember_code';
$config['password_field'] 	= 'password';
$config['user_table']	  	= 'MPE_USERS';
$config['user_model']		= 'user_model';
$config['general_model']		= 'general_model';

$config['admin_mail'] 		= '';
$config['admin_name']		= '';
$config['reset_subject'] 	= '';
$config['reset_template'] 	= '';

//Own 401 Page - define here
$config['401_override'] 	= '';

//Own Login Page?
$config['401_login_page'] 	= '';
