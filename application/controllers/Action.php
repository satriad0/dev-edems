<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * untuk melakukan approve atau reject ERF, EAT, Documents
 */
class Action extends CI_Controller {

    function __construct() {
        // code...
        parent::__construct();
        $this->load->model('M_hris', 'hris');
        $this->load->model("erf/M_request", "req");
        $this->load->model("eat/M_assign", "assig");
        $this->load->model("bast/M_bast", "bast");
        $this->load->model("dok_eng/M_dok_eng", "dok_eng");
        $this->load->model("dok_eng/M_tender", "tender");
        $this->load->model("scm/M_scm", "scm");
        $this->load->model("assist/M_req_aanwidjing", "req_aan");
        $this->load->model("assist/M_req_evaluation", "req_eva");
        $this->load->model("assist/M_aanwidjing", "aan");
        $this->load->model("assist/M_evatech", "evatech");
        $this->load->model("others/M_approval", "app");
    }

    public function erf($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->req->get_datarequest($akey[3]);


        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($akey[2]) . "' OR k.mk_email like '%" . $this->db->escape_str($akey[2]) . "@%')"); //salah
        $peg = (array) $peg[0];

        $dataerf = array(
            'TITLE' => 'Engineering Request Form',
            'SHORT' => 'ERF',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $data['DESCPSECTION'],
            'WBS' => $data['WBS_CODE'],
            'WBS_DESC' => $data['WBS_TEXT'],
            'UK_CODE' => $data['UK_CODE'],
            'LATAR_BELAKANG' => $this->toHtml($data['LATAR_BELAKANG']),
            'CUST_REQ' => $this->toHtml($data['CUST_REQ']),
            'TECH_INFO' => $this->toHtml($data['TECH_INFO']),
            'RES_MIT' => $this->toHtml($data['RES_MIT']),
            'NAMA_PEKERJAAN' => $data['NAMA_PEKERJAAN'],
            'DESCPSECTION' => $data['DESCPSECTION'],
            'TIPE' => $data['TIPE'],
            'CONSTRUCT_COST' => $data['CONSTRUCT_COST'],
            'ENG_COST' => $data['ENG_COST'],
            'FILES' => $data['FILES'],
            'FILES2' => $data['FILES2'],
            'FILES3' => $data['FILES3'],
            'APPROVE' => $data['APPROVE_AT'],
            'UPDATED' => $data['UPDATE_AT'],
            'REJECTED' => $data['REJECT_DATE'],
            'STATUS' => $data['STATUS'],
            'NO' => $data['NO_PENGAJUAN'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'LOKASI' => $data['LOKASI'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'd' => $url,
            'ASPECT1' => $data['ASPECT1'],
            'ASPECT2' => $data['ASPECT2'],
            'ASPECT3' => $data['ASPECT3'],
            'ASPECT4' => $data['ASPECT4']
        );

        $this->load->view('action/erf', $dataerf);
    }

    public function eat($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->assig->get_record($akey[6]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($akey[5]) . "' OR k.mk_email like '%" . $this->db->escape_str($akey[5]) . "@%')"); //salah
        $peg = (array) $peg[0];

        $dataeat = array(
            'TITLE' => 'Engineering Assign Task',
            'SHORT' => 'EAT',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_PENUGASAN'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'NO_PENGAJUAN' => $data['NO_PENGAJUAN'],
            'DESKRIPSI' => $data['SHORT_TEXT'],
            'LOKASI' => $data['LOKASI'],
            'BY' => $data['CREATE_BY'],
            'AT' => $data['CREATE_AT'],
            'STARTDATE' => date("d M Y", strtotime($data['START_DATE']) ),
            'ENDDATE' => date("d M Y", strtotime($data['END_DATE']) ),
            'TIM_PENGKAJI' => $data['TIM_PENGKAJI'],
            'OBJECTIVE' => $this->toHtml($data['OBJECTIVE']),
            'CUS_REQ' => $this->toHtml($data['CUS_REQ']),
            'SCOPE_ENG' => $this->toHtml($data['SCOPE_ENG']),
            'KOMITE' => $data['KOMITE_PENGARAH'],
            'TWKOMITE' => $this->toHtml($data['TW_KOM_PENGARAH']),
            'TWPENGKAJI' => $this->toHtml($data['TW_TIM_PENGKAJI']),
            'LEAD' => $data['LEAD_ASS'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'PLANT_SECTION' => $data['PLANT_SECTION'],
            'DESCPSECTION' => $data['DESCPSECTION'],
            'FILES' => $data['FILES'],
            'd' => $url,
            'UPDATED' => $data['UPDATE_AT'],
            'REJECTED' => $data['REJECT_DATE'],
            'STATUS' => $data['STATUS'],
            'APPROVE0_BY' => $data['APPROVE0_BY'],
            'APPROVE1_BY' => $data['APPROVE1_BY'],
            'APPROVE2_BY' => $data['APPROVE2_BY'],
            'APPROVE0_AT' => $data['APPROVE0_AT'],
            'APPROVE1_AT' => $data['APPROVE1_AT'],
            'APPROVE2_AT' => $data['APPROVE2_AT'],
        );

        $this->load->view('action/eat', $dataeat);
    }

    public function dok_eng($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);

        // echo "<pre>";
        // print_r($akey);
        // exit;

        list($badge, $nama) = split(' - ', $akey[5]);

        $data = $this->dok_eng->get_record("A.ID = " . $akey[0]);
        $data = $data[0];


        if ($akey[4] == 'CRT' || $akey[4] == 'Paraf' || $akey[4] == 'TTD') {
            $awhere['REF_ID'] = $akey[0];
            $awhere['REF_TABEL'] = 'DOK_ENG';
            $awhere['TIPE'] = $akey[4];
            $awhere['APP_NAME'] = $nama;
            $dtApp = $this->app->get_data($awhere);
            $dtApp = $dtApp[0];
            $approve = $dtApp['UPDATE_AT'];
        } else {
            $approve = $data['APPROVE' . $akey[1] . '_AT'];
        }

        $pieces = explode(" - ", $akey[5]);

        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($pieces[1]) . "' OR k.mk_email like '%" . $this->db->escape_str($pieces[1]) . "@%')"); //salah
        $peg = (array) $peg[0];

        $datadoc = array(
            'TITLE' => 'Documents Engineering',
            'SHORT' => 'Doc-Eng',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_DOK_ENG'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'DESKRIPSI' => $data['PACKET_TEXT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'NOMINAL' => $data['NOMINAL'],
            'KAJIAN' => $data['KAJIAN_FILE'],
            'RKS' => $data['RKS_FILE'],
            'BQ' => $data['BQ_FILE'],
            'DRAW' => $data['DRAW_FILE'],
            'ECE' => $data['ECE_FILE'],
            'd' => $url,
            'CHECK' => $approve,
            'STATUS' => $data['STATUS'],
        );

        $this->load->view('action/dok', $datadoc);
    }

    public function bast($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);

        $data = $this->bast->get_record($akey[6]);

        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[5]}' OR k.mk_email like '%{$akey[5]}@%')"); //salah
        $peg = (array) $peg[0];
        $foreign = $this->dok_eng->get_record("A.ID = " . $data['ID_DOK_ENG']);
        $foreign = $foreign[0];

        $dataerf = array(
            'TITLE' => 'Berita Acara Serah Terima',
            'SHORT' => 'BAST',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $data['UK_TEXT'],
            //
            'NO_BAST' => $data['NO_BAST'],
            'PACKET_TEXT' => $data['PACKET_TEXT'],
            'UK_TEXT' => $data['UK_TEXT'],
            'LOKASI' => $data['LOKASI'],
            'HDR_TEXT' => $this->toHtml($data['HDR_TEXT']),
            'MID_TEXT' => $this->toHtml($data['MID_TEXT']),
            'FTR_TEXT' => $this->toHtml($data['FTR_TEXT']),
            'JAB' => $akey[1],
            'CHECK' => $data['APPROVE' . $akey[1] . '_AT'],
            'STATUS' => $data['STATUS'],
            'RKS' => $foreign['RKS_FILE'],
            'BQ' => $foreign['RKS_FILE'],
            'DRAW' => $foreign['RKS_FILE'],
            'd' => $url,
        );

        $this->load->view('action/bast', $dataerf);
    }

    public function tender($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);

        list($badge, $nama) = split(' - ', $akey[5]);

        $data = $this->tender->get_record("A.ID = " . $akey[0]);
        $data = $data[0];

        $awhere['REF_ID'] = $akey[0];
        $awhere['REF_TABEL'] = 'DOK_TEND';
        $awhere['TIPE'] = $akey[4];
        $awhere['APP_NAME'] = $nama;
        $dtApp = $this->app->get_data($awhere);
        $dtApp = $dtApp[0];
        $approve = $dtApp['UPDATE_AT'];

        $pieces = explode(" - ", $akey[5]);

        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($pieces[1]) . "' OR k.mk_email like '%" . $this->db->escape_str($pieces[1]) . "@%')"); //salah
        $peg = (array) $peg[0];

        $dtTender = array(
            'TITLE' => 'Documents Tender',
            'SHORT' => 'Tender',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_DOK_TEND'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'DESKRIPSI' => $data['PACKET_TEXT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            // 'PROGRESS' => $data['PROGRESS'],
            'KAJIAN' => $data['KAJIAN_FILE'],
            'RKS' => $data['RKS_FILE'],
            'BQ' => $data['BQ_FILE'],
            'DRAW' => $data['DRAW_FILE'],
            'ECE' => $data['ECE_FILE'],
            'd' => $url,
            'CHECK' => $approve,
            'STATUS' => $data['STATUS'],
        );

        $this->load->view('action/tend', $dtTender);
    }

    public function scm($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);

        list($badge, $nama) = split(' - ', $akey[5]);

        $data = $this->scm->get_record("A.ID = " . $akey[0]);
        $data = $data[0];

        $awhere['REF_ID'] = $akey[0];
        $awhere['REF_TABEL'] = 'DOK_TEND';
        $awhere['TIPE'] = $akey[4];
        $awhere['APP_NAME'] = $nama;
        $dtApp = $this->app->get_data($awhere);
        $dtApp = $dtApp[0];
        $approve = $dtApp['UPDATE_AT'];

        $pieces = explode(" - ", $akey[5]);

        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($pieces[1]) . "' OR k.mk_email like '%" . $this->db->escape_str($pieces[1]) . "@%')"); //salah
        $peg = (array) $peg[0];

        $dtTender = array(
            'TITLE' => 'Documents Tender',
            'SHORT' => 'Tender',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_DOK_TEND'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'DESKRIPSI' => $data['PACKET_TEXT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'KAJIAN' => $data['KAJIAN_FILE'],
            'RKS' => $data['RKS_FILE'],
            'BQ' => $data['BQ_FILE'],
            'DRAW' => $data['DRAW_FILE'],
            'ECE' => $data['ECE_FILE'],
            'OTHERS' => $data['OTHERS_FILE'],
            'd' => $url,
            'CHECK' => $approve,
            'STATUS' => $data['STATUS'],
        );

        $this->load->view('action/scm', $dtTender);
    }

    public function awr($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->req_aan->get_record($akey[4]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($akey[3]) . "' OR k.mk_email like '" . $this->db->escape_str($akey[3]) . "')"); //salah
        $peg = (array) $peg[0];

        $datatrn = array(
            'TITLE' => 'Aanwijzing Request',
            'SHORT' => 'AWR',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_PENDAMPINGAN'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'NO_PENGAJUAN' => $data['NO_PENGAJUAN'],
            'DESKRIPSI' => $data['SHORT_TEXT'],
            'PACKET' => $data['PACKET_TEXT'],
            'FL_TEXT' => $data['FL_TEXT'],
            'BY' => $data['CREATE_BY'],
            'AT' => $data['CREATE_AT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'PLANT_SECTION' => $data['PLANT_SECTION'],
            'DESCPSECTION' => $data['DESCPSECTION'],
            'FILES' => $data['FILES'],
            'd' => $url,
            'UPDATED' => $data['UPDATE_AT'],
            'REJECTED' => $data['REJECT_AT'],
            'STATUS' => $data['STATUS'],
            'APPROVE1_BY' => $data['APPROVE1_BY'],
            'APPROVE2_BY' => $data['APPROVE2_BY'],
            'APPROVE1_AT' => $data['APPROVE1_AT'],
            'APPROVE2_AT' => $data['APPROVE2_AT'],
        );

        $this->load->view('action/awr', $datatrn);
    }

    public function tvr($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->req_eva->get_record($akey[4]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($akey[3]) . "' OR k.mk_email like '" . $this->db->escape_str($akey[3]) . "')"); //salah
        $peg = (array) $peg[0];

        $datatrn = array(
            'TITLE' => 'Technical Evaluation Request',
            'SHORT' => 'TVR',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_PENDAMPINGAN'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'NO_PENGAJUAN' => $data['NO_PENGAJUAN'],
            'DESKRIPSI' => $data['SHORT_TEXT'],
            'PACKET' => $data['PACKET_TEXT'],
            'FL_TEXT' => $data['FL_TEXT'],
            'BY' => $data['CREATE_BY'],
            'AT' => $data['CREATE_AT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'PLANT_SECTION' => $data['PLANT_SECTION'],
            'DESCPSECTION' => $data['DESCPSECTION'],
            'FILES' => $data['FILES'],
            'd' => $url,
            'UPDATED' => $data['UPDATE_AT'],
            'REJECTED' => $data['REJECT_AT'],
            'STATUS' => $data['STATUS'],
            'APPROVE1_BY' => $data['APPROVE1_BY'],
            'APPROVE2_BY' => $data['APPROVE2_BY'],
            'APPROVE1_AT' => $data['APPROVE1_AT'],
            'APPROVE2_AT' => $data['APPROVE2_AT'],
        );

        $this->load->view('action/tvr', $datatrn);
    }

    public function aws($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->aan->get_record($akey[4]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($akey[3]) . "' OR k.mk_email like '" . $this->db->escape_str($akey[3]) . "')"); //salah
        $peg = (array) $peg[0];

        $datatrn = array(
            'TITLE' => 'Aanwidjing Assistance',
            'SHORT' => 'AWS',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_PENDAMPINGAN'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'NO_PENGAJUAN' => $data['NO_PENGAJUAN'],
            'DESKRIPSI' => $data['SHORT_TEXT'],
            'PACKET' => $data['PACKET_TEXT'],
            'FL_TEXT' => $data['FL_TEXT'],
            'BY' => $data['CREATE_BY'],
            'AT' => $data['CREATE_AT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'PLANT_SECTION' => $data['PLANT_SECTION'],
            'DESCPSECTION' => $data['DESCPSECTION'],
            'PENGKAJI' => $data['TIM_PENGKAJI'],
            'FILES' => $data['FILES'],
            'd' => $url,
            'UPDATED' => $data['UPDATE_AT'],
            'REJECTED' => $data['REJECT_AT'],
            'STATUS' => $data['STATUS'],
            'APPROVE1_BY' => $data['APPROVE1_BY'],
            'APPROVE2_BY' => $data['APPROVE2_BY'],
            'APPROVE1_AT' => $data['APPROVE1_AT'],
            'APPROVE2_AT' => $data['APPROVE2_AT'],
        );

        $this->load->view('action/aws', $datatrn);
    }

    public function tva($key) {
        $this->load->library('Template');
        $url = $key;

        $key = str_replace('6s4', '/', $key);
        $key = str_replace('1p4', '+', $key);
        $key = str_replace('sM9', '=', $key);
        $key = $this->my_crypt($key, 'd');
        $akey = explode(";", $key);
        $data = $this->evatech->get_record($akey[4]);
        $data = $data[0];
        $peg = $this->hris->get_hris_where("(k.mk_nama = '" . $this->db->escape_str($akey[3]) . "' OR k.mk_email like '" . $this->db->escape_str($akey[3]) . "')"); //salah
        $peg = (array) $peg[0];

        $datatrn = array(
            'TITLE' => 'Technical Evaluation Assistance',
            'SHORT' => 'tva',
            'DATE' => $akey[2],
            'BADGE' => $peg['NOBADGE'],
            'NAMA' => $peg['NAMA'],
            'UK_TEXT' => $peg['UK_TEXT'],
            'NO' => $data['NO_PENDAMPINGAN'],
            'NOTIFIKASI' => $data['NOTIFIKASI'],
            'NO_PENGAJUAN' => $data['NO_PENGAJUAN'],
            'DESKRIPSI' => $data['SHORT_TEXT'],
            'PACKET' => $data['PACKET_TEXT'],
            'FL_TEXT' => $data['FL_TEXT'],
            'BY' => $data['CREATE_BY'],
            'AT' => $data['CREATE_AT'],
            'COMPANY' => $data['COMPANY'],
            'COMP_TEXT' => $data['COMP_TEXT'],
            'PLANT_SECTION' => $data['PLANT_SECTION'],
            'DESCPSECTION' => $data['DESCPSECTION'],
            'PENGKAJI' => $data['TIM_PENGKAJI'],
            'FILES' => $data['FILES'],
            'd' => $url,
            'UPDATED' => $data['UPDATE_AT'],
            'REJECTED' => $data['REJECT_AT'],
            'STATUS' => $data['STATUS'],
            'APPROVE1_BY' => $data['APPROVE1_BY'],
            'APPROVE2_BY' => $data['APPROVE2_BY'],
            'APPROVE1_AT' => $data['APPROVE1_AT'],
            'APPROVE2_AT' => $data['APPROVE2_AT'],
        );

        $this->load->view('action/tva', $datatrn);
    }

    public function deleteTmp() {
        if ($_SERVER['SERVER_ADDR'] == '10.15.5.150') {
            $folder_path = "/opt/lampp/htdocs/dev/e-dems/media/upload/tmp";
        } else {
            $folder_path = "/opt/lampp/APP/e-dems/media/upload/tmp";
        }

        $files = glob($folder_path . '/*');

        foreach ($files as $file) {
            if (is_file($file))
                unlink($file);
        }
    }

}

?>
