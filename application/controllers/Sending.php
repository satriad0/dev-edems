<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * untuk melakukan approve atau reject ERF, EAT, Documents
 */
class Action extends CI_Controller
{

  function __construct()
  {
    // code...
    parent::__construct();
	  $this->load->model('M_hris', 'hris');
		$this->load->model("erf/M_request", "req");
		$this->load->model("eat/M_assign", "assig");
    $this->load->model("bast/M_bast", "bast");
		$this->load->model("dok_eng/M_dok_eng", "dok_eng");
    $this->load->model("others/M_approval", "app");
  }

  public function erf($key){
    $this->load->library('Template');
    $url = $key;

    $key = str_replace('6s4', '/', $key);
    $key =  str_replace('1p4', '+', $key);
    $key =  str_replace('sM9', '=', $key);
    $key = $this->my_crypt($key, 'd');
    $akey = explode(";", $key);
    $data = $this->req->get_datarequest($akey[3]);


    $data = $data[0];
    $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[2]}' OR k.mk_email like '%{$akey[2]}@%')"); //salah
    $peg = (array) $peg[0];

    //$n_form=$this->my_crypt("{$data['NO_PENUGASAN']};1;{$data['NOTIFIKASI']};{$data['OBJECTIVE']};{$peg['NOBADGE']};{$akey[5]};{$akey[6]}";"",'e');


    $dataerf = array(
      'TITLE' => 'Engineering Request Form',
      'SHORT' => 'ERF',
      'DATE' => $akey[2],
      'BADGE' => $peg['NOBADGE'],
      'NAMA' => $peg['NAMA'],
      'UK_TEXT' => $data['UK_TEXT'],

      'WBS' => $data['WBS_CODE'],
      'WBS_DESC' => $data['WBS_TEXT'],
      'UK_CODE' => $data['UK_CODE'],
      'LATAR_BELAKANG' => $this->toHtml($data['LATAR_BELAKANG']),
      'CUST_REQ' => $this->toHtml($data['CUST_REQ']),
      'TECH_INFO' => $this->toHtml($data['TECH_INFO']),
      'RES_MIT' => $this->toHtml($data['RES_MIT']),
      'NAMA_PEKERJAAN' => $data['NAMA_PEKERJAAN'],
      'DESCPSECTION' => $data['DESCPSECTION'],
      'TIPE' => $data['TIPE'],
      'CONSTRUCT_COST' => $data['CONSTRUCT_COST'],
      'ENG_COST' => $data['ENG_COST'],
      'FILES' => $data['FILES'],

      'APPROVE' => $data['APPROVE_AT'],
      'UPDATED' => $data['UPDATE_AT'],
      'REJECTED' => $data['REJECT_DATE'],
      'STATUS' => $data['STATUS'],

      'NO' => $data['NO_PENGAJUAN'],
      'NOTIFIKASI' => $data['NOTIFIKASI'],
      'LOKASI' => $data['LOKASI'],
      'd' => $url,
    );

    //
    // $param['APP']['DATE']=$akey[2];
    // $param['APP']['BADGE']=$peg['NOBADGE'];
    // $param['APP']['NAMA']=$peg['NAMA'];
    // $param['APP']['UK_TEXT']=$peg['UK_TEXT'];
    // $param['TRN']['NO']="<b>No. {$param['short']} :</b> {$data['NO_PENUGASAN']}";
    // $param['TRN']['NOTIFIKASI']="<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
    // $param['TRN']['DESKRIPSI']="<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
    // $param['TRN']['LOKASI']="<b>Lokasi :</b> {$data['LOKASI']}";

    //echo $this->template->view_app($param);
     $this->load->view('action/erf',$dataerf);
  }

  public function eat($key){
    $this->load->library('Template');
    $url = $key;

    $key = str_replace('6s4', '/', $key);
    $key =  str_replace('1p4', '+', $key);
    $key =  str_replace('sM9', '=', $key);
    $key = $this->my_crypt($key, 'd');
    $akey = explode(";", $key);
    $data = $this->assig->get_record($akey[6]);
    $data = $data[0];
    $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[5]}' OR k.mk_email like '%{$akey[5]}@%')"); //salah
    $peg = (array) $peg[0];

    //$n_form=$this->my_crypt("{$data['NO_PENUGASAN']};1;{$data['NOTIFIKASI']};{$data['OBJECTIVE']};{$peg['NOBADGE']};{$akey[5]};{$akey[6]}";"",'e');

    $dataeat = array(
      'TITLE' => 'Engineering Assign Task',
      'SHORT' => 'EAT',
      'DATE' => $akey[2],
      'BADGE' => $peg['NOBADGE'],
      'NAMA' => $peg['NAMA'],
      'UK_TEXT' => $peg['UK_TEXT'],

      'NO' => $data['NO_PENUGASAN'],
      'NOTIFIKASI' => $data['NOTIFIKASI'],
      'DESKRIPSI' => $data['SHORT_TEXT'],
      'LOKASI' => $data['LOKASI'],
      'BY' => $data['CREATE_BY'],
      'AT' => $data['CREATE_AT'],
      'STARTDATE' => $data['START_DATE'],
      'ENDDATE' => $data['END_DATE'],
      'TIM_PENGKAJI' => $data['TIM_PENGKAJI'],
      'OBJECTIVE' => $this->toHtml($data['OBJECTIVE']),
      'CUS_REQ' => $this->toHtml($data['CUS_REQ']),
      'SCOPE_ENG' => $this->toHtml($data['SCOPE_ENG']),
      'KOMITE' => $data['KOMITE_PENGARAH'],
      'TWKOMITE' => $this->toHtml($data['TW_KOM_PENGARAH']),
      'TWPENGKAJI' => $this->toHtml($data['TW_TIM_PENGKAJI']),
      'LEAD' => $data['LEAD_ASS'],
      'COMPANY' => $data['COMPANY'],
      'd' => $url,

      'UPDATED' => $data['UPDATE_AT'],
      'REJECTED' => $data['REJECT_DATE'],
      'STATUS' => $data['STATUS'],
      'APPROVE0_BY' => $data['APPROVE0_BY'],
      'APPROVE1_BY' => $data['APPROVE1_BY'],
      'APPROVE2_BY' => $data['APPROVE2_BY'],
      'APPROVE0_AT' => $data['APPROVE0_AT'],
      'APPROVE1_AT' => $data['APPROVE1_AT'],
      'APPROVE2_AT' => $data['APPROVE2_AT'],


    );

    //
    // $param['APP']['DATE']=$akey[2];
    // $param['APP']['BADGE']=$peg['NOBADGE'];
    // $param['APP']['NAMA']=$peg['NAMA'];
    // $param['APP']['UK_TEXT']=$peg['UK_TEXT'];
    // $param['TRN']['NO']="<b>No. {$param['short']} :</b> {$data['NO_PENUGASAN']}";
    // $param['TRN']['NOTIFIKASI']="<b>Notifikasi :</b> {$data['NOTIFIKASI']}";
    // $param['TRN']['DESKRIPSI']="<b>Deskripsi :</b> {$data['SHORT_TEXT']}";
    // $param['TRN']['LOKASI']="<b>Lokasi :</b> {$data['LOKASI']}";

    //echo $this->template->view_app($param);
     $this->load->view('action/eat',$dataeat);
  }

  public function dok_eng($key){
    $this->load->library('Template');
    $url = $key;

    $key = str_replace('6s4', '/', $key);
    $key =  str_replace('1p4', '+', $key);
    $key =  str_replace('sM9', '=', $key);
    $key = $this->my_crypt($key, 'd');
    $akey = explode(";", $key);

    list($badge, $nama) =  split(' - ',$akey[5]);

    $data = $this->dok_eng->get_record("A.ID = ".$akey[0]);
    $data = $data[0];

    if ($akey[4] == 'Paraf' || $akey[4] == 'TTD') {
      $dtApp = $this->app->get_data("REF_ID = '{$akey[0]}' AND TIPE = '{$akey[4]}' AND APP_NAME = '{$nama}'");
      $dtApp = $dtApp[0];
      $approve = $dtApp['UPDATE_AT'];
    }else {
      $approve = $data['APPROVE'.$akey[1].'_AT'];
    }

    $pieces = explode(" - ", $akey[5]);
    //print_r($pieces);

    $peg = $this->hris->get_hris_where("(k.mk_nama = '{$pieces[1]}' OR k.mk_email like '%{$pieces[1]}@%')"); //salah
    $peg = (array) $peg[0];
    // echo "<pre>";
    // print_r($peg);
    // print_r($data);
    // print_r($akey);
    // echo "</pre>";
    // exit;
    //$n_form=$this->my_crypt("{$data['NO_PENUGASAN']};1;{$data['NOTIFIKASI']};{$data['OBJECTIVE']};{$peg['NOBADGE']};{$akey[5]};{$akey[6]}";"",'e');

    $datadoc = array(
      'TITLE' => 'Documents Engineering',
      'SHORT' => 'Doc-Eng',
      'DATE' => $akey[2],
      'BADGE' => $peg['NOBADGE'],
      'NAMA' => $peg['NAMA'],
      'UK_TEXT' => $peg['UK_TEXT'],


      'NO' => $data['NO_DOK_ENG'],
      'NOTIFIKASI' => $data['NOTIFIKASI'],
      'DESKRIPSI' => $data['PACKET_TEXT'],
      'COMPANY' => $data['COMPANY'],
      'PROGRESS' => $data['PROGRESS'],
      'RKS' => $data['RKS_FILE'],
      'BQ' => $data['BQ_FILE'],
      'DRAW' => $data['DRAW_FILE'],
      'ECE' => $data['ECE_FILE'],
      'd' => $url,
      'CHECK' => $approve,
      'STATUS' => $data['STATUS'],
    );

    //echo $this->template->view_app($param);
     $this->load->view('sending/dok',$datadoc);
  }

  public function bast($key){
    $this->load->library('Template');
    $url = $key;

    $key = str_replace('6s4', '/', $key);
    $key =  str_replace('1p4', '+', $key);
    $key =  str_replace('sM9', '=', $key);
    $key = $this->my_crypt($key, 'd');
    $akey = explode(";", $key);

    $data = $this->bast->get_record($akey[6]);

    $data = $data[0];
    $peg = $this->hris->get_hris_where("(k.mk_nama = '{$akey[5]}' OR k.mk_email like '%{$akey[5]}@%')"); //salah
    $peg = (array) $peg[0];
    $foreign = $this->dok_eng->get_record("A.ID = ".$data['ID_DOK_ENG']);
    $foreign = $foreign[0];


    // echo "<pre>";
    // print_r($akey[1]);
    // echo "</pre>";
    // echo("APPROVE"$akey[1]);
    //$n_form=$this->my_crypt("{$data['NO_PENUGASAN']};1;{$data['NOTIFIKASI']};{$data['OBJECTIVE']};{$peg['NOBADGE']};{$akey[5]};{$akey[6]}";"",'e')

    $dataerf = array(
       'TITLE' => 'Berita Acara Serah Terima',
       'SHORT' => 'BAST',
       'DATE' => $akey[2],
       'BADGE' => $peg['NOBADGE'],
       'NAMA' => $peg['NAMA'],
       'UK_TEXT' => $data['UK_TEXT'],
      //
       'NO_BAST' => $data['NO_BAST'],
       'PACKET_TEXT' => $data['PACKET_TEXT'],
       'UK_TEXT' => $data['UK_TEXT'],
       'LOKASI' => $data['LOKASI'],
       'HDR_TEXT' => $this->toHtml($data['HDR_TEXT']),
       'MID_TEXT' => $this->toHtml($data['MID_TEXT']),
       'FTR_TEXT' => $this->toHtml($data['FTR_TEXT']),
       'JAB' => $akey[1],
       'CHECK' => $data['APPROVE'.$akey[1].'_AT'],
      // 'NAMA_PEKERJAAN' => $data['NAMA_PEKERJAAN'],
      // 'DESCPSECTION' => $data['DESCPSECTION'],
      //
      // 'APPROVE' => $data['APPROVE_AT'],
      // 'UPDATED' => $data['UPDATE_AT'],
      // 'REJECTED' => $data['REJECT_DATE'],
       'STATUS' => $data['STATUS'],
       'RKS' => $foreign['RKS_FILE'],
       'BQ' => $foreign['RKS_FILE'],
       'DRAW' => $foreign['RKS_FILE'],
       // 'ECE' => $foreign['RKS_FILE'],
      //
      // 'NO' => $data['NO_PENGAJUAN'],
      // 'NOTIFIKASI' => $data['NOTIFIKASI'],
      // 'LOKASI' => $data['LOKASI'],
       'd' => $url,
    );

    //echo $this->template->view_app($param);
     $this->load->view('action/bast',$dataerf);
  }

}
 ?>
