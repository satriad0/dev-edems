<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// class Auth extends APIs {
class Setting extends CI_Controller {

    function __construct()
	{
		parent::__construct();

		 $this->load->model('M_menu', 'menu');
		 $this->load->model('M_setting', 'setting');
	}

	public function test(){
		print_r($this->test);
	}

  public function send_email(){

    $token = $this->post('token');

    // $em =array('bagus.hid@gmail.com', 'fboyz283@gmail.com', 'official@omcahyo.com', 'm.r.sucahyo@gmail.com', 'oadhim@gmail.com');
    $em =array('mohamad.mansur@sisi.id', 'rusnam21@gmail.com', 'bagus.hid@gmail.com', 'fboyz283@gmail.com', 'm.r.sucahyo@gmail.com', 'oadhim@gmail.com');

    if ($this->validasi($token)) {
        $request = $this->request();
        $payload = $this->payload($token)->data;

        // print_r($request['ALL_MAIL']);
        $data = json_decode($request['ALL_MAIL'], true);

        $to = array();
        $cc = array();
        $code = $this->my_crypt(date("h:i:sa d-m-Y")."|".$request['ID_DOC']."|".$request['MENU']."|".$request['NOTIF'], 'e');
        $full = array();
        foreach ($data as $key => $value) {
            $em_key = 1;//array_rand($data);
            $tmp = array();
            $tmp['GROUP_MENU'] = $request['MENU'];
            $tmp['TIPE'] = $value['TIPE'];
            $tmp['COMPANY'] = $value['COMPANY'];
            $tmp['PLANT'] = $value['PLANT'];
            // $tmp['EMAIL'] = $value['EMAIL'];
            $tmp['EMAIL'] = $em[$em_key];
            $tmp['ID_DOC'] = $request['ID_DOC'];
            $tmp['NOTIF'] = $request['NOTIF'];
            $tmp['CREATE_AT'] = '';
            $tmp['CREATE_BY'] = $payload->username;
            $tmp['VERF_CODE'] = $code;
            // print_r( json_decode($value) );

            if ($this->setting->insert_email($tmp)) {
                $tmp['inserted'] = true;

                if ($value['TIPE']=='TO') {
                    array_push($to, $tmp['EMAIL']);
                }else if($value['TIPE']=='CC') {
                    array_push($cc, $tmp['EMAIL']);
                }

            }
            $full[] = $tmp;

        }
        echo $this->email($request['NOTIF'], $request['NOTIF'], '', $to, $cc);
        //
        print_r($full);
    }
  }

	public function migrasi_order()
	{
		$this->load->model('apd/M_order', 'order');
		// $this->load->model('apd/M_order', 'order');
		$this->db->where('KODE_UK', 'migrasi');
		$query = $this->db->get('SHE_APD_ORDER_APD');

		$tmp = array();
		foreach ($query->result_array() as $key => $value) {
			$out = array();
			$param['ID'] = $value['ID'];
			$badge = $value['NO_BADGE'];
			// $company = $value['COMPANY'];
			$users = $this->dbhris->get_user_hris($badge, '', '');

			$out['ORDER'] = $value;
			$out['USERS'] = $users;

			$update = array();
			$update['ID'] = $value['ID'];
			if ($value['KODE_UK']==''||$value['KODE_UK']=='migrasi') {
				$update['KODE_UK'] = $users->UNIT_KERJA ;
				$update['UK_TEXT'] = $users->UK_TEXT ;
			}

			if ($value['CODE_COMP']==''||$value['CODE_COMP']=='migrasi') {
				$update['CODE_COMP'] = $users->COMPANY ;
			}
			if ($value['CODE_PLANT']==''||$value['CODE_PLANT']=='migrasi') {
				if ($users->LOKASI =='Gresik') {
					$update['CODE_PLANT'] = 5002;
				}
				if ($users->LOKASI =='Tuban') {
					$update['CODE_PLANT'] = 5001;
				}
			}


			if (count($update)>1) {

				print_r($update);
				echo "<br>==================================================================";
				print_r($value);
				echo "==================================================================";
				print_r($users);
				echo "==================================================================";
				// echo $this->order->update_order($update);
				// echo $this->db->last_query();
				echo "==================================================================";
			}

			// $tmp[] = $out;
		}

		// $this->response('success', 200, $tmp);
	}

	public function menu($tipe, $id = NULL){


		$token = $_POST['token'];
		if ($this->validasi($token)) {
			$payload = $this->payload($token)->data;
			if ($tipe=='add') {
				$this->_menu_add($payload);
			}else if($tipe=='edit'){
				$this->_menu_edit($payload, $id);
			}else if ($tipe=='view'){
				$this->_menu_view();
			}else if ($tipe=='delete'){
				$this->_menu_delete($id);
			}

		}
	}
	//=====================================================================================================================
	//MENU
	//=====================================================================================================================
		public function _menu_add($payload){

			$data = array(
				'"NAME"' => $_POST['nama'],
				'DESCRIPTION' => $_POST['description'],
				'"GROUP"' => $_POST['submenu'],
				'URL' => $_POST['url'],
				'URL_HOME' => $_POST['url_home']
			);

			$result = $this->menu->insert_menu($data);

			if ($result) {
				echo $this->response('success', 200);
			}else{
				echo $this->response('error', 400);
			}

		}

		public function _menu_edit($payload, $id){

			$data = array(
				'"ID"' => $id,
				'"NAME"' => $_POST['nama'],
				'DESCRIPTION' => $_POST['description'],
				'"GROUP"' => $_POST['submenu'],
				'URL' => $_POST['url'],
				'URL_HOME' => $_POST['url_home']
			);

			$result = $this->menu->update_menu($data);
			// echo $this->db->last_query();
			if ($result) {
				echo $this->response('success', 200);
			}else{
				echo $this->response('error', 400);
			}

		}
		public function _menu_view(){
			$result = $this->menu->view_menu();
			// echo $this->db->last_query();
			if ($result) {
				echo $this->response('success', 200, $result);
			}else{
				echo $this->response('error', 400);
			}
		}

		public function _menu_delete($id){
			$result = $this->menu->delete_menu($id);
			// echo $this->db->last_query();
			if ($result) {
				echo $this->response('success', 200, $result);
			}else{
				echo $this->response('error', 400);
			}
		}

	public function unitkerja(){
		$token = $this->post('token');

		if ($this->validasi($token)) {
			$payload = $this->payload($token)->data;

			$result = $this->dbhris->get_unit_kerja($payload);

			if ($result) {
				echo $this->response2('success', 200, $result);
			}else{
				echo $this->response('error', 400);

			}
		}
	}

	public function company(){

		$result = $this->setting->get_company_plant();

		if ($result) {
			// print_r($result);
			$data = array();
			foreach ($result as $key => $value) {

				$data['COMPANY'][$value['COMPANY']] = $value['COMPANY_TEXT'];


			}
			foreach ($result as $key => $value) {
					$tmp = array();

				if($value['PLANT']){
					$data['PLANT'][$value['COMPANY']][$value['PLANT']] = $value['PLANT_TEXT'];
				}
        // else{
				// 	$data['PLANT'][$value['COMPANY']][] = NULL;
				// }

			}
			echo $this->response('success', 200, $data);
		}
	}


}
