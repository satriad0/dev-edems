<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Evatech extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    // public $muk_kode='';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("assist/M_evatech", "assig");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }
        
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'tva';
        $where['GEN_TYPE'] = 'FORM_GROUP';
        $global = $this->general_model->get_data($where);
        unset($where);
        
        $header["tittle"] = $global->GEN_DESC;
        $header["short_tittle"] = "tva";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        $header['page_state'] = $state . ";special_access={$info['special_access']}";

        $yyyy = date("Y");
        $xxxx = $this->assig->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "///PT/{$kodemax}//{$yyyy}";
        // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');

        $str2 = "
                k.mdept_kode = '{$info['dept_code']}' AND k.mk_eselon_code='20'
                ";
        $data['BIRO'] = $this->dbhris->get_hris_where($str2);

        // $this->db->where("PD.UK_CODE = '{$info['uk_kode']}'"); // GEN_PAR2
        $this->db->where("PE.PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')"); // GEN_PAR2
        $data['foreigns'] = $this->assig->get_foreign();

        // set approval manager
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'DE_MGR';
        $this->db->like('GEN_PAR5', $info['uk_kode']);
        $tmpDeMgr = $this->general_model->get_data_result($where);
        unset($where);

        $str = "
            k.muk_kode = '{$info['uk_kode']}' AND k.mk_emp_subgroup='30'
          ";

        if ($tmpDeMgr) {
            $data['MGR_APP'] = $tmpDeMgr;
            $tmpDeMgr = $tmpDeMgr[0];
            if ($info['uk_kode'] == $tmpDeMgr->GEN_PAR5) {
                $str .= " AND k.mk_email='{$tmpDeMgr->GEN_PAR4}'";
            }
        }
        $data['MANAGER'] = $this->dbhris->get_hris_where($str);
        unset($where);

        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'General';
        $where['GEN_CODE'] = 'ASSIST_TEXT';
        $data['ASSIST_TEXT'] = $this->general_model->get_data($where);
        unset($where);

        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'EAT_TIM';
        $data['PENGKAJI'] = $this->general_model->get_data_result($where);
        unset($where);

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        } else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }

        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('assist/evatech', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('search');
        $order = $this->input->post('order');

        $key = array(
            'search' => $search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
            $key['comp_view'] = $info['uk_kode'];
        }

        $data = $this->assig->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->assig->recFil($key),
            'recordsTotal' => $this->assig->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (count($info) > 0) {
            $this->db->where("PE.PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')"); // GEN_PAR2
            $result = $this->assig->get_foreign();

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function lead_ass() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $key = "({$muk_kode})";
        $result = $this->assig->get_lead($info['company'], $key);

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function pengkaji() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
        $where['GEN_TYPE'] = 'General';
        $global = $this->general_model->get_data($where);

        $result = $this->assig->get_lead($info['company'], $global->GEN_PAR5);

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function manager() {
        $this->load->model('M_hris', 'dbhris');
        $nama = '';
        if (isset($_POST['search'])) {
            $nama = $_POST['search'];
        }

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_hris_where("k.mdept_kode = '{$info['dept_code']}' AND k.mk_emp_subgroup='30' AND k.mk_nama LIKE '%{$nama}%'");
        // echo $this->dbhris->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$atas->NOBADGE}'");
        $peg = (array) $peg[0];

        $param['short'] = 'Technical Evaluation Assistance';
        $param['link'] = 'tva';
        $param['title'] = 'Technical Evaluation Assistance';
        $param['sender'] = "{$info['no_badge']} - {$info['name']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_PENDAMPINGAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$param['SHORT_TEXT']}";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);

        $this->email($this->template->set_app($param), $subject, '', $tomail, $cc);
    }

    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(0);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data = $this->assig->get_record($param['ID']);
            $data = $data[0];

            // $atasan = (object);
            $str2 = "
                    k.mk_nopeg = '{$data['APPROVE2_BADGE']}'
                    ";
            $atasan = $this->dbhris->get_hris_where($str2);
            $atasan = $atasan[0];
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }

            if ($data) {

                $n_form = $this->my_crypt("{$data['NO_PENDAMPINGAN']};{$data['NOTIFIKASI']};{$info['no_badge']};{$atasan->EMAIL};{$data['ID']};{$info['uk_kode']}", 'e');
                $n_form = str_replace('/', '6s4', $n_form);
                $n_form = str_replace('+', '1p4', $n_form);
                $n_form = str_replace('=', 'sM9', $n_form);
                $NOTIF_NO = $data['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                $to = $atasan->EMAIL;
                $cc = array();
                $subject = "Resend : EVATECH APPROVAL";
                $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {
        if (isset($_POST) && isset($_POST['ID_PENGAJUAN']) && isset($_POST['NOTIFIKASI'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }

            $tmpparam = $param;
            unset($param['NO_ERF']);
            unset($param['SHORT_TEXT']);

            // $ar_app = new stdClass();
            // if ($lvl == '1') {
            //     $ar_app = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE1_BADGE']}'");
            //     $ar_app = $ar_app[0];
            //     unset($param['APPROVE1_BADGE']);
            // } else {
            //     $ar_app = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE0_BADGE']}'");
            //     $ar_app = $ar_app[0];
            // }
            $param['NO_PENDAMPINGAN'] = $this->generateNo('NO_PENDAMPINGAN', 'MPE_PENDAMPINGAN', 'PT', $param['NO_PENDAMPINGAN']);
            $param['TIPE'] = 'EVATECH';
            $param['APPROVE1_BADGE'] = $atasan->NOBADGE;
            $param['APPROVE1_BY'] = $atasan->NAMA;
            $param['APPROVE1_JAB'] = $atasan->JAB_TEXT;
            $param['STATUS'] = 'In Approval';
            $param['CREATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENDAMPINGAN'], 'evatech', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }
            $result = $this->assig->save($param);
            // echo $this->db->last_query();

            if ($result['ID']) {
                // send email
                $n_form = $this->my_crypt("{$tmpparam['NO_PENDAMPINGAN']};{$tmpparam['NOTIFIKASI']};{$info['no_badge']};{$atasan->EMAIL};{$result['ID']};{$info['uk_kode']}", 'e');

                $to = $atasan->EMAIL;
                $cc = array();
                $subject = "EVATECH APPROVAL";
                $this->send_mail($subject, $tmpparam, $atasan, $n_form, $to, $cc);


                $res_message = 'Data has been saved' . '-';
                if (!$result['ID']) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            // echo "test";
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['ID_PENGAJUAN'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $atasan = new stdClass;
            if (isset($param['APPROVE2_BADGE'])) {
                $str2 = "
                      k.mk_nopeg = '{$param['APPROVE2_BADGE']}'
                      ";
                $atasan = $this->dbhris->get_hris_where($str2);
                $atasan = $atasan[0];
                if (!$atasan) {
                    $atasan->NOBADGE = 'null';
                    $atasan->NAMA = 'null';
                    $atasan->EMAIL = 'null';
                }
                $param['STATUS'] = 'In Approval DE';
            }
            // $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');

            $param['ID'] = $_POST['ID'];
            $param['UPDATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENDAMPINGAN'] . strtotime("now"), 'evatech', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
                $param['STATUS'] = 'Closed';
            }

            $tmpparam = $param;
            unset($param['NO_PENDAMPINGAN']);
            unset($param['NO_ERF']);
            unset($param['SHORT_TEXT']);

            $result = $this->assig->updateData('update', $param);


            if ($result) {
                // send email
                $cAtasan = (array) $atasan;
                if (!empty($cAtasan)) {
                    $n_form = $this->my_crypt("{$tmpparam['NO_PENDAMPINGAN']};{$tmpparam['NOTIFIKASI']};{$info['no_badge']};{$atasan->EMAIL};{$param['ID']};{$info['uk_kode']}", 'e');

                    $to = $atasan->EMAIL;
                    $cc = array();
                    $subject = "EVATECH APPROVAL";
                    $this->send_mail($subject, $tmpparam, $atasan, $n_form, $to, $cc);
                }

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        // echo $e;
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];

            $reason = $_POST['reason'];
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');

            list($no_form, $notifikasi, $no_badge, $atas_email, $id, $uk_kode) = split(";", $no_form);
            $data['no_form'] = $no_form;
            $data['STATUS'] = 'Approved DE';
            $this->db->set('NOTE2', $reason);
            $this->db->set('APPROVE2_AT', "CURRENT_DATE", false);
            $result = $this->assig->updateData('approve', $data);
            // echo $this->db->last_query();

            if ($result) {
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function reject() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        // echo $e;
        if (isset($_POST) && isset($_POST['e'])) {
            $e = $_POST['e'];

            $reason = $_POST['reason'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');

            list($no_form, $notifikasi, $no_badge, $atas_email, $id, $uk_kode) = split(";", $this->my_crypt($e, 'd'));
            $data['no_form'] = $no_form;
            $data['STATUS'] = 'Rejected';
            // $data['APPROVE'.$lvl.'_BY']=$nama;
            $this->db->set('REJECT_NOTE', $reason);
            $this->db->set('REJECT_AT', 'CURRENT_DATE', FALSE);
            $this->db->set('REJECT_BY', 'APPROVE1_BY', FALSE);
            $result = $this->assig->updateData('approve', $data);

            if ($result) {
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['id'] = $id;
            $data['UPDATE_BY'] = $info['name'];
            $result = $this->assig->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id) {
        if ($id) {

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // $list_data = $_POST;

            $where['GEN_VAL'] = '1';
            $where['GEN_TYPE'] = 'General';
            $where['GEN_CODE'] = 'EVA_ASSIST';
            $fText = $this->general_model->get_data($where);
            unset($where);

            $list_data = $this->assig->get_record($id);
            $list_data = $list_data[0];
            $dtApp1 = $this->dbhris->get_hris_where("k.mk_nopeg = '{$list_data['APPROVE1_BADGE']}'");
            if ($dtApp1) {
                $dtApp1 = $dtApp1[0];
            }
            $dtApp2 = $this->dbhris->get_hris_where("k.mk_nopeg = '{$list_data['APPROVE2_BADGE']}'");
            if ($dtApp2) {
                $dtApp2 = $dtApp2[0];
            }
            
//            echo '<pre>';
//            var_dump($list_data);
            
            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 7);
            $this->fpdf->SetLeftMargin(20);

            // header
            $this->fpdf->Cell(30, 11, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 13, 'PNG'), 0, 1, 'C');
            $this->fpdf->Cell(19, 4, '', 0, 0, 'R');
            $this->fpdf->SetFont('Arial', '', 9);
            $this->fpdf->Cell(145, 1, 'PT. SEMEN INDONESIA (PERSERO)Tbk.', 0, 1);

            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'BU', 16);
            $this->fpdf->Cell(170, 7, "Technical Evaluation Assist Assign Task", 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 6, "{$list_data['NO_PENDAMPINGAN']}", 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 12);
            //$this->fpdf->Cell(170, 4, "({$list_data['STATUS']})", 0, 1, 'C');//Edited by Agung Tarmudi by Request of Indra.Yudhi
            // // Lampiran
            // if ($list_data['FILES']) {
            //     $this->fpdf->SetFont('Arial','B',12);
            //     $this->fpdf->Cell(170,4,"(Telah Dikerjakan)",0,1,'C');
            // }
            $this->fpdf->Cell(55, 6, " Tanggal", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['CREATE_AT']}");
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Ln(5);
            $this->fpdf->Cell(55, 6, " Nama Pekerjaan", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['PACKET_TEXT']}");
            $this->fpdf->Cell(55, 6, " Unit Kerja Peminta", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['DESCPSECTION']}");
            $this->fpdf->Cell(55, 6, "", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, "", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['UK_TEXT']}");
            $this->fpdf->Cell(55, 6, " Company", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['COMP_TEXT']}");
            $this->fpdf->Ln(3);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);
            // header content
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Ln(5);
            $this->fpdf->MultiCell(170, 5, str_replace(array('#nomor'), array($list_data['NO_PENDAMPINGAN']), $fText->GEN_PAR1));
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $sPengarah = str_replace('[', '      - ', $list_data['TIM_PENGKAJI']);
            $sPengarah = str_replace(']', '', $sPengarah);
            $sPengarah = str_replace(',"', ',      - ', $sPengarah);
            $sPengarah = str_replace('"', '', $sPengarah);
            $sPengarah = str_replace(',', "\n", $sPengarah);
            $this->fpdf->MultiCell(170, 5, "{$sPengarah}", 0);
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$fText->GEN_PAR2}");
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(38, 6, " No. ERF", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(130, 6, "{$list_data['NO_PENGAJUAN']}");
            $this->fpdf->Cell(38, 6, " No. EAT", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(130, 6, "{$list_data['NO_PENUGASAN']}");
            $this->fpdf->Cell(38, 6, " No. Dok. Engineering", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(130, 6, "{$list_data['NO_DOK_ENG']}");
//            $this->fpdf->Cell(38, 6, " Paket Pekerjaan", 0, 0, 'L');
//            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
//            $this->fpdf->MultiCell(130, 6, "{$list_data['PACKET_TEXT']}");

            // footer content
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Ln(3);
            $this->fpdf->MultiCell(170, 5, "{$fText->GEN_PAR3}");
            
            // approval
            $this->fpdf->Ln(15);
            $this->fpdf->Cell(85, 7, "", 0, 0, 'C');
            $this->fpdf->Cell(85, 7, "Disetujui Oleh,", 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(85, 7, "", 0, 0, 'C');
            $this->fpdf->Cell(85, 7, "{$dtApp2->DEPT_TEXT}", 0, 1, 'C');

//            // approval
//            $this->fpdf->Ln(15);
//            $this->fpdf->Cell(85, 7, "Disetujui Oleh,", 0, 0, 'C');
//            $this->fpdf->Cell(85, 7, "", 0, 1, 'C');
//            $this->fpdf->SetFont('Arial', 'B', 10);
//            $this->fpdf->Cell(85, 7, "{$dtApp2->DEPT_TEXT}", 0, 0, 'C');
//            $this->fpdf->Cell(85, 7, "", 0, 1, 'C');

            include(APPPATH . 'libraries/phpqrcode/qrlib.php');
            // approval de
            $this->fpdf->Cell(85, 30, "", 0, 0, 'C');
            // approval peminta
            $tmpApp2 = FCPATH . "media/upload/tmp/" . md5("TVA-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APP2") . ".png";
            $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE2_BY']};{$list_data['APPROVE2_AT']};", 'e');
            $str = str_replace('/', '6s4', $str);
            $str = str_replace('+', '1p4', $str);
            $str = str_replace('=', 'sM9', $str);
            $str = base_url() . "info/tva/" . $str;
            if ($list_data['APPROVE2_AT']) {
                QRcode::png($str, $tmpApp2);
                $this->fpdf->Cell(85, 30, $this->fpdf->Image($tmpApp2, $this->fpdf->GetX() + (85 / 2) - 12.5, $this->fpdf->GetY() + 1, 0, 30, 'PNG'), 0, 1, 'C');
            } else {
                $this->fpdf->Cell(85, 30, "", 0, 1, 'C');
            }
            //nama
            $this->fpdf->Ln(3);
            $this->fpdf->Cell(85, 5, "", 0, 0, 'C');
            $this->fpdf->Cell(85, 5, "({$dtApp2->NAMA})", 0, 1, 'C');
            // approval peminta
            $this->fpdf->Cell(85, 5, "", 0, 0, 'C');
            $this->fpdf->Cell(85, 5, "({$dtApp2->JAB_TEXT})", 0, 0, 'C');
//            // nama
//            $this->fpdf->Ln(3);
//            $this->fpdf->Cell(85, 30, "", 0, 1, 'C');
//            $this->fpdf->Cell(85, 5, "{$dtApp2->NAMA}", 0, 0, 'C');
//            // approval peminta
//            $this->fpdf->Cell(85, 5, "", 0, 1, 'C');
//            $this->fpdf->Cell(85, 5, "({$dtApp2->JAB_TEXT})", 0, 0, 'C');

            // line
            $this->fpdf->SetFont('Arial', '', 9);
            $this->fpdf->Ln(18);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);

            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->Cell(170, 4, "* dokumen ini dibuat secara otomatis oleh sistem e-DEMS.", 0, 1);
            if ($list_data['FILES']) {
                $this->fpdf->Cell(170, 4, "** data pendukung kami sertakan terlampir dengan dokumen ini.", 0, 1);
            }

            // $this->fpdf->Output("AANWIDJING {$list_data['NO_PENDAMPINGAN']} {$list_data['NOTIFIKASI']}.pdf",'D');
            // temporary download
            $tmpPath = FCPATH . "media/upload/tmp/" . md5("EVATECH {$list_data['NOTIFIKASI']} {$list_data['NO_PENDAMPINGAN']}") . ".pdf";
            $this->fpdf->Output($tmpPath, 'F');

            $arfile = array($tmpPath);
            // Lampiran
            if ($list_data['FILES']) {
                array_push($arfile, FCPATH . $list_data['FILES']);
            }

            $this->load->library('PDFMerger');
            $pdf = new PDFMerger;

            foreach ($arfile as $key => $value) {
                $pdf->addPDF($value, 'all');
            }
            // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page

            $pdf->merge('download', "EVATECH {$list_data['NO_PENDAMPINGAN']} {$list_data['NOTIFIKASI']}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

    function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->assig->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

}
