<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Handover extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("bast/M_bast", "bast");
        $this->load->model("erf/M_request", "req");
        $this->load->model("eat/M_assign", "assig");
        $this->load->model("dok_eng/M_dok_eng", "dok_eng");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $header["tittle"] = "Transmital";
        $header["short_tittle"] = "bast";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        $yyyy = date("Y");
        $xxxx = $this->bast->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "{$yyyy}///TF/{$kodemax}";
        $data['foreigns'] = $this->bast->get_foreign();
        // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
        // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
        // $data['FOREIGN'] = $this->bast->get_foreign();
        $header['page_state'] = $state;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('bast/handover', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('bast/handover', $data);
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('search');
        $order = $this->input->post('order');

        $key = array(
            'search' => $search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
        }

        $data = $this->bast->get_data($key);

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->bast->recFil($key),
            'recordsTotal' => $this->bast->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $result = $this->bast->get_foreign();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');
        // list($badge, $nama) =  split(' - ',$atas);
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$atas->NOBADGE}'");
        $peg = (array) $peg[0];

        $list_data = $this->bast->get_data_unit($param['NO_BAST']);
        $list_data = $list_data[0];

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);

        $param['short'] = 'Transmital';
        $param['link'] = 'bast';
        $param['title'] = 'Transmital Dokumen Engineering';
        $param['sender'] = "{$info['no_badge']} - {$info['name']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_BAST']}";
        $param['TRN']['NOTIFIKASI'] = "<b>No Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['PACKET_TEXT'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja Peminta :</b> {$param['DESCPSECTION']} - {$param['FL_TEXT']}";
        $param['TRN']['COMPANY'] = "<b>Company :</b> {$param['COMP_TEXT']}";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);

        $this->email($this->template->set_app($param), $subject, '', $tomail, $cc);


        // $message="Dengan Hormat $atas->NOBADGE - $atas->NAMA,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No BAST       : " . $param['NO_BAST'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('bast/handover/approve?e='. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function send_trm($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];
        // print_r($badge);

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DE';
        $param['link'] = 'eng/docs';
        $param['title'] = 'Document Engineering';
        if (isset($param['CREATE_BY'])) {
            $param['sender'] = "{$param['CREATE_BY']}";
        } else {
            $param['sender'] = "{$param['UPDATE_BY']}";
        }
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['title']} :</b> {$param['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        $param['TRN']['LOKASI']="<b>Unit Kerja Peminta :</b> {$param['DESCPSECTION']} - {$param['FL_TEXT']}";
        // echo '<pre>';
        // print_r($param);
        // exit;
        $this->email($this->template->conf_trm($param), $subject, '', $to, $cc);

        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(0);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data = $this->bast->get_record($param['ID']);
            $data = $data[0];

            $slevel = 'BIRO';
            $slevel2 = 'BIRO';
            if (isset($data['APPROVE1_0_AT']) && !isset($data['APPROVE1_AT'])) {
                $slevel = 'DEPT';
            }
            if ((isset($data['APPROVE2_0_AT']) && !isset($data['APPROVE2_AT'])) || !isset($data['APPROVE2_0_BY'])) {
                $slevel2 = 'DEPT';
            }
            // atasan pembuat
            $atasan = $this->dbhris->get_level_from($info['no_badge'], $slevel);
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
                $atasan->JAB_TEXT = 'null';
            }
            // atasan peminta
            $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$data['ID_DOK_ENG']} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
            $this->db->where($where);
            $dtErf = $this->req->get_datarequest();
            $dtErf = $dtErf[0];
            $where = "(k.mk_nama = '{$dtErf['CREATE_BY']}' OR k.mk_email LIKE '{$dtErf['CREATE_BY']}@%')";
            $pgErf = $this->dbhris->get_hris_where($where);
            $pgErf = $pgErf[0];
            $atasan2 = $this->dbhris->get_level_from($pgErf->NOBADGE, $slevel2);
            if (!$atasan2) {
                $atasan2->NOBADGE = 'null';
                $atasan2->NAMA = 'null';
                $atasan2->EMAIL = 'null';
                $atasan2->JAB_TEXT = 'null';
            }

            // print_r($data);

            if ($data) {
                // $n_form=$this->my_crypt("{$param['NO_BAST']};1;{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$atasan->NOBADGE};{$atasan->NAMA};{$result['ID']}",'e');
                // $n_form = str_replace('/', '6s4', $n_form);
                // $n_form =  str_replace('+', '1p4', $n_form);
                // $n_form =  str_replace('=', 'sM9', $n_form);
                // $NOTIF_NO=$data['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                // $cc=array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                //$cc=array('purnawan.yose@gmail.com');
                // $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);
                $subject = "Resend : BAST APPROVAL REQUEST";

                if (!isset($data['APPROVE1_0_AT'])) {
                    // email atasan pembuat
                    $to = $atasan->EMAIL;
                    $n_form = $this->my_crypt("{$data['NO_BAST']};1_0;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};{$atasan->NOBADGE};{$atasan->NAMA};{$data['ID']}", 'e');
                    $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);
                } else if (!isset($data['APPROVE1_AT'])) {
                    // email atasan pembuat
                    $to = $atasan->EMAIL;
                    $n_form = $this->my_crypt("{$data['NO_BAST']};1;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};{$atasan->NOBADGE};{$atasan->NAMA};{$data['ID']}", 'e');
                    $this->send_mail($subject . " {$slevel2}.", $data, $atasan, $n_form, $to, $cc);
                }
                if (!isset($data['APPROVE2_0_AT']) && isset($data['APPROVE2_0_BY'])) {
                    // email atasan peminta
                    $to = $atasan2->EMAIL;
                    $n_form = $this->my_crypt("{$data['NO_BAST']};2_0;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};{$atasan2->NOBADGE};{$atasan2->NAMA};{$data['ID']}", 'e');
                    $this->send_mail($subject, $data, $atasan2, $n_form, $to, $cc);
                } else if (!isset($data['APPROVE2_AT'])) {
                    // email atasan peminta
                    $to = $atasan2->EMAIL;
                    $n_form = $this->my_crypt("{$data['NO_BAST']};2;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};{$atasan2->NOBADGE};{$atasan2->NAMA};{$data['ID']}", 'e');
                    $this->send_mail($subject . " {$slevel2}.", $data, $atasan2, $n_form, $to, $cc);
                }

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function resend_trans() {
        if (isset($_POST['ID'])) {
            error_reporting(0);

            $param = $_POST;
                    // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data = $this->bast->get_record($param['ID']);
            $data = $data[0];
            $list_data = $this->dok_eng->get_record("A.ID='{$data['ID_DOK_ENG']}'");
            $list_data = $list_data[0];
            $slevel2 = 'BIRO';
            // if ((isset($data['APPROVE2_0_AT']) && !isset($data['APPROVE2_AT']))|| !isset($data['APPROVE2_0_BY'])) {
            //   $slevel2 = 'DEPT';
            // }
            // atasan peminta
            $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$data['ID_DOK_ENG']} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
            $this->db->where($where);
            $dtErf = $this->req->get_datarequest();
            $dtErf = $dtErf[0];
            $where = "k.mk_nama = '{$dtErf['CREATE_BY']}' AND k.muk_kode='{$dtErf['UK_CODE']}'";
            $pgErf = $this->dbhris->get_hris_where($where);
            $pgErf = $pgErf[0];
            $atasan2 = $this->dbhris->get_level_from($pgErf->NOBADGE, $slevel2);
            if (!$atasan2) {
                $atasan2->NOBADGE = 'null';
                $atasan2->NAMA = 'null';
                $atasan2->EMAIL = 'null';
                $atasan2->JAB_TEXT = 'null';
            }

//             echo "<pre>";
//             print_r($dtErf);
//             print_r($pgErf);
//             print_r($atasan2);
//             exit;

            if ($data) {
                // send transmital
                $to = array();
                $cc = array();
                unset($where);
                $where['GEN_VAL'] = '1';
                $where['GEN_CODE'] = 'TRM_EMAIL';
                $where['GEN_TYPE'] = 'General';
                $data = $this->general_model->get_data_result($where);
                if (count($data) > 0) {
                    foreach ($data as $key => $value) {
                        if ($value->STATUS == 'to') {
                            $to[] = $value->GEN_PAR1;
                        } elseif ($value->STATUS == 'cc') {
                            $cc[] = $value->GEN_PAR1;
                        }
                    }
                    // echo "<pre>";
                    // print_r($tmpto);
                    // print_r($tmpcc);
                    // print_r($tmpbcc);
                    // echo "</pre>";
                    // exit;
                }
                $subject = "Transmital Confirmation";
                $this->send_trm($subject, $list_data, "{$atasan2->NOBADGE} - {$atasan2->NAMA}", $id, $to, $cc);

                // GET ERF CREATOR AND ERF APPROVER
                $erf = $this->bast->get_erf_creator($param['ID']);
                $erf = $erf[0];
                $where = "k.mk_nama = '{$erf['CREATE_BY']}'";
                $erfCreator = $this->dbhris->get_hris_where($where);
                $erfCreator = $erfCreator[0];

                $where = "k.mk_nama = '{$erf['APPROVE_BY']}'";
                $erfApprover = $this->dbhris->get_hris_where($where);
                $erfApprover = $erfApprover[0];
                
                $to = $erfCreator->EMAIL;
                $this->send_trm($subject, $list_data, "{$erfCreator->NOBADGE} - {$erfCreator->NAMA}", $id, $to, $cc);
                $to = $erfApprover->EMAIL;
                $this->send_trm($subject, $list_data, "{$erfApprover->NOBADGE} - {$erfApprover->NAMA}", $id, $to, $cc);
                // END OF GET ERF CREATOR AND ERF APPROVER


                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {
        if (isset($_POST) && isset($_POST['NO_BAST']) && isset($_POST['ID_DOK_ENG']) && isset($_POST['HDR_TEXT']) && isset($_POST['MID_TEXT']) && isset($_POST['FTR_TEXT'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            // atasan pembuat
            $str1 = '_0';
            $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            if (!$atasan) {
                $atasan = new stdClass();
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
                $atasan->JAB_TEXT = 'null';
            }
            $atasanGM = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
            if (!$atasanGM) {
                $atasanGM = new stdClass();
                $atasanGM->NOBADGE = 'null';
                $atasanGM->NAMA = 'null';
                $atasanGM->EMAIL = 'null';
                $atasanGM->JAB_TEXT = 'null';
            }
            // atasan peminta
            $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$_POST['ID_DOK_ENG']} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
            $this->db->where($where);
            $dtErf = $this->req->get_datarequest();
            $dtErf = $dtErf[0];
            $where = "(k.mk_nama = '{$dtErf['CREATE_BY']}' OR k.mk_email LIKE '{$dtErf['CREATE_BY']}@%')";
            $pgErf = $this->dbhris->get_hris_where($where);
            // echo $this->db->last_query();
            $pgErf = $pgErf[0];
            $atasan2 = $this->dbhris->get_level_from($pgErf->NOBADGE, 'BIRO');
            $str2 = '_0';
            if (!$atasan2) {
                $atasan2 = new stdClass();
                $atasan2->NOBADGE = 'null';
                $atasan2->NAMA = 'null';
                $atasan2->EMAIL = 'null';
                $atasan2->JAB_TEXT = 'null';
            }
            $atasan2GM = $this->dbhris->get_level_from($pgErf->NOBADGE, 'DEPT');
            if (!$atasan2GM) {
                $atasan2GM = new stdClass();
                $atasan2GM->NOBADGE = 'null';
                $atasan2GM->NAMA = 'null';
                $atasan2GM->EMAIL = 'null';
                $atasan2GM->JAB_TEXT = 'null';
            }
            // echo "<pre>";
            // print_r($info);
            // print_r($atasan);
            // print_r($atasanGM);
            // print_r($atasan2);
            // print_r($atasan2GM);
            // echo "</pre>";
            // echo "<br />".$this->db->last_query();
            // echo "<br />".$this->dbhris->last_query();
            // exit;

            $packet = $param['PACKET_TEXT'];
            unset($param['PACKET_TEXT']);
            $param['NO_BAST'] = $this->generateNo('NO_BAST', 'MPE_BAST', 'BA', $param['NO_BAST']);
            $param['STATUS'] = 'For Approval';
            $param['APPROVE1_0_BY'] = $atasan->NAMA;
            $param['APPROVE1_0_JAB'] = $atasan->JAB_TEXT;
            $param['APPROVE1_BY'] = $atasanGM->NAMA;
            $param['APPROVE1_JAB'] = $atasanGM->JAB_TEXT;
            $param['APPROVE2_0_BY'] = $atasan2->NAMA;
            $param['APPROVE2_0_JAB'] = $atasan2->JAB_TEXT;
            $param['APPROVE2_BY'] = $atasan2GM->NAMA;
            $param['APPROVE2_JAB'] = $atasan2GM->JAB_TEXT;
            $param['CREATE_BY'] = $info['username'];
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];

            $result = $this->bast->save($param);
            // echo $this->db->last_query();

            if ($result['ID']) {
                if ($atasan->NAMA == 'null') {
                    $str1 = '';
                    $atasan = $atasanGM;
                }
                $dept2 = '';
                if ($atasan2->NAMA == 'null') {
                    $str2 = '';
                    $dept2 = ' DEPT.';
                    $atasan2 = $atasan2GM;
                }
                $param['PACKET_TEXT'] = $packet;
                $to = $atasan->EMAIL;
                $to2 = $atasan->EMAIL;
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                //$cc=array();
                // $cc=array('purnawan.yose@gmail.com');
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                $subject = "BAST APPROVAL REQUEST";
                // email atasan pembuat
                $n_form = $this->my_crypt("{$param['NO_BAST']};1{$str1};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$atasan->NOBADGE};{$atasan->NAMA};{$result['ID']}", 'e');
                $this->send_mail($subject . " BIRO.", $param, $atasan, $n_form, $to, $cc);
                // email atasan peminta
                $n_form = $this->my_crypt("{$param['NO_BAST']};2{$str2};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$atasan2->NOBADGE};{$atasan2->NAMA};{$result['ID']}", 'e');
                $this->send_mail($subject . $dept2, $param, $atasan2, $n_form, $to2, $cc);

                $res_message = 'Data has been saved' . '-';
                if (!$result['ID']) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['NO_BAST']) && isset($_POST['ID_DOK_ENG']) && isset($_POST['HDR_TEXT']) && isset($_POST['MID_TEXT']) && isset($_POST['FTR_TEXT'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            // atasan pembuat
            $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
                $atasan->JAB_TEXT = 'null';
            }
            // atasan peminta
            $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$_POST['ID_DOK_ENG']} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
            $this->db->where($where);
            $dtErf = $this->req->get_datarequest();
            $dtErf = $dtErf[0];
            $where = "(k.mk_nama = '{$dtErf['CREATE_BY']}' OR k.mk_email LIKE '{$dtErf['CREATE_BY']}@%')";
            $pgErf = $this->dbhris->get_hris_where($where);
            $pgErf = $pgErf[0];
            $atasan2 = $this->dbhris->get_level_from($pgErf->NOBADGE, 'BIRO');
            $str2 = '_0';
            $dept2 = '';
            if (!$atasan2) {
                $atasan2 = $this->dbhris->get_level_from($pgErf->NOBADGE, 'DEPT');
                // $atasan2->NOBADGE='null';
                // $atasan2->NAMA='null';
                // $atasan2->EMAIL='null';
                // $atasan2->JAB_TEXT='null';
                $str2 = '';
                $dept2 = ' DEPT.';
            }

            $packet = $param['PACKET_TEXT'];
            unset($param['PACKET_TEXT']);
            $param['UPDATE_BY'] = $info['username'];
            $param['COMPANY'] = $info['company'];

            $result = $this->bast->updateData('update', $param);
            // echo $this->db->last_query();

            if ($result) {
                $param['PACKET_TEXT'] = $packet;
                $to = array('adhiq.rachmadhuha@sisi.id');
                // $cc=array();
                $cc = array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                $subject = "BAST APPROVAL REQUEST";
                // email atasan pembuat
                $n_form = $this->my_crypt("{$param['NO_BAST']};1_0;{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$atasan->NOBADGE};{$atasan->NAMA};{$_POST['ID']}", 'e');
                $this->send_mail($subject . " BIRO.", $param, $atasan, $n_form, $to, $cc);
                // email atasan peminta
                $n_form = $this->my_crypt("{$param['NO_BAST']};2{$str2};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$atasan2->NOBADGE};{$atasan2->NAMA};{$_POST['ID']}", 'e');
                $this->send_mail($subject . $dept2, $param, $atasan2, $n_form, $to, $cc);

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($no_form, $lvl, $notifikasi, $packet, $no_badge, $nama, $id) = split(";", $no_form);

            $data['id'] = $id;
            $data['no_form'] = $no_form;
            $sts = "";
            $param = $this->bast->get_record($id);
            $param = $param[0];
            $param['PACKET_TEXT'] = $packet;

            // $atasan = new stdClass();
            $badge = '';
            if (strpos($lvl, '1') !== false) {
                // $data['STATUS'] = 'Approved Dept';
                $sts = 'Approved Dept';
                if (strpos($lvl, '0') !== false) {
                    $sts = 'Approved Biro';
                    $badge = $no_badge;
                    // $atasan = (object) $this->dbhris->get_level_from($no_badge, 'DEPT');
                }
            } else {
                // $data['STATUS'] = 'Approved Cust.';
                $sts = 'Approved GM Cust.';
                if (strpos($lvl, '0') !== false) {
                    $sts = 'Approved SM Cust.';
                    $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$param['ID_DOK_ENG']} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
                    $this->db->where($where);
                    $dtErf = $this->req->get_datarequest();
                    $dtErf = $dtErf[0];
                    $where = "(k.mk_nama = '{$dtErf['CREATE_BY']}' OR k.mk_email LIKE '{$dtErf['CREATE_BY']}@%')";
                    $pgErf = $this->dbhris->get_hris_where($where);
                    $pgErf = $pgErf[0];
                    $badge = $pgErf->NOBADGE;
                    // $atasan = (object) $this->dbhris->get_level_from($pgErf->NOBADGE, 'DEPT');
                }
            }

            // echo $this->dbhris->last_query();
            // echo $this->my_crypt($e,'d')."<br />";
            // echo "<pre>";
            // // print_r($pgErf);
            // print_r($atasan);
            // echo "</pre>";
            // exit;
            // $data['APPROVE'.$lvl.'_BY']=$nama;
            $this->db->set('STATUS', "(CASE '{$lvl}' WHEN '1' THEN (CASE WHEN APPROVE2_AT IS NOT NULL THEN 'Closed' ELSE '{$sts}' END) WHEN '2' THEN (CASE WHEN APPROVE1_AT IS NOT NULL THEN 'Closed' ELSE '{$sts}' END) ELSE '{$sts}' END)", false);
            $this->db->set('APPROVE' . $lvl . '_AT', "CURRENT_DATE", false);
            $this->db->set('NOTE' . $lvl, $reason);
            $result = $this->bast->updateData('approve', $data);
            // echo $this->db->last_query();
            // echo $this->my_crypt($e,'d');
            if ($lvl == '1' || $lvl == '2') {
                // set progress status
                // $ar_det['ID_PENUGASAN'] = $id;
                $ar_det['PROGRESS'] = '100';
                $this->db->where("ID_PENUGASAN = (SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID = (SELECT ID_DOK_ENG FROM MPE_BAST WHERE ID = '{$id}')) AND DESKRIPSI = (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID = (SELECT ID_DOK_ENG FROM MPE_BAST WHERE ID = '{$id}'))");
                $dtlresult = $this->assig->update_det($ar_det);
                // echo $this->db->last_query();

                $result2['message'] = "BAST '{$no_form}' was approved succesfully";
                $result2['status'] = '200';
                echo json_encode($result2);
                exit;
            }


            $atasan = $this->dbhris->get_level_from($badge, 'DEPT');
            // $atasan = $atasan[0];
            if (!$atasan) {
                $atasan = new stdClass();
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }
            $lvl = str_replace('_0', '', $lvl);
            $str = '';
            if ($lvl == '1') {
                $str = ' DEPT.';
            }
            // echo "$lvl<br />";
            // echo "<pre>";
            // print_r($pgErf);
            // print_r($atasan);
            // echo "</pre>";
            $to = $atasan->EMAIL;
            // $cc=array();
            //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
            // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
            $subject = "BAST APPROVAL REQUEST{$str}";

            $n_form = $this->my_crypt("{$param['NO_BAST']};{$lvl};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$atasan->NOBADGE};{$atasan->NAMA};{$param['ID']}", 'e');
            $this->send_mail($subject, $param, $atasan, $n_form, $to, $cc);

            if ($result) {
                $result2['message'] = "BAST '{$no_form}' was approved succesfully";
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function reject() {
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            $nama = $_POST['NAMA'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($no_form, $lvl, $notifikasi, $packet, $no_badge, $nama, $id) = split(";", $no_form);

            $data['id'] = $id;
            $data['no_form'] = $no_form;
            $sts = "";
            $param = $this->bast->get_record($id);
            $param = $param[0];
            $param['PACKET_TEXT'] = $packet;

            // $atasan = new stdClass();
            $badge = '';
            if (strpos($lvl, '1') !== false) {
                // $data['STATUS'] = 'Approved Dept';
                $sts = 'Approved Dept';
                if (strpos($lvl, '0') !== false) {
                    $sts = 'Approved Biro';
                    $badge = $no_badge;
                    // $atasan = (object) $this->dbhris->get_level_from($no_badge, 'DEPT');
                }
            } else {
                // $data['STATUS'] = 'Approved Cust.';
                $sts = 'Approved GM Cust.';
                if (strpos($lvl, '0') !== false) {
                    $sts = 'Approved SM Cust.';
                    $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$param['ID_DOK_ENG']} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
                    $this->db->where($where);
                    $dtErf = $this->req->get_datarequest();
                    $dtErf = $dtErf[0];
                    $where = "(k.mk_nama = '{$dtErf['CREATE_BY']}' OR k.mk_email LIKE '{$dtErf['CREATE_BY']}@%')";
                    $pgErf = $this->dbhris->get_hris_where($where);
                    $pgErf = $pgErf[0];
                    $badge = $pgErf->NOBADGE;
                }
            }
            $this->db->set('STATUS', "Rejected");
            $this->db->set('REJECT_AT', "CURRENT_DATE", false);
            $this->db->set('REJECT_NOTE', $reason);
            $this->db->set('REJECT_BY', $nama);
            $result = $this->bast->updateData('reject', $data);
            if ($result) {
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['id'] = $id;
            $data['DELETE_BY'] = $info['username'];
            $result = $this->bast->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id) {
        if ($id) {

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // $list_data = $_POST;
            $list_data = $this->bast->get_record($id);
            $list_data = $list_data[0];
            $dtApp1 = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['APPROVE1_BY']}' AND k.company = '{$list_data['COMPANY']}' AND j.mjab_nama = '{$list_data['APPROVE1_JAB']}'");
            if ($dtApp1) {
                $dtApp1 = $dtApp1[0];
            }
            $dtApp2 = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['APPROVE2_BY']}' AND j.mjab_nama = '{$list_data['APPROVE2_JAB']}'");
            if ($dtApp2) {
                $dtApp2 = $dtApp2[0];
            }
            $dtSM1 = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['APPROVE1_0_BY']}' AND k.company = '{$list_data['COMPANY']}' AND j.mjab_nama = '{$list_data['APPROVE1_0_JAB']}'");
            if ($dtSM1) {
                $dtSM1 = $dtSM1[0];
            }
            $dtSM2 = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['APPROVE2_0_BY']}' AND j.mjab_nama = '{$list_data['APPROVE2_0_JAB']}'");
            if ($dtSM2) {
                $dtSM2 = $dtSM2[0];
            }

            // echo "<pre>";;
            // print_r($list_data);
            // print_r($dtApp1);
            // print_r($dtApp2);
            // echo "</pre>";
            // exit;

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 7);
            $this->fpdf->SetLeftMargin(20);

            // header
            $this->fpdf->Cell(30, 11, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 13, 'PNG'), 0, 1, 'C');
            $this->fpdf->Cell(19, 4, '', 0, 0, 'R');
            $this->fpdf->SetFont('Arial', '', 9);
            $this->fpdf->Cell(145, 1, 'PT. SEMEN INDONESIA (PERSERO)Tbk.', 0, 1);

            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'BU', 16);
            $this->fpdf->Cell(170, 7, "Transmital Pengiriman Dokumen Engineering", 0, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 4, "{$list_data['NO_BAST']}", 0, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Ln(5);
            $this->fpdf->Cell(55, 6, " No. EAT", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['NO_PENUGASAN']}");
            // $this->fpdf->Cell(110,6,"{$list_data['LOKASI']}",0,1,'L');
            $this->fpdf->Cell(55, 6, " Nama Pekerjaan", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['PACKET_TEXT']}");
            // $this->fpdf->Cell(110,6,"{$list_data['PACKET_TEXT']}",0,1,'L');
            $this->fpdf->Cell(55, 6, " Unit Kerja Peminta", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['DESCPSECTION']}");
            // $this->fpdf->Cell(110,6,"{$list_data['UK_TEXT']}",0,1,'L');
            $this->fpdf->Cell(55, 6, "", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, "", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['FL_TEXT']}");
            // $this->fpdf->Cell(110,6,"{$list_data['LOKASI']}",0,1,'L');
            $this->fpdf->Cell(55, 6, " Company", 0, 0, 'L');
            $this->fpdf->Cell(5, 6, ":", 0, 0, 'C');
            $this->fpdf->MultiCell(110, 6, "{$list_data['COMP_TEXT']}");
            // $this->fpdf->Cell(110,6,"{$list_data['LOKASI']}",0,1,'L');
            // line
            $this->fpdf->Ln(3);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);
            // header content
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Ln(5);
            $this->fpdf->MultiCell(170, 5, "{$list_data['HDR_TEXT']} {$list_data['MID_TEXT']}");

            // $this->fpdf->Ln(5);
            // $this->fpdf->SetFont('Arial','B',10);
            // // $this->fpdf->Cell(10,5,"");
            // $this->fpdf->Cell(10,5," 1.",0,0,'R');
            // $this->fpdf->Cell(70,5," {$dtApp1->NAMA}");
            // $this->fpdf->Cell(10,5," : ",0,0,'C');
            // $this->fpdf->Cell(80,5,"{$dtApp1->DEPT_TEXT}",0,2);
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Cell(80,5,"PT Semen Indonesia (Persero) Tbk.",0,1);
            // $this->fpdf->SetFont('Arial','B',10);
            // // $this->fpdf->Cell(10,5,"");
            // $this->fpdf->Cell(10,5," 2.",0,0,'R');
            // $this->fpdf->Cell(70,5," {$dtApp2->NAMA}");
            // $this->fpdf->Cell(10,5," : ",0,0,'C');
            // $this->fpdf->Cell(80,5,"{$dtApp2->DEPT_TEXT}",0,2);
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Cell(80,5,"PT Semen Indonesia (Persero) Tbk.",0,1);
            // // $this->fpdf->Line($x,$y,($x+170),$y);
            // midle content
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Ln(3);
            // $this->fpdf->MultiCell(170,5,"{$list_data['MID_TEXT']}");

            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 10);
            // $this->fpdf->Cell(10,5," 1.",0,0,'R');
            $this->fpdf->MultiCell(170, 5, "{$list_data['PACKET_TEXT']}", 0, 'C');

            // footer content
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Ln(3);
            $this->fpdf->MultiCell(170, 5, "{$list_data['FTR_TEXT']}");

            // $this->fpdf->Ln(5);
            // $this->fpdf->SetFont('Arial','B',10);
            // $this->fpdf->Cell(85,7,"{$dtApp2->DEPT_TEXT}",1,0,'C');
            // $this->fpdf->Cell(85,7,"{$dtApp1->DEPT_TEXT}",1,1,'C');
            //
			// include(APPPATH.'libraries/phpqrcode/qrlib.php');
            // // approval peminta
            // $tmpApp2 = FCPATH."media/upload/tmp/".md5("BAST-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APP2").".png";
            // $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE2_BY']};{$list_data['APPROVE2_AT']};",'e');
            // $str = str_replace('/', '6s4', $str);
            // $str = str_replace('+', '1p4', $str);
            // $str = str_replace('=', 'sM9', $str);
            // $str = base_url()."info/bast/". $str;
            // if ($list_data['APPROVE2_AT']) {
            // 	QRcode::png($str, $tmpApp2);
            //   $this->fpdf->Cell(85,30,$this->fpdf->Image($tmpApp2,$this->fpdf->GetX()+(85/2)-12.5,$this->fpdf->GetY()+1,0,30,'PNG'),'LR',0,'C');
            // }else {
            //   $this->fpdf->Cell(85,30,"",'LR',0,'C');
            // }
            // // approval pembuat
            // $tmpApp1 = FCPATH."media/upload/tmp/".md5("BAST-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APP1").".png";
            // $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE1_BY']};{$list_data['APPROVE1_AT']};",'e');
            // $str = str_replace('/', '6s4', $str);
            // $str = str_replace('+', '1p4', $str);
            // $str = str_replace('=', 'sM9', $str);
            // $str = base_url()."info/bast/". $str;
            // if ($list_data['APPROVE1_AT']) {
            //   QRcode::png($str, $tmpApp1);
            //   $this->fpdf->Cell(85,30,$this->fpdf->Image($tmpApp1,$this->fpdf->GetX()+(85/2)-12.5,$this->fpdf->GetY()+1,0,30,'PNG'),'LR',1,'C');
            // }else {
            //   $this->fpdf->Cell(85,30,"",'LR',1,'C');
            // }
            // $this->fpdf->Cell(85,5,"{$dtApp2->NAMA}",'LBR',0,'C');
            // $this->fpdf->Cell(85,5,"{$dtApp1->NAMA}",'LBR',1,'C');
            //
      // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Ln(7);
            // $this->fpdf->MultiCell(170,5,"Tembusan :");
            // $this->fpdf->MultiCell(170,5,"  - SM of Design Integration");
            // $this->fpdf->MultiCell(170,5,"  - Arsip");
            //
      // // info SM
            // $this->fpdf->Ln(7);
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Cell(140,15,"paraf SM : ",0,0,'R');
            //
      // // $this->fpdf->SetFont('Arial','',10);
            // // $sAdd='';
            // // if ($list_data['APPROVE2_0_AT'] && $list_data['APPROVE2_0_BY']) {
            // //   $sAdd='Mengetahui SM,';
            // // }
            // // $this->fpdf->Cell(85,7,"$sAdd",0,0,'C');
            // // $sAdd='';
            // // if ($list_data['APPROVE1_0_AT'] && $list_data['APPROVE1_0_BY']) {
            // //   $sAdd='Mengetahui,';
            // // }
            // // $this->fpdf->Cell(85,7,"$sAdd",0,1,'C');
            // // approval SM peminta
            // $tmpApp2 = FCPATH."media/upload/tmp/".md5("BAST-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-SM2").".png";
            // $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE2_0_BY']};{$list_data['APPROVE2_0_AT']};",'e');
            // $str = str_replace('/', '6s4', $str);
            // $str = str_replace('+', '1p4', $str);
            // $str = str_replace('=', 'sM9', $str);
            // $str = base_url()."info/bast/". $str;
            // if ($list_data['APPROVE2_0_AT']) {
            // 	QRcode::png($str, $tmpApp2);
            //   $this->fpdf->Cell(15,15,$this->fpdf->Image($tmpApp2,$this->fpdf->GetX(),$this->fpdf->GetY(),0,15,'PNG'),1,0,'C');
            // }else {
            //   $this->fpdf->Cell(15,15,"",1,0,'C');
            // }
            // // approval SM pembuat
            // $tmpApp1 = FCPATH."media/upload/tmp/".md5("BAST-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-SM1").".png";
            // $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE1_0_BY']};{$list_data['APPROVE1_0_AT']};",'e');
            // $str = str_replace('/', '6s4', $str);
            // $str = str_replace('+', '1p4', $str);
            // $str = str_replace('=', 'sM9', $str);
            // $str = base_url()."info/bast/". $str;
            // if ($list_data['APPROVE1_0_AT']) {
            //   QRcode::png($str, $tmpApp1);
            //   $this->fpdf->Cell(15,15,$this->fpdf->Image($tmpApp1,$this->fpdf->GetX(),$this->fpdf->GetY(),0,15,'PNG'),1,1,'C');
            // }else {
            //   $this->fpdf->Cell(15,15,"",1,1,'C');
            // }
            // $this->fpdf->Cell(85,5,"{$dtSM2->NAMA}",0,0,'C');
            // $this->fpdf->Cell(85,5,"{$dtSM1->NAMA}",0,1,'C');
            // line
            $this->fpdf->SetFont('Arial', '', 9);
            $this->fpdf->Ln(8);
            $this->fpdf->MultiCell(170, 5, " - Staff Department Design Engineering - ", 0, 'R');
            $this->fpdf->Ln(10);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);

            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->MultiCell(170, 5, "* dokumen ini dibuat secara otomatis oleh sistem e-DEMS.");

            $this->fpdf->Output("BAST {$list_data['NO_BAST']} {$list_data['NOTIFIKASI']}.pdf", 'D');
        } else {
            echo "File Not Found!";
        }
    }

    function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->bast->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

    function get_record() {
        $id = $this->input->post('id');
        $data = $this->bast->get_record("{$id}");
        $return = json_encode($data);
        echo $return;
    }

}
