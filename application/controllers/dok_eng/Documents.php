<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Documents extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    protected $uk_parent = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("erf/M_request", "req");
        $this->load->model("eat/M_assign", "assig");
        $this->load->model("dok_eng/M_dok_eng", "dok_eng");
        $this->load->model("bast/M_bast", "bast");
        $this->load->model("others/M_approval", "app");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'docs';
        $where['GEN_TYPE'] = 'FORM_GROUP';
        $global = $this->general_model->get_data($where);
        unset($where);
        
        $header["tittle"] = $global->GEN_DESC;
        $header["short_tittle"] = "docs";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        // set auto increment trans no
        // $where = "A.ID=(SELECT MAX(ID) FROM MPE_DOK_ENG) AND NO_DOK_ENG LIKE '%DE-%'";
        // $trans = $this->dok_eng->get_record($where);;
        // echo "<pre>";
        // print_r($trans);
        // echo "</pre>";
        // exit;
        // set list approval hris
        // $where['GEN_VAL']='1';
        // $where['GEN_CODE']='EAT_PENGKAJI_UK';
        // $where['GEN_TYPE']='General';
        // $global = $this->general_model->get_data($where);
        // $uk_parent = $global->GEN_PAR5;

        $strc = "
            k.muk_kode = '{$info['uk_kode']}' AND (k.mk_eselon_code='30' OR k.mk_eselon_code='40' OR k.mk_eselon_code='50')
            ";
        $str = "
            k.muk_kode = '{$info['uk_kode']}' AND (k.mk_eselon_code='30' OR k.mk_eselon_code='40')
            ";
        $str2 = "
            k.muk_kode = '{$info['uk_kode']}' AND k.mk_eselon_code='20'
            ";
        $str3 = "
            k.muk_kode = '50032326' AND k.mk_eselon_code='10'
            ";
        $yyyy = date("Y");
        $xxxx = $this->dok_eng->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "///TR/{$kodemax}//{$yyyy}";
        $data['LIST_CRT'] = $this->dbhris->get_hris_where($strc);
        $data['LIST_MNG'] = $this->dbhris->get_hris_where($str);
        $data['LIST_APP'] = $this->dbhris->get_hris_where($str2);
        $data['LIST_HDE'] = $this->dbhris->get_hris_where($str3); //50032326
        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $this->db->where("PN.UK_CODE = '{$info['uk_kode']}'");
        }

        // exit();
        $data['foreigns'] = $this->dok_eng->get_new_foreign();

        // echo "<pre>";
        // print_r($info);
        // echo "</pre>";
        // exit;
        // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
        // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
        // $data['FOREIGN'] = $this->dok_eng->get_foreign();

        $header['page_state'] = $state;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';

            $global = (array) $this->general_model->get_data($where);

            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('dok_eng/dok_engineer', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }

        // $this->load->view('general/header', $header);
        // $this->load->view('dok_eng/dok_engineer', $data);
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        //$search = $this->input->post('search');
        $search = $this->input->post('columns');
        $order = $this->input->post('order');

        $key = array(
            //'search' => $search['value'],
             'search' => $search[0]['search']['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
        }

        $data = $this->dok_eng->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->dok_eng->recFil($key),
            'recordsTotal' => $this->dok_eng->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $result = $this->dok_eng->get_foreign();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function foreign_dtl($f_id) {
        if (isset($f_id)) {
            $result = $this->dok_eng->get_dtl_foreign($f_id);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        // $info = $this->session->userdata;
        // $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];

        $list_data = $this->dok_eng->get_record("A.NO_DOK_ENG='{$param['NO_DOK_ENG']}'");
        $list_data = $list_data[0];

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DE';
        $param['link'] = 'dok_eng';
        $param['title'] = 'Document Engineering';
        //$param['title'] = "Request Approval Document Engineering - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}";
        if (isset($param['CREATE_BY'])) {
            $param['sender'] = "{$param['CREATE_BY']}";
        } else {
            $param['sender'] = "{$param['UPDATE_BY']}";
        }
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['title']} :</b> {$param['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja Peminta :</b> {$list_data['DESCPSECTION']} - {$list_data['FL_TEXT']}";
        $param['TRN']['COMPANY'] = "<b>Company :</b> {$list_data['COMP_TEXT']}";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$param['PROGRESS']}%";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);
        
        //subject baru task [task 30]
        $subjectnew = "{$subject} - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}";


        $this->email($this->template->set_app($param), $subjectnew, '', $tomail, $cc);


        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function send_trm($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        // $info = $this->session->userdata;
        // $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];
        // print_r($badge);

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DE';
        $param['link'] = 'eng/docs';
        $param['title'] = 'Document Engineering';
        //$param['title'] = "Request Approval Document Engineering - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}";
        if (isset($param['CREATE_BY'])) { 
            $param['sender'] = "{$param['CREATE_BY']}";
        } else {
            $param['sender'] = "{$param['UPDATE_BY']}";
        }
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['title']} :</b> {$param['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        $param['TRN']['LOKASI']="<b>Unit Kerja Peminta :</b> {$param['DESCPSECTION']} - {$param['FL_TEXT']}";
        // echo '<pre>';
        // print_r($param);
        // exit;
        
        //subject baru task [task 30]
        $subjectnew = "{$subject} - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}";

        $this->email($this->template->conf_trm($param), $subjectnew, '', $to, $cc);


        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(1);

            $param = $_POST;
            $info = $this->session->userdata;
            $info = $info['logged_in'];
            if (empty($info['username'])) {
                redirect('login');
                exit;
            }

            // get limit ece
            $awhere['GEN_TYPE'] = 'General';
            $awhere['GEN_CODE'] = 'ECE_LIMIT';
            $awhere['GEN_VAL'] = '1';
            $global = (array) $this->general_model->get_data($awhere);

            $data = $this->dok_eng->get_record("A.ID='{$param['ID']}'");
            $data = $data[0];

            if ($data) {
                if ($data['STATUS'] == "CRT Approved") {
                    $dtApp = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'Paraf' AND DELETE_AT IS NULL");
                    //print_r($dtApp);
                    if (count($dtApp)) {
                        foreach ($dtApp as $key) {
                            if ($key['STATE'] == 'New') {
                                //$mailto = explode('@', $key['APP_EMAIL']);
                                $to = array($key['APP_EMAIL']);
                                //$to = array($mailto[0].'@semenindonesia.com');
                                //$subject = "Resend : MGR DOCUMENTS - Request Approval Document Engineering";
                                
                                $subject = "Resend : Request Approval Document Engineering";
                                
                                $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};Paraf;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                                $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                                // break;
                            }
                        }
                    }
                }if ($data['STATUS'] == "MGR Pengkaji Approved") {
                    $dtApp = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'TTD' AND DELETE_AT IS NULL");
                    if (count($dtApp)) {
                        foreach ($dtApp as $key) {
                            if ($key['STATE'] == 'New') {
//                                $mailto = explode('@', $key['APP_EMAIL']);
                                $to = array($key['APP_EMAIL']);
//                                $to = array($mailto[0].'@semenindonesia.com');
                                //$subject = "Resend : BIRO DOCUMENTS - Request Approval Document Engineering";
                                
                                $subject = "Resend : Request Approval Document Engineering";
                                
                                $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};TTD;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                                $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                                // break;
                            }
                        }
                    }
                } elseif ($data['STATUS'] == "SM Pengkaji Approved") {

                    if ($data['APPROVE3_MAN'] == '1') {
                        //$subject = "Resend : DEPT DOCUMENTS - Request Approval Document Engineering";
                        
                        $subject = "Resend : Request Approval Document Engineering";
                        
                        $to = $data['APPROVE3_EMAIL'];
                        $n_form = $this->my_crypt("{$data['ID']};3;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};GM;{$data['APPROVE3_BADGE']} - {$data['APPROVE3_BY']}", 'e');
                        $this->send_mail($subject, $data, "{$data['APPROVE3_BADGE']} - {$data['APPROVE3_BY']}", $n_form, $to, $cc);
                    } else {
                        //$subject = "Resend : DEPT DOCUMENTS - Request Approval Document Engineering";
                        
                        $subject = "Resend : Request Approval Document Engineering";
                         
                        $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$data['CREATE_BY']}' OR k.mk_email LIKE '{$data['CREATE_BY']}@%')");
                        $info = $info[0];
                        $atasan2 = $this->dbhris->get_level_from($info->NOBADGE, 'DEPT');
                        $to = $atasan2->EMAIL;
                        $n_form = $this->my_crypt("{$data['ID']};3;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};GM;{$atasan2->NOBADGE} - {$atasan2->NAMA}", 'e');
                        $this->send_mail($subject, $data, "{$atasan2->NOBADGE} - {$atasan2->NAMA}", $n_form, $to, $cc);
                    }
                    
                } elseif ($data['STATUS'] == "MGR Approved") {
                    $subject = "Resend : Request Approval Document Engineering";
                    $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$data['CREATE_BY']}' OR k.mk_email LIKE '{$data['CREATE_BY']}@%')");
                    $info = $info[0];
                    $atasan = $this->dbhris->get_level_from($info->NOBADGE, 'BIRO');
                    $to = $atasan->EMAIL;
                    $n_form = $this->my_crypt("{$data['ID']};2;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};SM;{$atasan->NOBADGE} - {$atasan->NAMA}", 'e');
                    $this->send_mail($subject, $data, "{$atasan->NOBADGE} - {$atasan->NAMA}", $n_form, $to, $cc);
                } elseif ($data['STATUS'] == "SM Approved") {
                    if ((float) $data['NOMINAL'] > (float) $global['GEN_PAR1'] || (isset($list_data['KAJIAN_FILE']) && !empty($list_data['KAJIAN_FILE']))) {
                        $subject = "Resend : Request Approval Document Engineering";
                        $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$data['CREATE_BY']}' OR k.mk_email LIKE '{$data['CREATE_BY']}@%')");
                        $info = $info[0];
                        $atasan2 = $this->dbhris->get_level_from($info->NOBADGE, 'DEPT');
                        $to = $atasan2->EMAIL;
                        $n_form = $this->my_crypt("{$data['ID']};3;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};GM;{$atasan2->NOBADGE} - {$atasan2->NAMA}", 'e');
                        $this->send_mail($subject, $data, "{$atasan2->NOBADGE} - {$atasan2->NAMA}", $n_form, $to, $cc);
                    }
                } else {
                    $dtPar = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'CRT' AND DELETE_AT IS NULL");
                    if (count($dtPar)) {
                        foreach ($dtPar as $key) {
                            if ($key['STATE'] == 'New') {
                                $to = array($key['APP_EMAIL']);
                                //$subject = "Resend : PACKET APPROVAL";
                                $subject = "Resend : Request Approval Document Engineering";
                                $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};CRT;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                                $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                            }
                        }
                    }
                }
                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function createTrans($list_data = array()) {
        $id = $list_data['ID'];

        $ar_det['PROGRESS'] = '100';
        $this->db->where("ID_PENUGASAN = (SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID = '{$id}') AND DESKRIPSI = (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID = '{$id}')");
        $dtlresult = $this->assig->update_det($ar_det);

        // create log progress
        unset($data);
        $data['REF_ID'] = $id;
        $data['REF_TABLE'] = 'MPE_DTL_PENUGASAN';
        $data['DESKRIPSI'] = 'Log Progress Engineering';
        $data['NOTE'] = 'Complete';
        $data['LOG_RES1'] = $ar_det['PROGRESS'];
        $data['CREATE_BY'] = $list_data['CREATE_BY'];
        $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
        $res_hasil = $this->general_model->log_saving($data);

        // data erf
        $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$id} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
        $this->db->where($where);
        $dtErf = $this->req->get_datarequest();
        $dtErf = $dtErf[0];
        $where = " k.mk_nama = '{$dtErf['CREATE_BY']}' AND k.muk_kode='{$dtErf['UK_CODE']}'";
        $pgErf = $this->dbhris->get_hris_where($where);
        $pgErf = $pgErf[0];
        $atas = $this->dbhris->get_level_from($pgErf->NOBADGE, 'BIRO');

        // send transmital
        $to = array();
        $cc = array();
        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'TRM_EMAIL';
        $where['GEN_TYPE'] = 'General';
        $data = $this->general_model->get_data_result($where);
        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                if ($value->STATUS == 'to') {
                    $to[] = $value->GEN_PAR1;
                } elseif ($value->STATUS == 'cc') {
                    $cc[] = $value->GEN_PAR1;
                }
            }
        }
        $subject = "Transmital Receipt";
        $this->send_trm($subject, $list_data, "{$atas->NOBADGE} - {$atas->NAMA}", $id, $to, $cc);

        // create transmital
        $this->db->where('ID_DOK_ENG', $id);
        $dtTrm = $this->bast->get_record();
        if (count($dtTrm) == 0) { 
            $sTipe = 'Kajian Engineering';
            if (strpos($dtErf['TIPE'], 'Detail') !== false) {
                $sTipe = 'Detail Design Engineering';
            }
            $trn_no = str_replace('TR', 'TF', $list_data['NO_DOK_ENG']);
            unset($param);
            $param['NO_BAST'] = $this->generateNo('NO_BAST', 'MPE_BAST', 'TF', $trn_no);
            $param['ID_DOK_ENG'] = $id;
            $param['NOTIFIKASI'] = $list_data['NOTIFIKASI'];
            $param['HDR_TEXT'] = "Pada hari ini " . $this->hari_ini() . ", tanggal " . trim($this->penyebut(date('j'))) . ", bulan " . $this->bulan_ini() . ", tahun " . trim($this->penyebut(date('Y'))) . " (" . date('d-m-Y') . ") menyatakan, ";
            $param['MID_TEXT'] = "bahwa {$sTipe} untuk Investasi PT Semen Indonesia (Persero) Tbk, dengan pekerjaan sebagai berikut :";
            $param['FTR_TEXT'] = "Dinyatakan selesai 100% dan diserahterimakan kepada unit kerja sesuai dengan rencana.\n\nDemikian Transmital Pengiriman Dokumen Engineering ini dibuat dengan sebenar-benarnya untuk dapat dipergunakan sebagaimana mestinya.";
            $param['STATUS'] = 'Closed';
            $param['COMPANY'] = $list_data['COMPANY'];
            $param['DEPT_CODE'] = $list_data['DEPT_CODE'];
            $param['DEPT_TEXT'] = $list_data['DEPT_TEXT'];
            $param['UK_CODE'] = $list_data['UK_CODE'];
            $param['UK_TEXT'] = $list_data['UK_TEXT'];
            $param['CREATE_BY'] = $list_data['APPROVE3_BY'];
            $param['APPROVE1_BY'] = $list_data['APPROVE3_BY'];
            $param['APPROVE1_JAB'] = $list_data['APPROVE3_JAB'];
            $param['APPROVE2_BY'] = $atas->NAMA;
            $param['APPROVE2_JAB'] = $atas->JAB_TEXT;
            $result = $this->bast->save($param);
        }
    }

    public function create() {
        if (isset($_POST) && isset($_POST['NO_DOK_ENG']) && isset($_POST['ID_PENUGASAN']) && isset($_POST['PACKET_TEXT'])) {
            $param = $_POST;
            
            $LIST_CRT = $_POST['LIST_CRT'];
            unset($param['LIST_CRT']);

            
            $LIST_APP = $_POST['LIST_APP'];
            unset($param['LIST_APP']);
            
            $LIST_PARAF = $_POST['LIST_PARAF'];
            unset($param['LIST_PARAF']);

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            if (empty($info['username'])) {
                redirect('login');
                exit;
            }

            $param['NO_DOK_ENG'] = $this->generateNo('NO_DOK_ENG', 'MPE_DOK_ENG', 'TR', $param['NO_DOK_ENG']);
            $param['CREATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            // RKS
            if (isset($_FILES['RKSPATH'])) {
                $url_files = $this->upload($_FILES['RKSPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_RKS");
                $param['RKS_FILE'] = $url_files;
                $this->db->set('RKS_AT', "CURRENT_DATE", false);
            }
            // BQ
            if (isset($_FILES['BQPATH'])) {
                $url_files = $this->upload($_FILES['BQPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_BQ");
                $param['BQ_FILE'] = $url_files;
                $this->db->set('BQ_AT', "CURRENT_DATE", false);
            }
            // Drawing
            if (isset($_FILES['DRAWPATH'])) {
                $url_files = $this->upload($_FILES['DRAWPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_Drawing");
                $param['DRAW_FILE'] = $url_files;
                $this->db->set('DRAW_AT', "CURRENT_DATE", false);
            }
            // ECE
            if (isset($_FILES['ECEPATH'])) {
                $url_files = $this->upload($_FILES['ECEPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_ECE");
                $param['ECE_FILE'] = $url_files;
                $this->db->set('ECE_AT', "CURRENT_DATE", false);
            }
            // Kajian
            if (isset($_FILES['KAJIANPATH'])) {
                $url_files = $this->upload($_FILES['KAJIANPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_KAJIAN");
                $param['KAJIAN_FILE'] = $url_files;
                $this->db->set('KAJIAN_AT', "CURRENT_DATE", false);
            }

            if ($LIST_APP || $LIST_PARAF) {
                $param['STATUS'] = 'In Approval';
            }
            
            // print_r($param);exit;

            $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
            $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            if (!$data['DEPT']) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
                $atasan->JAB_TEXT = 'null';
            }
            if (!$data['BIRO']) {
                $atasan2->NOBADGE = 'null';
                $atasan2->NAMA = 'null';
                $atasan2->EMAIL = 'null';
                $atasan2->JAB_TEXT = 'null';
            }
            $tmpname = explode("@", $data['DEPT']->NAMA);
            $username = $tmpname[0];
            $tmpname2 = explode("@", $data['BIRO']->NAMA);
            $username2 = $tmpname2[0];

            $param['APPROVE2_BY'] = $username2;
            $param['APPROVE2_JAB'] = $data['BIRO']->JAB_TEXT;
            $param['APPROVE3_BY'] = $username;
            $param['APPROVE3_JAB'] = $data['DEPT']->JAB_TEXT;

            // auto approve Di matikan karena berubah pikiran
//            $sAppAuto = '';
//            if (!isset($_FILES['KAJIANPATH'])) {
//                //get limit ece
//                $awhere['GEN_TYPE'] = 'General';
//                $awhere['GEN_CODE'] = 'ECE_LIMIT';
//                $awhere['GEN_VAL'] = '1';
//                $global = (array) $this->general_model->get_data($awhere);
//
//                if ((float) $param['NOMINAL'] > (float) $global['GEN_PAR1']) {
//                    $param['STATUS'] = 'GM Approved';
//                    $this->db->set('APPROVE3_AT', "CURRENT_DATE", false);
//                } else
//                    $param['STATUS'] = 'SM Pengkaji Approved';
//
//                $sAppAuto = 'Approved';
//            }

            $result = $this->dok_eng->save($param);
            // echo $this->db->last_query();
            // set progress status
            $ar_det['PROGRESS'] = '95';
            $this->db->where('ID', $param['ID_PACKET']);
            $dtlresult = $this->assig->update_det($ar_det);
            // create log progress
            unset($data);
            $data['REF_ID'] = $result['REF_ID'];
            $data['REF_TABLE'] = 'MPE_DTL_PENUGASAN';
            $data['DESKRIPSI'] = 'Log Progress Engineering';
            $data['NOTE'] = 'Document Uploaded';
            $data['LOG_RES1'] = $ar_det['PROGRESS'];
            $data['CREATE_BY'] = $param['CREATE_BY'];
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $res_hasil = $this->general_model->log_saving($data);


            if ($result['REF_ID']) {
                if ($LIST_CRT) {
                    $tmparr = explode(':', $LIST_CRT);
                    for ($i = 0; $i < count($tmparr); $i++) {
                        $adata = explode(' - ', $tmparr[$i]);
                        $tmpdata['REF_ID'] = $result['REF_ID'];
                        $tmpdata['REF_TABEL'] = 'DOK_ENG';
                        $tmpdata['TIPE'] = 'CRT';
                        $tmpdata['LVL'] = $i + 1;
                        $tmpdata['STATE'] = 'New';
                        $tmpdata['APP_BADGE'] = $adata[0];
                        $tmpdata['APP_NAME'] = $adata[1];
                        $tmpdata['APP_JAB'] = $adata[3];
                        $tmpdata['APP_EMAIL'] = $adata[2];
                        $tmpdata['STATUS'] = 'In Approval';
                        $tmpdata['NOTE'] = '';
                        $tmpdata['COMPANY'] = $info['company'];
                        $tmpdata['CREATE_BY'] = $info['name'];
                        
                        if ($sAppAuto == 'Approved') {
                            $tmpdata['STATE'] = 'Approved';
                            $tmpdata['STATUS'] = 'Approved';
                            $tmpdata['UPDATE_BY'] = $adata[1];
                            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                        }

                        $this->app->save($tmpdata);

                        if ($sAppAuto == '') {
                            // send email
                            $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$tmpdata['TIPE']};{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');

                            $to = array($tmpdata['APP_EMAIL']);
                            $cc = array();
                            $subject = "Request Approval Document Engineering";
                            $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);
                        }

                        unset($adata);
                        unset($tmpdata);
                    }
                }
                if ($LIST_PARAF) {
                    $tmparr = explode(':', $LIST_PARAF);
                    for ($i = 0; $i < count($tmparr); $i++) {
                        $adata = explode(' - ', $tmparr[$i]);
                        $tmpdata['REF_ID'] = $result['REF_ID'];
                        $tmpdata['REF_TABEL'] = 'DOK_ENG';
                        $tmpdata['TIPE'] = 'Paraf';
                        $tmpdata['LVL'] = $i + 1;
                        $tmpdata['STATE'] = 'New';
                        $tmpdata['APP_BADGE'] = $adata[0];
                        $tmpdata['APP_NAME'] = $adata[1];
                        $tmpdata['APP_JAB'] = $adata[3];
                        $tmpdata['APP_EMAIL'] = $adata[2];
                        $tmpdata['NOTE'] = '';
                        $tmpdata['COMPANY'] = $info['company'];
                        $tmpdata['CREATE_BY'] = $info['name'];
                        if ($sAppAuto == 'Approved') {
                            $tmpdata['STATE'] = 'Approved';
                            $tmpdata['STATUS'] = 'Approved';
                            $tmpdata['UPDATE_BY'] = $adata[1];
                            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                        }

                        $this->app->save($tmpdata);
                        unset($adata);
                        unset($tmpdata);
                    }
                }
                if ($LIST_APP) {
                    $tmparr = explode(':', $LIST_APP);
                    for ($i = 0; $i < count($tmparr); $i++) {
                        $adata = explode(' - ', $tmparr[$i]);
                        $tmpdata['REF_ID'] = $result['REF_ID'];
                        $tmpdata['REF_TABEL'] = 'DOK_ENG';
                        $tmpdata['TIPE'] = 'TTD';
                        $tmpdata['LVL'] = $i + 1;
                        $tmpdata['STATE'] = 'New';
                        $tmpdata['APP_BADGE'] = $adata[0];
                        $tmpdata['APP_NAME'] = $adata[1];
                        $tmpdata['APP_JAB'] = $adata[3];
                        $tmpdata['APP_EMAIL'] = $adata[2];
                        $tmpdata['NOTE'] = '';
                        $tmpdata['COMPANY'] = $info['company'];
                        $tmpdata['CREATE_BY'] = $info['name'];
                        if ($sAppAuto == 'Approved') {
                            $tmpdata['STATE'] = 'Approved';
                            $tmpdata['STATUS'] = 'Approved';
                            $tmpdata['UPDATE_BY'] = $adata[1];
                            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                        }

                        $this->app->save($tmpdata);
                        unset($adata);
                        unset($tmpdata);
                    }
                }

                if ($sAppAuto == 'Approved') {
                    $param['ID'] = $result['REF_ID'];
                    $this->createTrans($param);
                }

                $res_message = 'Data has been saved' . '-';
                if (!$result['REF_ID']) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['NO_DOK_ENG']) && isset($_POST['ID_PENUGASAN']) && isset($_POST['PACKET_TEXT'])) {
            $param = $_POST;
            $LIST_APP = $_POST['LIST_APP'];
            unset($param['LIST_APP']);
            $LIST_PARAF = $_POST['LIST_PARAF'];
            unset($param['LIST_PARAF']);
            $LIST_CRT = $_POST['LIST_CRT'];
            unset($param['LIST_CRT']);

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            if (empty($info['username'])) {
                redirect('login');
                exit;
            }

            $param['UPDATE_BY'] = $info['username'];
            $param['COMPANY'] = $info['company'];
            // RKS
            if (isset($_FILES['RKSPATH'])) {
                $url_files = $this->upload($_FILES['RKSPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_RKS');
                $param['RKS_FILE'] = $url_files;
                $this->db->set('RKS_AT', "CURRENT_DATE", false);
            }
            // BQ
            if (isset($_FILES['BQPATH'])) {
                $url_files = $this->upload($_FILES['BQPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_BQ');
                $param['BQ_FILE'] = $url_files;
                $this->db->set('BQ_AT', "CURRENT_DATE", false);
            }
            // Drawing
            if (isset($_FILES['DRAWPATH'])) {
                $url_files = $this->upload($_FILES['DRAWPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_Drawing');
                $param['DRAW_FILE'] = $url_files;
                $this->db->set('DRAW_AT', "CURRENT_DATE", false);
            }
            // ECE
            if (isset($_FILES['ECEPATH'])) {
                $url_files = $this->upload($_FILES['ECEPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_ECE');
                $param['ECE_FILE'] = $url_files;
                $this->db->set('ECE_AT', "CURRENT_DATE", false);
            }
            // Kajian
            if (isset($_FILES['KAJIANPATH'])) {
                $url_files = $this->upload($_FILES['KAJIANPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_KAJIAN");
                $param['KAJIAN_FILE'] = $url_files;
                $this->db->set('KAJIAN_AT', "CURRENT_DATE", false);
            }

            // auto approve
            $sAppAuto = '';
            if (!isset($_FILES['KAJIANPATH'])) {
                $sAppAuto = 'Approved';
            }

            $result = $this->dok_eng->updateData('update', $param);

            if ($LIST_CRT) {
                // delete data
                unset($delParam);
                $delParam['REF_ID'] = $_POST['ID'];
                $delParam['REF_TABEL'] = 'DOK_ENG';
                $delParam['TIPE'] = 'CRT';
                $delParam['DELETE_BY'] = $info['name'];
                $this->app->deleteData($delParam);

                $tmparr = explode(':', $LIST_CRT);
                for ($i = 0; $i < count($tmparr); $i++) {
                    $adata = explode(' - ', $tmparr[$i]);
                    $tmpdata['REF_ID'] = $_POST['ID'];
                    $tmpdata['REF_TABEL'] = 'DOK_ENG';
                    $tmpdata['TIPE'] = 'CRT';
                    $tmpdata['LVL'] = $i + 1;
                    $tmpdata['STATE'] = 'New';
                    $tmpdata['APP_BADGE'] = $adata[0];
                    $tmpdata['APP_NAME'] = $adata[1];
                    $tmpdata['APP_JAB'] = $adata[3];
                    $tmpdata['APP_EMAIL'] = $adata[2];
                    $tmpdata['STATUS'] = 'In Approval';
                    $tmpdata['NOTE'] = '';
                    $tmpdata['COMPANY'] = $info['company'];
                    $tmpdata['CREATE_BY'] = $info['name'];
                    if ($sAppAuto == 'Approved') {
                        $tmpdata['STATE'] = 'Approved';
                        $tmpdata['STATUS'] = 'Approved';
                        $tmpdata['UPDATE_BY'] = $adata[1];
                        $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                    }

                    $this->app->save($tmpdata);

                    // send email
                    // if (isset($_FILES['KAJIANPATH']) || (isset($_FILES['RKSPATH']) && isset($_FILES['BQPATH']) && isset($_FILES['DRAWPATH']) && isset($_FILES['ECEPATH']))) {
                    if ($sAppAuto == '') {
                        $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};{$tmpdata['TIPE']};{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');

                        $to = array($tmpdata['APP_EMAIL']);
                        $cc = array();
                        $subject = "Request Approval Document Engineering";
                        $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);
                    }
                    // }

                    unset($adata);
                    unset($tmpdata);
                }
            }
            if ($LIST_PARAF) {
                // delete data
                unset($delParam);
                $delParam['REF_ID'] = $_POST['ID'];
                $delParam['REF_TABEL'] = 'DOK_ENG';
                $delParam['TIPE'] = 'Paraf';
                $delParam['DELETE_BY'] = $info['name'];
                $this->app->deleteData($delParam);

                $tmparr = explode(':', $LIST_PARAF);
                for ($i = 0; $i < count($tmparr); $i++) {
                    $adata = explode(' - ', $tmparr[$i]);
                    $tmpdata['REF_ID'] = $_POST['ID'];
                    $tmpdata['REF_TABEL'] = 'DOK_ENG';
                    $tmpdata['TIPE'] = 'Paraf';
                    $tmpdata['LVL'] = $i + 1;
                    $tmpdata['STATE'] = 'New';
                    $tmpdata['APP_BADGE'] = $adata[0];
                    $tmpdata['APP_NAME'] = $adata[1];
                    $tmpdata['APP_JAB'] = $adata[3];
                    $tmpdata['APP_EMAIL'] = $adata[2];
                    $tmpdata['STATUS'] = 'In Approval';
                    $tmpdata['NOTE'] = '';
                    $tmpdata['COMPANY'] = $info['company'];
                    $tmpdata['CREATE_BY'] = $info['name'];
                    if ($sAppAuto == 'Approved') {
                        $tmpdata['STATE'] = 'Approved';
                        $tmpdata['STATUS'] = 'Approved';
                        $tmpdata['UPDATE_BY'] = $adata[1];
                        $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                    }

                    $this->app->save($tmpdata);

                    unset($adata);
                    unset($tmpdata);
                }
            }
            if ($LIST_APP) {
                // delete data
                unset($delParam);
                $delParam['REF_ID'] = $_POST['ID'];
                $delParam['REF_TABEL'] = 'DOK_ENG';
                $delParam['TIPE'] = 'TTD';
                $delParam['DELETE_BY'] = $info['name'];
                $this->app->deleteData($delParam);

                $tmparr = explode(':', $LIST_APP);
                for ($i = 0; $i < count($tmparr); $i++) {
                    $adata = explode(' - ', $tmparr[$i]);
                    $tmpdata['REF_ID'] = $_POST['ID'];
                    $tmpdata['REF_TABEL'] = 'DOK_ENG';
                    $tmpdata['TIPE'] = 'TTD';
                    $tmpdata['LVL'] = $i + 1;
                    $tmpdata['STATE'] = 'New';
                    $tmpdata['APP_BADGE'] = $adata[0];
                    $tmpdata['APP_NAME'] = $adata[1];
                    $tmpdata['APP_JAB'] = $adata[3];
                    $tmpdata['APP_EMAIL'] = $adata[2];
                    $tmpdata['STATUS'] = 'In Approval';
                    $tmpdata['NOTE'] = '';
                    $tmpdata['COMPANY'] = $info['company'];
                    $tmpdata['CREATE_BY'] = $info['name'];
                    if ($sAppAuto == 'Approved') {
                        $tmpdata['STATE'] = 'Approved';
                        $tmpdata['STATUS'] = 'Approved';
                        $tmpdata['UPDATE_BY'] = $adata[1];
                        $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                    }

                    $this->app->save($tmpdata);
                    unset($adata);
                    unset($tmpdata);
                }
            }


            if ($result) {

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
            $pgw = explode(' - ', $nama);

            $list_data = $this->dok_eng->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];
            //get limit ece
            $awhere['GEN_TYPE'] = 'General';
            $awhere['GEN_CODE'] = 'ECE_LIMIT';
            $awhere['GEN_VAL'] = '1';
            $global = (array) $this->general_model->get_data($awhere);

            $param['NOTIFIKASI'] = $notifikasi;
            $param['PACKET_TEXT'] = $paket;
            $param['NO_DOK_ENG'] = $list_data['NO_DOK_ENG'];
            $param['CREATE_BY'] = $list_data['CREATE_BY'];

            $reason = $_POST['reason'];

            // update approval
            unset($data); // clear array
            if ($tipe == 'CRT' || $tipe == 'Paraf' || $tipe == 'TTD') {
                $data['ID'] = $id;
                $data['TIPE'] = $tipe;
                $data['APP_NAME'] = $pgw[1];
                $data['STATE'] = 'Approved';
                $data['STATUS'] = 'Approved';
                $data['UPDATE_BY'] = $pgw[1];
                $data['NOTE'] = $reason;
                $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                $result = $this->app->updateData('approve', $data);

                $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = '{$tipe}' AND (STATE='New' OR STATE='Rejected') AND DELETE_AT IS NULL");
                if (!$dtApp) {
                    if ($tipe == 'CRT') {

                        $data2['ID'] = $id;
                        $data2['STATUS'] = 'CRT Approved';
                        $result = $this->dok_eng->updateData('approve', $data2);

                        $LIST_PARAF = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf' AND STATE='New' AND DELETE_AT IS NULL");
                        foreach ($LIST_PARAF as $value) {
                            $tmpdata['ID'] = $value['REF_ID'];
                            $tmpdata['REF_ID'] = $value['REF_ID'];
                            $tmpdata['REF_TABEL'] = 'DOK_ENG';
                            $tmpdata['TIPE'] = $value['TIPE'];
                            $tmpdata['LVL'] = $value['LVL'];
                            $tmpdata['STATE'] = 'New';
                            $tmpdata['APP_BADGE'] = $value['APP_BADGE'];
                            $tmpdata['APP_NAME'] = $value['APP_NAME'];
                            $tmpdata['APP_JAB'] = $value['APP_JAB'];
                            $tmpdata['APP_EMAIL'] = $value['APP_EMAIL'];
                            $tmpdata['STATUS'] = 'In Approval';
                            $tmpdata['NOTE'] = '';
                            $tmpdata['COMPANY'] = $value['COMPANY'];
                            $tmpdata['CREATE_BY'] = $value['CREATE_BY'];

                            $this->db->where('APP_NAME', $value['APP_NAME']);
                            $this->db->where('TIPE', $value['TIPE']);
                            $this->db->where('REF_TABEL', $tmpdata['REF_TABEL']);
                            $update = $this->app->updateData('approve', $tmpdata);

                            $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$list_data['TIPE']};Paraf;{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');

                            $to = array($tmpdata['APP_EMAIL']);
                            $cc = array();
                            //$subject = "MGR DOCUMENTS - APPROVAL REQUEST";
                            $subject = "Request Approval Document Engineering";
                            $this->send_mail($subject, $list_data, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);

                            unset($adata);
                            unset($tmpdata);
                        }
                    }if ($tipe == 'Paraf') {

                        $data2['ID'] = $id;
                        $data2['STATUS'] = 'MGR Pengkaji Approved';
                        $result = $this->dok_eng->updateData('approve', $data2);

                        $LIST_TTD = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD' AND STATE='New' AND DELETE_AT IS NULL");
                        foreach ($LIST_TTD as $value) {
                            $tmpdata['ID'] = $value['REF_ID'];
                            $tmpdata['REF_ID'] = $value['REF_ID'];
                            $tmpdata['REF_TABEL'] = 'DOK_ENG';
                            $tmpdata['TIPE'] = $value['TIPE'];
                            $tmpdata['LVL'] = $value['LVL'];
                            $tmpdata['STATE'] = 'New';
                            $tmpdata['APP_BADGE'] = $value['APP_BADGE'];
                            $tmpdata['APP_NAME'] = $value['APP_NAME'];
                            $tmpdata['APP_JAB'] = $value['APP_JAB'];
                            $tmpdata['APP_EMAIL'] = $value['APP_EMAIL'];
                            $tmpdata['STATUS'] = 'In Approval';
                            $tmpdata['NOTE'] = '';
                            $tmpdata['COMPANY'] = $value['COMPANY'];
                            $tmpdata['CREATE_BY'] = $value['CREATE_BY'];

                            $this->db->where('APP_NAME', $value['APP_NAME']);
                            $this->db->where('TIPE', $value['TIPE']);
                            $this->db->where('REF_TABEL', $tmpdata['REF_TABEL']);
                            $update = $this->app->updateData('approve', $tmpdata);

                            $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$list_data['TIPE']};TTD;{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');

                            $to = array($tmpdata['APP_EMAIL']);
                            $cc = array();
                            //$subject = "BIRO DOCUMENTS - APPROVAL REQUEST";
                            $subject = "Request Approval Document Engineering";
                            $this->send_mail($subject, $list_data, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);

                            unset($adata);
                            unset($tmpdata);
                        }
                    } elseif ($tipe == 'TTD') {

                        $data2['ID'] = $id;
                        $data2['STATUS'] = 'SM Pengkaji Approved';
                        $result = $this->dok_eng->updateData('approve', $data2);

                        if ((float) $list_data['NOMINAL'] > (float) $global['GEN_PAR1'] || (isset($list_data['KAJIAN_FILE']) && !empty($list_data['KAJIAN_FILE']))) {
                            $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$list_data['CREATE_BY']}' OR k.mk_email LIKE '{$list_data['CREATE_BY']}@%')");
                            $info = $info[0];
                            $atasan = $this->dbhris->get_level_from($info->NOBADGE, 'DEPT');

                            $n_form = $this->my_crypt("{$id};3;{$param['NOTIFIKASI']};{$list_data['TIPE']};GM;{$atasan->NOBADGE} - {$atasan->NAMA}", 'e');
                            $to = $atasan->EMAIL;
                            $cc = array();
                            //$subject = "DEPT DOCUMENTS - APPROVAL REQUEST";
                            $subject = "Request Approval Document Engineering";
                            $this->send_mail($subject, $list_data, "{$atasan->NOBADGE} - {$atasan->NAMA}", $n_form, $to, $cc);
                        } else {
                            $this->createTrans($list_data);
                            // $ar_det['PROGRESS'] = '100';
                            // $this->db->where("ID_PENUGASAN = (SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID = '{$id}') AND DESKRIPSI = (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID = '{$id}')");
                            // $dtlresult = $this->assig->update_det($ar_det);
                            //
                  // // data erf
                            // $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$id} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
                            // $this->db->where($where);
                            // $dtErf = $this->req->get_datarequest();
                            // $dtErf = $dtErf[0];
                            // $where = " k.mk_nama = '{$dtErf['CREATE_BY']}' AND k.muk_kode='{$dtErf['UK_CODE']}'";
                            // $pgErf = $this->dbhris->get_hris_where($where);
                            // $pgErf = $pgErf[0];
                            // $atas = $this->dbhris->get_level_from($pgErf->NOBADGE, 'BIRO');
                            //
                  // // send transmital
                            // $to=array();
                            // $cc=array();
                            // unset($where);
                            // $where['GEN_VAL']='1';
                            // $where['GEN_CODE']='TRM_EMAIL';
                            // $where['GEN_TYPE']='General';
                            // $data = $this->general_model->get_data_result($where);
                            // if (count($data)>0) {
                            // 	foreach ($data as $key => $value) {
                            // 		if ($value->STATUS=='to') {
                            // 			$to[]=$value->GEN_PAR1;
                            // 		}elseif ($value->STATUS=='cc') {
                            // 			$cc[]=$value->GEN_PAR1;
                            // 		}
                            // 	}
                            // }
                            // $subject="Transmital Confirmation";
                            // $this->send_trm($subject, $list_data, "{$atas->NOBADGE} - {$atas->NAMA}", $id, $to, $cc);
                            //
                  // // create transmital
                            // $this->db->where('ID_DOK_ENG',$id);
                            // $dtTrm = $this->bast->get_record();
                            // if (count($dtTrm)==0) {
                            //   $sTipe = 'Kajian Engineering';
                            //   if (strpos($dtErf['TIPE'], 'Detail') !== false) {
                            //     $sTipe = 'Detail Design Engineering';
                            //   }
                            //   $trn_no = str_replace('TR','TF',$list_data['NO_DOK_ENG']);
                            //   unset($param);
                            //   $param['NO_BAST']= $this->generateNo('NO_BAST','MPE_BAST', 'TF', $trn_no);
                            //   $param['ID_DOK_ENG']=$id;
                            //   $param['NOTIFIKASI']=$notifikasi;
                            //   $param['HDR_TEXT']="Pada hari ini " . $this->hari_ini() . ", tanggal " . trim($this->penyebut(date('j'))) . ", bulan " . $this->bulan_ini() . ", tahun " . trim($this->penyebut(date('Y'))) . " (" . date('d-m-Y') . ") menyatakan, ";
                            //   $param['MID_TEXT']="bahwa {$sTipe} untuk Investasi PT Semen Indonesia (Persero) Tbk, dengan pekerjaan sebagai berikut :";
                            //   $param['FTR_TEXT']="Dinyatakan selesai 100% dan diserahterimakan kepada unit kerja sesuai dengan rencana.\n\nDemikian Transmital Pengiriman Dokumen Engineering ini dibuat dengan sebenar-benarnya untuk dapat dipergunakan sebagaimana mestinya.";
                            //   $param['STATUS'] = 'Closed';
                            //   $param['COMPANY'] = $list_data['COMPANY'];
                            //   $param['DEPT_CODE'] = $list_data['DEPT_CODE'];
                            //   $param['DEPT_TEXT'] = $list_data['DEPT_TEXT'];
                            //   $param['UK_CODE'] = $list_data['UK_CODE'];
                            //   $param['UK_TEXT'] = $list_data['UK_TEXT'];
                            //   $param['CREATE_BY'] = $list_data['APPROVE3_BY'];
                            //   $param['APPROVE1_BY'] = $list_data['APPROVE3_BY'];
                            //   $param['APPROVE1_JAB'] = $list_data['APPROVE3_JAB'];
                            //   $param['APPROVE2_BY'] = $atas->NAMA;
                            //   $param['APPROVE2_JAB'] = $atas->JAB_TEXT;
                            //   $result = $this->bast->save($param);
                            // }
                        }
                    }
                }
            } else {

                $data['ID'] = $id;
                if ($lvl == '1') {
                    $slvl = 'Mgr';
                } elseif ($lvl == '2') {
                    $slvl = 'SM';
                } else {
                    $slvl = 'GM';
                }
                $data['STATUS'] = $slvl . ' Approved';
                //$data['APPROVE'.$lvl.'_BY']=$pgw[1];
                $data['NOTE' . $lvl] = $reason;
                $this->db->set('APPROVE' . $lvl . '_AT', "CURRENT_DATE", false);
                $result = $this->dok_eng->updateData('approve', $data);

                if ($tipe == 'MGR') {
                    $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$list_data['CREATE_BY']}' OR k.mk_email LIKE '{$list_data['CREATE_BY']}@%')");
                    $info = $info[0];
                    $atasan = $this->dbhris->get_level_from($info->NOBADGE, 'BIRO');

                    $n_form = $this->my_crypt("{$id};2;{$param['NOTIFIKASI']};{$list_data['TIPE']};SM;{$atasan->NOBADGE} - {$atasan->NAMA}", 'e');
                    $to = $atasan->EMAIL;
                    //$subject = "BIRO Document APPROVAL";
                    $subject = "Request Approval Document Engineering";
                    $this->send_mail($subject, $list_data, "{$atasan->NOBADGE} - {$atasan->NAMA}", $n_form, $to, $cc);
                } elseif ($tipe == 'SM') {

                    if ((float) $list_data['NOMINAL'] > (float) $global['GEN_PAR1'] || (isset($list_data['KAJIAN_FILE']) && !empty($list_data['KAJIAN_FILE']))) {
                        $info = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' OR k.mk_email LIKE '{$list_data['CREATE_BY']}@%'");
                        $info = $info[0];
                        $atasan2 = $this->dbhris->get_level_from($info->NOBADGE, 'DEPT');

                        $n_form = $this->my_crypt("{$id};3;{$param['NOTIFIKASI']};{$list_data['TIPE']};GM;{$atasan2->NOBADGE} - {$atasan2->NAMA}", 'e');
                        $to = $atasan2->EMAIL;
                        //$subject = "Department Document APPROVAL";
                        $subject = "Request Approval Document Engineering";
                        $this->send_mail($subject, $list_data, "{$atasan2->NOBADGE} - {$atasan2->NAMA}", $n_form, $to, $cc);
                    } else {
                        $this->createTrans($list_data);
                        // $ar_det['PROGRESS'] = '100';
                        // $this->db->where("ID_PENUGASAN = (SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID = '{$id}') AND DESKRIPSI = (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID = '{$id}')");
                        // $dtlresult = $this->assig->update_det($ar_det);
                        //
            // // data erf
                        // $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$id} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
                        // $this->db->where($where);
                        // $dtErf = $this->req->get_datarequest();
                        // $dtErf = $dtErf[0];
                        // $where = " k.mk_nama = '{$dtErf['CREATE_BY']}' AND k.muk_kode='{$dtErf['UK_CODE']}'";
                        // $pgErf = $this->dbhris->get_hris_where($where);
                        // $pgErf = $pgErf[0];
                        // $atas = $this->dbhris->get_level_from($pgErf->NOBADGE, 'BIRO');
                        //
            // // send transmital
                        // $to=array();
                        // $cc=array();
                        // unset($where);
                        // $where['GEN_VAL']='1';
                        // $where['GEN_CODE']='TRM_EMAIL';
                        // $where['GEN_TYPE']='General';
                        // $data = $this->general_model->get_data_result($where);
                        // if (count($data)>0) {
                        // 	foreach ($data as $key => $value) {
                        // 		if ($value->STATUS=='to') {
                        // 			$to[]=$value->GEN_PAR1;
                        // 		}elseif ($value->STATUS=='cc') {
                        // 			$cc[]=$value->GEN_PAR1;
                        // 		}
                        // 	}
                        // }
                        // $subject="Transmital Confirmation";
                        // $this->send_trm($subject, $list_data, "{$atas->NOBADGE} - {$atas->NAMA}", $id, $to, $cc);
                        //
            // // create transmital
                        // $this->db->where('ID_DOK_ENG',$id);
                        // $dtTrm = $this->bast->get_record();
                        // if (count($dtTrm)==0) {
                        //   $sTipe = 'Kajian Engineering';
                        //   if (strpos($dtErf['TIPE'], 'Detail') !== false) {
                        //     $sTipe = 'Detail Design Engineering';
                        //   }
                        //   $trn_no = str_replace('TR','TF',$list_data['NO_DOK_ENG']);
                        //   unset($param);
                        //   $param['NO_BAST']= $this->generateNo('NO_BAST','MPE_BAST', 'TF', $trn_no);
                        //   $param['ID_DOK_ENG']=$id;
                        //   $param['NOTIFIKASI']=$notifikasi;
                        //   $param['HDR_TEXT']="Pada hari ini " . $this->hari_ini() . ", tanggal " . trim($this->penyebut(date('j'))) . ", bulan " . $this->bulan_ini() . ", tahun " . trim($this->penyebut(date('Y'))) . " (" . date('d-m-Y') . ") menyatakan :";
                        //   $param['MID_TEXT']="bahwa {$sTipe} untuk Investasi PT Semen Indonesia (Persero) Tbk, dengan pekerjaan sebagai berikut :";
                        //   $param['FTR_TEXT']="Dinyatakan selesai 100% dan diserahterimakan kepada unit kerja sesuai dengan rencana.\n\nDemikian Transmital Pengiriman Dokumen Engineering ini dibuat dengan sebenar-benarnya untuk dapat dipergunakan sebagaimana mestinya.";
                        //   $param['STATUS'] = 'Closed';
                        //   $param['COMPANY'] = $list_data['COMPANY'];
                        //   $param['DEPT_CODE'] = $list_data['DEPT_CODE'];
                        //   $param['DEPT_TEXT'] = $list_data['DEPT_TEXT'];
                        //   $param['UK_CODE'] = $list_data['UK_CODE'];
                        //   $param['UK_TEXT'] = $list_data['UK_TEXT'];
                        //   $param['CREATE_BY'] = $list_data['APPROVE3_BY'];
                        //   $param['APPROVE1_BY'] = $list_data['APPROVE3_BY'];
                        //   $param['APPROVE1_JAB'] = $list_data['APPROVE3_JAB'];
                        //   $param['APPROVE2_BY'] = $atas->NAMA;
                        //   $param['APPROVE2_JAB'] = $atas->JAB_TEXT;
                        //   $result = $this->bast->save($param);
                        // }
                    }
                } else {
                    $ar_det['PROGRESS'] = '100';
                    $this->db->where("ID_PENUGASAN = (SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID = '{$id}') AND DESKRIPSI = (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID = '{$id}')");
                    $dtlresult = $this->assig->update_det($ar_det);

                    // data erf
                    $where = "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID=(SELECT ID_PENUGASAN FROM MPE_DOK_ENG WHERE ID={$id} AND DELETE_AT IS NULL) AND DELETE_AT IS NULL)";
                    $this->db->where($where);
                    $dtErf = $this->req->get_datarequest();
                    $dtErf = $dtErf[0];
                    $where = " k.mk_nama = '{$dtErf['CREATE_BY']}' AND k.muk_kode='{$dtErf['UK_CODE']}'";
                    $pgErf = $this->dbhris->get_hris_where($where);
                    $pgErf = $pgErf[0];
                    $atas = $this->dbhris->get_level_from($pgErf->NOBADGE, 'BIRO');

                    // send transmital
                    $to = array();
                    $cc = array();
                    unset($where);
                    $where['GEN_VAL'] = '1';
                    $where['GEN_CODE'] = 'TRM_EMAIL';
                    $where['GEN_TYPE'] = 'General';
                    $data = $this->general_model->get_data_result($where);
                    if (count($data) > 0) {
                        foreach ($data as $key => $value) {
                            if ($value->STATUS == 'to') {
                                $to[] = $value->GEN_PAR1;
                            } elseif ($value->STATUS == 'cc') {
                                $cc[] = $value->GEN_PAR1;
                            }
                        }
                    }
                    $subject = "Transmital Receipt";
                    $this->send_trm($subject, $list_data, "{$atas->NOBADGE} - {$atas->NAMA}", $id, $to, $cc);

                    // create transmital
                    $this->db->where('ID_DOK_ENG', $id);
                    $dtTrm = $this->bast->get_record();
                    if (count($dtTrm) == 0) {
                        $sTipe = 'Kajian Engineering';
                        if (strpos($dtErf['TIPE'], 'Detail') !== false) {
                            $sTipe = 'Detail Design Engineering';
                        }
                        $trn_no = str_replace('TR', 'TF', $list_data['NO_DOK_ENG']);
                        unset($param);
                        $param['NO_BAST'] = $this->generateNo('NO_BAST', 'MPE_BAST', 'TF', $trn_no);
                        $param['ID_DOK_ENG'] = $id;
                        $param['NOTIFIKASI'] = $notifikasi;
                        $param['HDR_TEXT'] = "Pada hari ini " . $this->hari_ini() . ", tanggal " . trim($this->penyebut(date('j'))) . ", bulan " . $this->bulan_ini() . ", tahun " . trim($this->penyebut(date('Y'))) . " (" . date('d-m-Y') . ") menyatakan :";
                        $param['MID_TEXT'] = "bahwa {$sTipe} untuk Investasi PT Semen Indonesia (Persero) Tbk, dengan pekerjaan sebagai berikut :";
                        $param['FTR_TEXT'] = "Dinyatakan selesai 100% dan diserahterimakan kepada unit kerja sesuai dengan rencana.\n\nDemikian Transmital Pengiriman Dokumen Engineering ini dibuat dengan sebenar-benarnya untuk dapat dipergunakan sebagaimana mestinya.";
                        $param['STATUS'] = 'Closed';
                        $param['COMPANY'] = $list_data['COMPANY'];
                        $param['DEPT_CODE'] = $list_data['DEPT_CODE'];
                        $param['DEPT_TEXT'] = $list_data['DEPT_TEXT'];
                        $param['UK_CODE'] = $list_data['UK_CODE'];
                        $param['UK_TEXT'] = $list_data['UK_TEXT'];
                        $param['CREATE_BY'] = $list_data['APPROVE3_BY'];
                        $param['APPROVE1_BY'] = $list_data['APPROVE3_BY'];
                        $param['APPROVE1_JAB'] = $list_data['APPROVE3_JAB'];
                        $param['APPROVE2_BY'] = $atas->NAMA;
                        $param['APPROVE2_JAB'] = $atas->JAB_TEXT;
                        $result = $this->bast->save($param);
    

                        // GET ERF CREATOR AND ERF APPROVER
                        $erf = $this->bast->get_erf_creator($result['ID']);
                        $erf = $erf[0];
                        $where = "k.mk_nama = '{$erf['CREATE_BY']}'";
                        $erfCreator = $this->dbhris->get_hris_where($where);
                        $erfCreator = $erfCreator[0];

                        $where = "k.mk_nama = '{$erf['APPROVE_BY']}'";
                        $erfApprover = $this->dbhris->get_hris_where($where);
                        $erfApprover = $erfApprover[0];
                        
                        $to = $erfCreator->EMAIL;
                        $this->send_trm($subject, $list_data, "{$erfCreator->NOBADGE} - {$erfCreator->NAMA}", $id, $to, $cc);
                        $to = $erfApprover->EMAIL;
                        $this->send_trm($subject, $list_data, "{$erfApprover->NOBADGE} - {$erfApprover->NAMA}", $id, $to, $cc);
                        // END OF GET ERF CREATOR AND ERF APPROVER
                    }
                }
            }
            if ($result) {
                $result2['message'] = "Documents Paket '{$paket}' was approved succesfully" . $pgw[1];
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function reject() {
        $e = $_POST['e'];
        $e = str_replace('6s4', '/', $e);
        $e = str_replace('1p4', '+', $e);
        $e = str_replace('sM9', '=', $e);
        $no_form = $this->my_crypt($e, 'd');
        list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
        $pgw = explode(' - ', $nama);

        $list_data = $this->dok_eng->get_record("A.ID='{$id}'");
        $list_data = $list_data[0];

        $reason = $_POST['reason'];


        // update approval
        unset($data); // clear array
        if ($tipe == 'CRT' || $tipe == 'Paraf' || $tipe == 'TTD') {
            $data['ID'] = $id;
            $data['TIPE'] = $tipe;
            $data['APP_NAME'] = $pgw[1];
            $data['STATE'] = 'Rejected';
            $data['STATUS'] = 'Rejected';
            $data['UPDATE_BY'] = $pgw[1];
            $data['NOTE'] = $reason;
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $result = $this->app->updateData('approve', $data);
        }
        // code...
        unset($data); // clear array
        $data['ID'] = $id;
        $data['STATUS'] = 'Rejected';
        $this->db->set('REJECT_REASON', $reason);
        $this->db->set('REJECT_BY', $pgw[0]);
        $this->db->set('REJECT_DATE', "CURRENT_DATE", false);
        $result = $this->dok_eng->updateData('approve', $data);

        if ($result) {
            $result2['message'] = 'success';
            $result2['status'] = '200';
            echo json_encode($result2);
        } else {
            $result2['message'] = 'fail';
            $result2['status'] = '400';
            echo json_encode($result2);
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['ID'] = $id;
            $data['DELETE_BY'] = $info['name'];
            $result = $this->dok_eng->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id) {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if ($id && isset($info)) {
            // error_reporting(0); // -1
            // ini_set('display_errors', 0); //1
            // $list_data = $_POST;
            $list_data = $this->dok_eng->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];
            echo '<pre>';
            var_dump($list_data);
            $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
            $dtPar = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
            $dtCrt = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'CRT'");
            // set list approval hris
            $where['GEN_VAL'] = '1';
            $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
            $where['GEN_TYPE'] = 'General';
            $global = $this->general_model->get_data($where);
            $dtCre = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' AND k.company = '{$list_data['COMPANY']}' AND (uk.muk_parent LIKE '" . $global->GEN_PAR5 . "' OR k.muk_kode = '" . $global->GEN_PAR5 . "')");
            if ($dtCre) {
                $dtCre = $dtCre[0];
            }

//            echo '<pre>';
//            var_dump($list_data);
            //get limit ece
            $awhere['GEN_TYPE'] = 'General';
            $awhere['GEN_CODE'] = 'ECE_LIMIT';
            $awhere['GEN_VAL'] = '1';
            $limit_ece = (array) $this->general_model->get_data($awhere);
            unset($awhere);

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 13);
            $this->fpdf->SetLeftMargin(20);

            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX() + 7, $this->fpdf->GetY(), 0, 15, 'PNG'), 'LT', 0, 'C');
            $this->fpdf->Cell(100, 6, "PT. SEMEN INDONESIA (PERSERO)Tbk.", 'T', 0, 'C');
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/si_group.jpg', $this->fpdf->GetX() + 3, $this->fpdf->GetY() + 4, 0, 10, 'JPG'), 'TR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(170, 6, "{$dtCre->DEPT_TEXT}", 'LR', 1, 'C');
            $this->fpdf->Cell(170, 6, "{$dtCre->CC_TEXT}", 'LBR', 1, 'C');

            $this->fpdf->Cell(170, 6, "", 'LBR', 1);
            if ($list_data['COLOR'] == 'Merah') {
                $this->fpdf->SetFillColor(204, 255, 204);
            } elseif ($list_data['COLOR'] == 'Kuning') {
                $this->fpdf->SetFillColor(230, 230, 0);
            } elseif ($list_data['COLOR'] == 'Biru') {
                $this->fpdf->SetFillColor(200, 220, 255);
            } elseif ($list_data['COLOR'] == 'Hijau') {
                $this->fpdf->SetFillColor(66, 244, 143);
            } elseif ($list_data['COLOR'] == 'Orange') {
                $this->fpdf->SetFillColor(245, 173, 66);
            } else {
                $this->fpdf->SetFillColor(239, 239, 245);
            }
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(50, 6, " No. Dokumen", 'LT', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'T', 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NO_DOK_ENG']}", 'TR', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Tanggal", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 0, 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['CREATE_AT']}", 'R', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Notifikasi", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 0, 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NOTIFIKASI']}", 'R', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Tahun", 'BL', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'B', 0, 'C', 1);
            $time = strtotime($list_data['CREATE_AT']);
            $year = date('Y', $time);
            $this->fpdf->Cell(110, 6, "{$year}", 'BR', 1, 'L', 1);
            $this->fpdf->Cell(170, 6, "", 'LBR', 1);

            $this->fpdf->SetFont('Arial', 'BU', 18);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Ln(5);
            $this->fpdf->Cell(170, 6, "DOKUMEN ENGINEERING", 0, 1, 'C');
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'B', 18);
            $this->fpdf->Ln(10);
            $this->fpdf->MultiCell(170, 8, strtoupper("TOR, BQ dan Drawing"), 0, 'C');
            $this->fpdf->Ln(10);
            $this->fpdf->MultiCell(170, 8, "{$list_data['PACKET_TEXT']}", 0, 'C');

            $this->fpdf->SetFont('Arial', 'B', 8);
            if (strlen($list_data['PACKET_TEXT']) < 45) {
                $this->fpdf->Ln(15);
            } elseif (strlen($list_data['PACKET_TEXT']) < 90) {
                $this->fpdf->Ln(5);
            }
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Cell(170, 5, "Pengesahan", 1, 0, 'L');

            include(APPPATH . 'libraries/phpqrcode/qrlib.php');

            // approval info
            $this->fpdf->SetFont('Arial', 'B', 8);
            $width = (170 / (1));
            $this->fpdf->Ln(5);
            if ((float) $list_data['NOMINAL'] > (float) $limit_ece['GEN_PAR1'] || (isset($list_data['KAJIAN_FILE']) && !empty($list_data['KAJIAN_FILE']))) {
                $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
                // jabatan
                $this->fpdf->Ln(5);
                $this->fpdf->SetFont('Arial', 'I', 8);
                $y1 = $this->fpdf->GetY();
                $x1 = $this->fpdf->GetX();
                $lnmax = 10;
                $h = 8;
                if (strlen($list_data['APPROVE3_JAB']) > $lnmax)
                    $h = 4;
                else
                    $h = 8;
                $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE3_JAB']}", 'LBR', 'C');

                // QR code
                $this->fpdf->Ln(0);
                if ($list_data['APPROVE3_AT']) {
                    $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-3-GM") . ".png";
                    $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE3_BY']};{$list_data['APPROVE3_AT']};", 'e');
                    $str = str_replace('/', '6s4', $str);
                    $str = str_replace('+', '1p4', $str);
                    $str = str_replace('=', 'sM9', $str);
                    $str = base_url() . "info/dok_eng/" . $str;
                    QRcode::png($str, $tmpCre);
                    $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
                }
                // name
                $this->fpdf->Ln(25);
                $this->fpdf->SetFont('Arial', 'B', 7.8);
                $this->fpdf->Cell($width, 5, "({$list_data['APPROVE3_BY']})", 'LBR', 1, 'C');
            }

            // SM Pengkaji Approval
            $width = (170 / (count($dtApp)));
            $this->fpdf->Ln(5);
            foreach ($dtApp as $key) {
                $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
            }
            // jabatan
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'I', 8);
            $y1 = $this->fpdf->GetY();
            $x1 = $this->fpdf->GetX();
            $lnmax = 26;
            if (count($dtApp) > 4) {
                $lnmax = 20;
            } elseif (count($dtApp) < 4) {
                $lnmax = 17;
            }
            $y2 = $this->fpdf->GetY();
            $pos_x = 20;
            $inum = 0;
            foreach ($dtApp as $key) {
                if (strlen($key['APP_JAB']) > $lnmax) {
                    $h = 4;
                } else {
                    $i++;
                    $h = 8;
                    if ($i == (count($dtapp) - 2)) {
                        $h = 4;
                    }
                }
                $this->fpdf->SetXY($pos_x, $y1);
                $this->fpdf->MultiCell($width, $h, "{$key['APP_JAB']}", 'LR', 'C');
                $pos_x += $width;
            }
            // QR code
            $hQRapp = 20;
            foreach ($dtApp as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpApp = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-TTD") . ".png";
                    $strApp = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                    $strApp = str_replace('/', '6s4', $strApp);
                    $strApp = str_replace('+', '1p4', $strApp);
                    $strApp = str_replace('=', 'sM9', $strApp);
                    $strApp = base_url() . "info/dok_eng/" . $strApp;
                    QRcode::png($strApp, $tmpApp);
                    $this->fpdf->Cell($width, $hQRapp, $this->fpdf->Image($tmpApp, $this->fpdf->GetX() + ($width / 2) - ($hQRapp / 2), $this->fpdf->GetY(), 0, $hQRapp, 'PNG'), 'LTR', 0, 'C');
                } else {
                    $this->fpdf->Cell($width, $hQRapp, "", "LTR", 0, 'C');
                }
            }
            // name
            $this->fpdf->SetFont('Arial', 'BI', 7);
            $this->fpdf->Ln($hQRapp);
            foreach ($dtApp as $key) {
                $this->fpdf->Cell($width, 5, "({$key['APP_NAME']})", 'LBR', 0, 'C');
            }

            if ((float) $list_data['NOMINAL'] <= (float) $limit_ece['GEN_PAR1'] && empty($list_data['KAJIAN_FILE'])) {
                $this->fpdf->Ln(35);
            }

            //paraf and created by
            $this->fpdf->Ln(10);
            $this->fpdf->Cell(85, 8, "Dibuat : ", 0, 0, 'C');
            $this->fpdf->Cell(85, 8, "Diverifikasi : ", 0, 1, 'C');
            $width = 20;
            $this->fpdf->SetFont('Arial', '', 8);
            if (count($dtCrt) > 0) {
                $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2), 10, '', 0, 0, 'R');
                foreach ($dtCrt as $key) {
                    if ($key['STATE'] == 'Approved') {
                        $tmpPar = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-CRT") . ".png";
                        $strPar = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                        $strPar = str_replace('/', '6s4', $strPar);
                        $strPar = str_replace('+', '1p4', $strPar);
                        $strPar = str_replace('=', 'sM9', $strPar);
                        $strPar = base_url() . "info/dok_eng/" . $strPar;
                        QRcode::png($strPar, $tmpPar);
                        $this->fpdf->Cell($width, 20, $this->fpdf->Image($tmpPar, $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 20, 'PNG'), 1, 0, 'C');
                    } else {
                        $this->fpdf->Cell($width, 20, "", 1, 0, 'C');
                    }
                }
                $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2), 10, '', 0, 0, 'R');
            }
            $this->fpdf->Cell(((85 - ($width * count($dtPar))) / 2), 10, '', 0, 0, 'R');
            foreach ($dtPar as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpPar = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-Paraf") . ".png";
                    $strPar = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                    $strPar = str_replace('/', '6s4', $strPar);
                    $strPar = str_replace('+', '1p4', $strPar);
                    $strPar = str_replace('=', 'sM9', $strPar);
                    $strPar = base_url() . "info/dok_eng/" . $strPar;
                    QRcode::png($strPar, $tmpPar);
                    $this->fpdf->Cell($width, 20, $this->fpdf->Image($tmpPar, $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 20, 'PNG'), 1, 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 20, "", 1, 0, 'C');
                }
            }
            $this->fpdf->Cell(((85 - ($width * count($dtPar))) / 2), 10, '', 0, 0, 'R');

            // paraf name
            $this->fpdf->Cell(15, 15, "", 0, 1, 'L');
            $this->fpdf->SetFont('Arial', '', 5.5);
            if (count($dtCrt) > 0) {
                $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2), 10, '', 0, 0, 'R');
                foreach ($dtCrt as $key) {
                    if ($key['STATE'] == 'Approved') {
                        $tmpname = explode(" ", $key['APP_NAME']);
                        $this->fpdf->Cell($width, 15, $tmpname[0] . " " . $tmpname[1], 0, 0, 'C');
                    } else {
                        $this->fpdf->Cell($width, 15, "", 0, 0, 'C');
                    }
                }
                $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2), 10, '', 0, 0, 'R');
            } else {
                $this->fpdf->Cell(85, "", 0, 0);
            }
            $this->fpdf->Cell(((85 - ($width * count($dtPar))) / 2), 10, '', 0, 0, 'R');
            foreach ($dtPar as $key) {
                if ($key['STATE'] == 'Approved') {
                    $tmpname = explode(" ", $key['APP_NAME']);
                    $this->fpdf->Cell($width, 15, $tmpname[0] . " " . $tmpname[1], 0, 0, 'C');
                } else {
                    $this->fpdf->Cell($width, 15, "", 0, 0, 'C');
                }
            }

            // line
            $this->fpdf->Ln(23);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);
            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->Ln(2);
            $this->fpdf->Cell(170, 2, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 1);

            // temporary download
            $tmpPath = FCPATH . "media/upload/tmp/" . md5("{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}") . ".pdf";
            $this->fpdf->Output($tmpPath, 'F');

            $arfile = array($tmpPath);
            // RKS
            if ($list_data['RKS_FILE']) {
                // adding footer
//                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-TR") . ".pdf";
//                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['RKS_FILE'], $list_data, 'TR', $tmpPath, 'F');
//                array_push($arfile, $tmpLoc);
                array_push($arfile, FCPATH . $list_data['RKS_FILE']);
            }
            // BQ
            if ($list_data['BQ_FILE']) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-BQ") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['BQ_FILE'], $list_data, 'BQ', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
            }
            // Drawwing
            if ($list_data['DRAW_FILE']) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-DW") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['DRAW_FILE'], $list_data, 'DW', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
            }
            // // ECE
            // if ($list_data['ECE_FILE']) {
            //   array_push($arfile, $list_data['ECE_FILE']);
            // }
            // Kajian
            if ($list_data['KAJIAN_FILE']) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-KJ") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['KAJIAN_FILE'], $list_data, 'KJ', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
            }

            $this->load->library('PDFMerger');
            $pdf = new PDFMerger;

            foreach ($arfile as $key => $value) {
                $pdf->addPDF($value, 'all');
            }
            // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page

            $pdf->merge('download', "{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

    public function setFtrCode($src, $dtList = array(), $jenis = 'TR', $output = 'test.pdf', $tipe = 'D') {
        if (!class_exists("FPDF")) {
            require_once APPPATH . 'libraries/fpdf/fpdf.php';
        }
        if (!class_exists("FPDI")) {
            require_once APPPATH . 'libraries/fpdi/fpdi.php';
        }

        $pdf = new FPDI();
        $pageCount = $pdf->setSourceFile($src);

        // generate qrcode
        $lembar = $pageCount;
        $tmpFtr = FCPATH . "media/upload/tmp/ftr" . md5("DOK_ENG-{$dtList['ID']}-{$dtList['NOTIFIKASI']}") . ".png";
        $strFtr = $this->my_crypt("{$dtList['ID']};{$jenis};{$lembar};", 'e');
        $strFtr = str_replace('/', '6s4', $strFtr);
        $strFtr = str_replace('+', '1p4', $strFtr);
        $strFtr = str_replace('=', 'sM9', $strFtr);
        $strFtr = base_url() . "info/dokumen/" . $strFtr;
        QRcode::png($strFtr, $tmpFtr);

        // looping all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($templateId);
            if ($size['w'] > $size['h']) {
                $pdf->AddPage('L', array($size['w'], $size['h']));
            } else {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }
            if ($pageNo == $pageCount) {
                // $img = $_SERVER['DOCUMENT_ROOT'] . get_save_url() . D('user')->where('id = ' . get_user_id())->getField('pic_yin');
            }

            // line
            // $this->fpdf->SetFont('Arial','',7);
            // $this->fpdf->SetLineWidth(0.5);
            // $pdf->SetY(($size['h']-30));
            // $pdf->Write(5,'* dokumen ini di approve oleh sistem. ');
            // first add an imported page
            $pdf->useTemplate($templateId);

            //overlay a image on top of template
            /* $pdf->SetFont('Arial', '', 7);
              $pdf->Image($tmpFtr, 30, ($size['h'] - 30), 0, 20);
              $pdf->Text(52, ($size['h'] - 27), "erf : {$dtList['NO_PENGAJUAN']}");
              $pdf->Text(52, ($size['h'] - 24), "eat : {$dtList['NO_PENUGASAN']}");
              $pdf->Text(52, ($size['h'] - 18), "dibuat : {$dtList['NAME_CRT']}");
              $pdf->Text(52, ($size['h'] - 15), "diverifikasi : {$dtList['NAME_PARAF']}");
              $pdf->Text(52, ($size['h'] - 12), "disetujui : {$dtList['NAME_APP']}");
              $pdf->Text(32, ($size['h'] - 8), '* dokumen ini di approve oleh sistem. ');
              $pdf->SetFont('Helvetica');
              $pdf->SetXY(-5, -5); */

            if ($dtList['APPROVE3_AT']) {
                $headde = ", " . $dtList['APPROVE3_BY'];
            } else {
                $headde = '';
            }
            $pdf->SetFont('Arial', '', 7);
            $pdf->Image($tmpFtr, 30, ($size['h'] - 30), 0, 20);
            $pdf->Text(52, ($size['h'] - 27), "ERFSASA : {$dtList['NO_PENGAJUAN']}");
            $pdf->Text(52, ($size['h'] - 24), "EAT : {$dtList['NO_PENUGASAN']}");
            $pdf->Text(52, ($size['h'] - 21), "DOC ENG : {$dtList['NO_PENUGASAN']}");
            $pdf->Text(52, ($size['h'] - 18), "dibuat : {$dtList['NAME_CRT']}");
            $pdf->Text(52, ($size['h'] - 15), "diverifikasi : {$dtList['NAME_PARAF']}");
            $pdf->Text(52, ($size['h'] - 12), "disetujui : {$dtList['NAME_APP']} $headde");
            $pdf->Text(32, ($size['h'] - 8), '* dokumen ini di approve oleh sistem e-DEMS. ');
            $pdf->SetFont('Helvetica');
            $pdf->SetXY(-5, -5); //$pdf->SetXY(-5, -5);
        }
        $pdf->Output($tipe, $output);

        if ($tipe == 'F') {
            return $output;
        } else {
            return 'true';
        }
    }

    public function get_app() {
        $id = $this->input->post('id');
        $tipe = $this->input->post('tipe');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = '{$tipe}'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_paraf() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd_smgm() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->dok_eng->get_record("A.ID = '{$id}'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_prog_dok() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->dok_eng->getIDDOK("$id");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->dok_eng->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }
    
    public function set_log_download(){
        //JENIS_DOKUMEN, USER_ID, NAMA_USER, JAB_USER, ID_DOK, TIPE, TIME_AT
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        //print_r($info);
        $data = array(
            'JENIS_DOKUMEN' => $this->input->post('jd'),
            'USER_ID' => $info['id'],
            'NAMA_USER' => $info['name'],
            'JAB_USER' => $info['pos_text'],
            'UK_USER' => $info['unit_kerja'],
            'ID_DOK' => $this->input->post('idok'), 
            'TIPE' => $this->input->post('tp'),
        );
        $result = $this->dok_eng->set_log_download($data); 
        if ($result) {
            echo $this->response('success', 200, $result);
        } else { 
            echo $this->response('error', 400);
        }
    }
    
    public function getHistoriDownload(){
       $id = $_POST['idok'];
       
       $result = $this->dok_eng->get_histori_download($id);
      
        
        //print_r($json);
        //exit();
        
       echo $this->response('success', 200, $result);
    }

    public function unlink_preview_document(){
    	$doc = $this->input->post("document");
    	$src = "media/upload/tmp/".$doc;
    	unlink($src);
    }

    public function preview_document_upload(){
    	if (!class_exists("FPDF")) {
            require_once APPPATH . 'libraries/fpdf/fpdf.php';
        }
        if (!class_exists("FPDI")) {
            require_once APPPATH . 'libraries/fpdi/fpdi.php';
        }

        $config['upload_path']= './media/upload/tmp/';
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload($this->input->post("DOC"))){
            $dokumen = $this->upload->data();
        } else {
            $error = $this->upload->display_errors();
            echo json_encode(array("status" => "error", "message" => $error));
            exit();
        }

    	$no_eat = $this->input->post("NO_PENUGASAN");
    	$no_erf = $this->assig->get_no_erf($no_eat);
    	$crt_list = $this->input->post("CRT_LIST");
    	$crt = explode(":", $crt_list);
    	$crt_approve = "";
    	foreach ($crt as $crt_key => $crt_value) {
    		$nama = explode("-", $crt_value);
    		$crt_approve .= trim($nama[1]).", ";
    	}

    	$paraf_list = $this->input->post("PARAF_LIST");
    	$paraf = explode(":", $paraf_list);
    	$paraf_approve = "";
    	foreach ($paraf as $paraf_key => $paraf_value) {
    		$nama = explode("-", $paraf_value);
    		$paraf_approve .= trim($nama[1]).", ";
    	}

    	$app_list = $this->input->post("APP_LIST");
    	$app = explode(":", $app_list);
    	$app_approve = "";
    	foreach ($app as $app_key => $app_value) {
    		$nama = explode("-", $app_value);
    		$app_approve .= trim($nama[1]).", ";
    	}

    	$hde_list = $this->input->post("HDE_LIST");
    	$hde = explode(":", $hde_list);
    	$hde_approve = "";
    	foreach ($hde as $hde_key => $hde_value) {
    		$nama = explode("-", $hde_value);
    		$hde_approve .= trim($nama[1]).", ";
    	}

        $src = "media/upload/tmp/".$dokumen["file_name"];

        $pdf = new FPDI();
        try {
            $pageCount = $pdf->setSourceFile($src);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $templateId = $pdf->importPage($pageNo);
                $size = $pdf->getTemplateSize($templateId);
                if ($size['w'] > $size['h']) {
                    $pdf->AddPage('L', array($size['w'], $size['h']));
                } else {
                    $pdf->AddPage('P', array($size['w'], $size['h']));
                }
                $pdf->useTemplate($templateId);

                $pdf->SetFont('Arial', '', 7);
                $pdf->Image('media/upload/tmp/ftr232522cbd8a62a0a44986d1892d9b12e.png', 10, ($size['h'] - 29), 0, 20);
                $pdf->Text(30, ($size['h'] - 27), "ERF");
                $pdf->Text(48, ($size['h'] - 27), ": $no_erf->NO_ERF");

                $pdf->Text(30, ($size['h'] - 24), "EAT");
                $pdf->Text(48, ($size['h'] - 24), ": $no_eat");

                $pdf->Text(30, ($size['h'] - 21), "DOC ENG");
                $pdf->Text(48, ($size['h'] - 21), ": ");

                $pdf->Text(30, ($size['h'] - 18), "DIBUAT");
                $pdf->Text(48, ($size['h'] - 18), ": $crt_approve");

                $pdf->Text(30, ($size['h'] - 15), "DIVERIFIKASI");
                $pdf->Text(48, ($size['h'] - 15), ": $paraf_approve");

                $pdf->Text(30, ($size['h'] - 12), "DISETUJUI");
                $pdf->Text(48, ($size['h'] - 12), ": $app_approve");

                $pdf->Text(30, ($size['h'] - 9), "DISAHKAN");
                $pdf->Text(48, ($size['h'] - 9), ": $hde_approve");
                
                $pdf->Text(10, ($size['h'] - 5), '* dokumen ini di approve oleh sistem e-DEMS. ');
                $pdf->SetFont('Helvetica');
                $pdf->SetXY(-5, -5);
            }

            unlink($src);
            $pdf->Output($src, "F");
            echo json_encode(array("status" => "success","data" => base_url().$src, "file_name" => $dokumen["file_name"]));
        } catch(Exception $e){
            echo json_encode(array("status" => "error", "message" => "PDF diupload Tidak support untuk preview"));
        }
        
        
        
    }
}
