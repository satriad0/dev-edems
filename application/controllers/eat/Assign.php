<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Assign extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    // public $muk_kode='';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("eat/M_assign", "assig");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'eat';
        $where['GEN_TYPE'] = 'FORM_GROUP';
        $global = $this->general_model->get_data($where);
        unset($where);

        $header["tittle"] = $global->GEN_DESC;
        $header["short_tittle"] = "eat";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        $header['page_state'] = $state . ";special_access={$info['special_access']}";

        $yyyy = date("Y");
        $xxxx = $this->assig->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "///EA/{$kodemax}//{$yyyy}";
        // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
        $data['DEPT'] = 'GM of Design & Engineering';
        $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
        $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')"); // GEN_PAR2
        $data['foreigns'] = $this->assig->get_foreign();
        // echo "<pre>";
        // print_r($data['BIRO']);
        // echo $this->db->last_query();
        // exit;
        // get manager
        // $where['GEN_VAL']='1';
        // $where['GEN_CODE']='EAT_PENGKAJI_UK';
        // $where['GEN_TYPE']='General';
        // $global = $this->general_model->get_data($where);
        // set approval manager
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'DE_MGR';
        $this->db->like('GEN_PAR5', $info['uk_kode']);
        $tmpDeMgr = $this->general_model->get_data_result($where);
        unset($where);

        $str = "";

        // $str = "
        //     k.muk_kode = '{$info['uk_kode']}' AND k.mk_emp_subgroup='30'
        //   ";

        if ($tmpDeMgr) {
            $a = array();
            $data['MGR_APP'] = $tmpDeMgr;
            // $tmpDeMgr = $tmpDeMgr[0];
            $str .= "k.mk_email IN (";
            foreach ($tmpDeMgr as $deMgr) {
                if ($info['uk_kode'] == $deMgr->GEN_PAR5) {
                    array_push($a, $deMgr->GEN_PAR4);
                }
            }
            $str .= "'".implode("','", $a)."'";
            $str .= ")";
            
        }
        $data['MANAGER'] = $this->dbhris->get_hris_where($str);
        // print_r($data['MANAGER']);exit();
        // echo $this->db->last_query();
        // exit;
        unset($where);
        // $where['GEN_VAL']='1';
        // $where['GEN_CODE']='EAT_PENGKAJI_UK';
        // $where['GEN_TYPE']='General';
        // $global = $this->general_model->get_data($where);
        // $data['PENGKAJI'] = $this->assig->get_pengkaji($info['company'], $global->GEN_PAR5); // 1st
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'EAT_TIM';
        // $this->db->like('GEN_PAR1',$info['uk_kode']);
        // $this->db->where_in($info['uk_kode'],"GEN_PAR1");
        $data['PENGKAJI'] = $this->general_model->get_data_result($where);
//         echo $this->db->last_query();
//         exit;
        unset($where);

        // $pengkaji = $data['PENGKAJI'];
        // $muk_kode='';
        // foreach ($pengkaji as $key => $value) {
        //   if ($muk_kode) {
        //     $muk_kode .= ',';
        //   }
        //   $muk_kode .= $value['muk_kode'];
        // }
        // $data['LEAD_ASS'] = $this->assig->get_lead($info['company'],$muk_kode); // 2nd
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'EAT_ASSIST';
        $this->db->like('GEN_PAR2', $info['uk_kode']);
        $data['LEAD_ASS'] = $this->general_model->get_data_result($where);

        unset($where);
//         echo "<pre>";
//         print_r($data);
//         echo "</pre>";
//         exit;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        } else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }

        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('eat/assign_task', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('eat/assign_task', $data);
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        //$search = $this->input->post('search');
        $search = $this->input->post('columns');
        $order = $this->input->post('order');

        $key = array(
           // 'search' => $search['value'],
            'search' => $search[0]['search']['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
        }

        $data = $this->assig->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->assig->recFil($key),
            'recordsTotal' => $this->assig->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $result = $this->assig->get_foreign();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function lead_ass() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $key = "({$muk_kode})";
        $result = $this->assig->get_lead($info['company'], $key);

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function pengkaji() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
        $where['GEN_TYPE'] = 'General';
        $global = $this->general_model->get_data($where);

        $result = $this->assig->get_lead($info['company'], $global->GEN_PAR5);

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function manager() {
        $this->load->model('M_hris', 'dbhris');
        $nama = '';
        if (isset($_POST['search'])) {
            $nama = $_POST['search'];
        }

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_hris_where("k.mdept_kode = '{$info['dept_code']}' AND k.mk_emp_subgroup='30' AND k.mk_nama LIKE '%{$nama}%'");
        // echo $this->dbhris->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$atas->NOBADGE}'");
        $peg = (array) $peg[0];

        $list_data = $this->assig->get_data_unit($param['NO_PENUGASAN']);
        $list_data = $list_data[0];

        $param['short'] = 'EAT';
        $param['link'] = 'eat';
        $param['title'] = 'Engineering Assign Task';
        //$param['title'] = "Request Approval EAT - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}"; 
        $param['sender'] = "{$info['no_badge']} - {$info['name']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_PENUGASAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$param['SHORT_TEXT']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja Peminta :</b> {$list_data['DESCPSECTION']} - {$list_data['FL_TEXT']}";
        $param['TRN']['COMPANY'] = "<b>Company :</b> {$list_data['COMP_TEXT']}";
//        $param['TRN']['LOKASI'] = "<b>Lokasi :</b> {$param['LOKASI']}";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);
        
        //subject baru task [task 29]
        $subjectnew = "{$subject} - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}";

        $this->email($this->template->set_app($param), $subjectnew, '', $tomail, $cc);
    }
 
    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(1);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];
            if (empty($info['username'])) {
                redirect('login');
                exit;
            }

            $data = $this->assig->get_record($param['ID']);
            $data = $data[0];

            $lvl = 1;
            $pangkat = 'BIRO';
            if (isset($data['APPROVE1_AT']) && !isset($data['APPROVE2_AT'])) {
                $lvl = 2;
                $pangkat = 'DEPT';
            }
            // $atasan = (object);
            if (isset($data['APPROVE0_BY'])) {
                $lvl = 0;
                $atasan = $this->dbhris->get_hris_where("k.mk_nopeg = '{$data['APPROVE0_BADGE']}'");
                $atasan = $atasan[0];
            } else {
                $atasan = $this->dbhris->get_level_from($info['no_badge'], $pangkat);
            }
            // $atasan = $atasan[0];
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }

            if ($data) {

                $n_form = $this->my_crypt("{$data['NO_PENUGASAN']};{$lvl};{$data['NOTIFIKASI']};{$data['OBJECTIVE']};{$info['no_badge']};{$atasan->NAMA};{$data['ID']};{$info['uk_kode']}", 'e');
                $n_form = str_replace('/', '6s4', $n_form);
                $n_form = str_replace('+', '1p4', $n_form);
                $n_form = str_replace('=', 'sM9', $n_form);
                $NOTIF_NO = $data['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                $to = $atasan->EMAIL;
                $cc = array();
                $subject = "Resend : Request Approval EAT";
                $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {
        if (isset($_POST) && isset($_POST['ID_PENGAJUAN']) && isset($_POST['OBJECTIVE'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            if (empty($info['username'])) {
                redirect('login');
                exit;
            }

            // $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            // $atasan2 = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
            // $atasan = $atasan[0];
            // if (!$atasan){
            //   $atasan->NOBADGE='null';
            //   $atasan->NAMA='null';
            //   $atasan->EMAIL='null';
            //   $atasan->JAB_TEXT='null';
            // }
            // if (!$atasan2){
            //   $atasan2->NOBADGE='null';
            //   $atasan2->NAMA='null';
            //   $atasan2->EMAIL='null';
            //   $atasan2->JAB_TEXT='null';
            // }
            // $tmpname=explode("@",$atasan->NAMA);
            // $username=$tmpname[0];
            // $tmpname2=explode("@",$atasan2->NAMA);
            // $username2=$tmpname2[0];
            // $list_pengkaji = $param['TIM_PENGKAJI'];
            // $list_pengkaji = json_decode($list_pengkaji, TRUE);
            // $param['TIM_PENGKAJI'] = serialize($list_pengkaji);
            $lvl = $param['LVL'];
            unset($param['LVL']);
            $ar_app = new stdClass();
            if ($lvl == '1') {
                $ar_app = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE1_BADGE']}'");
                $ar_app = $ar_app[0];
                unset($param['APPROVE1_BADGE']);
            } else {
                $ar_app = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE0_BADGE']}'");
                $ar_app = $ar_app[0];
            }
            $param['NO_PENUGASAN'] = $this->generateNo('NO_PENUGASAN', 'MPE_PENUGASAN', 'EA', $param['NO_PENUGASAN']);
            // $param['APPROVE1_BY'] = $username;
            // $param['APPROVE1_JAB'] = $atasan->JAB_TEXT;
            // $param['APPROVE2_BY'] = $username2;
            // $param['APPROVE2_JAB'] = $atasan2->JAB_TEXT;
            $param['STATUS'] = 'For Approval';
            $param['CREATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            unset($param['NO_ERF']);
            $short_text = $param['SHORT_TEXT'];
            unset($param['SHORT_TEXT']);
            $lokasi = $param['LOKASI'];
            unset($param['LOKASI']);
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENUGASAN'], 'eat', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH2'])) {
                $url_files = $this->upload($_FILES['STRPATH2'], (object) $info, $param['NO_PENUGASAN'], 'eat', $param['NOTIFIKASI']);
                $param['FILES2'] = $url_files;
                $this->db->set('FILES2_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH3'])) {
                $url_files = $this->upload($_FILES['STRPATH3'], (object) $info, $param['NO_PENUGASAN'], 'eat', $param['NOTIFIKASI']);
                $param['FILES3'] = $url_files;
                $this->db->set('FILES3_AT', "CURRENT_DATE", false);
            }

            // $list_pekerjaan = $param['LIST_PEKERJAAN'];
            // $list_pekerjaan = json_decode($list_pekerjaan, TRUE);
            // $param['LIST_PEKERJAAN'] = serialize($list_pekerjaan);

            $result = $this->assig->save($param);
            // echo $this->db->last_query();

            if ($result['ID']) {
                $list_pekerjaan = json_decode($param['LIST_PEKERJAAN'], TRUE);
                $ar_pekerjaan = array();
                foreach ($list_pekerjaan as $key => $value) {
                    $tmp['ID_PENUGASAN'] = $result['ID'];
                    $tmp['DESKRIPSI'] = $value;
                    // $tmp['START_DATE'] = $param['START_DATE'];
                    $tmp['PROGRESS'] = '0';
                    $tmp['CREATE_BY'] = $info['name'];
                    $tmp['COMPANY'] = $info['company'];
                    $tmp['DEPT_CODE'] = $info['dept_code'];
                    $tmp['DEPT_TEXT'] = $info['dept_text'];
                    $tmp['UK_CODE'] = $info['uk_kode'];
                    $tmp['UK_TEXT'] = $info['unit_kerja'];
                    array_push($ar_pekerjaan, $tmp);
                }

                // delete detail
                $dparam['id'] = $result['ID'];
                $dparam['DELETE_BY'] = $info['name'];
                $this->db->where("DELETE_AT IS NULL");
                $dresult = $this->assig->deleteDtl($dparam);

                // insert detail
                $dtlresult = $this->assig->insert_det($ar_pekerjaan);

                // send email
                $param['SHORT_TEXT'] = $short_text;
                $param['LOKASI'] = $lokasi;

                $n_form = $this->my_crypt("{$param['NO_PENUGASAN']};{$lvl};{$param['NOTIFIKASI']};{$param['OBJECTIVE']};{$info['no_badge']};{$ar_app->NAMA};{$result['ID']};{$info['uk_kode']}", 'e');

                $to = $ar_app->EMAIL;
                $cc = array();
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                $subject = "Request Approval EAT";
                $this->send_mail($subject, $param, $ar_app, $n_form, $to, $cc);


                $res_message = 'Data has been saved' . '-';
                if (!$result['ID']) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            // echo "test";
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['ID_PENGAJUAN']) && isset($_POST['OBJECTIVE'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            if (empty($info['username'])) {
                redirect('login');
                exit;
            }

            // $manager = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE0_BADGE']}'");
            // $manager = $manager[0];
            // $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            // $atasan2 = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
            // // $atasan = $atasan[0];
            // if (!$atasan){
            //   $atasan->NOBADGE='null';
            //   $atasan->NAMA='null';
            //   $atasan->EMAIL='null';
            //   $atasan->JAB_TEXT='null';
            // }
            // if (!$atasan2){
            //   $atasan2->NOBADGE='null';
            //   $atasan2->NAMA='null';
            //   $atasan2->EMAIL='null';
            //   $atasan2->JAB_TEXT='null';
            // }
            // $tmpname=explode("@",$atasan->NAMA);
            // $username=$tmpname[0];
            // $tmpname2=explode("@",$atasan2->NAMA);
            // $username2=$tmpname2[0];
            // $param['APPROVE1_BY'] = $username;
            // $param['APPROVE1_JAB'] = $atasan->JAB_TEXT;
            // $param['APPROVE2_BY'] = $username2;
            // $param['APPROVE2_JAB'] = $atasan2->JAB_TEXT;
            $lvl = $param['LVL'];
            unset($param['LVL']);
            $ar_app = new stdClass();
            if ($lvl == '1') {
                $ar_app = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE1_BADGE']}'");
                $ar_app = $ar_app[0];
                unset($param['APPROVE1_BADGE']);
            } else {
                $ar_app = $this->dbhris->get_hris_where("k.mk_nopeg = '{$param['APPROVE0_BADGE']}'");
                $ar_app = $ar_app[0];
            }
            $param['ID'] = $_POST['ID'];
            $param['UPDATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            unset($param['NO_ERF']);
            $short_text = $param['SHORT_TEXT'];
            unset($param['SHORT_TEXT']);
            $lokasi = $param['LOKASI'];
            unset($param['LOKASI']);
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENUGASAN'], 'eat', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH2'])) {
                $url_files = $this->upload($_FILES['STRPATH2'], (object) $info, $param['NO_PENUGASAN'], 'eat', $param['NOTIFIKASI']);
                $param['FILES2'] = $url_files;
                $this->db->set('FILES2_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH3'])) {
                $url_files = $this->upload($_FILES['STRPATH3'], (object) $info, $param['NO_PENUGASAN'], 'eat', $param['NOTIFIKASI']);
                $param['FILES3'] = $url_files;
                $this->db->set('FILES3_AT', "CURRENT_DATE", false);
            }

            $result = $this->assig->updateData('update', $param);


            if ($result) {
                $list_pekerjaan = json_decode($param['LIST_PEKERJAAN'], TRUE);
                $ar_pekerjaan = array();
                foreach ($list_pekerjaan as $key => $value) {
                    $tmp['ID_PENUGASAN'] = $param['ID'];
                    $tmp['DESKRIPSI'] = $value;
                    // $time = strtotime($param['START_DATE']);
                    // $tmp['START_DATE'] = date('Y-m-d',$time);
                    $tmp['PROGRESS'] = '0';
                    $tmp['CREATE_BY'] = $info['name'];
                    $tmp['COMPANY'] = $info['company'];
                    $tmp['DEPT_CODE'] = $info['dept_code'];
                    $tmp['DEPT_TEXT'] = $info['dept_text'];
                    $tmp['UK_CODE'] = $info['uk_kode'];
                    $tmp['UK_TEXT'] = $info['unit_kerja'];
                    array_push($ar_pekerjaan, $tmp);
                }
                // delete detail
                $dparam['id'] = $param['ID'];
                $dparam['DELETE_BY'] = $info['name'];
                $dresult = $this->assig->deleteDtl($dparam);

                // insert detail
                $dtlresult = $this->assig->insert_det($ar_pekerjaan);

                // send email
                $param['SHORT_TEXT'] = $short_text;
                $param['LOKASI'] = $lokasi;

                $n_form = $this->my_crypt("{$param['NO_PENUGASAN']};$lvl;{$param['NOTIFIKASI']};{$param['OBJECTIVE']};{$info['no_badge']};{$ar_app->NAMA};{$param['ID']};{$info['uk_kode']}", 'e');

                // $to=array('adhiq.rachmadhuha@sisi.id');
                $to = $ar_app->EMAIL;
                $cc = array();
                // $cc=array('purnawan.yose@gmail.com');
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                $subject = "EAT APPROVAL REQUEST";
                $this->send_mail($subject, $param, $ar_app, $n_form, $to, $cc);


                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        // echo $e;
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];

            $reason = $_POST['reason'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');

            list($no_form, $lvl, $notifikasi, $objective, $no_badge, $nama, $id, $uk_kode) = split(";", $no_form);
            $data['no_form'] = $no_form;
            if ($lvl == 0) {
                $data['STATUS'] = 'MGR Approved';
                $this->db->set('NOTE0', $reason);
            } else if ($lvl == 1) {
                $data['STATUS'] = 'SM Approved';
                $this->db->set('NOTE', $reason);
            } else {
                $data['STATUS'] = 'GM Approved';
                $this->db->set('NOTE2', $reason);
            }
            // $data['APPROVE'.$lvl.'_BY']=$nama;
            $this->db->set('APPROVE' . $lvl . '_AT', "CURRENT_DATE", false);
            $result = $this->assig->updateData('approve', $data);
            // echo $this->db->last_query();
            // cek status unit kerja
            $where['GEN_VAL'] = '1';
            $where['GEN_TYPE'] = 'APP_LIMIT';
            $where['GEN_CODE'] = 'EAT_DE';
            $limit = $this->general_model->get_data_result($where);
            $limit = $limit[0];
            unset($where);
            $lApp = 0;
            if ($limit->GEN_PAR1 != $uk_kode) {
                // code...
                $lApp = (int) $limit->GEN_PAR3;
            }

            // set progress status
            // if ($lvl>1) {
            if ($lvl == $lApp) {
                $ar_det['ID_PENUGASAN'] = $id;
                $ar_det['PROGRESS'] = '25';
                $dtlresult = $this->assig->update_det($ar_det);
            }

            if ($result) {
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);

                // sendmail
                // if ($lvl<2) {
                // echo "<pre>";
                // print_r($limit);
                // echo "{$lApp} : {$lvl} <br />";
                // echo "{$limit['GEN_PAR1']} : {$uk_kode} <br />";
                // echo "{$limit->GEN_PAR1} : {$uk_kode} <br />";
                if ($lvl < $lApp) {
                    $slvl = 'BIRO';
                    if ($lvl == 1) {
                        $slvl = 'DEPT';
                    }
                    $atasan = $this->dbhris->get_level_from($no_badge, $slvl);
                    $dtEAT = $this->assig->get_record($id);
                    $dtEAT = $dtEAT[0];
                    // $atasan = $atasan[0];
                    if (!$atasan) {
                        $atasan->NOBADGE = 'null';
                        $atasan->NAMA = 'null';
                        $atasan->EMAIL = 'null';
                    }
                    $to = $atasan->EMAIL;
                    $subject = "EAT APPROVAL REQUEST";
                    $e = $this->my_crypt("{$no_form};" . ($lvl + 1) . ";$notifikasi;$objective;$no_badge;{$atasan->NAMA};$id", 'e');
                    // $param['NO_PENUGASAN'] = $no_form;
                    // $param['NOTIFIKASI'] = $notifikasi;
                    // $param['OBJECTIVE'] = $objective;
                    $this->send_mail($subject, $dtEAT, $atasan, $e, $to, $cc);
                }
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function reject() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        // echo $e;
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];

            $reason = $_POST['reason'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');

            list($no_form, $notifikasi, $nama) = split(";", $this->my_crypt($e, 'd'));
            $data['no_form'] = $no_form;
            $data['STATUS'] = 'Rejected';
            // $data['APPROVE'.$lvl.'_BY']=$nama;
            $this->db->set('REJECT_REASON', $reason);
            $this->db->set('REJECT_BY', $nama);
            $result = $this->assig->updateData('approve', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                //$param = array(
                //    'STATUS' => 'REL',
                //    'NOTIF_NO' => $notifikasi
                //);
                //$del_sap = $this->sap_del->deletenotif($param);
                // echo $this->response('success', 200, $result);
                // echo "ERF '{$no_form}' was approved succesfully";
                // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['id'] = $id;
            $data['DELETE_BY'] = $info['name'];
            $result = $this->assig->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id) {
        if ($id) {
            error_reporting(1);

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // $list_data = $_POST;
            $list_data = $this->assig->get_record($id);
            $list_data = $list_data[0];
            // cek status unit kerja
            $where['GEN_VAL'] = '1';
            $where['GEN_TYPE'] = 'APP_LIMIT';
            $where['GEN_CODE'] = 'EAT_DE';
            $limit = $this->general_model->get_data_result($where);
            $limit = $limit[0];
            unset($where);

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 7);
            $this->fpdf->SetLeftMargin(20);
            $this->fpdf->SetRightMargin(20);

            $this->fpdf->Cell(12, 4, '', 0, 0);
            $this->fpdf->Cell(50, 8, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 15, 'PNG'), 0, 1, 'C');
            $this->fpdf->Cell(170, 1, 'R/538/012', 0, 1, 'R');
            $this->fpdf->Cell(0, 9, '', 0, 1);
            $this->fpdf->SetFont('Arial', 'B', 6);
            $this->fpdf->Cell(64, 1, 'PT. SEMEN INDONESIA (PERSERO)Tbk.', 0, 0);

            // HEADER
            $this->fpdf->Ln(2);
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->Cell(170, 7, "ENGINEERING ASSIGN TASK", 1, 1, 'C');
            // $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 5, "{$list_data['NO_PENUGASAN']}", 1, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tanggal", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['CREATE_AT']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  ERF No.", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['NO_PENGAJUAN']}", 1, 1);

            $rw = 1;
            if ((strlen($list_data['SHORT_TEXT']) / 65) > 1) {
                $rw = 2;
            }
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, (5 * $rw), "  Project Name", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(120, 5, "{$list_data['SHORT_TEXT']}", 1);
            // $this->fpdf->Cell(120, 5, "{$list_data['SHORT_TEXT']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tipe ", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['TIPE']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tanggal Mulai", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['START_DATE_FORMAT']}", 1, 1);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tanggal Selesai", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['END_DATE_FORMAT']}", 1, 1);


            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Customer / UK", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['DESCPSECTION']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, " ", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['FL_TEXT']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Company", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['COMP_TEXT']}", 1, 1);

            // $this->fpdf->SetFont('Arial','B',10);
            // $this->fpdf->Cell(50,5,"  Bussiness Area",1,0);
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Cell(120,5,"{$list_data['LOKASI']}",1,1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Lead of Tech. Asst.", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['LEAD_ASS']}", 1, 1);

            // $this->fpdf->SetFont('Arial','B',10);
            // $this->fpdf->Cell(50,5,"  Notification",1,0);
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Cell(120,5,"{$list_data['NOTIFIKASI']}",1,1);
            // CONTENT
            $this->fpdf->Ln(1);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "OBJECTIVE", 1, 1);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['OBJECTIVE']}", 1);

            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "CUSTOMER REQUIREMENT", 1, 1);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['CUS_REQ']}", 1);

            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "ENGINEERING SCOPE", 1, 1);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['SCOPE_ENG']}", 1);

            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 5, "TIM PELAKSANA", 1, 1);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Komite Pengarah :", 'L', 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['KOMITE_PENGARAH']}", 'R', 1);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(170, 5, "  Tim Pengkaji :", 'LR', 1);

            $sPengarah = str_replace('[', '      - ', $list_data['TIM_PENGKAJI']);
            $sPengarah = str_replace(']', '', $sPengarah);
            $sPengarah = str_replace(',"', ',      - ', $sPengarah);
            $sPengarah = str_replace('"', '', $sPengarah);
            $sPengarah = str_replace(',', "\n", $sPengarah);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$sPengarah}", 'LR');

            // $this->fpdf->SetFont('Arial', 'B', 10);
            // $this->fpdf->Cell(170, 5, "  Tugas & Wewenang Komite Pengarah :", 'LR', 1);
            // $this->fpdf->SetFont('Arial', '', 10);
            // $this->fpdf->MultiCell(170, 5, "{$list_data['TW_KOM_PENGARAH']}", 'LR');
            // $this->fpdf->SetFont('Arial', 'B', 10);
            // $this->fpdf->Cell(170, 5, "  Tugas & Wewenang Tim Pengkaji :", 'LR', 1);
            // $this->fpdf->SetFont('Arial', '', 10);
            // $this->fpdf->Cell(5,5," ",'L',0);
            // $this->fpdf->MultiCell(165,5,"{$list_data['TW_TIM_PENGKAJI']}",'R');
            // $this->fpdf->MultiCell(170, 5, "{$list_data['TW_TIM_PENGKAJI']}", 'LR');
            $this->fpdf->Cell(170, 5, "", 'LBR', 1);

            if ($this->fpdf->getY() > 270) {
                // code...
                $this->fpdf->AddPage();
            }
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "APPROVAL NOTE", 1, 1);

            // FOOTER
            // include(APPPATH.'libraries/phpqrcode/qrlib.php');
            $tmpApp = FCPATH . "media/upload/tmp/" . md5("{$list_data['NOTIFIKASI']}-{$list_data['SHORT_TEXT']}") . ".png";
            if ($limit->GEN_PAR1 == $list_data['UK_CODE']) {
                $this->fpdf->SetFont('Arial', '', 10);
                $this->fpdf->MultiCell(170, 5, "{$list_data['NOTE0']}", 1);
                // $this->fpdf->SetFont('Arial', '', 10);
                // $this->fpdf->MultiCell(170, 5, "{$list_data['NOTE0']}", 1);
                if ($list_data['APPROVE0_AT']) {
                    include(APPPATH . 'libraries/phpqrcode/qrlib.php');
                    $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE0_BY']};{$list_data['APPROVE0_AT']};", 'e');
                    $str = str_replace('/', '6s4', $str);
                    $str = str_replace('+', '1p4', $str);
                    $str = str_replace('=', 'sM9', $str);
                    $str = base_url() . "info/eat/" . $str;
                    QRcode::png($str, $tmpApp);
                    if (!file_exists($tmpApp)) {
                        echo 'We cannot generated a QR code, please back and try again later!';
                        echo '<hr />';
                        exit;
                    }
                }

                $this->fpdf->Ln(3);
                if ($this->fpdf->getY() > 225) {
                    // code...
                    $this->fpdf->AddPage();
                }
                $this->fpdf->Cell(170, 3, "", 'LTR', 1);
                $this->fpdf->SetFont('Arial', 'U', 10);
                $this->fpdf->Cell(170, 7, "Ketua Tim Pengarah", 'LR', 1);
                if ($list_data['APPROVE0_AT']) {
                    $this->fpdf->Cell(170, 25, $this->fpdf->Image($tmpApp, $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 25, 'PNG'), 'LR', 1);
                } else if ($list_data['REJECT_DATE']) {
                    $this->fpdf->Cell(170, 25, 'Rejected', 'LR', 1, 'C');
                } else {
                    $this->fpdf->Cell(170, 25, "", 'LR', 1);
                }
                $this->fpdf->SetFont('Arial', 'BU', 10);
                $this->fpdf->Cell(170, 5, "{$list_data['APPROVE0_BY']}", 'LR', 1);
                // $this->fpdf->MultiCell(170,5,"{$list_data['APPROVE0_BY']}",1);
                $this->fpdf->SetFont('Arial', '', 10);
                $this->fpdf->Cell(170, 7, "{$list_data['APPROVE0_JAB']} PT Semen Indonesia", 'LBR', 1);
            } else {
                $this->fpdf->SetFont('Arial', '', 10);
                $this->fpdf->MultiCell(170, 5, "{$list_data['NOTE']}", 1);
                if ($list_data['APPROVE1_AT']) {
                    include(APPPATH . 'libraries/phpqrcode/qrlib.php');
                    $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE1_BY']};{$list_data['APPROVE1_AT']};", 'e');
                    $str = str_replace('/', '6s4', $str);
                    $str = str_replace('+', '1p4', $str);
                    $str = str_replace('=', 'sM9', $str);
                    $str = base_url() . "info/eat/" . $str;
                    QRcode::png($str, $tmpApp);
                    if (!file_exists($tmpApp)) {
                        echo 'We cannot generated a QR code, please back and try again later!';
                        echo '<hr />';
                        exit;
                    }
                }

                $this->fpdf->Ln(3);
                if ($this->fpdf->getY() > 225) {
                    // code...
                    $this->fpdf->AddPage();
                }
                $this->fpdf->Cell(170, 3, "", 'LTR', 1);
                $this->fpdf->SetFont('Arial', 'U', 10);
                $this->fpdf->Cell(170, 7, "Ketua Komite Pengarah", 'LR', 1);
                if ($list_data['APPROVE1_AT']) {
                    $this->fpdf->Cell(170, 25, $this->fpdf->Image($tmpApp, $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 25, 'PNG'), 'LR', 1);
                } else if ($list_data['REJECT_DATE']) {
                    $this->fpdf->Cell(170, 25, 'Rejected', 'LR', 1, 'C');
                } else {
                    $this->fpdf->Cell(170, 25, "", 'LR', 1);
                }
                $this->fpdf->SetFont('Arial', 'BU', 10);
                $this->fpdf->Cell(170, 5, "{$list_data['APPROVE1_BY']}", 'LR', 1);
                // $this->fpdf->MultiCell(170,5,"{$list_data['APPROVE1_BY']}",1);
                $this->fpdf->SetFont('Arial', '', 10);
                $this->fpdf->Cell(170, 7, "{$list_data['APPROVE1_JAB']} PT Semen Indonesia", 'LBR', 1);
            }
            $this->fpdf->SetFont('Arial', '', 8);
            $this->fpdf->Cell(170, 4, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 1);
            // if ($list_data['FILES']) {
            //     $this->fpdf->Cell(170, 4, "** data pendukung kami sertakan terlampir dengan dokumen ini.", 0, 1);
            // }
            // generating
            // FILENAME
            $tmpPath = "{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}.pdf";
            $this->fpdf->Output($tmpPath, 'D');


            // temporary download
            // $tmpPath = FCPATH . "media/upload/tmp/" . md5("{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}") . ".pdf";
            // $this->fpdf->Output($tmpPath, 'F');
            //
            // $arfile = array($tmpPath);
            // // Lampiran
            // if ($list_data['FILES']) {
            //     array_push($arfile, FCPATH . $list_data['FILES']);
            // }
            //
            // $this->load->library('PDFMerger');
            // $pdf = new PDFMerger;
            //
            // foreach ($arfile as $key => $value) {
            //     $pdf->addPDF($value, 'all');
            // }
            // // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page
            //
            // $pdf->merge('download', "{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

    function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->assig->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

    public function get_approver_lead_tech(){
        $tech = $this->input->post("tech");
        $data = $this->assig->get_approver_lead_tech($tech);
        echo json_encode(array("data" => $data));
    }
    
    public function buat_revisi_pekerjaan(){
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (empty($info['username'])) {
            redirect('login');
            exit;
        }
        
        $id_eat = $this->input->post("id_eat");
        $pp = $this->input->post("pp");
        $tmp = array();
        
        $tmp['ID_PENUGASAN'] = $id_eat;
        $tmp['DESKRIPSI'] = $pp;
        $tmp['PROGRESS'] = '0';
        $tmp['CREATE_BY'] = $info['name'];
        $tmp['COMPANY'] = $info['company'];
        $tmp['DEPT_CODE'] = $info['dept_code'];
        $tmp['DEPT_TEXT'] = $info['dept_text'];
        $tmp['UK_CODE'] = $info['uk_kode'];
        $tmp['UK_TEXT'] = $info['unit_kerja'];
        
     
        $result =  $this->assig->insert_paket_pekerjaan_revisi($tmp);
        if ($result) {
           echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function edit_paket_pekerjaan(){
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (empty($info['username'])) {
            redirect('login');
            exit;
        }

        $id_eat = $this->input->post("id_eat");
        $num = $this->input->post("num");
        $pp = $this->input->post("pp");
        $tmp = array();
        
        $tmp['ID_PENUGASAN'] = $id_eat;
        $tmp['DESKRIPSI'] = $pp;
        $tmp['UPDATE_BY'] = $info['name'];
     
        $result = $this->assig->edit_paket_pekerjaan($tmp, $num);
        if ($result) {
           echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function delete_paket_pekerjaan(){
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (empty($info['username'])) {
            redirect('login');
            exit;
        }

        $id_eat = $this->input->post("id_eat");
        $num = $this->input->post("num");
        $pp = $this->input->post("pp");
        $tmp = array();
        
        $tmp['ID_PENUGASAN'] = $id_eat;
        $tmp['ID_DTL_PENUGASAN'] = $num;
        $tmp['DESKRIPSI'] = $pp;
        $tmp['UPDATE_BY'] = $info['name'];
        $result = $this->assig->delete_paket_pekerjaan($tmp, $num);

        // print_r($tmp);
        if ($result) {
           echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

}
