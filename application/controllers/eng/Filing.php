<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filing extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    protected $uk_parent = '';

    public function __construct() {
        parent::__construct();
        $this->load->model('M_hris', 'dbhris');
        $this->load->model("eng/M_filing", "dok_eng");
        $this->load->model("others/M_approval", "app");
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $header["tittle"] = "Archives";
        $header["short_tittle"] = "fil";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
            // get count pending status
            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }

        // set auto increment trans no
        // $where = "A.ID=(SELECT MAX(ID) FROM MPE_DOK_ENG) AND NO_DOK_ENG LIKE '%DE-%'";
        // $trans = $this->dok_eng->get_record($where);;
        // echo "<pre>";
        // print_r($trans);
        // echo "</pre>";
        // exit;
        // set list approval hris
        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
        $where['GEN_TYPE'] = 'General';
        $global = $this->general_model->get_data($where);
        $str = "
            (uk.muk_parent LIKE '{$global->GEN_PAR5}'
          	OR k.muk_kode = '{$global->GEN_PAR5}') AND k.mk_emp_subgroup='30'
          ";
        $str2 = "
            (uk.muk_parent LIKE '{$global->GEN_PAR5}'
          	OR k.muk_kode = '{$global->GEN_PAR5}') AND k.mk_emp_subgroup='20'
          ";
        $uk_parent = $global->GEN_PAR5;

        $yyyy = date("Y");
        $xxxx = $this->dok_eng->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "{$yyyy}///TR/{$kodemax}";
        $data['LIST_MNG'] = $this->dbhris->get_hris_where($str);
        $data['LIST_APP'] = $this->dbhris->get_hris_where($str2);
        $data['foreigns'] = $this->dok_eng->get_foreign();
        // echo "<pre>";
        // print_r($data['LIST_APP']);
        // echo "</pre>";
        // exit;
        // $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
        // $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
        // $data['FOREIGN'] = $this->dok_eng->get_foreign();
        $header['page_state'] = $state;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('eng/filing', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('eng/filing', $data);
        $this->load->view('general/footer');
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('search');
        $order = $this->input->post('order');

        $key = array(
            'search' => $search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
        }

        $data = $this->dok_eng->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->dok_eng->recFil($key),
            'recordsTotal' => $this->dok_eng->recTot($key)
        );

        echo json_encode($return);
    }

    public function foreign() {
        $result = $this->dok_eng->get_foreign();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function foreign_dtl($f_id) {
        if (isset($f_id)) {
            $result = $this->dok_eng->get_dtl_foreign($f_id);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function atasan($level) {
        $this->load->model('M_hris', 'dbhris');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $result = $this->dbhris->get_level_from($info['no_badge'], $level);
        // $this->db->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        list($badge, $nama) = split(' - ', $atas);
        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$badge}'");
        $peg = (array) $peg[0];
        //print_r($badge);

        $e = str_replace('/', '6s4', $e);
        $e = str_replace('+', '1p4', $e);
        $e = str_replace('=', 'sM9', $e);
        $param['short'] = 'DE';
        $param['link'] = 'dok_eng';
        $param['title'] = 'Document Engineering';
        $param['sender'] = "{$param['CREATE_BY']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_DOK_ENG']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Paket :</b> {$param['PACKET_TEXT']}";
        // $param['TRN']['LOKASI']="<b>Progress :</b> {$param['PROGRESS']}%";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);

        $this->email($this->template->set_app($param), $subject, '', $tomail, $cc);


        // $message="Dengan Hormat $atas,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi    : " . $param['NOTIFIKASI'] . "<br/>
        // No Dokumen       : " . $param['NO_DOK_ENG'] . "<br/>
        // Nama Paket       : " . $param['PACKET_TEXT'] . "<br/> <br/> <br/>
        //
    //
    // Click link berikut untuk lebih lanjut <a href='" . base_url('action/dok/  '. $e) . "'> APPROVAL DOCUMENTS </a> <br/><br/><br/>
        //
    // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        if (isset($_POST['ID'])) {
            error_reporting(1);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data = $this->dok_eng->get_record("A.ID='{$param['ID']}'");
            $data = $data[0];
            // echo $this->db->last_query();
            // exit;
            // print_r($data);

            if ($data) {

                // $to=array($atasan->EMAIL);
                //$to=array('adhiq.rachmadhuha@sisi.id');
                $to = array('purnawan.yose@gmail.com');
                $cc = array('purnawan.yose@gmail.com', 'adhiq.rachmadhuha@sisi.id');
                // $cc=array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                // $dtPar = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'Paraf'");
                // if (count($dtPar)) {
                //   foreach ($dtPar as $key) {
                //     if ($key['STATUS']=='In Approval') {
                //       $subject="Resend : PACKET APPROVAL";
                //       $n_form=$this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};TTD;{$key['APP_BADGE']} - {$key['APP_NAME']}",'e');
                //       $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                //     }
                //   }
                // }
                // $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);

                if ($data['STATUS'] == "Mgr Pengkaji Approved") {
                    $dtApp = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'TTD'");
                    //print_r($dtApp);
                    if (count($dtApp)) {
                        foreach ($dtApp as $key) {
                            if ($key['STATE'] == 'New') {
                                $subject = "Resend : DOCUMENT ENGINERING APPROVAL";
                                $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};TTD;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                                $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                                // break;
                            }
                        }
                    }
                } elseif ($data['STATUS'] == "SM Pengkaji Approved") {
                    $subject = "Resend : DOCUMENT ENGINERING APPROVAL";
                    $mgr = $this->dok_eng->getMgr("00001048");
                    $mgr = $mgr[0];

                    $n_form = $this->my_crypt("{$data['ID']};3;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};MGR;{$mgr['GEN_CODE']} - {$mgr['GEN_DESC']}", 'e');
                    // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                    //$subject="Manager Document APPROVAL";
                    $this->send_mail($subject, $data, "{$mgr['GEN_CODE']} - {$mgr['GEN_DESC']}", $n_form, $to, $cc);
                    // break;
                } elseif ($data['STATUS'] == "Mgr Approved") {
                    $subject = "Resend : DOCUMENT ENGINERING APPROVAL";
                    $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$data['CREATE_BY']}' OR k.mk_email LIKE '{$data['CREATE_BY']}@%')");
                    $info = $info[0];
                    $atasan = $this->dbhris->get_level_from($info->NOBADGE, 'BIRO');

                    $n_form = $this->my_crypt("{$data['ID']};2;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};SM;{$atasan->NOBADGE} - {$atasan->NAMA}", 'e');
                    //$subject="BIRO Document APPROVAL";
                    $this->send_mail($subject, $data, "{$atasan->NOBADGE} - {$atasan->NAMA}", $n_form, $to, $cc);
                    // break;
                } elseif ($data['STATUS'] == "SM Approved") {
                    $subject = "Resend : DOCUMENT ENGINERING APPROVAL";
                    $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$data['CREATE_BY']}' OR k.mk_email LIKE '{$data['CREATE_BY']}@%')");
                    $info = $info[0];
                    $atasan2 = $this->dbhris->get_level_from($info->NOBADGE, 'DEPT');

                    $n_form = $this->my_crypt("{$data['ID']};3;{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};GM;{$atasan2->NOBADGE} - {$atasan2->NAMA}", 'e');
                    //$subject="BIRO Document APPROVAL";
                    $this->send_mail($subject, $data, "{$atasan2->NOBADGE} - {$atasan2->NAMA}", $n_form, $to, $cc);
                    // break;
                } else {
                    $dtPar = $this->app->get_data("REF_ID = '{$param['ID']}' AND TIPE = 'Paraf'");
                    if (count($dtPar)) {
                        foreach ($dtPar as $key) {
                            if ($key['STATE'] == 'New') {
                                $subject = "Resend : PACKET APPROVAL";
                                $n_form = $this->my_crypt("{$key['REF_ID']};{$key['LVL']};{$data['NOTIFIKASI']};{$data['PACKET_TEXT']};Paraf;{$key['APP_BADGE']} - {$key['APP_NAME']}", 'e');
                                $this->send_mail($subject, $data, "{$key['APP_BADGE']} - {$key['APP_NAME']}", $n_form, $to, $cc);
                            }
                        }
                    }
                }
                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {
        if (isset($_POST) && isset($_POST['NO_DOK_ENG']) && isset($_POST['ID_PENUGASAN']) && isset($_POST['PACKET_TEXT']) && isset($_POST['PROGRESS'])) {
            $param = $_POST;
            $LIST_APP = $_POST['LIST_APP'];
            unset($param['LIST_APP']);
            $LIST_PARAF = $_POST['LIST_PARAF'];
            unset($param['LIST_PARAF']);
            // echo "<pre>";
            // print_r($param);
            // echo "</pre>";
            // exit;


            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $param['CREATE_BY'] = $info['name'];
            $param['COMPANY'] = $info['company'];
            // RKS
            if (isset($_FILES['RKSPATH'])) {
                $url_files = $this->upload($_FILES['RKSPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_RKS");
                $param['RKS_FILE'] = $url_files;
                $this->db->set('RKS_AT', "CURRENT_DATE", false);
            }
            // BQ
            if (isset($_FILES['BQPATH'])) {
                $url_files = $this->upload($_FILES['BQPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_BQ");
                $param['BQ_FILE'] = $url_files;
                $this->db->set('BQ_AT', "CURRENT_DATE", false);
            }
            // Drawing
            if (isset($_FILES['DRAWPATH'])) {
                $url_files = $this->upload($_FILES['DRAWPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_Drawing");
                $param['DRAW_FILE'] = $url_files;
                $this->db->set('DRAW_AT', "CURRENT_DATE", false);
            }
            // ECE
            if (isset($_FILES['ECEPATH'])) {
                $url_files = $this->upload($_FILES['ECEPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_ECE");
                $param['ECE_FILE'] = $url_files;
                $this->db->set('ECE_AT', "CURRENT_DATE", false);
            }
            // Kajian
            if (isset($_FILES['KAJIANPATH'])) {
                $url_files = $this->upload($_FILES['KAJIANPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_KAJIAN");
                $param['KAJIAN_FILE'] = $url_files;
                $this->db->set('KAJIAN_AT', "CURRENT_DATE", false);
            }

            if ($LIST_APP || $LIST_PARAF) {
                $param['STATUS'] = 'In Approval';
            }

            $data['DEPT'] = $this->dbhris->get_level_from($info['no_badge'], 'DEPT');
            $data['BIRO'] = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            if (!$data['DEPT']) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
                $atasan->JAB_TEXT = 'null';
            }
            if (!$data['BIRO']) {
                $atasan2->NOBADGE = 'null';
                $atasan2->NAMA = 'null';
                $atasan2->EMAIL = 'null';
                $atasan2->JAB_TEXT = 'null';
            }
            $tmpname = explode("@", $data['DEPT']->NAMA);
            $username = $tmpname[0];
            $tmpname2 = explode("@", $data['BIRO']->NAMA);
            $username2 = $tmpname2[0];

            $mgr = $this->dok_eng->getMgr("00001048");
            $mgr = $mgr[0];

            $param['APPROVE1_BY'] = $mgr['GEN_DESC'];
            $param['APPROVE1_JAB'] = $mgr['GEN_PAR2'];
            $param['APPROVE2_BY'] = $username2;
            $param['APPROVE2_JAB'] = $data['BIRO']->JAB_TEXT;
            $param['APPROVE3_BY'] = $username;
            $param['APPROVE3_JAB'] = $data['DEPT']->JAB_TEXT;

            $result = $this->dok_eng->save($param);
            // echo $this->db->last_query();

            if ($result['REF_ID']) {
                if ($LIST_APP) {
                    $tmparr = explode(':', $LIST_APP);
                    for ($i = 0; $i < count($tmparr); $i++) {
                        $adata = explode(' - ', $tmparr[$i]);
                        $tmpdata['REF_ID'] = $result['REF_ID'];
                        $tmpdata['REF_TABEL'] = 'DOK_ENG';
                        $tmpdata['TIPE'] = 'TTD';
                        $tmpdata['LVL'] = $i + 1;
                        $tmpdata['STATE'] = 'New';
                        $tmpdata['APP_BADGE'] = $adata[0];
                        $tmpdata['APP_NAME'] = $adata[1];
                        $tmpdata['APP_JAB'] = $adata[3];
                        $tmpdata['APP_EMAIL'] = $adata[2];
                        $tmpdata['NOTE'] = '';
                        $tmpdata['COMPANY'] = $info['company'];
                        $tmpdata['CREATE_BY'] = $info['name'];

                        $this->app->save($tmpdata);
                        unset($adata);
                        unset($tmpdata);
                    }
                }
                if ($LIST_PARAF) {
                    $tmparr = explode(':', $LIST_PARAF);
                    for ($i = 0; $i < count($tmparr); $i++) {
                        $adata = explode(' - ', $tmparr[$i]);
                        $tmpdata['REF_ID'] = $result['REF_ID'];
                        $tmpdata['REF_TABEL'] = 'DOK_ENG';
                        $tmpdata['TIPE'] = 'Paraf';
                        $tmpdata['LVL'] = $i + 1;
                        $tmpdata['STATE'] = 'New';
                        $tmpdata['APP_BADGE'] = $adata[0];
                        $tmpdata['APP_NAME'] = $adata[1];
                        $tmpdata['APP_JAB'] = $adata[3];
                        $tmpdata['APP_EMAIL'] = $adata[2];
                        $tmpdata['STATUS'] = 'In Approval';
                        $tmpdata['NOTE'] = '';
                        $tmpdata['COMPANY'] = $info['company'];
                        $tmpdata['CREATE_BY'] = $info['name'];

                        $this->app->save($tmpdata);

                        // send email
                        $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};Paraf;{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');
                        // $n_form = str_replace('/', '6s4', $n_form);
                        // $n_form =  str_replace('+', '1p4', $n_form);
                        // $n_form =  str_replace('=', 'sM9', $n_form);

                        $to = array('adhiq.rachmadhuha@sisi.id');
                        $cc = array('purnawan.yose@gmail.com');
                        // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                        $cc = array();
                        $subject = "PAKET APPROVAL";
                        $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);

                        unset($adata);
                        unset($tmpdata);
                    }
                }

                $res_message = 'Data has been saved' . '-';
                if (!$result['REF_ID']) {
                    // if($result != 1){
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function update() {
        if (isset($_POST) && isset($_POST['ID']) && isset($_POST['NO_DOK_ENG']) && isset($_POST['ID_PENUGASAN']) && isset($_POST['PACKET_TEXT']) && isset($_POST['PROGRESS'])) {
            $param = $_POST;
            $LIST_APP = $_POST['LIST_APP'];
            unset($param['LIST_APP']);
            $LIST_PARAF = $_POST['LIST_PARAF'];
            unset($param['LIST_PARAF']);

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $param['UPDATE_BY'] = $info['username'];
            $param['COMPANY'] = $info['company'];
            // RKS
            if (isset($_FILES['RKSPATH'])) {
                $url_files = $this->upload($_FILES['RKSPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_RKS');
                $param['RKS_FILE'] = $url_files;
                $this->db->set('RKS_AT', "CURRENT_DATE", false);
            }
            // BQ
            if (isset($_FILES['BQPATH'])) {
                $url_files = $this->upload($_FILES['BQPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_BQ');
                $param['BQ_FILE'] = $url_files;
                $this->db->set('BQ_AT', "CURRENT_DATE", false);
            }
            // Drawing
            if (isset($_FILES['DRAWPATH'])) {
                $url_files = $this->upload($_FILES['DRAWPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_Drawing');
                $param['DRAW_FILE'] = $url_files;
                $this->db->set('DRAW_AT', "CURRENT_DATE", false);
            }
            // ECE
            if (isset($_FILES['ECEPATH'])) {
                $url_files = $this->upload($_FILES['ECEPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . '_ECE');
                $param['ECE_FILE'] = $url_files;
                $this->db->set('ECE_AT', "CURRENT_DATE", false);
            }
            // Kajian
            if (isset($_FILES['KAJIANPATH'])) {
                $url_files = $this->upload($_FILES['KAJIANPATH'], (object) $info, $param['NO_DOK_ENG'], 'dok_eng', $param['NOTIFIKASI'] . "_KAJIAN");
                $param['KAJIAN_FILE'] = $url_files;
                $this->db->set('KAJIAN_AT', "CURRENT_DATE", false);
            }

            if ($LIST_APP) {
                $tmparr = explode(':', $LIST_APP);
                for ($i = 0; $i < count($tmparr); $i++) {
                    $adata = explode(' - ', $tmparr[$i]);
                    $tmpdata['REF_ID'] = $_POST['ID'];
                    $tmpdata['REF_TABEL'] = 'DOK_ENG';
                    $tmpdata['TIPE'] = 'TTD';
                    $tmpdata['LVL'] = $i + 1;
                    $tmpdata['STATE'] = 'New';
                    $tmpdata['APP_BADGE'] = $adata[0];
                    $tmpdata['APP_NAME'] = $adata[1];
                    $tmpdata['APP_JAB'] = $adata[3];
                    $tmpdata['APP_EMAIL'] = $adata[2];
                    if ($i == 0) {
                        $tmpdata['STATUS'] = 'In Approval';

                        // send email
                        $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};TTD;{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');
                        // $n_form = str_replace('/', '6s4', $n_form);
                        // $n_form =  str_replace('+', '1p4', $n_form);
                        // $n_form =  str_replace('=', 'sM9', $n_form);

                        $to = array('adhiq.rachmadhuha@sisi.id');
                        // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                        $cc = array();
                        $subject = "DOCUMENT ENGINERING APPROVAL";
                        $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);
                    }
                    $tmpdata['NOTE'] = '';
                    $tmpdata['COMPANY'] = $info['company'];
                    $tmpdata['CREATE_BY'] = $info['name'];

                    $this->app->save($tmpdata);
                    unset($adata);
                    unset($tmpdata);
                }
                // set trans status
                $param['STATUS'] = 'In Approval';
            }
            if ($LIST_PARAF) {
                $tmparr = explode(':', $LIST_PARAF);
                for ($i = 0; $i < count($tmparr); $i++) {
                    $adata = explode(' - ', $tmparr[$i]);
                    $tmpdata['REF_ID'] = $_POST['ID'];
                    $tmpdata['REF_TABEL'] = 'DOK_ENG';
                    $tmpdata['TIPE'] = 'Paraf';
                    $tmpdata['LVL'] = $i + 1;
                    $tmpdata['STATE'] = 'New';
                    $tmpdata['APP_BADGE'] = $adata[0];
                    $tmpdata['APP_NAME'] = $adata[1];
                    $tmpdata['APP_JAB'] = $adata[3];
                    $tmpdata['APP_EMAIL'] = $adata[2];
                    $tmpdata['STATUS'] = 'In Approval';
                    $tmpdata['NOTE'] = '';
                    $tmpdata['COMPANY'] = $info['company'];
                    $tmpdata['CREATE_BY'] = $info['name'];

                    $this->app->save($tmpdata);

                    // send email
                    $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};Paraf;{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');
                    // $n_form = str_replace('/', '6s4', $n_form);
                    // $n_form =  str_replace('+', '1p4', $n_form);
                    // $n_form =  str_replace('=', 'sM9', $n_form);

                    $to = array('adhiq.rachmadhuha@sisi.id');
                    // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                    $cc = array();
                    $subject = "PAKET APPROVAL";
                    $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);

                    unset($adata);
                    unset($tmpdata);
                }
            }

            $result = $this->dok_eng->updateData('update', $param);
            // echo $this->db->last_query();

            if ($result) {

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function approve() {
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            // $e =  str_replace(' ', '+', $e);
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
            $pgw = explode(' - ', $nama);

            $list_data = $this->dok_eng->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];

            $param['NOTIFIKASI'] = $notifikasi;
            $param['PACKET_TEXT'] = $paket;
            $param['NO_DOK_ENG'] = $list_data['NO_DOK_ENG'];
            $param['PROGRESS'] = $list_data['PROGRESS'];
            $param['CREATE_BY'] = $list_data['CREATE_BY'];

            // echo "<pre>";
            // print_r($list_data['NO_DOK_ENG']);
            // echo "</pre>";
            //$data = $this->dok_eng->get_record("A.ID = ".$akey[0]);

            $reason = $_POST['reason'];

            // update approval
            unset($data); // clear array


            if ($tipe == 'Paraf' || $tipe == 'TTD') {
                $data['ID'] = $id;
                $data['TIPE'] = $tipe;
                $data['APP_NAME'] = $pgw[1];
                $data['STATE'] = 'Approved';
                $data['STATUS'] = 'Approved';
                $data['UPDATE_BY'] = $pgw[1];
                $data['NOTE'] = $reason;
                $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
                $result = $this->app->updateData('approve', $data);

                $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = '{$tipe}' AND STATE='New'");
                if (!$dtApp) {
                    if ($tipe == 'Paraf') {

                        $data2['ID'] = $id;
                        $data2['STATUS'] = 'Mgr Pengkaji Approved';
                        $result = $this->dok_eng->updateData('approve', $data2);

                        $LIST_TTD = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD' AND STATE='New'");
                        // echo "<pre>";
                        // print_r($LIST_TTD);
                        // echo "</pre>";
                        foreach ($LIST_TTD as $value) {
                            $tmpdata['ID'] = $value['ID'];
                            $tmpdata['REF_ID'] = $value['REF_ID'];
                            $tmpdata['REF_TABEL'] = 'DOK_ENG';
                            $tmpdata['TIPE'] = $value['TIPE'];
                            $tmpdata['LVL'] = $value['LVL'];
                            $tmpdata['STATE'] = 'New';
                            $tmpdata['APP_BADGE'] = $value['APP_BADGE'];
                            $tmpdata['APP_NAME'] = $value['APP_NAME'];
                            $tmpdata['APP_JAB'] = $value['APP_JAB'];
                            $tmpdata['APP_EMAIL'] = $value['APP_EMAIL'];
                            $tmpdata['STATUS'] = 'In Approval';
                            $tmpdata['NOTE'] = '';
                            $tmpdata['COMPANY'] = $value['COMPANY'];
                            $tmpdata['CREATE_BY'] = $value['CREATE_BY'];

                            $this->db->set('STATUS', "In Approval");
                            $this->db->where('APP_NAME', $value['APP_NAME']);
                            $this->db->where('TIPE', $value['TIPE']);
                            $update = $this->app->updateData('approve', $tmpdata);

                            $n_form = $this->my_crypt("{$tmpdata['REF_ID']};{$tmpdata['LVL']};{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};TTD;{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", 'e');
                            // $n_form = str_replace('/', '6s4', $n_form);
                            // $n_form =  str_replace('+', '1p4', $n_form);
                            // $n_form =  str_replace('=', 'sM9', $n_form);

                            $to = array('adhiq.rachmadhuha@sisi.id');
                            $cc = array('purnawan.yose@gmail.com');
                            // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                            $cc = array();
                            $subject = "PAKET APPROVAL";
                            $this->send_mail($subject, $param, "{$tmpdata['APP_BADGE']} - {$tmpdata['APP_NAME']}", $n_form, $to, $cc);

                            unset($adata);
                            unset($tmpdata);
                        }
                    } elseif ($tipe == 'TTD') {

                        $data2['ID'] = $id;
                        $data2['STATUS'] = 'SM Pengkaji Approved';
                        $result = $this->dok_eng->updateData('approve', $data2);

                        $info = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' OR k.mk_email LIKE '{$list_data['CREATE_BY']}@%'");
                        $info = $info[0];

                        $atasan = $this->dbhris->get_level_from($info->NOBADGE, 'BIRO');

                        $mgr = $this->dok_eng->getMgr("00001048");
                        $mgr = $mgr[0];

                        //print_r($mgr['GEN_DESC']);

                        $n_form = $this->my_crypt("{$id};3;{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};MGR;{$mgr['GEN_CODE']} - {$mgr['GEN_DESC']}", 'e');
                        $to = array('adhiq.rachmadhuha@sisi.id');
                        $cc = array('purnawan.yose@gmail.com');
                        // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                        $cc = array();
                        $subject = "Manager Document APPROVAL";
                        $this->send_mail($subject, $param, "{$mgr['GEN_CODE']} - {$mgr['GEN_DESC']}", $n_form, $to, $cc);

                        // if($result){
                        //    $result2['message'] = "Documents Paket '{$paket}' was approved succesfully".$pgw[1];
                        //    $result2['status'] = '200';
                        //    echo json_encode($result2);
                        // }else{
                        //   echo $this->response('error', 400);
                        // }
                    }
                }
            } else {

                $data['ID'] = $id;
                if ($lvl == '1') {
                    $slvl = 'Mgr';
                } elseif ($lvl == '2') {
                    $slvl = 'SM';
                } else {
                    $slvl = 'GM';
                }
                $data['STATUS'] = $slvl . ' Approved';
                //$data['APPROVE'.$lvl.'_BY']=$pgw[1];
                $data['NOTE' . $lvl] = $reason;
                $this->db->set('APPROVE' . $lvl . '_AT', "CURRENT_DATE", false);
                $result = $this->dok_eng->updateData('approve', $data);

                if ($tipe == 'MGR') {
                    $info = $this->dbhris->get_hris_where("(k.mk_nama = '{$list_data['CREATE_BY']}' OR k.mk_email LIKE '{$list_data['CREATE_BY']}@%')");
                    $info = $info[0];
                    $atasan = $this->dbhris->get_level_from($info->NOBADGE, 'BIRO');

                    $n_form = $this->my_crypt("{$id};2;{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};SM;{$atasan->NOBADGE} - {$atasan->NAMA}", 'e');
                    $to = array('adhiq.rachmadhuha@sisi.id');
                    $cc = array('purnawan.yose@gmail.com');
                    // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                    $cc = array();
                    $subject = "BIRO Document APPROVAL";
                    $this->send_mail($subject, $param, "{$atasan->NOBADGE} - {$atasan->NAMA}", $n_form, $to, $cc);
                    // if($result){
                    //    $result2['message'] = "Documents Paket '{$paket}' was approved succesfully".$pgw[1];
                    //    $result2['status'] = '200';
                    //    echo json_encode($result2);
                    // }else{
                    //   $result2['message'] = "Documents Paket '{$paket}' was not approved".$pgw[1];
                    //   $result2['status'] = '400';
                    //   echo json_encode($result2);
                    // }
                } elseif ($tipe == 'SM') {
                    $info = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' OR k.mk_email LIKE '{$list_data['CREATE_BY']}@%'");
                    $info = $info[0];
                    $atasan2 = $this->dbhris->get_level_from($info->NOBADGE, 'DEPT');

                    $n_form = $this->my_crypt("{$id};3;{$param['NOTIFIKASI']};{$param['PACKET_TEXT']};GM;{$atasan2->NOBADGE} - {$atasan2->NAMA}", 'e');
                    $to = array('adhiq.rachmadhuha@sisi.id');
                    $cc = array('purnawan.yose@gmail.com');
                    // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                    $cc = array();
                    $subject = "Department Document APPROVAL";
                    $this->send_mail($subject, $param, "{$atasan2->NOBADGE} - {$atasan2->NAMA}", $n_form, $to, $cc);
                    //
                    // print_r($param);
                    //
          // if($result){
                    //    $result2['message'] = "Documents Paket '{$paket}' was approved succesfully".$pgw[1];
                    //    $result2['status'] = '200';
                    //    echo json_encode($result2);
                    // }else{
                    //   $result2['message'] = "Documents Paket '{$paket}' was not approved".$pgw[1];
                    //   $result2['status'] = '400';
                    //   echo json_encode($result2);
                    // }
                } else {
                    
                }
            }
            if ($result) {
                $result2['message'] = "Documents Paket '{$paket}' was approved succesfully" . $pgw[1];
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function reject() {
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            $nama = $_POST['nama'];
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            list($id, $lvl, $notifikasi, $paket, $tipe, $nama) = split(";", $no_form);
            $pgw = explode(' - ', $nama);
            $data['ID'] = $id;
            // $data['APPROVE_BY']=$nama;
            $data['STATUS'] = 'Rejected';
            $this->db->set('REJECT_REASON', $reason);
            $this->db->set('REJECT_BY', $pgw[0]);
            $result = $this->dok_eng->updateData('reject', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                //$param = array(
                //    'STATUS' => 'REL',
                //    'NOTIF_NO' => $notifikasi
                //);
                //$del_sap = $this->sap_del->deletenotif($param);
                // echo $this->response('success', 200, $result);
                // echo "ERF '{$no_form}' was approved succesfully";
                // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['ID'] = $id;
            $data['DELETE_BY'] = $info['name'];
            $result = $this->dok_eng->updateData('delete', $data);

            if ($result) {
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function export_pdf($id, $tipe = 'All') {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if ($id && isset($info)) {
            error_reporting(1);

            // $list_data = $_POST;
            $list_data = $this->dok_eng->get_record("A.ID='{$id}'");
            $list_data = $list_data[0];
            $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
            $dtPar = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
            $dtCrt = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'CRT'");
            // set list approval hris
            $where['GEN_VAL'] = '1';
            $where['GEN_CODE'] = 'EAT_PENGKAJI_UK';
            $where['GEN_TYPE'] = 'General';
            $global = $this->general_model->get_data($where);
            $dtCre = $this->dbhris->get_hris_where("k.mk_nama = '{$list_data['CREATE_BY']}' AND k.company = '{$list_data['COMPANY']}' AND (uk.muk_parent LIKE '" . $global->GEN_PAR5 . "' OR k.muk_kode = '" . $global->GEN_PAR5 . "')");
            if ($dtCre) {
                $dtCre = $dtCre[0];
            }

            //get limit ece
            $awhere['GEN_TYPE'] = 'General';
            $awhere['GEN_CODE'] = 'ECE_LIMIT';
            $awhere['GEN_VAL'] = '1';
            $limit_ece = (array) $this->general_model->get_data($awhere);
            unset($awhere);
            // echo "<pre>";
            // print_r($dtCre);
            // print_r($global);
            // echo "</pre>";
            // exit;

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 13);
            $this->fpdf->SetLeftMargin(20);

            // $this->fpdf->Cell(12,4,'',0,0);
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX() + 7, $this->fpdf->GetY(), 0, 15, 'PNG'), 'LT', 0, 'C');
            $this->fpdf->Cell(100, 6, "PT. SEMEN INDONESIA (PERSERO)Tbk.", 'T', 0, 'C');
            $this->fpdf->Cell(35, 6, $this->fpdf->Image($uri . 'media/logo/si_group.jpg', $this->fpdf->GetX() + 3, $this->fpdf->GetY() + 4, 0, 10, 'JPG'), 'TR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(170, 6, "{$dtCre->DEPT_TEXT}", 'LR', 1, 'C');
            $this->fpdf->Cell(170, 6, "{$dtCre->CC_TEXT}", 'LBR', 1, 'C');

            $this->fpdf->Cell(170, 6, "", 'LBR', 1, 'C');
            if ($list_data['COLOR'] == 'Merah') {
                $this->fpdf->SetFillColor(204, 255, 204);
            } elseif ($list_data['COLOR'] == 'Kuning') {
                $this->fpdf->SetFillColor(230, 230, 0);
            } elseif ($list_data['COLOR'] == 'Biru') {
                $this->fpdf->SetFillColor(200, 220, 255);
            } elseif ($list_data['COLOR'] == 'Hijau') {
                $this->fpdf->SetFillColor(66, 244, 143);
            } elseif ($list_data['COLOR'] == 'Orange') {
                $this->fpdf->SetFillColor(245, 173, 66);
            } else {
                $this->fpdf->SetFillColor(239, 239, 245);
            }
            $this->fpdf->SetFont('Arial', '', 12);
            $this->fpdf->Cell(50, 6, " No. Dokumen", 'LT', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'T', 0, 'C', 1);
            $trn_no = $list_data['NO_DOK_ENG'];
            if ($tipe != 'TR') {
                if ($tipe != 'DRW') {
                    $trn_no = str_replace("TR", substr($tipe, 0, 2), $trn_no);
                } else {
                    $trn_no = str_replace("TR", 'DW', $trn_no);
                }
            }
            $this->fpdf->Cell(110, 6, "{$trn_no}", 'TR', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Notifikasi", 'L', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 0, 0, 'C', 1);
            $this->fpdf->Cell(110, 6, "{$list_data['NOTIFIKASI']}", 'R', 1, 'L', 1);
            $this->fpdf->Cell(50, 6, " Tahun", 'BL', 0, 'L', 1);
            $this->fpdf->Cell(10, 6, ":", 'B', 0, 'C', 1);
            $time = strtotime($list_data['CREATE_AT']);
            $year = date('Y', $time);
            $this->fpdf->Cell(110, 6, "{$year}", 'BR', 1, 'L', 1);
            $this->fpdf->Cell(170, 6, "", 'LBR', 1);

            $this->fpdf->SetFont('Arial', 'BU', 18);
            $this->fpdf->Cell(12, 4, '', 0, 2);
            $this->fpdf->Ln(5);
            $this->fpdf->Cell(170, 6, "DOKUMEN ENGINEERING", 0, 1, 'C');
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'B', 18);
            $sJudul = '';
            if (($tipe == 'TR')) {
                $this->fpdf->Ln(40);
                $sJudul = "Draft \n Term of Refference (TOR)";
            } else if (($tipe == 'BQ')) {
                $sJudul = 'Bill of Quantity (BQ)';
            } else if (($tipe == 'DRW')) {
                $sJudul = 'Drawing';
            } else if (($tipe == 'EC')) {
                $sJudul = 'Engineering Cost Estimate (ECE)';
            } else if (($tipe == 'KJ')) {
                $sJudul = 'KAJIAN';
            } else {
                $sJudul = 'TOR, BQ dan Drawing';
            }
            $this->fpdf->Ln(10);
            $this->fpdf->MultiCell(170, 8, strtoupper("{$sJudul}"), 0, 'C');
            $this->fpdf->Ln(10);
            $this->fpdf->MultiCell(170, 8, "{$list_data['PACKET_TEXT']}", 0, 'C');

            // jika tidak tor pake ttd
            if ($tipe != 'TR') {
                $this->fpdf->SetFont('Arial', 'B', 8);
                if (strlen($list_data['PACKET_TEXT']) < 45) {
                    $this->fpdf->Ln(15);
                } elseif (strlen($list_data['PACKET_TEXT']) < 90) {
                    $this->fpdf->Ln(5);
                }
                $this->fpdf->Cell(12, 4, '', 0, 2);
                $this->fpdf->Cell(170, 5, "Pengesahan", 1, 1, 'L');

                include(APPPATH . 'libraries/phpqrcode/qrlib.php');
                // approval info
                $this->fpdf->SetFont('Arial', 'B', 8);
                $width = (170 / (1));
                if ((float) $list_data['NOMINAL'] > (float) $limit_ece['GEN_PAR1'] || $tipe == 'KJ') {
                    $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
                    // jabatan
                    $this->fpdf->Ln(5);
                    $this->fpdf->SetFont('Arial', 'I', 8);
                    $y1 = $this->fpdf->GetY();
                    $x1 = $this->fpdf->GetX();
                    $lnmax = 15;
                    $h = 8;
                    if (strlen($list_data['APPROVE3_JAB']) > $lnmax)
                        $h = 4;
                    else
                        $h = 8;
                    $this->fpdf->MultiCell($width, $h, "{$list_data['APPROVE3_JAB']}", 'LBR', 'C');

                    // QR code
                    $this->fpdf->Ln(0);
                    if ($list_data['APPROVE3_AT']) {
                        $tmpCre = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-3-GM") . ".png";
                        $str = $this->my_crypt("{$list_data['ID']};{$list_data['APPROVE3_BY']};{$list_data['APPROVE3_AT']};", 'e');
                        $str = str_replace('/', '6s4', $str);
                        $str = str_replace('+', '1p4', $str);
                        $str = str_replace('=', 'sM9', $str);
                        $str = base_url() . "info/dok_eng/" . $str;
                        QRcode::png($str, $tmpCre);
                        $this->fpdf->Cell($width, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + ($width / 2) - 12.5, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LTR', 0, 'C');
                    } else {
                        $this->fpdf->Cell($width, 25, "", "LTR", 0, 'C');
                    }
                    // name
                    $this->fpdf->Ln(25);
                    $this->fpdf->SetFont('Arial', 'B', 7.8);
                    $this->fpdf->Cell($width, 5, "({$list_data['APPROVE3_BY']})", 'LBR', 1, 'C');
                }

                // SM Pengkaji Approval
                $width = (170 / (count($dtApp)));
                // $this->fpdf->Ln(5);
                foreach ($dtApp as $key) {
                    $this->fpdf->Cell($width, 5, "Disetujui Oleh,", 'LTR', 0, 'C');
                }
                // jabatan
                $this->fpdf->Ln(5);
                $this->fpdf->SetFont('Arial', 'I', 8);
                $y1 = $this->fpdf->GetY();
                $x1 = $this->fpdf->GetX();
                $lnmax = 26;
                if (count($dtApp) > 4) {
                    $lnmax = 20;
                } elseif (count($dtApp) <= 4) {
                    $lnmax = 17;
                }
                $y2 = $this->fpdf->GetY();
                $pos_x = 20;
                $inum = 0;
                foreach ($dtApp as $key) {
                    if (strlen($key['APP_JAB']) > $lnmax) {
                        $h = 4;
                    } else {
                        $i++;
                        $h = 8;
                        if ($i == (count($dtapp) - 2)) {
                            $h = 4;
                        }
                    }
                    $this->fpdf->SetXY($pos_x, $y1);
                    $this->fpdf->MultiCell($width, $h, "{$key['APP_JAB']}", 'LR', 'C');
                    $pos_x += $width;
                }
                // QR code
                $hQRapp = 20;
                foreach ($dtApp as $key) {
                    if ($key['STATE'] == 'Approved') {
                        $tmpApp = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-TTD") . ".png";
                        $strApp = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                        $strApp = str_replace('/', '6s4', $strApp);
                        $strApp = str_replace('+', '1p4', $strApp);
                        $strApp = str_replace('=', 'sM9', $strApp);
                        $strApp = base_url() . "info/dok_eng/" . $strApp;
                        QRcode::png($strApp, $tmpApp);
                        $this->fpdf->Cell($width, $hQRapp, $this->fpdf->Image($tmpApp, $this->fpdf->GetX() + ($width / 2) - ($hQRapp / 2), $this->fpdf->GetY(), 0, $hQRapp, 'PNG'), 'LTR', 0, 'C');
                    } else {
                        $this->fpdf->Cell($width, $hQRapp, "", "LTR", 0, 'C');
                    }
                }
                // name
                $this->fpdf->SetFont('Arial', 'BI', 7);
                $this->fpdf->Ln($hQRapp);
                foreach ($dtApp as $key) {
                    $this->fpdf->Cell($width, 5, "({$key['APP_NAME']})", 'LBR', 0, 'C');
                }

                if ((float) $list_data['NOMINAL'] <= (float) $limit_ece['GEN_PAR1'] && $tipe != 'KJ') {
                    if (strlen($list_data['PACKET_TEXT']) < 45) {
                        $this->fpdf->Ln(35);
                    } elseif (strlen($list_data['PACKET_TEXT']) < 90) {
                        $this->fpdf->Ln(20);
                    } else {
                        $this->fpdf->Ln(5);
                    }
                }

                //paraf and created by
                $this->fpdf->Ln(10);
                // $width = floor(170/(count($dtPar)));
                $this->fpdf->Cell(85, 8, "Dibuat : ", 0, 0, 'C');
                $this->fpdf->Cell(85, 8, "Diverifikasi : ", 0, 1, 'C');
                $width = 20;
                $this->fpdf->SetFont('Arial', '', 8);

                $qrcount = count($dtCrt);
                $qrwidth = 20;
                $widthcell = 0;
                $minwidth = 0;
                $minwidth2 = 0;
                if($qrcount > 5){
                	$qrwidth = 15;
                	$minwidth = 10;
                	$minwidth2= 5;
                } else if($qrcount > 4){
                	$widthcell = 3;
                	$qrwidth = 17;
                }

                if (count($dtCrt) > 0) {
                    $this->fpdf->Cell(((85 + $widthcell - ($width * count($dtCrt))) / 2) + $minwidth, 10, '', 0, 0, 'R');
                    foreach ($dtCrt as $key) {
                        if ($key['STATE'] == 'Approved') {
                            $tmpPar = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-CRT") . ".png";
                            $strPar = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                            $strPar = str_replace('/', '6s4', $strPar);
                            $strPar = str_replace('+', '1p4', $strPar);
                            $strPar = str_replace('=', 'sM9', $strPar);
                            $strPar = base_url() . "info/dok_eng/" . $strPar;
                            QRcode::png($strPar, $tmpPar);
                            $this->fpdf->Cell($width - $widthcell - $minwidth2, $qrwidth, $this->fpdf->Image($tmpPar, $this->fpdf->GetX(), $this->fpdf->GetY(), 0, $qrwidth, 'PNG'), 1, 0, 'C');
                        } else {
                            $this->fpdf->Cell($width, 20, "", 1, 0, 'C');
                        }
                    }
                    $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2), 10, '', 0, 0, 'R');
                }
                $this->fpdf->Cell(((85 - ($width * count($dtPar))) / 2), 10, '', 0, 0, 'R');

                // SIZE QR CODE
                $qrcount = count($dtPar);
                $qrheight = 20;
                $widthcell = 0;
                $groupcell_par = 20 * $qrcount;
                $minwidth2 = 0;
                $mingroupcellpar = 1;
                $minwidth = 0;
                if($qrcount > 5){
                	$mingroupcellpar = 0;
                	$qrheight = 15;
                	$groupcell_par = 140;
                	$minwidth2 = 5;
                	$minwidth = 29;
                } else if($qrcount > 4){
                	$mingroupcellpar = 0;
                	$qrheight = 17;
                	$widthcell = 3;
                	$groupcell_par = 140;
                }

                $this->fpdf->Cell((($groupcell_par - ($width * count($dtPar))) / 2) + $minwidth - $mingroupcellpar, 10, '', 0, 0, 'R');
                foreach ($dtPar as $key) {
                    if ($key['STATE'] == 'Approved') {
                        $tmpPar = FCPATH . "media/upload/tmp/" . md5("DOK_ENG-{$list_data['ID']}-{$list_data['NOTIFIKASI']}-APPROVE{$key['LVL']}-Paraf") . ".png";
                        $strPar = $this->my_crypt("{$list_data['ID']};{$key['APP_NAME']};{$key['UPDATE_AT']};", 'e');
                        $strPar = str_replace('/', '6s4', $strPar);
                        $strPar = str_replace('+', '1p4', $strPar);
                        $strPar = str_replace('=', 'sM9', $strPar);
                        $strPar = base_url() . "info/dok_eng/" . $strPar;
                        QRcode::png($strPar, $tmpPar);
                        $this->fpdf->Cell($width - $widthcell-$minwidth2, $qrheight, $this->fpdf->Image($tmpPar, $this->fpdf->GetX(), $this->fpdf->GetY(), 0, $qrheight, 'PNG'), 1, 0, 'C');
                    } else {
                        $this->fpdf->Cell($width, 20, "", 1, 0, 'C');
                    }
                }

                // paraf name
                // $this->fpdf->Ln(5);
                $heightcellcrt = 15;
                $widthnamecrt = 0;
                $fontsizecrt = 5.5;
                $maxname = 0;
                $minwidth3 = 0;
                if(count($dtCrt) > 5){
                	$fontsizecrt = 4;
                	$heightcellcrt = 4;
                	$maxname = 10;
                	$minwidth3 = 5;
                } else if(count($dtCrt) > 4){
                	$heightcellcrt = 10;
                	$widthnamecrt = 2.5;
                	$fontsizecrt = 5;
                }
                $this->fpdf->Cell(15, 15, "", 0, 1, 'L');
                $this->fpdf->SetFont('Arial', '', $fontsizecrt);
                if (count($dtCrt) > 0) {
                    $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2) + $maxname, 10, '', 0, 0, 'R');
                    foreach ($dtCrt as $key) {
                        if ($key['STATE'] == 'Approved') {
                            $tmpname = explode(" ", $key['APP_NAME']);
                            $this->fpdf->Cell($width - $widthnamecrt - $minwidth3, $heightcellcrt, $tmpname[0] . " " . $tmpname[1], 0, 0, 'C');
                        } else {
                            $this->fpdf->Cell($width, $heightcellcrt, "", 0, 0, 'C');
                        }
                    }
                    $this->fpdf->Cell(((85 - ($width * count($dtCrt))) / 2), 10, '', 0, 0, 'R');
                } else {
                    $this->fpdf->Cell(85, "", 0, 0);
                }

                $heightcellpar = 15;
                $widthnamepar = 0;
                $widthcellpar = 0;
                $maxname = 0;
                $minwidth3 = 0;
                if(count($dtPar) > 5){
                	$fontsizecrt = 4;
                	$heightcellpar = 4;
                	$maxname = 39;
                	$minwidth3 = 5;
                } else if(count($dtPar) > 4){
                	$maxname = 19;
                	$heightcellpar = 10;
                	$widthnamepar = 2.5;
                	$widthcellpar = 18;
                }

                $this->fpdf->Cell(((85 - ($width * count($dtPar))) / 2) + $maxname, 10, '', 0, 0, 'R');
                foreach ($dtPar as $key) {
                    if ($key['STATE'] == 'Approved') {
                        $tmpname = explode(" ", $key['APP_NAME']);
                        $this->fpdf->Cell($width - $widthnamepar - $minwidth3, $heightcellpar, $tmpname[0] . " " . $tmpname[1], 0, 0, 'C');
                    } else {
                        $this->fpdf->Cell($width, $heightcellpar, "", 0, 0, 'C');
                    }
                }
            } else {
                $this->fpdf->Ln(80);
            }

            // line
            $this->fpdf->Ln(23);
            $y = $this->fpdf->GetY();
            $x = $this->fpdf->GetX();
            $this->fpdf->SetLineWidth(0.5);
            $this->fpdf->Line($x, $y, ($x + 170), $y);
            $this->fpdf->SetFont('Arial', '', 7);
            $this->fpdf->Ln(2);
            $this->fpdf->Cell(170, 2, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 1);

            // temporary download
            $tmpPath = FCPATH . "media/upload/tmp/" . md5("{$list_data['NO_DOK_ENG']} {$list_data['PACKET_TEXT']}") . ".pdf";
            $this->fpdf->Output();
            // $this->fpdf->Output($tmpPath, 'F');

            $arfile = array($tmpPath);
            // RKS
            if ($list_data['RKS_FILE'] && ($tipe == 'All' || $tipe == 'TR')) {
                // adding footer
//                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-TR") . ".pdf";
//                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['RKS_FILE'], $list_data, 'TR', $tmpPath, 'F');
//                array_push($arfile, $tmpLoc);
                array_push($arfile, FCPATH . $list_data['RKS_FILE']);
            }
            // BQ
            if ($list_data['BQ_FILE'] && ($tipe == 'All' || $tipe == 'BQ')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-BQ") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['BQ_FILE'], $list_data, 'BQ', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['BQ_FILE']);
            }
            // Drawwing
            if ($list_data['DRAW_FILE'] && ($tipe == 'All' || $tipe == 'DRW')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-DW") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['DRAW_FILE'], $list_data, 'DW', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['DRAW_FILE']);
            }
            // ECE
            if ($list_data['ECE_FILE'] && ($tipe == 'All' || $tipe == 'EC')) {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-EC") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['ECE_FILE'], $list_data, 'DW', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, $list_data['ECE_FILE']);
            }
            // Kajian
            if ($list_data['KAJIAN_FILE'] && $tipe == 'KJ') {
                // adding footer
                $tmpPath = FCPATH . "media/upload/tmp/DE" . md5("{$list_data['NO_DOK_ENG']}-{$list_data['PACKET_TEXT']}-KJ") . ".pdf";
                $tmpLoc = $this->setFtrCode(FCPATH . $list_data['KAJIAN_FILE'], $list_data, 'KJ', $tmpPath, 'F');
                array_push($arfile, $tmpLoc);
                // array_push($arfile, FCPATH.$list_data['KAJIAN_FILE']);
            }

            $this->load->library('PDFMerger');
            $pdf = new PDFMerger;

            foreach ($arfile as $key => $value) {
                $pdf->addPDF($value, 'all');
            }
            // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page
            $sAdd = '';
            if ($tipe != 'All') {
                // code...
                $sAdd = '-' . $tipe;
            }
            $pdf->merge('download', "{$trn_no} {$list_data['PACKET_TEXT']}{$sAdd}.pdf");
            // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
        } else {
            echo "File Not Found!";
        }
    }

    public function setFtrCode($src, $dtList = array(), $jenis = 'TR', $output = 'test.pdf', $tipe = 'D') {
        if (!class_exists("FPDF")) {
            require_once APPPATH . 'libraries/fpdf/fpdf.php';
        }
        if (!class_exists("FPDI")) {
            require_once APPPATH . 'libraries/fpdi/fpdi.php';
        }

        $pdf = new FPDI();
        $pageCount = $pdf->setSourceFile($src);

        // generate qrcode
        $lembar = $pageCount;
        $tmpFtr = FCPATH . "media/upload/tmp/ftr" . md5("DOK_ENG-{$dtList['ID']}-{$dtList['NOTIFIKASI']}") . ".png";
        $strFtr = $this->my_crypt("{$dtList['ID']};{$jenis};{$lembar};", 'e');
        $strFtr = str_replace('/', '6s4', $strFtr);
        $strFtr = str_replace('+', '1p4', $strFtr);
        $strFtr = str_replace('=', 'sM9', $strFtr);
        $strFtr = base_url() . "info/dokumen/" . $strFtr;
        QRcode::png($strFtr, $tmpFtr);

        // looping all pages
        for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
            $templateId = $pdf->importPage($pageNo);
            $size = $pdf->getTemplateSize($templateId);
            if ($size['w'] > $size['h']) {
                $pdf->AddPage('L', array($size['w'], $size['h']));
            } else {
                $pdf->AddPage('P', array($size['w'], $size['h']));
            }
            if ($pageNo == $pageCount) {
                // $img = $_SERVER['DOCUMENT_ROOT'] . get_save_url() . D('user')->where('id = ' . get_user_id())->getField('pic_yin');
            }

            // line
            // $this->fpdf->SetFont('Arial','',7);
            // $this->fpdf->SetLineWidth(0.5);
            // $pdf->SetY(($size['h']-30));
            // $pdf->Write(5,'* dokumen ini di approve oleh sistem. ');
            // first add an imported page
            $pdf->useTemplate($templateId);

            //overlay a image on top of template
            if ($dtList['APPROVE3_AT']) {
                $headde = $dtList['APPROVE3_BY'];
            } else {
                $headde = '';
            }
            $pdf->SetFont('Arial', '', 7);
            $pdf->Image($tmpFtr, 10, ($size['h'] - 29), 0, 20);
            $pdf->Text(30, ($size['h'] - 27), "ERF");
            $pdf->Text(48, ($size['h'] - 27), ": {$dtList['NO_PENGAJUAN']}");

            $pdf->Text(30, ($size['h'] - 24), "EAT");
            $pdf->Text(48, ($size['h'] - 24), ": {$dtList['NO_PENUGASAN']}");

            $pdf->Text(30, ($size['h'] - 21), "DOC ENG");
            $pdf->Text(48, ($size['h'] - 21), ": {$dtList['NO_DOK_ENG']}");

            $pdf->Text(30, ($size['h'] - 18), "DIBUAT");
            $pdf->Text(48, ($size['h'] - 18), ": {$dtList['NAME_CRT']}");

            $pdf->Text(30, ($size['h'] - 15), "DIVERIFIKASI");
            $pdf->Text(48, ($size['h'] - 15), ": {$dtList['NAME_PARAF']}");

            $pdf->Text(30, ($size['h'] - 12), "DISETUJUI");
            $pdf->Text(48, ($size['h'] - 12), ": {$dtList['NAME_APP']}");

            $pdf->Text(30, ($size['h'] - 9), "DISAHKAN");
            $pdf->Text(48, ($size['h'] - 9), ": $headde");
            
            $pdf->Text(10, ($size['h'] - 5), '* dokumen ini di approve oleh sistem e-DEMS. ');
            $pdf->SetFont('Helvetica');
            $pdf->SetXY(-5, -5); //$pdf->SetXY(-5, -5);
        }
        $pdf->Output($tipe, $output);
        // $pdf->Output();

        if ($tipe == 'F') {
            return $output;
        } else {
            return 'true';
        }
    }

    public function get_paraf() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'Paraf'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd() {
        $id = $this->input->post('id');
        $dtApp = $this->app->get_data("REF_ID = '{$id}' AND TIPE = 'TTD'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_ttd_smgm() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->dok_eng->get_record("A.ID = '{$id}'");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function get_prog_dok() {
        $id = $this->input->post('id');
        $dtApp = $data = $this->dok_eng->getIDDOK("$id");
        $return = json_encode($dtApp);
        echo $return;
    }

    public function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->dok_eng->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

}
