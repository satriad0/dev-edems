<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Request extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    private $info = array();

    function __construct() {
        parent::__construct();

        $this->load->model('M_hris', 'dbhris');
        $this->load->model('erf/M_request', 'req');
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }

        $where['GEN_VAL'] = '1';
        $where['GEN_CODE'] = 'erf';
        $where['GEN_TYPE'] = 'FORM_GROUP';
        $global = $this->general_model->get_data($where);
        unset($where);
        
        $header["tittle"] = $global->GEN_DESC;
        $header["short_tittle"] = "erf";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }

            // get count pending status
            if (isset($val['tabel'])) {
                $npending = 0;
                if ($info['special_access'] == 'true') {
                    $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                } else {
                    if (strpos($val['stat'], "-child") !== false) {
                        if (strpos($val['stat'], "{$val['code']}-child=1") !== false) {
                            $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
                            $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans']);
                        }
                    } else {
                        $npending = $this->general_model->get_count_pending($val['tabel'], $val['ref_tabel'], $val['ststrans'], $info['uk_kode']);
                    }
                }
                $header['roles'][$i]['count'] = $npending;
            }
        }


        $yyyy = date("Y");
        $xxxx = $this->req->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data['no'] = "///EM/{$kodemax}//{$yyyy}";
        // $data["autocode"] = $this->req->id_mpeAuto();
        // $data["autocodenopeng"] = $this->req->no_pengajuan();
        $data["locations"] = $this->req->getLocCode();
        $data['aspect0'] = $this->req->getAspect(0);
        $data['aspect1'] = $this->req->getAspect(1);
        $data['aspect2'] = $this->req->getAspect(2);
        $data['aspect3'] = $this->req->getAspect(3);
        $data["areas"] = $this->req->getAreaCode('');
        $data["disiplin"] = $this->req->getDisiplin();
        // $data["areas"] = $data["areas"][0];
        $data["mplant"] = $this->req->getKodeUnit();
        $data["pplant"] = $this->req->getKodeUnitP();
        $data["funloc"] = $this->req->getFunloc();
        $data["mfunloc"] = $this->req->getFunlocMplant();
        // $where['GEN_VAL']='1';
        // $where['GEN_TYPE']='ERF_PGROUP';
        // $data['pgroup'] = $this->general_model->get_data($where);
        $where['GEN_TYPE'] = 'DEF_PGROUP';
        $where['GEN_PAR2'] = $info['company'];
        $data['pgroup'] = $this->general_model->get_data($where);
        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'DEF_PGROUP';
        $where['GEN_PAR2'] = '2000';
        $data['kpgroup'] = $this->general_model->get_data($where);
        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'DEF_PGROUP';
        $data['apgroup'] = $this->general_model->get_data_result($where);
        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'General';
        $where['GEN_CODE'] = 'ENG_FEE';
        $this->db->order_by('GEN_PAR1', 'ASC');
        $data['eng_fee'] = $this->general_model->get_data($where);

        unset($where);
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'General';
        $where['GEN_CODE'] = 'ERF_CUST_REQ';
        $this->db->order_by('GEN_PAR1', 'ASC');
        $data['opt_cust'] = $this->general_model->get_data_result($where);
        // echo $this->db->last_query();
        // echo "<pre>";
        // print_r($data['opt_cust']);
        // echo "</pre>";
        // exit;
        // $data["unitkerja"] = $this->req->getMunitKerja();

        $header['page_state'] = $state . ";special_access={$info['special_access']}";

        $sHdr = '';
        // echo "<pre>";
        // print_r($info);
        // print_r($header);
        // exit;
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        } else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('erf/request_form', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('erf/request_form', $data);
        $this->load->view('general/footer');
    }

    public function get_pgroup_kajian() {
        $result = FALSE;
        if (isset($_POST['TIPE'])) {
            if (strpos($_POST['TIPE'], 'Kajian') !== false) {
                $where['GEN_VAL'] = '1';
                $where['GEN_TYPE'] = 'DEF_PGROUP';
                $where['GEN_PAR2'] = '2000';
                $result = $this->general_model->get_data($where);
            }
        }

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function view_funcloc() {
        $q = '';
        if (isset($_GET['q'])) {
            $q = $_GET['q'];
            $q = strtoupper($q);
        } else {
            $q = strtoupper($_POST['search']);
        }
        $json = $this->req->getFunlocMplant($q);
        echo json_encode($json);
    }

    public function view_psection() {
        $where = '';
        $result = FALSE;
        if (isset($_POST['MPLANT'])) {
            $where = "MPLANT = '{$_POST['MPLANT']}'";
            $result = $this->req->getPsection($where);
            // echo json_encode($result);
        }

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function view_pgroup() {
        $where = '';
        $result = FALSE;
        if (isset($_POST['PPLANT'])) {
            $where = "PPLANT = {$_POST['PPLANT']}";
            $result = $this->req->getPgroup($where);
            // echo json_encode($result);
        }

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function view($id = null) {
        if (isset($id)) {
            $result = $this->req->get_datarequest($id);
            // code...
        } else {
            $result = $this->req->get_datarequest();
        }
        // echo $this->db->last_query();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('columns');
        $order = $this->input->post('order');
        $child = $this->input->post('view');
        
        //columns[3][search][value]

        $key = array(
            'search' => $search[0]['search']['value'],  //$search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
            $key['uk_code'] = $info['uk_kode'];
            if (isset($child) && $child == 'child') {
                $key['comp_view'] = $info['uk_kode'];
            }
        }
        // $data	= $this->Job_m->get($key);

        $data = $this->req->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->req->recFil($key),
            'recordsTotal' => $this->req->recTot($key)
        );

        echo json_encode($return);
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$atas->NOBADGE}'");
        $peg = (array) $peg[0];

        $list_data = $this->req->get_data_unit($param['NO_PENGAJUAN']);
        $list_data = $list_data[0];

        $param['short'] = 'ERF';
        $param['link'] = 'erf';
        $param['title'] = 'Engineering Request Form';
        
        $param['sender'] = "{$info['no_badge']} - {$info['name']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_PENGAJUAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Nama Pekerjaan :</b> {$param['NAMA_PEKERJAAN']}";
        $param['TRN']['LOKASI'] = "<b>Unit Kerja Peminta :</b> {$list_data['DESCPSECTION']} - {$list_data['FL_TEXT']}";
        $param['TRN']['COMPANY'] = "<b>Company :</b> {$list_data['COMP_TEXT']}";
//        $param['TRN']['LOKASI'] = "<b>Lokasi :</b> {$param['LOKASI']}";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);
        
        //subject baru task [task 28]
        $subjectnew = "{$subject} - {$peg['NAMA']} - {$peg['JAB_TEXT']} - {$peg['UK_TEXT']}";

        $this->email($this->template->set_app($param), $subjectnew, '', $tomail, $cc);

        // $message="Dengan Hormat $atasan->NOBADGE - $atasan->NAMA,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi   : " . $NOTIF_NO . "<br/>
        // No Project      : " . $param['NO_PENGAJUAN'] . "<br/>
        // Nama Project    : " . $param['NAMA_PEKERJAAN'] . "<br/> <br/> <br/>
        //
        //
        // Click link berikut untuk lebih lanjut <a href='" . base_url('erf/request/approve?e='. $n_form) . "'> APPROVAL PROJECT  </a> <br/><br/><br/>
        //
        // Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (isset($_POST['ID_MPE']) && isset($info)) {
            error_reporting(0);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);

            $data = $this->req->get_datarequest($param['ID_MPE']);
            $data = $data[0];
            $atasan = $this->dbhris->get_hris_where("(k.mk_nopeg = '{$data['APPROVE_BADGE']}' OR (k.mk_nama = '{$data['APPROVE_BY']}' AND j.mjab_nama = '{$data['APPROVE_JAB']}'))");
            $atasan = $atasan[0];
            // print_r($data);

            if ($data) {

                $n_form = $this->my_crypt("{$data['NO_PENGAJUAN']};{$data['NOTIFIKASI']};{$data['APPROVE_BY']};{$data['ID_MPE']}", 'e');
                $n_form = str_replace('/', '6s4', $n_form);
                $n_form = str_replace('+', '1p4', $n_form);
                $n_form = str_replace('=', 'sM9', $n_form);
                $NOTIF_NO = $data['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                $to = $atasan->EMAIL;
                // $cc=array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                $subject = "Resend : Request Approval ERF";
                $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create() {

        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (isset($_POST) && isset($_POST['FUNCT_LOC']) && isset($_POST['NO_PENGAJUAN']) && isset($info)) {
            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            // echo "<pre>";
            // print_r($param);
            // print_r($info);
            // echo "</pre>";
            // exit;
            $atasan = new stdClass();
            // $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            // exit;
            // $atasan = $atasan[0];
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }
            // jika yang create di atas sm
            if ($info['position'] <= 20 || $info['pos_text'] == 'General Manager' || $info['pos_text'] == 'Senior Manager') {
                $atasan->NOBADGE = $info['no_badge'];
                $atasan->NAMA = $info['name'];
                $atasan->COMPANY = $info['company'];
                $atasan->EMAIL = $info['email'];
                $atasan->DEPT_CODE = $info['dept_code'];
                $atasan->DEPT_TEXT = $info['dept_text'];
                $atasan->UNIT_KERJA = $info['uk_kode'];
                $atasan->UK_TEXT = $info['unit_kerja'];
                $atasan->COST_CENTER = $info['cost_center'];
                $atasan->CC_TEXT = $info['cc_text'];
                $atasan->ACTIVE = $info['active'];
                $atasan->POSITION = $info['position'];
                $atasan->POS_TEXT = $info['pos_text'];
                $atasan->LOKASI = '';
                $atasan->ALAMAT = '';
                $atasan->TGL_MASUK = '';
                $atasan->TGL_LAHIR = '';
            }
            // $tmpname = explode("@", $atasan->NAMA);
            // $username = $tmpname[0];

            $param['NO_PENGAJUAN'] = $this->generateNo('NO_PENGAJUAN', 'MPE_PENGAJUAN', 'EM', $param['NO_PENGAJUAN']);
            $param['STATUS'] = 'Open';
            $param['CREATE_BY'] = $info['name'];
            // $param['APPROVE_BY'] = $username;
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            $cdata = explode(' - ', $_POST['LIST_APP']);
            unset($param['LIST_APP']);
            if (count($cdata) > 0) {
                $param['APPROVE_BADGE'] = $cdata[0];
                $param['APPROVE_BY'] = $cdata[1];
                $param['APPROVE_EMAIL'] = $cdata[2];
                $param['APPROVE_JAB'] = $cdata[3];
                $atasan->NOBADGE = $cdata[0];
                $atasan->NAMA = $cdata[1];
                $atasan->EMAIL = $cdata[2];
            }
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH2'])) {
                $url_files = $this->upload($_FILES['STRPATH2'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES2'] = $url_files;
                $this->db->set('FILES2_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH3'])) {
                $url_files = $this->upload($_FILES['STRPATH3'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES3'] = $url_files;
                $this->db->set('FILES3_AT', "CURRENT_DATE", false);
            }
            $result = $this->req->save($param);


            if ($result) {

                $n_form = $this->my_crypt("{$param['NO_PENGAJUAN']};{$param['NOTIFIKASI']};{$atasan->NAMA};{$param['ID_MPE']}", 'e');
                $n_form = str_replace('/', '6s4', $n_form);
                $n_form = str_replace('+', '1p4', $n_form);
                $n_form = str_replace('=', 'sM9', $n_form);
                $NOTIF_NO = $param['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                $to = $atasan->EMAIL;
                $cc = array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                $subject = "Request Approval ERF";
                $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                $this->send_mail($subject, $param, $atasan, $n_form, $to, $cc);
                // $message="Dengan Hormat $atasan->NOBADGE - $atasan->NAMA,<br /><br />
                //
                // Mohon untuk ditindak lanjuti approval project berikut :<br />
                // No Notifikasi   : " . $NOTIF_NO . "<br/>
                // No Project      : " . $param['NO_PENGAJUAN'] . "<br/>
                // Nama Project    : " . $param['NAMA_PEKERJAAN'] . "<br/> <br/> <br/>
                //
                //
                // Click link berikut untuk lebih lanjut <a href='" . base_url('erf/request/approve?e='. $n_form) . "'> APPROVAL PROJECT  </a> <br/><br/><br/>
                //
                // Demikian, terima kasih";
                // $this->email($message, $subject, '', $to, $cc);

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function update() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];
        if (isset($_POST) && isset($_POST['ID_MPE']) && isset($info)) {
            $param = $_POST;
            // $sap_result = $this->create_notif($param);

            $atasan = new stdClass();
            // $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            // exit;
            // $atasan = $atasan[0];
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }
            // jika yang create di atas sm
            if ($info['position'] <= 20 || $info['pos_text'] == 'General Manager' || $info['pos_text'] == 'Senior Manager') {
                $atasan->NOBADGE = $info['no_badge'];
                $atasan->NAMA = $info['name'];
                $atasan->COMPANY = $info['company'];
                $atasan->EMAIL = $info['email'];
                $atasan->DEPT_CODE = $info['dept_code'];
                $atasan->DEPT_TEXT = $info['dept_text'];
                $atasan->UNIT_KERJA = $info['uk_kode'];
                $atasan->UK_TEXT = $info['unit_kerja'];
                $atasan->COST_CENTER = $info['cost_center'];
                $atasan->CC_TEXT = $info['cc_text'];
                $atasan->ACTIVE = $info['active'];
                $atasan->POSITION = $info['position'];
                $atasan->POS_TEXT = $info['pos_text'];
                $atasan->LOKASI = '';
                $atasan->ALAMAT = '';
                $atasan->TGL_MASUK = '';
                $atasan->TGL_LAHIR = '';
            }
            // $tmpname = explode("@", $atasan->NAMA);
            // $username = $tmpname[0];

            $param['UPDATE_BY'] = $info['name'];
            // $param['APPROVE_BY'] = $username;
            $param['COMPANY'] = $info['company'];
            $cdata = explode(' - ', $_POST['LIST_APP']);
            unset($param['LIST_APP']);
            if (count($cdata) > 0) {
                $param['APPROVE_BADGE'] = $cdata[0];
                $param['APPROVE_BY'] = $cdata[1];
                $param['APPROVE_EMAIL'] = $cdata[2];
                $param['APPROVE_JAB'] = $cdata[3];
                $atasan->NOBADGE = $cdata[0];
                $atasan->NAMA = $cdata[1];
                $atasan->EMAIL = $cdata[2];
            }
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH2'])) {
                $url_files = $this->upload($_FILES['STRPATH2'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES2'] = $url_files;
                $this->db->set('FILES2_AT', "CURRENT_DATE", false);
            }
            if (isset($_FILES['STRPATH3'])) {
                $url_files = $this->upload($_FILES['STRPATH3'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES3'] = $url_files;
                $this->db->set('FILES3_AT', "CURRENT_DATE", false);
            }


            $result = $this->req->updateData('update', $param);
//            echo $this->db->last_query();

            $data = $this->req->get_datarequest($param['ID_MPE']);
            $data = $data[0];
            // print_r($data);
            // exit;

            if ($result) {

                $n_form = $this->my_crypt("{$data['NO_PENGAJUAN']};{$data['NOTIFIKASI']};{$atasan->NAMA};{$data['ID_MPE']}", 'e');
                $n_form = str_replace('/', '6s4', $n_form);
                $n_form = str_replace('+', '1p4', $n_form);
                $n_form = str_replace('=', 'sM9', $n_form);
                $NOTIF_NO = $data['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                $to = $atasan->EMAIL;
                $cc = array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                $subject = "APPROVAL REQUEST - ERF";
                $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);
                // $message="Dengan Hormat $atasan->NOBADGE - $atasan->NAMA,<br /><br />
                //
                // Mohon untuk ditindak lanjuti approval project berikut :<br />
                // No Notifikasi   : " . $NOTIF_NO . "<br/>
                // No Project      : " . $param['NO_PENGAJUAN'] . "<br/>
                // Nama Project    : " . $param['NAMA_PEKERJAAN'] . "<br/> <br/> <br/>
                //
                //
                // Click link berikut untuk lebih lanjut <a href='" . base_url('erf/request/approve?e='. $n_form) . "'> APPROVAL PROJECT  </a> <br/><br/><br/>
                //
                // Demikian, terima kasih";
                // $this->email($message, $subject, '', $to, $cc);

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function revision() {
        // error_reporting(1);

        $this->load->model('sap/M_updatenotif', 'sap_upd');

        if (isset($_POST) && isset($_POST['ID_MPE']) && isset($_POST['FUNCT_LOC']) && isset($_POST['NO_PENGAJUAN'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $this->db->where('NOTIFIKASI', $param['NOTIFIKASI']);
            $this->db->where('REV_NO', ((int) $param['REV_NO'] - 1));
            // echo $_POST['ID_MPE'];
            $dtReq = $this->req->get_datarequest($_POST['ID_MPE']);
            $dtReq = $dtReq[0];
            // echo $this->db->last_query();
            // print_r($dtReq);
            $dtReq['REV_NO'] = ((int) $param['REV_NO'] - 1);
            $dtReq['STATE'] = 'Revised';
            $dtReq['UPDATE_BY'] = $info['name'];
            unset($dtReq['DESCMPLANT']);
            unset($dtReq['DESCPSECTION']);
            unset($dtReq['DESCPLANT']);
            unset($dtReq['DESCPGROUP']);
            unset($dtReq['OBJNR']);
            unset($dtReq['FL_TEXT']);
            unset($dtReq['UPDATE_AT']);
            $result = $this->req->updateData('revision', $dtReq);

            $param['STATUS'] = $dtReq['STATUS'];
            $param['COMPANY'] = $dtReq['COMPANY'];
            $param['CREATE_AT'] = $dtReq['CREATE_AT'];
            $param['CREATE_BY'] = $dtReq['CREATE_BY'];
            $param['APPROVE_AT'] = $dtReq['APPROVE_AT'];
            $param['APPROVE_BY'] = $dtReq['APPROVE_BY'];
            $param['APPROVE_JAB'] = $dtReq['APPROVE_JAB'];
            $param['UPDATE_BY'] = $info['name'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }

            // update sap
            // $param['USERNAME'] = $info['username'];
            // $res_sap = $this->sap_upd->updatenotif($param);
            // unset($param['USERNAME']);
            // echo "<pre>";
            // print_r($param);
            // print_r($res_sap);
            // echo "</pre>";
            // exit;
            // if (!$res_sap) {
            // 	// code...
            // 	exit;
            // }
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $result = $this->req->save($param);

            if ($result) {
                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function delete($id, $no_notif) {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['id'] = $id;
            $data['DELETE_BY'] = $info['username'];
            $result = $this->req->updateData('delete', $data);

            if ($result) {
                $param = array(
                    'STATUS' => 'DEL',
                    'NOTIF_NO' => $no_notif
                );
                $del_sap = $this->sap_del->deletenotif($param);
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function approve() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            $nama = $_POST['nama'];
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');

            // $no_form= $this->my_crypt($e,'d');
            list($no_form, $notifikasi, $nama) = split(";", $this->my_crypt($e, 'd'));
            $data['no_form'] = $no_form;
            // $data['APPROVE_BY']=$nama;
            $data['STATUS'] = 'Approved';
            $this->db->set('NOTE', $reason);
            $result = $this->req->updateData('approve', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                $param = array(
                    'STATUS' => 'REL',
                    'NOTIF_NO' => $notifikasi
                );
                $del_sap = $this->sap_del->deletenotif($param);

                // echo $this->response('success', 200, $result);
                // echo "ERF '{$no_form}' was approved succesfully";
                // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function reject() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        // echo $e;
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            $nama = $_POST['nama'];
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            // $no_form= $this->my_crypt($e,'d');
            list($no_form, $notifikasi, $nama) = split(";", $this->my_crypt($e, 'd'));
            $data['no_form'] = $no_form;
            // $data['APPROVE_BY']=$nama;
            $data['STATUS'] = 'Rejected';
            $this->db->set('NOTE', $reason);
            $this->db->set('REJECT_BY', $nama);
            $result = $this->req->updateData('reject', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                $param = array(
                    'STATUS' => 'DEL',
                    'NOTIF_NO' => $no_notif
                );
                $del_sap = $this->sap_del->deletenotif($param);

                // echo $this->response('success', 200, $result);
                // echo "ERF '{$no_form}' was approved succesfully";
                // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function de_reject() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        // echo $e;
        if (isset($_POST) && isset($_POST['ID_MPE']) && isset($_POST['NOTIFIKASI'])) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];
            $data = $_POST;

            $data['STATUS'] = 'Rejected';
            $data['REJECT_BY'] = $info['name'];
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $_POST['NOTIFIKASI'], 'erf', 'Reject');
                $data['REJECT_FILE'] = $url_files;
            }

            $result = $this->req->updateData('reject_de', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                $param = array(
                    'STATUS' => 'DEL',
                    'NOTIF_NO' => $data['NOTIFIKASI']
                );
                $del_sap = $this->sap_del->deletenotif($param);

                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function getApproval() {
        $id = $this->input->post('id');
        $data = $this->req->getCheckApprove($id);
        // echo $this->db->last_query();
        $return = json_encode($data);
        echo $return;
    }

    public function getLogPengajuan() {
        $id = $this->input->post('id');
        $data = $this->req->getLogPengajuan($id);
        // echo $this->db->last_query();
        $return = json_encode($data);
        echo $return;
    }

    public function getLogPenugasan() {
        $id = $this->input->post('id');
        $data = $this->req->getLogPenugasan($id);
        $return = json_encode($data);
        echo $return;
    }

    public function export_pdf($id) {
        if ($id) {
            error_reporting(0);

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $list_data = $this->req->get_datarequest($id);
            $list_data = $list_data[0];

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 7);
            $this->fpdf->SetLeftMargin(20);
            $this->fpdf->SetRightMargin(20);

            $this->fpdf->Cell(14, 4, '', 0, 0);
            $this->fpdf->Cell(50, 7, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 12, 'PNG'), 0, 1, 'C');
            $this->fpdf->Cell(170, 1, 'R/538/012', 0, 1, 'R');
            $this->fpdf->Cell(0, 5, '', 0, 1);
            $this->fpdf->SetFont('Arial', 'B', 6);
            $this->fpdf->Cell(64, 1, 'PT. SEMEN INDONESIA (PERSERO)Tbk.', 0, 0);

            // HEADER
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->Cell(170, 7, "ENGINEERING REQUIREMENT FORM", 0, 1, 'C');
            $this->fpdf->Cell(170, 7, "{$list_data['NO_PENGAJUAN']}", 0, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "General Information", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tanggal", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['CREATE_AT']}", 1, 1);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Notifikasi", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['NOTIFIKASI']}", 1, 1);
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tipe", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['TIPE']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $rw = 1;
            if ((strlen($list_data['NAMA_PEKERJAAN']) / 65) > 1) {
                $rw = 2;
            }
            $this->fpdf->Cell(50, (5 * $rw), "  Nama Pekerjaan", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(120, 5, "{$list_data['NAMA_PEKERJAAN']}", 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Unit Kerja Peminta", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['DESCPSECTION']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['FL_TEXT']}", 1, 1);

            // $this->fpdf->SetFont('Arial', 'B', 10);
            // $this->fpdf->Cell(50, 5, "  Kode Unit Kerja", 1, 0);
            // $this->fpdf->SetFont('Arial', '', 10);
            // $this->fpdf->Cell(120, 5, "{$list_data['UK_CODE']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Company", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['COMP_TEXT']}", 1, 1);

            // $this->fpdf->SetFont('Arial','B',10);
            // $this->fpdf->Cell(50,5,"  K.A",1,0);
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->Cell(120,5,"{$list_data['DESCPSECTION']}",1,1);
            // $this->fpdf->SetFont('Arial', 'B', 10);
            // $this->fpdf->Cell(50, 5, "  Detail Lokasi Pekerjaan", 1, 0);
            // $this->fpdf->SetFont('Arial', '', 10);
            // $this->fpdf->Cell(120, 5, "{$list_data['LOKASI']}", 1, 1);

            if (strpos($list_data['TIPE'], 'Detail') !== false) {
                $this->fpdf->SetFont('Arial', 'B', 10);
                $this->fpdf->Cell(50, 5, "  Kode Proyek", 1, 0);
                $this->fpdf->SetFont('Arial', '', 10);
                $this->fpdf->Cell(120, 5, "{$list_data['WBS_CODE']}", 1, 1);

                $this->fpdf->SetFont('Arial', 'B', 10);
                $rw = 1;
                if ((strlen($list_data['WBS_TEXT']) / 65) > 1) {
                    $rw = 2;
                }
                $this->fpdf->Cell(50, (5 * $rw), "  Nama Proyek", 1, 0);
                $this->fpdf->SetFont('Arial', '', 10);
                $this->fpdf->MultiCell(120, 5, "{$list_data['WBS_TEXT']}", 1);
            }

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tanggal Mulai", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['DATE_START']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Tanggal Selesai", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['DATE_END']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Estimasi Biaya Pekerjaan", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            setlocale(LC_MONETARY, "en_US");
            $this->fpdf->Cell(120, 5, "Rp " . number_format($list_data['CONSTRUCT_COST'], 0), 1, 1);
            // $this->fpdf->Cell(120, 5, money_format("Rp ", (float)$list_data['CONSTRUCT_COST']), 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Estimasi Biaya Engineering", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "Rp " . number_format($list_data['ENG_COST'], 0), 1, 1);

            // CONTENT
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Latar Belakang", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['LATAR_BELAKANG']}", 1);

            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Customer Request", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['CUST_REQ']}", 1);
            
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Safety & Environmental Aspect", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['ASPECT1']}, {$list_data['ASPECT2']}, {$list_data['ASPECT3']}, {$list_data['ASPECT4']}", 1);

            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Technical Information", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['TECH_INFO']}", 1);

            // $this->fpdf->Ln(3);
            // $this->fpdf->SetFont('Arial','B',14);
            // $this->fpdf->Cell(170,7,"Resiko & Mitigasi",1,1,'C');
            // $this->fpdf->SetFont('Arial','',10);
            // $this->fpdf->MultiCell(170,5,"{$list_data['RES_MIT']}",1);

            $this->fpdf->Ln(3);
            if ($this->fpdf->getY() > 265) {
                // code...
                $this->fpdf->AddPage();
            }
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Approval Note", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['NOTE']}", 1);

            // FOOTER
            if ($this->fpdf->getY() > 215) {
                // code...
                $this->fpdf->AddPage();
            }
            $tmpCre = FCPATH . "media/upload/tmp/" . md5("ERF-{$list_data['ID_MPE']}-{$list_data['NOTIFIKASI']}-CREATE") . ".png";
            $tmpApp = FCPATH . "media/upload/tmp/" . md5("ERF-{$list_data['ID_MPE']}-{$list_data['NOTIFIKASI']}-APPROVE") . ".png";
            // generating
            include(APPPATH . 'libraries/phpqrcode/qrlib.php');
            $str = $this->my_crypt("{$list_data['ID_MPE']};{$list_data['CREATE_BY']};{$list_data['CREATE_AT']};", 'e');
            $str = str_replace('/', '6s4', $str);
            $str = str_replace('+', '1p4', $str);
            $str = str_replace('=', 'sM9', $str);
            $str = base_url() . "info/erf/" . $str;
            QRcode::png($str, $tmpCre);
            if ($list_data['APPROVE_AT']) {
                $strApp = $this->my_crypt("{$list_data['ID_MPE']};{$list_data['APPROVE_BY']};{$list_data['APPROVE_AT']};", 'e');
                $strApp = str_replace('/', '6s4', $strApp);
                $strApp = str_replace('+', '1p4', $strApp);
                $strApp = str_replace('=', 'sM9', $strApp);
                $strApp = base_url() . "info/erf/" . $strApp;
                QRcode::png($strApp, $tmpApp);
                if (!file_exists($tmpApp)) {
                    echo 'We cannot generated a QR code, please back and try again later!';
                    echo '<hr />';
                    exit;
                }
                if (!file_exists($tmpCre)) {
                    echo 'We cannot generated a QR code, please back and try again later!';
                    echo '<hr />';
                    exit;
                }
            }
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Persetujuan", 1, 1, 'C');
            // $this->fpdf->Ln(5);
            // $this->fpdf->Cell(170,3,"",'LTR',1);
            $this->fpdf->SetFont('Arial', 'U', 10);
            $this->fpdf->Cell(85, 7, "Dibuat Oleh :", 'LR', 0, 'C');
            $this->fpdf->Cell(85, 7, "Disetujui Oleh :", 'LR', 1, 'C');
            $this->fpdf->Cell(85, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + 30, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LR', 0, 'C');
            if ($list_data['APPROVE_AT']) {
                $this->fpdf->Cell(85, 25, $this->fpdf->Image($tmpApp, $this->fpdf->GetX() + 30, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LR', 1, 'C');
            } else if ($list_data['REJECT_DATE']) {
                $this->fpdf->Cell(85, 25, 'Rejected', 'LR', 1, 'C');
            } else {
                $this->fpdf->Cell(85, 25, "", 'LR', 1);
            }
            $this->fpdf->SetFont('Arial', 'BU', 10);
            $this->fpdf->Cell(85, 5, "{$list_data['CREATE_BY']}", 'LR', 0, 'C');
            $this->fpdf->Cell(85, 5, "{$list_data['APPROVE_BY']}", 'LR', 1, 'C');
            // $this->fpdf->MultiCell(170,5,"{$list_data['APPROVE2_BY']}",1);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(85, 7, "PT Semen Indonesia", 'LBR', 0, 'C');
            $this->fpdf->Cell(85, 7, "PT Semen Indonesia", 'LBR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 8);
            $this->fpdf->Cell(170, 4, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 1);
            if ($list_data['FILES']) {
                $this->fpdf->Cell(170, 4, "** data pendukung kami sertakan terlampir dengan dokumen ini.", 0, 1);
            }

            // FILENAME
            $tmpPath = "{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}.pdf";
            $this->fpdf->Output($tmpPath, 'D');


            // // temporary download
            // $tmpPath = FCPATH."media/upload/tmp/". md5("{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}") .".pdf";
            // $this->fpdf->Output($tmpPath,'F');
            //
            // $arfile = array($tmpPath);
            // // RKS
            // if ($list_data['FILES']) {
            //   array_push($arfile, FCPATH.$list_data['FILES']);
            // }
            //
            // $this->load->library('PDFMerger');
            // $pdf = new PDFMerger;
            //
            // foreach ($arfile as $key => $value) {
            //   $pdf->addPDF($value, 'all');
            // }
            // // $pdf->addPDF('samplepdfs/three.pdf', 'all'); //  'all' or '1,3'->page
            //
            // $pdf->merge('download', "{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}.pdf");
        } else {
            echo "File Not Found!";
        }
    }

    function getKDLOC() {
        $json = $this->req->getLocCode();
        if ($json) {
            echo $this->response('success', 200, $json);
        } else {
            echo $this->response('error', 400);
        }
    }

    function getKDArea() {
        $nama = '';
        if (isset($_POST['search'])) {
            $nama = $_POST['search'];
        }

        $result = $this->req->getAreaCode($nama);
        // echo $this->dbhris->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->req->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }
    
    public function getProgresReport(){
       $idMpe = $_POST['ID_MPE'];
       
       $id_dtl_pen = $this->req->getIdPenugasan($idMpe);
       
       foreach ($id_dtl_pen as $key => $Value) {
					
			$data['ID'] 		    = $Value['ID'];
			$data['DESKRIPSI'] 	    = $Value['DESKRIPSI'];
			$data['PROGRESS']       = $Value['PROGRESS'];
			
			$data['DT_PROGRESS'] 	= $this->req->getList_progress($Value['ID']);
					
			$json[] = $data;
       }
        
        //print_r($json);
        //exit();
        
       echo $this->response('success', 200, $json);
    }

    public function getApproverErfTech(){
        $idUnit = $this->input->post("id");
        $data = $this->req->get_approver_erf_tech($idUnit);
        echo $this->response("success", 200, $data);
    }

    public function get_doc_erf(){
        $id = $this->input->post("id");
        $data = $this->req->get_list_doc_erf($id);
        echo $this->response("success", 200, $data);
    }

}
