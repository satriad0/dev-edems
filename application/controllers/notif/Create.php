<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Create extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    private $info = array();

    function __construct() {
        parent::__construct();

        $this->load->model('M_hris', 'dbhris');
        $this->load->model('notif/M_n_create', 'req');
    }

    public function index() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        if (empty($info['username'])) {
            redirect('login');
        }
        // echo "<pre>";
        // print_r($info);
        // echo "</pre>";
        // exit;
        $header["tittle"] = "SAP Notification";
        $header["short_tittle"] = "ntf";
        // $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        // view roles
        $this->db->distinct();
        $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
        $header['roles'] = json_decode(json_encode($header['roles']), true);
        $state = '';
        foreach ($header['roles'] as $i => $val) {
            $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
            $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
            if ($val['code'] == $header["short_tittle"]) {
                // code...
                $state = $val['stat'];
            }
        }

        // $yyyy = date("Y");
        // $xxxx = $this->req->getCount();
        // $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        // $data['no'] = "{$yyyy}///EM/{$kodemax}";
        // $data["locations"] = $this->req->getLocCode();
        // $data["areas"] = $this->req->getAreaCode('');
        // $data["mplant"] = $this->req->getKodeUnit();
        // $data["pplant"] = $this->req->getKodeUnitP();
        // $data["funloc"] = $this->req->getFunloc();
        // $data["mfunloc"] = $this->req->getFunlocMplant();
        $where['GEN_VAL'] = '1';
        $where['GEN_TYPE'] = 'DEF_PGROUP';
        $where['GEN_PAR2'] = $info['company'];
        $data['pgroup'] = $this->general_model->get_data($where);

        // $data["unitkerja"] = $this->req->getMunitKerja();
        $header['page_state'] = $state;

        $sHdr = '';
        if (isset($info['themes'])) {
            if ($info['themes'] != 'default')
                $sHdr = "_" . $info['themes'];
        }else {
            unset($where);
            $where['GEN_CODE'] = 'themes-app';
            $where['GEN_TYPE'] = 'General';
            $global = (array) $this->general_model->get_data($where);
            if (isset($global) && $global['GEN_VAL'] != '0') {
                $i_par = $global['GEN_VAL'];
                $sHdr = "_" . $global['GEN_PAR' . $i_par];
            }
        }
        if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('notif/create_no', $data);
        } else {
            $header["tittle"] = "Forbidden";
            $header["short_tittle"] = "403";

            $this->load->view('general/header' . $sHdr, $header);
            $this->load->view('forbidden');
        }
        // $this->load->view('general/header', $header);
        // $this->load->view('erf/request_form', $data);
        $this->load->view('general/footer');
    }

    public function view_funcloc() {
        $q = '';
        if (isset($_GET['q'])) {
            $q = $_GET['q'];
            $q = strtoupper($q);
        } else {
            $q = strtoupper($_POST['search']);
        }
        $json = $this->req->getFunlocMplant($q);
        echo json_encode($json);
    }

    public function view_psection() {
        $where = '';
        $result = FALSE;
        if (isset($_POST['MPLANT'])) {
            $where = "MPLANT = '{$_POST['MPLANT']}'";
            $result = $this->req->getPsection($where);
            // echo json_encode($result);
        }

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function view_pgroup() {
        $where = '';
        $result = FALSE;
        if (isset($_POST['PPLANT'])) {
            $where = "PPLANT = {$_POST['PPLANT']}";
            $result = $this->req->getPgroup($where);
            // echo json_encode($result);
        }

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function view($id = null) {
        if (isset($id)) {
            $result = $this->req->get_datarequest($id);
            // code...
        } else {
            $result = $this->req->get_datarequest();
        }
        // echo $this->db->last_query();

        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    public function data_list() {
        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $search = $this->input->post('search');
        $order = $this->input->post('order');

        $key = array(
            'search' => $search['value'],
            'ordCol' => $order[0]['column'],
            'ordDir' => $order[0]['dir'],
            'length' => $this->input->post('length'),
            'start' => $this->input->post('start')
        );

        if ($info['special_access'] == 'false' || $info['special_access'] == '0') {
            $key['name'] = $info['name'];
            $key['username'] = $info['username'];
            $key['dept_code'] = $info['dept_code'];
        }
        // $data	= $this->Job_m->get($key);

        $data = $this->req->get_data($key);
        // echo $this->db->last_query();

        $return = array(
            'draw' => $this->input->post('draw'),
            'data' => $data,
            'recordsFiltered' => $this->req->recFil($key),
            'recordsTotal' => $this->req->recTot($key)
        );

        echo json_encode($return);
    }

    public function send_mail($subject, $param = array(), $atas, $e, $to = array(), $cc = array()) {
        $this->load->library('Template');

        $info = $this->session->userdata;
        $info = $info['logged_in'];

        $peg = $this->dbhris->get_hris_where("k.mk_nopeg = '{$atas->NOBADGE}'");
        $peg = (array) $peg[0];

        $param['short'] = 'ERF';
        $param['link'] = 'erf';
        $param['title'] = 'Engineering Request Form';
        $param['sender'] = "{$info['no_badge']} - {$info['name']}";
        $param['code'] = $e;

        $param['APP']['BADGE'] = $peg['NOBADGE'];
        $param['APP']['NAMA'] = $peg['NAMA'];
        $param['APP']['UK_TEXT'] = $peg['UK_TEXT'];
        $param['TRN']['NO'] = "<b>No. {$param['short']} :</b> {$param['NO_PENGAJUAN']}";
        $param['TRN']['NOTIFIKASI'] = "<b>Notifikasi :</b> {$param['NOTIFIKASI']}";
        $param['TRN']['DESKRIPSI'] = "<b>Deskripsi :</b> {$param['NAMA_PEKERJAAN']}";
        $param['TRN']['LOKASI'] = "<b>Lokasi :</b> {$param['LOKASI']}";
        $mailto = str_replace("SIG.ID", "SEMENINDONESIA.COM", $to);
        $tomail = str_replace("sig.id", "SEMENINDONESIA.COM", $mailto);

        $this->email($this->template->set_app($param), $subject, '', $tomail, $cc);

        // $message="Dengan Hormat $atasan->NOBADGE - $atasan->NAMA,<br /><br />
        // Mohon untuk ditindak lanjuti approval project berikut :<br />
        // No Notifikasi   : " . $NOTIF_NO . "<br/>
        // No Project      : " . $param['NO_PENGAJUAN'] . "<br/>
        // Nama Project    : " . $param['NAMA_PEKERJAAN'] . "<br/> <br/> <br/>
        //
		//
		// Click link berikut untuk lebih lanjut <a href='" . base_url('erf/request/approve?e='. $n_form) . "'> APPROVAL PROJECT  </a> <br/><br/><br/>
        //
		// Demikian, terima kasih";
        // $this->email($message, $subject, '', $to, $cc);
    }

    public function resend() {
        if (isset($_POST['ID_MPE'])) {
            error_reporting(0);

            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            // $atasan = $atasan[0];
            if (!$atasan) {
                $atasan->NOBADGE = 'null';
                $atasan->NAMA = 'null';
                $atasan->EMAIL = 'null';
            }

            $data = $this->req->get_datarequest($param['ID_MPE']);
            $data = $data[0];
            // print_r($data);

            if ($data) {

                $n_form = $this->my_crypt("{$data['NO_PENGAJUAN']};{$data['NOTIFIKASI']};{$data['APPROVE_BY']};{$data['ID_MPE']}", 'e');
                $n_form = str_replace('/', '6s4', $n_form);
                $n_form = str_replace('+', '1p4', $n_form);
                $n_form = str_replace('=', 'sM9', $n_form);
                $NOTIF_NO = $data['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                $to = $atasan->EMAIL;
                // $cc=array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                $subject = "Resend : APPROVAL ERF";
                $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                $this->send_mail($subject, $data, $atasan, $n_form, $to, $cc);

                $res_message = 'Email has been sent' . '-';
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function create_data() {
        if (isset($_POST) && isset($_POST['FUNCT_LOC']) && isset($_POST['NOTIFIKASI'])) {
            $param = $_POST;
            // $sap_result = $this->create_notif($param);
            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // echo "<pre>";
            // print_r($this->session);
            // print_r($info);
            // echo "</pre>";
            // exit;
            // $atasan = $this->dbhris->get_level_from($info['no_badge'], 'BIRO');
            // if (!$atasan){
            // 	$atasan->NOBADGE='null';
            // 	$atasan->NAMA='null';
            // 	$atasan->EMAIL='null';
            // }
            // $tmpname=explode("@",$atasan->NAMA);
            // $username=$tmpname[0];
            // $param['NO_PENGAJUAN'] = $this->generateNo('NO_PENGAJUAN','MPE_PENGAJUAN', 'EM', $param['NO_PENGAJUAN']);
            // $param['STATUS'] = 'Open';
            $param['CREATE_BY'] = $info['name'];
            // $param['APPROVE_BY'] = $username;
            $param['COMPANY'] = $info['company'];
            $param['DEPT_CODE'] = $info['dept_code'];
            $param['DEPT_TEXT'] = $info['dept_text'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            // if(isset($_FILES['STRPATH'])){
            //   $url_files = $this->upload($_FILES['STRPATH'], (object)$info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
            //   $param['FILES'] = $url_files;
            //   $this->db->set('FILES_AT', "CURRENT_DATE", false);
            // }
            $result = $this->req->save($param);


            if ($result) {

                // $n_form=$this->my_crypt("{$param['NO_PENGAJUAN']};{$param['NOTIFIKASI']};{$atasan->NAMA};{$param['ID_MPE']}",'e');
                // $n_form = str_replace('/', '6s4', $n_form);
                // $n_form =  str_replace('+', '1p4', $n_form);
                // $n_form =  str_replace('=', 'sM9', $n_form);
                // $NOTIF_NO=$param['NOTIFIKASI'];
                // $to=array($atasan->EMAIL);
                // $to=$atasan->EMAIL;
                // $cc=array();
                // $cc=array('INDRA.NOFIANDI@SEMENINDONESIA.COM');
                //$cc=array('adhiq.rachmadhuha@sisi.id,febry.k@sisi.id');
                // $subject="APPROVAL ERF";
                // $data['SENDER'] = "{$info['no_badge']} - {$info['name']}";
                // $this->send_mail($subject, $param, $atasan, $n_form, $to, $cc);

                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function revision() {
        // error_reporting(1);

        $this->load->model('sap/M_updatenotif', 'sap_upd');

        if (isset($_POST) && isset($_POST['ID_MPE']) && isset($_POST['FUNCT_LOC']) && isset($_POST['NO_PENGAJUAN'])) {
            $param = $_POST;

            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $this->db->where('NOTIFIKASI', $param['NOTIFIKASI']);
            $this->db->where('REV_NO', ((int) $param['REV_NO'] - 1));
            // echo $_POST['ID_MPE'];
            $dtReq = $this->req->get_datarequest($_POST['ID_MPE']);
            $dtReq = $dtReq[0];
            // echo $this->db->last_query();
            // print_r($dtReq);
            $dtReq['REV_NO'] = ((int) $param['REV_NO'] - 1);
            $dtReq['STATE'] = 'Revised';
            $dtReq['UPDATE_BY'] = $info['name'];
            unset($dtReq['DESCMPLANT']);
            unset($dtReq['DESCPSECTION']);
            unset($dtReq['DESCPLANT']);
            unset($dtReq['DESCPGROUP']);
            unset($dtReq['OBJNR']);
            unset($dtReq['FL_TEXT']);
            unset($dtReq['UPDATE_AT']);
            $result = $this->req->updateData('revision', $dtReq);

            $param['STATUS'] = $dtReq['STATUS'];
            $param['COMPANY'] = $dtReq['COMPANY'];
            $param['CREATE_AT'] = $dtReq['CREATE_AT'];
            $param['CREATE_BY'] = $dtReq['CREATE_BY'];
            $param['APPROVE_AT'] = $dtReq['APPROVE_AT'];
            $param['APPROVE_BY'] = $dtReq['APPROVE_BY'];
            $param['APPROVE_JAB'] = $dtReq['APPROVE_JAB'];
            $param['UPDATE_BY'] = $info['name'];
            $param['UK_CODE'] = $info['uk_kode'];
            $param['UK_TEXT'] = $info['unit_kerja'];
            if (isset($_FILES['STRPATH'])) {
                $url_files = $this->upload($_FILES['STRPATH'], (object) $info, $param['NO_PENGAJUAN'], 'erf', $param['NOTIFIKASI']);
                $param['FILES'] = $url_files;
                $this->db->set('FILES_AT', "CURRENT_DATE", false);
            }

            // update sap
            // $param['USERNAME'] = $info['username'];
            // $res_sap = $this->sap_upd->updatenotif($param);
            // unset($param['USERNAME']);
            // echo "<pre>";
            // print_r($param);
            // print_r($res_sap);
            // echo "</pre>";
            // exit;
            // if (!$res_sap) {
            // 	// code...
            // 	exit;
            // }
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $result = $this->req->save($param);

            if ($result) {
                $res_message = 'Data has been saved' . '-';
                if ($result != 1) {
                    $res_message = 'Failed to save data. Please check input parameter!';
                }
                echo $this->response('success', 200, $res_message);
            } else {
                echo $this->response('error', 400);
            }
        } else {
            echo $this->response('error', 400);
        }
    }

    public function delete($id) {
        if ($id) {
            $info = $this->session->userdata;
            $info = $info['logged_in'];

            $data['id'] = $id;
            $data['DELETE_BY'] = $info['username'];
            // get data notifikasi
            $query = $this->req->get_datarequest($id);
            // delete data
            $result = $this->req->updateData('delete', $data);

            if ($result) {

                $param = array(
                    'STATUS' => 'DEL',
                    'NOTIF_NO' => $query[0]['NOTIFIKASI']
                );
                $del_sap = $this->sap_del->deletenotif($param);
                echo $this->response('success', 200, $result);
            } else {
                echo $this->response('error', 400);
            }
        }
    }

    public function approve() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            $nama = $_POST['nama'];
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');

            // $no_form= $this->my_crypt($e,'d');
            list($no_form, $notifikasi, $nama) = split(";", $this->my_crypt($e, 'd'));
            $data['no_form'] = $no_form;
            // $data['APPROVE_BY']=$nama;
            $data['STATUS'] = 'Approved';
            $this->db->set('NOTE', $reason);
            $result = $this->req->updateData('approve', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                $param = array(
                    'STATUS' => 'REL',
                    'NOTIF_NO' => $notifikasi
                );
                $del_sap = $this->sap_del->deletenotif($param);

                // echo $this->response('success', 200, $result);
                // echo "ERF '{$no_form}' was approved succesfully";
                // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function reject() {
        $this->load->model('sap/M_deletenotif', 'sap_del');
        // echo $e;
        if (isset($_POST) && isset($_POST['e']) && isset($_POST['reason'])) {
            $e = $_POST['e'];
            $reason = $_POST['reason'];
            $nama = $_POST['nama'];
            $e = str_replace('6s4', '/', $e);
            $e = str_replace('1p4', '+', $e);
            $e = str_replace('sM9', '=', $e);
            $no_form = $this->my_crypt($e, 'd');
            // $no_form= $this->my_crypt($e,'d');
            list($no_form, $notifikasi, $nama) = split(";", $this->my_crypt($e, 'd'));
            $data['no_form'] = $no_form;
            // $data['APPROVE_BY']=$nama;
            $data['STATUS'] = 'Rejected';
            $this->db->set('NOTE', $reason);
            $this->db->set('REJECT_BY', $nama);
            $result = $this->req->updateData('reject', $data);

            if ($result) {
                // $ar_form = explode(" - ",$no_form);
                $param = array(
                    'STATUS' => 'DEL',
                    'NOTIF_NO' => $no_notif
                );
                $del_sap = $this->sap_del->deletenotif($param);

                // echo $this->response('success', 200, $result);
                // echo "ERF '{$no_form}' was approved succesfully";
                // echo "ERF '{$no_form}' ({$this->my_crypt('letmein','e')}) was approved succesfully";
                $result2['message'] = 'success';
                $result2['status'] = '200';
                echo json_encode($result2);
            } else {
                $result2['message'] = 'fail';
                $result2['status'] = '400';
                echo json_encode($result2);
            }
        }
    }

    public function getApproval() {
        $id = $this->input->post('id');
        $data = $this->req->getCheckApprove($id);
        // echo $this->db->last_query();
        $return = json_encode($data);
        echo $return;
    }

    public function getLogPengajuan() {
        $id = $this->input->post('id');
        $data = $this->req->getLogPengajuan($id);
        // echo $this->db->last_query();
        $return = json_encode($data);
        echo $return;
    }

    public function getLogPenugasan() {
        $id = $this->input->post('id');
        $data = $this->req->getLogPenugasan($id);
        $return = json_encode($data);
        echo $return;
    }

    public function export_pdf($id) {
        if ($id) {
            error_reporting(0);

            $info = $this->session->userdata;
            $info = $info['logged_in'];
            // echo "<pre>";
            // print_r($info);
            // echo "</pre>";
            // exit;
            // $list_data = $_POST;
            $list_data = $this->req->get_datarequest($id);
            $list_data = $list_data[0];

            $uri = base_url();
            $this->load->library('Fpdf_gen');
            $this->fpdf->SetFont('Arial', 'B', 7);
            $this->fpdf->SetLeftMargin(20);
            $this->fpdf->SetRightMargin(20);

            $this->fpdf->Cell(12, 4, '', 0, 0);
            $this->fpdf->Cell(50, 10, $this->fpdf->Image($uri . 'media/logo/Logo_SI.png', $this->fpdf->GetX(), $this->fpdf->GetY(), 0, 18, 'PNG'), 0, 1, 'C');
            $this->fpdf->Cell(170, 1, 'R/538/012', 0, 1, 'R');
            $this->fpdf->Cell(0, 9, '', 0, 1);
            $this->fpdf->SetFont('Arial', 'B', 7);
            $this->fpdf->Cell(64, 1, 'PT. SEMEN INDONESIA (PERSERO)Tbk.', 0, 0);

            // HEADER
            $this->fpdf->Ln(5);
            $this->fpdf->SetFont('Arial', 'B', 16);
            $this->fpdf->Cell(170, 7, "ENGINEERING REQUEST FORM", 0, 1, 'C');

            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "General Information", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Notifikasi", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['NOTIFIKASI']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Nama Pekerjaan", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['NAMA_PEKERJAAN']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Unit Kerja Peminta", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['UK_TEXT']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Kode Unit Kerja", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['UK_CODE']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  K.A", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['DESCPSECTION']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Lokasi", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['LOKASI']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Kode WBS", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['WBS_CODE']}", 1, 1);

            $this->fpdf->SetFont('Arial', 'B', 10);
            $this->fpdf->Cell(50, 5, "  Deskripsi WBS", 1, 0);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(120, 5, "{$list_data['WBS_TEXT']}", 1, 1);

            // CONTENT
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Latar Belakang", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['LATAR_BELAKANG']}", 1);

            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Customer Request", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['CUST_REQ']}", 1);

            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Technical Information", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['TECH_INFO']}", 1);

            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Resiko & Mitigasi", 1, 1, 'C');
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->MultiCell(170, 5, "{$list_data['RES_MIT']}", 1);

            // FOOTER

            $tmpCre = FCPATH . "media/upload/tmp/" . md5("ERF-{$list_data['ID_MPE']}-{$list_data['NOTIFIKASI']}-CREATE") . ".png";
            $tmpApp = FCPATH . "media/upload/tmp/" . md5("ERF-{$list_data['ID_MPE']}-{$list_data['NOTIFIKASI']}-APPROVE") . ".png";
            // generating
            include(APPPATH . 'libraries/phpqrcode/qrlib.php');
            $str = $this->my_crypt("{$list_data['ID_MPE']};{$list_data['CREATE_BY']};{$list_data['CREATE_AT']};", 'e');
            $str = str_replace('/', '6s4', $str);
            $str = str_replace('+', '1p4', $str);
            $str = str_replace('=', 'sM9', $str);
            $str = base_url() . "info/erf/" . $str;
            QRcode::png($str, $tmpCre);
            if ($list_data['APPROVE_AT']) {
                $strApp = $this->my_crypt("{$list_data['ID_MPE']};{$list_data['APPROVE_BY']};{$list_data['APPROVE_AT']};", 'e');
                $strApp = str_replace('/', '6s4', $strApp);
                $strApp = str_replace('+', '1p4', $strApp);
                $strApp = str_replace('=', 'sM9', $strApp);
                $strApp = base_url() . "info/erf/" . $strApp;
                QRcode::png($strApp, $tmpApp);
                if (!file_exists($tmpApp)) {
                    echo 'We cannot generated a QR code, please back and try again later!';
                    echo '<hr />';
                    exit;
                }
                if (!file_exists($tmpCre)) {
                    echo 'We cannot generated a QR code, please back and try again later!';
                    echo '<hr />';
                    exit;
                }
            }
            $this->fpdf->Ln(3);
            $this->fpdf->SetFont('Arial', 'B', 14);
            $this->fpdf->Cell(170, 7, "Persetujuan", 1, 1, 'C');
            // $this->fpdf->Ln(5);
            // $this->fpdf->Cell(170,3,"",'LTR',1);
            $this->fpdf->SetFont('Arial', 'U', 10);
            $this->fpdf->Cell(85, 7, "Dibuat Oleh :", 'LR', 0, 'C');
            $this->fpdf->Cell(85, 7, "Disetujui Oleh :", 'LR', 1, 'C');
            $this->fpdf->Cell(85, 25, $this->fpdf->Image($tmpCre, $this->fpdf->GetX() + 30, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LR', 0, 'C');
            if ($list_data['APPROVE_AT']) {
                $this->fpdf->Cell(85, 25, $this->fpdf->Image($tmpApp, $this->fpdf->GetX() + 30, $this->fpdf->GetY(), 0, 25, 'PNG'), 'LR', 1, 'C');
            } else {
                $this->fpdf->Cell(85, 25, "", 'LR', 1);
            }
            $this->fpdf->SetFont('Arial', 'BU', 10);
            $this->fpdf->Cell(85, 5, "{$info['name']}", 'LR', 0, 'C');
            $this->fpdf->Cell(85, 5, "{$list_data['APPROVE_BY']}", 'LR', 1, 'C');
            // $this->fpdf->MultiCell(170,5,"{$list_data['APPROVE2_BY']}",1);
            $this->fpdf->SetFont('Arial', '', 10);
            $this->fpdf->Cell(85, 7, "{$info['JAB_TEXT']} PT Semen Indonesia", 'LBR', 0, 'C');
            $this->fpdf->Cell(85, 7, "{$list_data['APPROVE2_JAB']} PT Semen Indonesia", 'LBR', 1, 'C');
            $this->fpdf->SetFont('Arial', '', 8);
            $this->fpdf->Cell(170, 7, "* dokumen ini di approve oleh sistem e-DEMS.", 0, 0);

            // FILENAME
            $tmpPath = "{$list_data['COMPANY']}-{$list_data['NOTIFIKASI']} {$list_data['LOKASI']}.pdf";
            $this->fpdf->Output($tmpPath, 'D');
        } else {
            echo "File Not Found!";
        }
    }

    function getKDLOC() {
        $json = $this->req->getLocCode();
        if ($json) {
            echo $this->response('success', 200, $json);
        } else {
            echo $this->response('error', 400);
        }
    }

    function getKDArea() {
        $nama = '';
        if (isset($_POST['search'])) {
            $nama = $_POST['search'];
        }

        $result = $this->req->getAreaCode($nama);
        // echo $this->dbhris->last_query();
        if ($result) {
            echo $this->response('success', 200, $result);
        } else {
            echo $this->response('error', 400);
        }
    }

    function generateNoDoc() {
        $yyyy = date("Y");
        $xxxx = $this->req->getCount();
        $kodemax = str_pad($xxxx, 3, "0", STR_PAD_LEFT);
        $data = array("tahun" => $yyyy, "nomor" => $kodemax);
        echo json_encode($data);
    }

}
