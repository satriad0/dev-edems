<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
  protected $uk_parent='';
 	public function __construct(){
 		parent::__construct();
    $this->load->model('M_hris', 'dbhris');
 		// $this->load->model("General_model", "gen");
 		$this->load->model("others/M_roles", "roles");
 	}

	public function index()
	{
    $session = $this->session->userdata('logged_in');
    $info = (array)$session;
    if(empty($info['username'])){
     redirect('login');
    }

		$header["tittle"] = "Roles";
		$header["short_tittle"] = "rls";
    // view roles
    $this->db->distinct();
    $header['roles'] = $this->general_model->get_role_result("G.GEN_TYPE='FORM_GROUP' AND G.GEN_VAL='1' AND RD.ROLE_ID='{$info['role_id']}'");
    $header['roles']	= json_decode(json_encode($header['roles']), true);
    $state='';
    foreach ($header['roles'] as $i => $val) {
      $tmpaction = $this->general_model->get_action_result(" AND RD.ROLE_ID='{$info['role_id']}'", "G.GEN_TYPE='FORM_ACTION' AND G.GEN_VAL='1' AND G.GEN_PAR2='{$val['code']}'");
      $header['roles'][$i]['action'] = json_decode(json_encode($tmpaction), true);
      if ($val['code']==$header["short_tittle"]) {
        // code...
        $state = $val['stat'];
      }
      // get count pending status
      if (isset($val['tabel'])) {
        $npending = 0;
        if ($info['special_access'] == 'true') {
          $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
        }
        else {
          if (strpos( $val['stat'], "-child" ) !== false) {
            if (strpos( $val['stat'], "{$val['code']}-child=1" ) !== false) {
              $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$info['uk_kode']}' AND GEN_TYPE='DEF_PGROUP')");
              $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans']);
            }
          }
          else {
            $npending = $this->general_model->get_count_pending($val['tabel'],$val['ref_tabel'],$val['ststrans'],$info['uk_kode']);
          }
        }
        $header['roles'][$i]['count'] = $npending;
      }
    }

    // set table action
    $where['GEN_VAL']='1';
    $where['GEN_TYPE']='FORM_GROUP';
    $this->db->order_by('CAST(GEN_PAR4 AS INTEGER) asc');
    $global = $this->general_model->get_data_result($where);
    // $data['forms'] = $global;
    $data['forms']	= json_decode(json_encode($global), true);
    foreach ($data['forms'] as $i => $val) {
      $this->db->order_by('CAST(GEN_PAR3 AS INTEGER) asc');
      $tmp = $this->general_model->get_data_result("GEN_TYPE='FORM_ACTION' AND GEN_VAL='1' AND GEN_PAR1='{$val['GEN_PAR1']}' AND GEN_PAR2='{$val['GEN_CODE']}'");
      // $tmp->REF_ID = $val['ID'];
      $data['forms'][$i]['ACTION'] = json_decode(json_encode($tmp), true);
    }
    // echo "<pre>";
    // print_r($header);
    // print_r($data['forms']);
    // echo "</pre>";
    // exit;

    $header['page_state'] = $state;

    $sHdr = '';
    if (isset($info['themes'])) {
      if ($info['themes']!='default') $sHdr = "_".$info['themes'];
    }else {
      unset($where);
      $where['GEN_CODE']='themes-app';
      $where['GEN_TYPE']='General';
      $global = (array)$this->general_model->get_data($where);
      if (isset($global) && $global['GEN_VAL']!='0') {
        $i_par = $global['GEN_VAL'];
        $sHdr = "_".$global['GEN_PAR'.$i_par];
      }
    }
    if (strpos($state, "{$header["short_tittle"]}-read=1") !== false) {
      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('others/roles', $data);
    }else {
      $header["tittle"] = "Forbidden";
      $header["short_tittle"] = "403";

      $this->load->view('general/header'.$sHdr, $header);
      $this->load->view('forbidden');
    }
		// $this->load->view('general/header', $header);
		// $this->load->view('others/roles', $data);
		$this->load->view('general/footer');
	}

	public function data_list(){

		$search	= $this->input->post('search');
		$order	= $this->input->post('order');

		$key	= array(
			'search'	=>	$search['value'],
			'ordCol'	=>	$order[0]['column'],
			'ordDir'	=>	$order[0]['dir'],
			'length'	=>	$this->input->post('length'),
			'start'		=>	$this->input->post('start')
		);

  	$data	= json_decode(json_encode($this->roles->get_data($key)), true);
    foreach ($data as $i => $value) {
      $data[$i]['DTL'] = $this->roles->get_foreign($value['ID']);
    }
    // echo $this->db->last_query();

		$return	= array(
			'draw'				=>	$this->input->post('draw'),
			'data'				=>	$data,
			'recordsFiltered'	=>	$this->roles->recFil($key),
			'recordsTotal'		=>	$this->roles->recTot($key)
		);

		echo json_encode($return);
  }

	public function foreign(){
		$result = $this->roles->get_foreign();

		if($result){
      echo $this->response('success', 200, $result);
    }else{
      echo $this->response('error', 400);
    }
	}

  public function create(){
		if(isset($_POST) && isset($_POST['NAME']) && isset($_POST['DESCRIPTION']) && isset($_POST['SPECIAL'])){
			$param = $_POST;

			$info = $this->session->userdata;
			$info = $info['logged_in'];

			$param['CREATE_BY'] = $info['name'];
      $dtl = json_decode($param['DTL'], TRUE);
      unset($param['DTL']);
			// $param['COMPANY'] = $info['company'];

			$result = $this->roles->save($param);
			// echo $this->db->last_query();

      // insert detail
      if (isset($dtl) && count($dtl)>0) {
        $data = array();
        foreach ($dtl as $key => $value) {
          $arID = explode('-',$value);
          $tmp['ROLE_ID']=$result['ID'];
          $tmp['FORM_ID']=$arID[0];
          $tmp['PERMISSION_ID']=$arID[1];
          // $tmp['FORM_ID']=$value;
          // $data[]=$tmp;
          array_push($data, $tmp);
        }
        $result = $this->roles->savedtl($data);
      }

			if($result){

				$res_message = 'Data has been saved'. '-';
				// if(!$result['ID']){
				// // if($result != 1){
				// 	$res_message ='Failed to save data. Please check input parameter!';
				// }
				echo $this->response('success', 200, $res_message);
			}else {
				echo $this->response('error', 400);
			}
		}else {
			echo $this->response('error', 400);
		}
  }

  public function update(){
		if(isset($_POST) && isset($_POST['ID']) && isset($_POST['NAME']) && isset($_POST['DESCRIPTION']) && isset($_POST['SPECIAL'])){
			$param = $_POST;

			$info = $this->session->userdata;
			$info = $info['logged_in'];

			$param['UPDATE_BY'] = $info['username'];
      $dtl = json_decode($param['DTL'], TRUE);
      unset($param['DTL']);

			$result = $this->roles->updateData('update', $param);
			// echo $this->db->last_query();

      // insert detail
      if (isset($dtl) && count($dtl)>0) {
        $data = array();
        foreach ($dtl as $key => $value) {
          $arID = explode('-',$value);
          $tmp['ROLE_ID']=$param['ID'];
          $tmp['FORM_ID']=$arID[0];
          $tmp['PERMISSION_ID']=$arID[1];
          // $tmp['FORM_ID']=$value;
          // $data[]=$tmp;
          array_push($data, $tmp);
        }
        $result = $this->roles->savedtl($data);
      }

			if($result){

				$res_message = 'Data has been saved'. '-';
				if($result != 1){
					$res_message ='Failed to save data. Please check input parameter!';
				}
				echo $this->response('success', 200, $res_message);
			}else {
				echo $this->response('error', 400);
			}
		}else {
			echo $this->response('error', 400);
		}
  }

  public function delete($id){
		if($id){
			$info = $this->session->userdata;
			$info = $info['logged_in'];

			$data['ID']=$id;
			$data['DELETE_BY'] = $info['name'];
			$result = $this->roles->updateData('delete', $data);

			if($result){
	      echo $this->response('success', 200, $result);
	    }else{
	      echo $this->response('error', 400);
	    }
		}
  }



}
