<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 // require_once APPPATH . '/libraries/REST_Controller.php';

class Sap_deletenotif extends CI_Controller {

    function __construct(){
        parent::__construct();

        $this->load->model('sap/M_deletenotif', 'sapnotif');

    }

    public function delete($no_notif){
      if ($this->validasi($_POST['token'])) {
        $token = $_POST['token'];
        unset($_POST['token']);
        $payload = $this->payload($token)->data;
        if($no_notif){
          $param = array(
              'STATUS' => 'DEL',
              'NOTIF_NO' => $no_notif
          );
          $data = $this->sapnotif->deletenotif($param);

          $datanotif["data"] = $data;
        }
        echo json_encode($datanotif);
      }else {
        echo json_encode(array('data' => array('STATUS' => 'FAILED')));
      }
    }
    public function release($no_notif){
      if ($this->validasi($_POST['token'])) {
        $token = $_POST['token'];
        unset($_POST['token']);
        $payload = $this->payload($token)->data;
        if($no_notif){
          $param = array(
              'STATUS' => 'REL',
              'NOTIF_NO' => $no_notif
          );
          $data = $this->sapnotif->deletenotif($param);

          $datanotif["data"] = $data;
        }
        echo json_encode($datanotif);
      }else {
        echo json_encode(array('data' => array('STATUS' => 'FAILED')));
      }
    }
    public function complete($no_notif){
      if ($this->validasi($_POST['token'])) {
        $token = $_POST['token'];
        unset($_POST['token']);
        $payload = $this->payload($token)->data;
        if($no_notif){
          $param = array(
              'STATUS' => 'CMP',
              'NOTIF_NO' => $no_notif
          );
          $data = $this->sapnotif->deletenotif($param);

          $datanotif["data"] = $data;
        }
        echo json_encode($datanotif);
      }else {
        echo json_encode(array('data' => array('STATUS' => 'FAILED')));
      }
    }
}
