<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* Name:  FPDF
*
* Author: Jd Fiscus
* 	 	  jdfiscus@gmail.com
*         @iamfiscus
*
*
* Origin API Class: http://www.fpdf.org/
*
* Location: http://github.com/iamfiscus/Codeigniter-FPDF/
*
* Created:  06.22.2010
*
* Description:  This is a Codeigniter library which allows you to generate a PDF with the FPDF library
*
*/

class Fpdf_editing {

	public function __construct() {

		// require_once APPPATH.'third_party/fpdf/fpdf-1.8.php';
    //
		// $pdf = new FPDF();
		// $pdf->AddPage();
    //
		// $CI =& get_instance();
		// $CI->fpdf = $pdf;

    // require_once('fpdf.php');
    // require_once('fpdi.php');
    require_once APPPATH.'third_party/fpdf/fpdf-1.8.php';

    function generatePDF($source, $output, $text, $image) {

    $pdf = new FPDI('Portrait','mm',array(215.9,279.4)); // Array sets the X, Y dimensions in mm
    $pdf->AddPage();
    $pagecount = $pdf->setSourceFile($source);
    $tppl = $pdf->importPage(1);

    $pdf->useTemplate($tppl, 0, 0, 0, 0);

    $pdf->Image($image,10,10,50,50); // X start, Y start, X width, Y width in mm

    $pdf->SetFont('Helvetica','',10); // Font Name, Font Style (eg. 'B' for Bold), Font Size
    $pdf->SetTextColor(0,0,0); // RGB
    $pdf->SetXY(51.5, 57); // X start, Y start in mm
    $pdf->Write(0, $text);

    $pdf->Output($output, "F");
    }

	}

}
