<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_hris extends CI_Model {

    private $conn = NULL;

    function __construct() {
        parent::__construct();
    }

    public function get_hris_where($str = '') {
        $swhere = '';
        if ($str) {
            $swhere = ' AND ' . $str;
        }

        $conn = $this->load->database('hris', TRUE);
        // $sql = "
        //         SELECT
        //             k.mk_nopeg AS NOBADGE,
        //             k.mk_nama AS NAMA,
        //             k.company AS COMPANY,
        //             k.mk_email AS EMAIL,
        //             k.mjab_kode AS JAB_CODE,
        //             j.mjab_nama AS JAB_TEXT,
        //             k.mdept_kode AS DEPT_CODE,
        //             uk2.muk_nama AS DEPT_TEXT,
        //             k.muk_kode AS UNIT_KERJA,
        //             uk.muk_nama AS UK_TEXT,
        //             k.mk_cctr AS COST_CENTER,
        //             k.mk_cctr_text AS CC_TEXT,
        //             k.mk_emp_group AS ACTIVE,
        //             k.mk_emp_subgroup AS POSITION,
        //             k.mk_emp_subgroup_text AS POS_TEXT,
        //             k.lokasi AS LOKASI,
        //             k.mk_alamat_rumah AS ALAMAT,
        //             k.mk_tgl_masuk AS TGL_MASUK,
        //             k.mk_tgl_lahir AS TGL_LAHIR
        //         FROM
        //             hris.v_karyawan k
        //         LEFT JOIN hris.v_unit_kerja uk ON k.muk_kode = uk.muk_kode
        //         LEFT JOIN hris.v_unit_kerja uk2 ON k.mdept_kode = uk2.muk_kode
        //         JOIN hris.v_jabatan j ON j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()
        //         WHERE
        //             k.mk_emp_group = 1 AND k.mk_stat2_text LIKE '%active%' {$swhere}
        //         ";
        //
        // $query = $conn->query($sql);
        // echo $sql;

        $conn->select("
            k.mk_nopeg AS NOBADGE,
            k.mk_nama AS NAMA,
            k.company AS COMPANY,
            k.mk_email AS EMAIL,
            k.mjab_kode AS JAB_CODE,
            j.mjab_nama AS JAB_TEXT,
            k.mdept_kode AS DEPT_CODE,
            uk2.muk_nama AS DEPT_TEXT,
            k.muk_kode AS UNIT_KERJA,
            uk.muk_nama AS UK_TEXT,
            k.mk_cctr AS COST_CENTER,
            k.mk_cctr_text AS CC_TEXT,
            k.mk_emp_group AS ACTIVE,
            k.mk_emp_subgroup AS POSITION,
            k.mk_emp_subgroup_text AS POS_TEXT,
            k.lokasi AS LOKASI,
            k.mk_alamat_rumah AS ALAMAT,
            k.mk_tgl_masuk AS TGL_MASUK,
            k.mk_tgl_lahir AS TGL_LAHIR
            ");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->join("v_unit_kerja uk2", "k.mdept_kode = uk2.muk_kode", "left");
        $conn->join("v_jabatan j", "j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()", "left");
        $conn->where("k.mk_emp_group = 1 AND k.mk_stat2_text LIKE 'active%' {$swhere}");
        $query = $conn->get();
//        echo $conn->last_query();


        if ($query) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_user_hris_email($email) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                    k.mk_nopeg AS NOBADGE,
                    k.mk_nama AS NAMA,
                    k.company AS COMPANY,
                    k.mk_email AS EMAIL,
                    k.mdept_kode AS DEPT_CODE,
                    uk2.muk_nama AS DEPT_TEXT,
                    k.muk_kode AS UNIT_KERJA,
                    uk.muk_nama AS UK_TEXT,
                    k.mk_cctr AS COST_CENTER,
                    k.mk_cctr_text AS CC_TEXT,
                    k.mk_emp_group AS ACTIVE,
                    k.mk_emp_subgroup AS POSITION,
                    k.mk_emp_subgroup_text AS POS_TEXT,
                    k.lokasi AS LOKASI,
                    k.mk_alamat_rumah AS ALAMAT,
                    k.mk_tgl_masuk AS TGL_MASUK,
                    k.mk_tgl_lahir AS TGL_LAHIR
                FROM
                    hris.v_karyawan k
                JOIN hris.v_unit_kerja uk ON k.muk_kode = uk.muk_kode
                LEFT JOIN hris.v_unit_kerja uk2 ON k.mdept_kode = uk2.muk_kode
                WHERE
                    k.mk_email LIKE '%$email%'

                AND k.mk_emp_group = 1 AND k.mk_stat2_text LIKE '%active%'
        ";
        $query = $conn->query($sql);

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function get_user_hris($q, $eselon = '') {
        $conn = $this->load->database('hris', TRUE);

        $conn->select("
            k.mk_nopeg AS NOBADGE,
            k.mk_nama AS NAMA,
            k.company AS COMPANY,
            k.mk_email AS EMAIL,
            k.mjab_kode AS JAB_CODE,
            j.mjab_nama AS JAB_TEXT,
            k.mdept_kode AS DEPT_CODE,
            uk2.muk_nama AS DEPT_TEXT,
            k.muk_kode AS UNIT_KERJA,
            uk.muk_nama AS UK_TEXT,
            k.mk_cctr AS COST_CENTER,
            k.mk_cctr_text AS CC_TEXT,
            k.mk_emp_group AS ACTIVE,
            k.mk_emp_subgroup AS POSITION,
            k.mk_emp_subgroup_text AS POS_TEXT,
            k.lokasi AS LOKASI,
            k.mk_alamat_rumah AS ALAMAT,
            k.mk_tgl_masuk AS TGL_MASUK,
            k.mk_tgl_lahir AS TGL_LAHIR
            ");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->join("v_unit_kerja uk2", "k.mdept_kode = uk2.muk_kode", "left");
        $conn->join("v_jabatan j", "j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()", "left");
        $sAdd = "(
                LOWER(k.mk_nopeg) LIKE '%" . strtolower($q) . "%'
                OR LOWER(k.mk_nama) LIKE '%" . strtolower($q) . "%'
                OR LOWER(k.mk_email) LIKE '%" . strtolower($q) . "%'
                OR LOWER(j.mjab_nama) LIKE '%" . strtolower($q) . "%'
                )";
        $conn->where($sAdd, '', FALSE);
        if (isset($eselon) && !empty($eselon)) {
            $conn->where("k.mk_eselon_code {$eselon}", '', FALSE);
        }
        $conn->where("k.mk_emp_group = 1 AND k.mk_stat2_text LIKE 'active%'");
        $conn->limit(10);
        $query = $conn->get();

        // $sql = "
        //         SELECT
        //             k.mk_nopeg AS NOBADGE,
        //             k.mk_nama AS NAMA,
        //             k.company AS COMPANY,
        //             k.mk_email AS EMAIL,
        //             k.mdept_kode AS DEPT_CODE,
        //             uk2.muk_nama AS DEPT_TEXT,
        //             k.muk_kode AS UNIT_KERJA,
        //             uk.muk_nama AS UK_TEXT,
        //             k.mk_cctr AS COST_CENTER,
        //             k.mk_cctr_text AS CC_TEXT,
        //             k.mk_emp_group AS ACTIVE,
        //             k.mk_emp_subgroup AS POSITION,
        //             k.mk_emp_subgroup_text AS POS_TEXT,
        //             k.lokasi AS LOKASI,
        //             k.mk_alamat_rumah AS ALAMAT,
        //             k.mk_tgl_masuk AS TGL_MASUK,
        //             k.mk_tgl_lahir AS TGL_LAHIR
        //         FROM
        //             hris.v_karyawan k
        //         JOIN hris.v_unit_kerja uk ON k.muk_kode = uk.muk_kode
        //         LEFT JOIN hris.v_unit_kerja uk2 ON k.mdept_kode = uk2.muk_kode
        //         WHERE
        //             (k.mk_nopeg LIKE '%$nobadge%' OR k.mk_nama LIKE '%$nobadge%')
        //         AND k.company LIKE '%$company%'
        //         AND k.muk_kode LIKE '%$uk%'
        //         AND k.mk_emp_group = $active AND k.mk_stat2_text LIKE '%active%'
        // ";
        // // echo "$sql";
        // $query = $conn->query($sql);
        
        if ($query) {
            return $query->result();
        } else {
            return NULL;
        }
    }

    public function get_level_from($nobadge, $level, $active = 1, $empgroup = 3) {
        $conn = $this->load->database('hris', TRUE);
        // $sql = "
        //         SELECT
        //           k.mk_nopeg AS NOBADGE,
        //           k.mk_nama AS NAMA,
        //           k.company AS COMPANY,
        //           k.company_text AS COMP_TEXT,
        //           k.mk_email AS EMAIL,
        //           k.mdept_kode AS DEPT_CODE,
        //           uk2.muk_nama AS DEPT_TEXT,
        //           k.muk_kode AS UNIT_KERJA,
        //           uk.muk_nama AS UK_TEXT,
        //           k.mk_cctr AS COST_CENTER,
        //           k.mk_cctr_text AS CC_TEXT,
        //           k.mk_emp_group AS ACTIVE,
        //         	k.mjab_kode AS JAB_CODE,
        //         	j.mjab_nama AS JAB_TEXT,
        //           k.mk_emp_subgroup AS POSITION,
        //           k.mk_emp_subgroup_text AS POS_TEXT,
        //           k.lokasi AS LOKASI,
        //           k.mk_alamat_rumah AS ALAMAT,
        //           k.mk_tgl_masuk AS TGL_MASUK,
        //           k.mk_tgl_lahir AS TGL_LAHIR
        //         FROM
        //           v_karyawan k
        //         LEFT JOIN v_unit_kerja uk ON k.muk_kode = uk.muk_kode
        //         LEFT JOIN v_unit_kerja uk2 ON k.mdept_kode = uk2.muk_kode
        //         LEFT JOIN v_jabatan j ON j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()
        //         WHERE
        //           mk_nopeg IN (
        //             SELECT
        //               (
        //                 CASE atasan1_level
        //                 WHEN '$level' THEN
        //                   atasan1_nopeg
        //                 ELSE
        //                   atasan2_nopeg
        //                 END
        //               ) no_badge
        //             FROM
        //               v_atasan
        //             WHERE
        //               mk_nopeg IN (
        //                 '$nobadge'
        //               )
        //             AND (
        //               atasan1_level = '$level'
        //               OR atasan2_level = '$level'
        //             )
        //           )
        //         AND k.mk_emp_group = $active AND k.mk_stat2_text LIKE '%active%'
        //         ";

        $kd_level = "";
        if ($level == 'DEPT') {
            $kd_level = 'mdept_kode';
        } elseif ($level == 'BIRO')
            $kd_level = 'mbiro_kode';
        elseif ($level == 'SECT')
            $kd_level = 'msect_kode';
        else
            $kd_level = 'mgrp_kode';

        // $empgroup = 3;
        if ($level == 'BIRO') {
            $empgroup = 2;
        } elseif ($level == 'DEPT')
            $empgroup = 1;
        else
            $empgroup -= 1;

        $conn->select("
                      k.mk_nopeg AS NOBADGE,
                      k.mk_nama AS NAMA,
                      k.company AS COMPANY,
                      k.company_text AS COMP_TEXT,
                      k.mk_email AS EMAIL,
                      k.mdept_kode AS DEPT_CODE,
                      uk2.muk_nama AS DEPT_TEXT,
                      k.muk_kode AS UNIT_KERJA,
                      uk.muk_nama AS UK_TEXT,
                      k.mk_cctr AS COST_CENTER,
                      k.mk_cctr_text AS CC_TEXT,
                      k.mk_emp_group AS ACTIVE,
                      k.mjab_kode AS JAB_CODE,
                      j.mjab_nama AS JAB_TEXT,
                      k.mk_emp_subgroup AS POSITION,
                      k.mk_emp_subgroup_text AS POS_TEXT,
                      k.lokasi AS LOKASI,
                      k.mk_alamat_rumah AS ALAMAT,
                      k.mk_tgl_masuk AS TGL_MASUK,
                      k.mk_tgl_lahir AS TGL_LAHIR");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->join("v_unit_kerja uk2", "k.mdept_kode = uk2.muk_kode", "left");
        $conn->join("v_jabatan j", "j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()", "left");
        // $conn->where("k.mdept_kode = (SELECT mdept_kode FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        // $conn->where("k.{$kd_level} = (SELECT {$kd_level} FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        $conn->where("(k.mk_eselon_code LIKE '{$empgroup}%' OR (k.mk_nopeg in (SELECT atasan1_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}' AND atasan1_level='REPT') OR k.mk_nopeg in (SELECT atasan2_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}' AND atasan1_level='REPT')))", '', FALSE);
        $conn->where("k.mk_emp_group = 1");
        $conn->where("(k.mk_nopeg in (SELECT atasan1_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}') OR k.mk_nopeg in (SELECT atasan2_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}'))");
        $conn->where("k.mk_stat2_text LIKE 'Active%'");

        $query = $conn->get()->row();
        // echo $conn->last_query();

        if ($query) {
            return $query;
        } else {
            $empgroup -= 1;
            if ($empgroup == 1)
                $level = 'DEPT';
            elseif ($empgroup == 2)
                $level = 'BIRO';
            // echo $empgroup;
            // echo "<br />";
            // echo $level;
            // exit;
            if ($empgroup > 0) {
                $query2 = $this->get_level_from($nobadge, $level, 1, $empgroup);
                if ($query2) {
                    return $query2;
                } else {
                    $kosong = new stdClass();
                    $kosong->NOBADGE = 'null';
                    $kosong->NAMA = 'null';
                    $kosong->EMAIL = 'null';
                    return $kosong;
                }
            } else {
                $kosong = new stdClass();
                $kosong->NOBADGE = 'null';
                $kosong->NAMA = 'null';
                $kosong->EMAIL = 'null';
                return $kosong;
            }
        }
    }

    public function get_level_erf($nobadge, $level, $active = 1, $empgroup = 3) {
        $conn = $this->load->database('hris', TRUE);

        $kd_level = "";
        if ($level == 'DEPT') {
            $kd_level = 'mdept_kode';
        } elseif ($level == 'BIRO')
            $kd_level = 'mbiro_kode';
        elseif ($level == 'SECT')
            $kd_level = 'msect_kode';
        else
            $kd_level = 'mgrp_kode';

        // $empgroup = 3;
        if ($level == 'BIRO') {
            $empgroup = 2;
        } elseif ($level == 'DEPT')
            $empgroup = 1;
        else
            $empgroup -= 1;

        $conn->select("
                      k.mk_nopeg AS NOBADGE,
                      k.mk_nama AS NAMA,
                      k.company AS COMPANY,
                      k.company_text AS COMP_TEXT,
                      k.mk_email AS EMAIL,
                      k.mdept_kode AS DEPT_CODE,
                      uk2.muk_nama AS DEPT_TEXT,
                      k.muk_kode AS UNIT_KERJA,
                      uk.muk_nama AS UK_TEXT,
                      k.mk_cctr AS COST_CENTER,
                      k.mk_cctr_text AS CC_TEXT,
                      k.mk_emp_group AS ACTIVE,
                      k.mjab_kode AS JAB_CODE,
                      j.mjab_nama AS JAB_TEXT,
                      k.mk_emp_subgroup AS POSITION,
                      k.mk_emp_subgroup_text AS POS_TEXT,
                      k.lokasi AS LOKASI,
                      k.mk_alamat_rumah AS ALAMAT,
                      k.mk_tgl_masuk AS TGL_MASUK,
                      k.mk_tgl_lahir AS TGL_LAHIR");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->join("v_unit_kerja uk2", "k.mdept_kode = uk2.muk_kode", "left");
        $conn->join("v_jabatan j", "j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()", "left");
        // $conn->where("k.mdept_kode = (SELECT mdept_kode FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        // $conn->where("k.{$kd_level} = (SELECT {$kd_level} FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        // $conn->where("k.mk_eselon_code LIKE '{$empgroup}%'");
        $conn->where("k.mk_emp_group = 1");
        $conn->where("(k.mk_nopeg in (SELECT atasan1_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}' AND atasan1_level='{$level}') OR k.mk_nopeg in (SELECT atasan2_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}' AND atasan2_level='{$level}'))");
        $conn->where("k.mk_stat2_text LIKE 'Active%'");

        $query = $conn->get()->row();
        // echo $conn->last_query();

        if ($query) {
            return $query;
        } else {
            $empgroup -= 1;
            if ($empgroup == 1)
                $level = 'DEPT';
            elseif ($empgroup == 2)
                $level = 'BIRO';
            // echo $empgroup;
            // echo "<br />";
            // echo $level;
            // exit;
            if ($empgroup > 0) {
                $query2 = $this->get_level_from($nobadge, $level, 1, $empgroup);
                if ($query2) {
                    return $query2;
                } else {
                    $kosong = new stdClass();
                    $kosong->NOBADGE = 'null';
                    $kosong->NAMA = 'null';
                    $kosong->EMAIL = 'null';
                    return $kosong;
                }
            } else {
                $kosong = new stdClass();
                $kosong->NOBADGE = 'null';
                $kosong->NAMA = 'null';
                $kosong->EMAIL = 'null';
                return $kosong;
            }
        }
    }

    public function get_level_from_array($nobadge, $level, $active = 1, $empgroup = 3) {
        $conn = $this->load->database('hris', TRUE);

        $kd_level = "";
        if ($level == 'DEPT') {
            $kd_level = 'mdept_kode';
        } elseif ($level == 'BIRO')
            $kd_level = 'mbiro_kode';
        elseif ($level == 'SECT')
            $kd_level = 'msect_kode';
        else
            $kd_level = 'mgrp_kode';

        // $empgroup = 3;
        if ($level == 'BIRO') {
            $empgroup = 2;
        } elseif ($level == 'DEPT')
            $empgroup = 1;
        else
            $empgroup -= 1;

        $conn->select("
                      k.mk_nopeg AS NOBADGE,
                      k.mk_nama AS NAMA,
                      k.company AS COMPANY,
                      k.company_text AS COMP_TEXT,
                      k.mk_email AS EMAIL,
                      k.mdept_kode AS DEPT_CODE,
                      uk2.muk_nama AS DEPT_TEXT,
                      k.muk_kode AS UNIT_KERJA,
                      uk.muk_nama AS UK_TEXT,
                      k.mk_cctr AS COST_CENTER,
                      k.mk_cctr_text AS CC_TEXT,
                      k.mk_emp_group AS ACTIVE,
                      k.mjab_kode AS JAB_CODE,
                      j.mjab_nama AS JAB_TEXT,
                      k.mk_emp_subgroup AS POSITION,
                      k.mk_emp_subgroup_text AS POS_TEXT,
                      k.lokasi AS LOKASI,
                      k.mk_alamat_rumah AS ALAMAT,
                      k.mk_tgl_masuk AS TGL_MASUK,
                      k.mk_tgl_lahir AS TGL_LAHIR");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->join("v_unit_kerja uk2", "k.mdept_kode = uk2.muk_kode", "left");
        $conn->join("v_jabatan j", "j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()", "left");
        // $conn->where("k.mdept_kode = (SELECT mdept_kode FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        // $conn->where("k.{$kd_level} = (SELECT {$kd_level} FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        $conn->where("k.mk_eselon_code LIKE '{$empgroup}%'");
        $conn->where("k.mk_emp_group = 1");
        $conn->where("(k.mk_nopeg in (SELECT atasan1_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}') OR k.mk_nopeg in (SELECT atasan2_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}'))");
        $conn->where("k.mk_stat2_text LIKE 'Active%'");

        $query = $conn->get()->result();
        // echo $conn->last_query()."<br />"."<br />";

        if ($query) {
            return $query;
        } else {
            $empgroup -= 1;
            if ($empgroup == 1)
                $level = 'DEPT';
            elseif ($empgroup == 2)
                $level = 'BIRO';
            // echo $empgroup;
            // echo "<br />";
            // echo $level;
            // exit;
            if ($empgroup > 0) {
                $query2 = $this->get_level_from_array($nobadge, $level, 1, $empgroup);
                if ($query2) {
                    return $query2;
                } else {
                    return array();
                }
            } else {
                return array();
            }
            // $query2 = $this->get_level_from_array($nobadge, $level, 1, $empgroup);
            // if ($query2) {
            //   return $query2;
            // }else{
            //   return NULL;
            // }
        }
    }

    public function get_level_from_dept($nobadge, $active = 1, $empgroup = 3) {
        $conn = $this->load->database('hris', TRUE);
        // $empgroup = 2;
        $conn->select("
                      k.mk_nopeg AS NOBADGE,
                      k.mk_nama AS NAMA,
                      k.company AS COMPANY,
                      k.company_text AS COMP_TEXT,
                      k.mk_email AS EMAIL,
                      k.mdept_kode AS DEPT_CODE,
                      uk2.muk_nama AS DEPT_TEXT,
                      k.muk_kode AS UNIT_KERJA,
                      uk.muk_nama AS UK_TEXT,
                      k.mk_cctr AS COST_CENTER,
                      k.mk_cctr_text AS CC_TEXT,
                      k.mk_emp_group AS ACTIVE,
                      k.mjab_kode AS JAB_CODE,
                      j.mjab_nama AS JAB_TEXT,
                      k.mk_emp_subgroup AS POSITION,
                      k.mk_emp_subgroup_text AS POS_TEXT,
                      k.lokasi AS LOKASI,
                      k.mk_alamat_rumah AS ALAMAT,
                      k.mk_tgl_masuk AS TGL_MASUK,
                      k.mk_tgl_lahir AS TGL_LAHIR");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->join("v_unit_kerja uk2", "k.mdept_kode = uk2.muk_kode", "left");
        $conn->join("v_jabatan j", "j.mjab_kode = k.mjab_kode AND j.mjab_endda > NOW()", "left");
        // $conn->where("k.mdept_kode = (SELECT mdept_kode FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        // $conn->where("k.{$kd_level} = (SELECT {$kd_level} FROM v_karyawan WHERE mk_nopeg = '{$nobadge}')");
        // $conn->where("k.mk_eselon_code LIKE '{$empgroup}%'");
        $conn->where("k.mk_emp_group = 1");
        $conn->where("(k.mk_nopeg in (SELECT atasan1_nopeg FROM v_atasan WHERE mk_nopeg='{$nobadge}'))");
        $conn->where("k.mk_stat2_text LIKE 'Active%'");

        $query = $conn->get()->result();
        // echo $conn->last_query()."<br />"."<br />";

        if ($query) {
            return $query;
        } else {
            return NULL;
        }
    }

    public function get_list_uk_karyawan($tipe, $kode) {
        $conn = $this->load->database('hris', TRUE);
        // $empgroup = 2;
        $conn->distinct();
        $conn->select("k.muk_kode AS uk_code");
        $conn->from("v_karyawan k");
        $conn->join("v_unit_kerja uk", "k.muk_kode = uk.muk_kode", "left");
        $conn->where("k.mk_emp_group = 1");
        $conn->where("k.$tipe ='{$kode}'");
        $conn->where("k.mk_stat2_text LIKE 'Active%'");

        $query = $conn->get()->result_array();
        echo $conn->last_query() . "<br />" . "<br />";

        if ($query) {
            return $query;
        } else {
            return array();
        }
    }

    public function get_user_hris_comptext($nobadge, $company, $uk = '', $active = 1) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                    k.mk_nopeg AS NOBADGE,
                    k.mk_nama AS NAMA,
                    k.company AS COMPANY,
                    k.mk_email AS EMAIL,
                    k.muk_kode AS UNIT_KERJA,
                    uk.muk_nama AS UK_TEXT,
                    k.mk_cctr AS COST_CENTER,
                    k.mk_cctr_text AS CC_TEXT,
                    k.mk_emp_group AS ACTIVE,
                    k.mk_emp_subgroup AS POSITION,
                    k.mk_emp_subgroup_text AS POS_TEXT,
                    k.lokasi AS LOKASI,
                    k.mk_alamat_rumah AS ALAMAT,
                    k.mk_tgl_masuk AS TGL_MASUK,
                    k.mk_tgl_lahir AS TGL_LAHIR
                FROM
                    hris.v_karyawan k
                JOIN hris.v_unit_kerja uk ON k.muk_kode = uk.muk_kode
                WHERE
                    (k.mk_nopeg LIKE '%$nobadge%' OR k.mk_nama LIKE '%$nobadge%')
                AND k.company IN ($company)
                AND k.muk_kode LIKE '%$uk%'
                AND k.mk_emp_group = $active  AND k.mk_stat2_text LIKE '%active%' -- AND k.mk_stat2 LIKE '%$active%'
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function get_user_hris_comptextV2($nobadge, $company, $uk, $active = 1) {
        $conn = $this->load->database('hris', TRUE);

        $sql = "
                SELECT
                        k.mk_nopeg AS NOBADGE,
                        k.mk_nama AS NAMA,
                        k.company AS COMPANY,
                        k.mk_email AS EMAIL,
                        k.muk_kode AS UNIT_KERJA,
                        uk.muk_nama AS UK_TEXT,
                        k.mk_cctr AS COST_CENTER,
                        k.mk_cctr_text AS CC_TEXT,
                        k.mk_emp_group AS ACTIVE,
                        k.mk_emp_subgroup AS POSITION,
                        k.mk_emp_subgroup_text AS POS_TEXT,
                        k.lokasi AS LOKASI,
                        k.mk_alamat_rumah AS ALAMAT,
                        k.mk_tgl_masuk AS TGL_MASUK,
                        k.mk_tgl_lahir AS TGL_LAHIR
                FROM v_karyawan k
                INNER JOIN v_unit_kerja u ON k.muk_kode = uk.muk_kode
                WHERE a.mk_nopeg='$nobadge'
                AND k.mk_emp_group=$active
                AND k.muk_kode='$uk'
                AND (k.mk_nopeg='$nobadge' OR LOWER(k.mk_nama) LIKE '%$nobadge%')
                AND k.company IN ($company)
                AND k.mk_stat2_text LIKE '%active%'
                -- AND k.mk_stat2=$active
      ";
        // echo "$sql";
        $query = $conn->query($sql);

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function pegawai_hris($nobadge, $active = 1) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                    k.mk_nopeg AS NOBADGE,
                    k.mk_nama AS NAMA,
                    k.company AS COMPANY,
                    k.mk_email AS EMAIL,
                    k.muk_kode AS UNIT_KERJA,
                    uk.muk_nama AS UK_TEXT,
                    k.mk_cctr AS COST_CENTER,
                    k.mk_cctr_text AS CC_TEXT,
                    k.mk_emp_group AS ACTIVE,
                    k.mk_emp_subgroup AS POSITION,
                    k.mk_emp_subgroup_text AS POS_TEXT,
                    k.lokasi AS LOKASI,
                    k.mk_alamat_rumah AS ALAMAT,
                    k.mk_tgl_masuk AS TGL_MASUK,
                    k.mk_tgl_lahir AS TGL_LAHIR
                FROM
                    hris.v_karyawan k
                JOIN hris.v_unit_kerja uk ON k.muk_kode = uk.muk_kode
                WHERE
                    k.mk_nopeg = $nobadge
                    AND k.mk_emp_group = $active
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function get_pic_atas($nobadge) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                  SELECT a.mk_nopeg, k.mk_nama AS NAMA, a.atasan1_level AS LEVEL, a.atasan1_nopeg AS NO_BADGE, u.muk_level, k.mk_email AS EMAIL FROM v_atasan a
                  INNER JOIN v_karyawan k ON k.mk_nopeg=a.atasan1_nopeg
                  INNER JOIN v_unit_kerja u ON u.muk_level=a.atasan1_level  AND k.muk_kode=u.muk_kode
                  WHERE a.mk_nopeg='$nobadge'
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function get_user_hris_v2($nobadge, $company, $uk, $active = 1) {
        $conn = $this->load->database('hris', TRUE);

        $conn->select("
                k.mk_nopeg AS NOBADGE,
                k.mk_nama AS NAMA,
                k.company AS COMPANY,
                k.mk_email AS EMAIL,
                k.muk_kode AS UNIT_KERJA,
                uk.muk_nama AS UK_TEXT,
                k.mk_cctr AS COST_CENTER,
                k.mk_cctr_text AS CC_TEXT,
                k.mk_emp_group AS ACTIVE,
                k.mk_emp_subgroup AS POSITION,
                k.mk_emp_subgroup_text AS POS_TEXT,
                k.lokasi AS LOKASI,
                k.mk_alamat_rumah AS ALAMAT,
                k.mk_tgl_masuk AS TGL_MASUK,
                k.mk_tgl_lahir AS TGL_LAHIR
            ");
        $conn->from("hris.v_karyawan k");
        $conn->join("hris.v_unit_kerja uk", "k.muk_kode = uk.muk_kode");

        $conn->where("k.mk_emp_group", $active);
        $conn->where("k.muk_kode", $uk);

        $conn->like("k.mk_stat2_text", 'active');
        $conn->like("k.company", $company);
        $conn->like("k.mk_nopeg", $nobadge);
        $conn->or_like("LOWER(k.mk_nama)", $nobadge);

        $conn->limit(10);

        $query = $conn->get();

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function get_uk_hris_v2($q = null) {
        $conn = $this->load->database('hris', TRUE);

        $conn->like("uk.muk_kode", $q);
        $conn->or_like("LOWER(uk.muk_nama)", $q);

        $conn->limit(10);

        $query = $conn->get('hris.v_unit_kerja uk');

        if ($query) {
            return $query->result();
        } else {

            return NULL;
        }
    }

    public function get_user_hris_all($nobadge, $company, $uk, $active = 1) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                    *
                FROM
                    hris.v_karyawan k
                JOIN hris.v_unit_kerja uk ON k.muk_kode = uk.muk_kode
                WHERE
                    k.mk_nopeg LIKE '%{$nobadge}%'
                AND k.company LIKE '%{$company}%'
                AND k.muk_kode LIKE '%$uk%'
          AND k.mk_stat2_text LIKE '%active%'
                -- AND k.mk_emp_group = $active
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        return $query->row();
    }

    public function get_unit_kerja($param) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
    			SELECT
    				*
    			FROM
    				hris.v_unit_kerja uk
                WHERE uk.company = '{$param->company_side}'
       	";
        // echo "$sql";
        $query = $conn->query($sql);

        return $query->result_array();
    }

    public function get_unit_kerja_detail($uk_kode) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                    *
                FROM
                    hris.v_unit_kerja uk
                WHERE uk.muk_kode = '$uk_kode'
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        return $query->row_array();
    }

    public function get_users($param) {
        $conn = $this->load->database('hris', TRUE);

        $where = 'WHERE mk_stat2_text LIKE \'%active%\'';
        if (isset($param['sec'])) {
            $where = "WHERE msect_kode LIKE '%{$param['sec']}%' AND mk_stat2_text LIKE '%active%'";
        } else if (isset($param['biro'])) {
            $where = "WHERE mbiro_kode LIKE '%{$param['biro']}%' AND mk_stat2_text LIKE '%active%'";
        } else if (isset($param['dept'])) {
            $where = "WHERE mdept_kode LIKE '%{$param['dept']}%' AND mk_stat2_text LIKE '%active%'";
        }
        $sql = "
			SELECT * , v_karyawan.mk_email email FROM v_karyawan $where  ORDER BY mk_emp_subgroup ASC

    	";

        $query = $conn->query($sql);

        return $query->result_array();
    }

}
