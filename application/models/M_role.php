<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_role extends CI_Model
{
    // private $roles_tb = 'SHE_ROLES';
    // private $roles_users_tb = 'SHE_USERS_ROLES';
    // private $menu_tb = 'SHE_MENU';
    // private $role_permission_tb = 'SHE_ROLE_PERMISSIONS';
    // private $permissions_tb = 'SHE_PERMISSIONS';

    function __construct(){
        parent::__construct();


    }

    public function insert_role($param){

        $sql = $this->sql_insert($this->roles_tb, $param);

        $query = $this->db->query($sql);

        return (bool) $query;

    }
    public function update_role($param){

        $sql = $this->sql_update($this->roles_tb, $param, '"id"');

        $query = $this->db->query($sql);

        return (bool) $query;

    }

    public function get_role($id=NULL){
    	if ($id) {
    		$sql = "SELECT * FROM {$this->roles_tb} WHERE \"id\" = {$id} ";
    	}else {
    		$sql = "SELECT * FROM {$this->roles_tb}";
    	}

    	$query = $this->db->query($sql);

    	return $query->result_array();
    }

    public function json_role(){
        $sql = "
            SELECT
                MENU.\"GROUP\" MENU,
                MENU.\"ORDER\" POS,
                MENU.\"ID\" SUBMENU_ID,
                MENU.\"NAME\" SUBMENU,
                MENU.\"SUBORDER\" SUBPOS,
                MENU.ICON ,
                MENU.ICON_HOME ,
                MENU.IMG_HOME ,
                MENU.CLASS_BTN ,
                MENU.URL SUBMENU_URL,
                MENU.URL_HOME MENU_URL,
                MENU.DESCRIPTION SUBMENU_DES,
                PER.\"id\" SUBPOINT_ID,
                PER.\"key\" SUBPOINT,
                PER.\"description\" SUBPOINT_DES
            FROM
                {$this->permissions_tb} PER
            JOIN {$this->menu_tb} MENU ON MENU.\"NAME\" = PER.\"group\"
            ORDER BY MENU.\"ORDER\", MENU.SUBORDER,  PER.\"name\"
        ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function get_mine($id){
        $sql = "
            SELECT
                R.NAME ROLE_NAME,
                R.DESCRIPTION ROLE_DESCRIPTION,
                M.NAME MENU_NAME,
                M.URL MENU_URL,
                M.\"GROUP\" MENU_GROUP,
                M.DESCRIPTION MENU_DESCRIPTION,
                PER.\"group\" PERMISSION_GROUP,
                PER.\"key\" PERMISSION_NAME,
                PER.\"description\" PERMISSION_DESCRIPTION
            FROM
                {$this->roles_tb} R
            JOIN {$this->roles_users_tb} UR ON R.\"id\" = UR.ROLE_ID AND R.\"id\" = {$id}
            LEFT JOIN {$this->menu_tb} M ON UR.MENU_ID = M.ID
            RIGHT JOIN {$this->permissions_tb} PER ON PER.\"id\" = UR.PERMISSION_ID AND M.NAME = PER.\"group\"
            ORDER BY M.\"ORDER\", M.SUBORDER
        ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function get_role_detail($id){
    	$sql = "SELECT * FROM {$this->roles_tb} WHERE \"id\" = {$id} ";

    	$query = $this->db->query($sql);

    	return $query->result();
    }

    public function get_role_by_name($name){
        $sql = "SELECT * FROM {$this->roles_tb} WHERE \"NAME\" LIKE '%{$name}%' ";

        $query = $this->db->query($sql);

        return $query->result();
    }

    public function get_permission($id=NULL){
        if ($id) {
            $sql = "SELECT * FROM {$this->permissions_tb} WHERE \"id\" = {$id} ";
            $query = $this->db->query($sql);

            return $query->result_row();
        }else{
            $sql = "SELECT * FROM {$this->permissions_tb} ";

            $query = $this->db->query($sql);

            return $query->result_array();
        }



    }

    public function get_permission_id($key=NULL){

        $sql = "SELECT \"id\" ID FROM {$this->permissions_tb} WHERE \"key\" = '{$key}' ";
        $query = $this->db->query($sql);

        return $query->row()->ID;

    }

    public function permission_change($param){


        $sql = "SELECT * FROM {$this->roles_users_tb} WHERE ROLE_ID = {$param['ROLE_ID']} AND MENU_ID = {$param['MENU_ID']} AND PERMISSION_ID = {$param['PERMISSION_ID']}";

        $query = $this->db->query($sql);

        if($query->row()){
            $sql = "DELETE {$this->roles_users_tb} WHERE ROLE_ID = {$param['ROLE_ID']} AND MENU_ID = {$param['MENU_ID']} AND PERMISSION_ID = {$param['PERMISSION_ID']}";

            $query = $this->db->query($sql);

            return 'deleted';
        }else{
            $sql = $this->sql_insert($this->roles_users_tb, $param);

            $query = $this->db->query($sql);

            return 'inserted';
        }


        return false;


    }

    public function get_permission_by_role($id){
    	$sql = "SELECT
					P.\"name\",
					P.\"description\" ,
					P.\"group\"
				FROM
					 SHE_ROLES R
				JOIN SHE_ROLE_PERMISSIONS RP ON R.\"id\" = RP.\"role_id\"
				JOIN SHE_PERMISSIONS P ON RP.\"permission_id\" = P.\"id\"
				WHERE R.\"id\" = {$id}";

    	$query = $this->db->query($sql);

    	return $query->result_array();
    }



}
