<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_setting extends CI_Model
{
    private $conn = NULL;

    // private $com_tb = "SHE_MASTER_PLANT";
    // private $email_tb = "SHE_EMAIL_NOTIFICATION";

    function __construct(){
        parent::__construct();


    }
    public function get_company_plant(){
        $sql =  "SELECT * FROM {$this->com_tb}";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    public function get_email_by_verfcode($par){
      $sql = "
        SELECT
          *
        FROM
        SHE_EMAIL_NOTIFICATION
        WHERE
        VERF_CODE = '{$par['code']}'
        --AND status = 0
        --AND EMAIL = '{$par['email']}'
              ";

      $query = $this->db->query($sql);

      return $query->row_array();

    }

    public function is_email_exist($where = NULL){
      $this->db->select('GROUP_MENU, TIPE, NOTIF, EMAIL, STATUS, NOTE, CREATE_AT, CREATE_BY, DELETE_AT, DELETE_BY, COMPANY, PLANT, VERF_CODE, ID_DOC');
      $this->db->from($this->email_tb);
      $this->db->where($where);
      $query = $this->db->get();

      return $query->result_array();
    }

    public function insert_email($param){
        $sql = $this->sql_insert2($this->email_tb, $param, array('CREATE_AT'));

        $query = $this->db->query($sql);

        return (bool) $query;
    }

    public function set_status_email($ecode){
      $this->db->set('status', '1', FALSE);
      $this->db->where('VERF_CODE', $ecode);
      $query = $this->db->update($this->email_tb);
      // echo $this->db->last_query();
        // $sql = $this->sql_insert2($this->email_tb, $param, array('CREATE_AT'));
        //
        // $query = $this->db->query($sql);

      return (bool) $query;
    }


}
