<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_user extends CI_Model
{
    private $conn = NULL;

    // private $user_tb = 'SHE_USERS';

    function __construct(){
        parent::__construct();


    }

    public function get_user($nobadge){
        $sql = "SELECT * FROM SHE_USERS WHERE \"no_badge\" LIKE '%{$nobadge}%'";

        $query = $this->db->query($sql);

        return $query->row();
    }


    public function get_user_by_uk($param){
        $sql = "SELECT * FROM SHE_USERS WHERE \"uk_kode\" LIKE '%{$param['uk']}%' AND \"position\" = {$param['position']}";

        $query = $this->db->query($sql);

        return $query->row();
    }

    public function get_user_all($param){
    	$sql = "SELECT * FROM SHE_USERS WHERE \"plant\" LIKE '%{$param['plant']}%' AND \"company\" LIKE '%{$param['company']}%'";

    	$query = $this->db->query($sql);

    	return $query->result();
    }

    public function get_user_all_comptext($param){
      $param['company'] = str_replace('%20', ' ', $param['company']);

    	$sql = "SELECT u.*, c.COMPANY_TEXT FROM SHE_USERS u INNER JOIN SHE_MASTER_PLANT c ON c.COMPANY=u.\"company\" WHERE c.PLANT IS NULL AND u.\"plant\" LIKE '%{$param['plant']}%' AND u.\"company\" IN ({$param['company']})";
      // echo "{$sql}";
    	$query = $this->db->query($sql);

    	return $query->result();
    }

    public function get_plant_by_comptext($company){
      $company = str_replace('%20', ' ', $company);

      $this->db->select('COMPANY');
      $this->db->from('SHE_MASTER_PLANT');
      $this->db->where("PLANT IS NULL AND COMPANY_TEXT LIKE '%$company%'");
      $query = $this->db->get();

      if ($query){
        return $query->result_array();
      }

    }

    public function update_user($param){

    	// $where = array(
     //        '"no_badge"' => $param['no_badge']
     //        );
    	// unset($param['no_badge']);
    	// $sql = $this->sql_update($this->user_tb, $param, 'id', $where);

    	// $query = $this->db->query($sql);

    	// return (bool) $query;



		$this->db->where('"no_badge"', $param['no_badge']);
			unset($param['no_badge']);
		$result = $this->db->update($this->user_tb, $param);

		// echo $this->db->last_query();

		return (bool) $result;
    }


}
