<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_validasi extends CI_Model
{
    // private $roles_tb = 'SHE_ROLES';
    // private $roles_users_tb = 'SHE_USERS_ROLES';
    // private $menu_tb = 'SHE_MENU';
    // private $role_permission_tb = 'SHE_ROLE_PERMISSIONS';
    // private $permissions_tb = 'SHE_PERMISSIONS';

    function __construct(){
        parent::__construct();


    }

    public function check_apd_status($nobadge, $kode){
        $sql = "SELECT
                    *
                FROM
                    (
                        SELECT
                            O.NO_BADGE,
                            SUBSTR (DO.KODE_APD, 0, 3) KODE,
                            TO_CHAR (DO.CREATE_AT, 'YYYY MM DD') AS TANGGAL,
                            DO.CODE_COMP,
                            DO.CODE_PLANT
                        FROM
                            SHE_APD_DETAIL_ORDER DO
                        JOIN SHE_APD_ORDER_APD O ON DO.KODE_ORDER = O. ID
                        GROUP BY
                            SUBSTR (DO.KODE_APD, 0, 3),
                            O.NO_BADGE,
                            TO_CHAR (DO.CREATE_AT, 'YYYY MM DD'),
                            DO.CODE_COMP,
                            DO.CODE_PLANT
                    )
                WHERE NO_BADGE LIKE '%{$nobadge}%' AND KODE LIKE '%{$kode}%'";

        $result = $this->db->query($sql);

        return (bool) $result;
    }

    public function check_apd_expiration($no_badge){
        $sql = "
                SELECT
                    *
                FROM
                    (
                        SELECT
                            SUBSTR (DO.KODE_APD, 0, 3) KODE,
                            TO_CHAR (DO.CREATE_AT, 'YYYY MM DD') AS TANGGAL,
                            DO.MASA_EXP,
                            DO.RELEASE_AT,
                            DO.\"ID\" ID_DETAIL,
                            O.*,
                            M.KODE AS KODE_APD,
                            M.NAMA,
                            M.MERK

                        FROM
                            SHE_APD_DETAIL_ORDER DO
                        JOIN SHE_APD_ORDER_APD O ON DO.KODE_ORDER = O.\"ID\" AND DO.RELEASE_AT IS NOT NULL
                        JOIN SHE_APD_MASTER_APD M ON M.KODE = DO.KODE_APD AND M.CODE_PLANT = DO.CODE_PLANT
                        WHERE DO.FILES IS NULL
                    )
                WHERE NO_BADGE LIKE '%{$no_badge}%' AND KODE_ORDER LIKE '%RID%'

            ";

        $result = $this->db->query($sql);

        return $result->result_array();
    }





}
