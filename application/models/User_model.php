<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends CI_Model {

    var $user_table = 'MPE_USERS';
    var $role_table = NULL;

    function __construct() {
        parent::__construct();
        // $this->user_table = $this->config->item('user_table', 'acl_auth');
        // $this->role_table = $this->config->item('acl_table_roles', 'acl');
    }

    // public function has_role( $user, $role )
    // {
    //     // $this->db->join( 'user u', 'users.id = u.users_id' );
    //     $this->db->join( "SHE_ROLES r", "r.id = SHE_USERS.role_id" );
    //     return $this->get_by( array( 'r.name' => "{$role}", "SHE_USERS.id" => $user ) );
    // }

    public function field_exists($field) {
        // echo "<br />";
        return $this->db->field_exists($field, $this->user_table);
        // echo $this->db->field_exists( $field, $this->user_table );
    }

    /**
     * Insert a new row into the table. $data should be an associative array
     * of data to be inserted. Returns newly created ID.
     */
    public function save($data) {
        $query = $this->db->insert($this->user_table, $data);
        return $this->db->affected_rows();
    }

    public function log_user($param) {
        $id = $param['id'];
        unset($param['id']);
        $this->db->set('"last_login"', "CURRENT_DATE", false);
        $this->db->where('id', $id);
        $query = $this->db->update($this->user_table);

        return (bool) $query;
    }

    public function get_user($identity) {
        if (!$identity) {
            return false;
        }
        $this->db->from($this->user_table);
        $this->db->where($this->config->item('identity_field', 'acl_auth'), $identity);

        return $this->db->get()->row();
    }

    public function get_rem_code($identity) {
        if (!$identity) {
            return false;
        }
        $this->db->from($this->user_table);
        $this->db->where($this->config->item('remember_field', 'acl_auth'), $identity);

        return $this->db->get()->row();
    }

    public function get_user_hris($nobadge, $company = '', $uk = '', $active = 1) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                    k.mk_nopeg AS NOBADGE,
                    k.mk_stat2_text
                FROM
                    hris.v_karyawan k
                WHERE
                    (k.mk_nopeg LIKE '%$nobadge%' OR k.mk_nama LIKE '%$nobadge%')
                AND k.company LIKE '%$company%'
                AND k.muk_kode LIKE '%$uk%'
                AND k.mk_emp_group = $active AND k.mk_stat2_text LIKE '%active%'
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        return $query->row();
    }

    public function get_hris_by_username($username, $company = '', $uk = '', $active = 1) {
        $conn = $this->load->database('hris', TRUE);
        $sql = "
                SELECT
                  0 AS id,
                  0 AS role_id,
                  mk_email AS email,
                  SUBSTRING_INDEX(mk_email, '@', 1)AS username,
                  NULL AS password,
                  mk_nama AS name,
                  1 AS active,
                  k.company,
                  k.mdir_kode AS dir_code,
                  k.mkomp_kode AS komp_code,
                  k.mdept_kode AS dept_code,
                  uk2.muk_nama AS dept_text,
                  k.muk_kode AS uk_kode,
                  u.muk_nama AS unit_kerja,
                  k.mk_nopeg AS no_badge,
                  k.mk_cctr AS cost_center,
                  k.mk_cctr_text AS cc_text,
                  k.mk_emp_subgroup AS position,
                  k.mk_emp_subgroup_text AS pos_text,
                  k.mk_stat2_text
                FROM
                  hris.v_karyawan k
                INNER JOIN
                  v_unit_kerja u ON u.muk_kode=k.muk_kode
                LEFT JOIN
                  v_unit_kerja uk2 ON k.mdept_kode = uk2.muk_kode
                WHERE
                  k.mk_email LIKE '{$username}@%'
                  AND k.company LIKE '%{$company}%'
                  AND k.muk_kode LIKE '%{$uk}%'
                  AND k.mk_emp_group = {$active} AND k.mk_stat2_text LIKE '%active%'
        ";
        // echo "$sql";
        $query = $conn->query($sql);

        return $query->row();
    }

    public function check_token($token) {
        return ( $token === $this->reset_code );
    }

}
