<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_evatech extends CI_Model
{

    var $hris;

    public function __construct()
    {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
    }

    var $column = array(
        'ID', 'ID', 'CREATE_AT', 'NO_PENDAMPINGAN', 'NOTIFIKASI', 'NO_DOK_ENG', 'PACKET_TEXT', 'STATUS', 'DESCPSECTION', 'FL_TEXT', 'COMPANY', 'ID'
    );

    function _qry($key)
    {
        $this->db->select('
                        	A.ID,
                        	A.NO_PENDAMPINGAN,
                        	A.NOTIFIKASI,
                        	A.ID_PENGAJUAN,
                        	B.NO_PENGAJUAN,
                          A.ID_DOK_ENG,
                          (SELECT NO_DOK_ENG FROM MPE_DOK_ENG WHERE ID=A.ID_DOK_ENG AND rownum=1) NO_DOK_ENG,
                          (SELECT ID FROM MPE_PENUGASAN WHERE ID_PENGAJUAN=B.ID_MPE AND rownum=1) ID_PENUGASAN,
                          (SELECT NO_PENUGASAN FROM MPE_PENUGASAN WHERE ID_PENGAJUAN=B.ID_MPE AND rownum=1) NO_PENUGASAN,
                          A.PACKET_TEXT,
                        	B.NOTIFIKASI NOTIF_NO,
                        	B.NAMA_PEKERJAAN SHORT_TEXT,
                        	B.UK_TEXT,
                        	B.FUNCT_LOC,
                        	C.DESC FL_TEXT,
                          B.PLANT_SECTION,
                          (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=B.PLANT_SECTION AND MPLANT=B.MPLANT) DESCPSECTION,
                        	B.LOKASI,
                        	B.COMPANY,
                          (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=B.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                          (CASE WHEN A.APPROVE1_AT IS NOT NULL AND A.APPROVE2_AT IS NULL THEN \'New\' WHEN A.APPROVE2_AT IS NOT NULL AND A.FILES IS NULL THEN \'Upload\' ELSE \'\' END) STAT,
                        	A.HDR_TEXT,
                        	A.MID_TEXT,
                        	A.FTR_TEXT,
                          A.NOTE,
                        	A.CREATE_AT,
                        	A.CREATE_BY,
                        	A.UPDATE_AT,
                        	A.UPDATE_BY,
                          A.REJECT_AT,
                          A.REJECT_BY,
                          A.REJECT_NOTE,
                        	B.CUST_REQ,
                        	B.LATAR_BELAKANG,
                        	B.TECH_INFO,
                        	B.RES_MIT,
                          A.TIM_PENGKAJI,
                        	A.STATUS,
                          (CASE WHEN (APPROVE1_AT IS NOT NULL) THEN 1 ELSE 0 END) PENDING,
                          A.DEPT_CODE,
                          A.DEPT_TEXT,
                          A.UK_CODE,
                          A.UK_TEXT UNIT_KERJA,
                        	A.APPROVE1_AT,
                          A.APPROVE1_BADGE,
                        	A.APPROVE1_BY,
                        	A.APPROVE1_JAB,
                        	A.APPROVE2_AT,
                          A.APPROVE2_BADGE,
                        	A.APPROVE2_BY,
                        	A.APPROVE2_JAB,
                          A.FILES,
                          A.FILES_AT
                        ');
        $this->db->from('MPE_PENDAMPINGAN A');
        $this->db->join("MPE_PENGAJUAN B", "A.ID_PENGAJUAN = B.ID_MPE");
        $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');
        if ($key['search'] !== '') {
            $sAdd ="(
                    LOWER(A.NO_PENDAMPINGAN) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(A.NOTIFIKASI) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.UK_TEXT) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.LOKASI) LIKE '%".strtolower($key['search'])."%'
                    )";
            $this->db->where($sAdd, '', FALSE);
        }
          $this->db->where('A.TIPE','EVATECH');
        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('A.APPROVE2_BY IS NOT NULL');
        if (isset($key['comp_view'])) {
          $this->db->where("PLANNER_GROUP IN (SELECT GEN_CODE FROM MPE_GENERAL WHERE GEN_PAR6='{$key['comp_view']}' AND GEN_TYPE='DEF_PGROUP')");
        }
        $order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
    }

    function get($key)
    {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    function get_data($key)
    {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function recFil($key)
    {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function recTot($key)
    {
        $key['search'] = '';
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_record($key)
    {
        $this->db->select('
                        	A.ID,
                        	A.NO_PENDAMPINGAN,
                        	A.NOTIFIKASI,
                        	A.ID_PENGAJUAN,
                        	B.NO_PENGAJUAN,
                          A.ID_DOK_ENG,
                          (SELECT NO_DOK_ENG FROM MPE_DOK_ENG WHERE ID=A.ID_DOK_ENG AND rownum=1) NO_DOK_ENG,
                          (SELECT ID FROM MPE_PENUGASAN WHERE ID_PENGAJUAN=B.ID_MPE AND rownum=1) ID_PENUGASAN,
                          (SELECT NO_PENUGASAN FROM MPE_PENUGASAN WHERE ID_PENGAJUAN=B.ID_MPE AND rownum=1) NO_PENUGASAN,
                          A.PACKET_TEXT,
                        	B.NOTIFIKASI NOTIF_NO,
                        	B.NAMA_PEKERJAAN SHORT_TEXT,
                        	B.UK_TEXT,
                        	B.FUNCT_LOC,
                        	C.DESC FL_TEXT,
                          B.PLANT_SECTION,
                          (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=B.PLANT_SECTION AND MPLANT=B.MPLANT) DESCPSECTION,
                        	B.LOKASI,
                        	B.COMPANY,
                          (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=B.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                        	A.HDR_TEXT,
                        	A.MID_TEXT,
                        	A.FTR_TEXT,
                          A.NOTE,
                          (CASE WHEN A.APPROVE1_AT IS NOT NULL AND A.APPROVE2_AT IS NULL THEN \'New\' WHEN A.APPROVE2_AT IS NOT NULL AND A.FILES IS NULL THEN \'Upload\' ELSE \'\' END) STAT,
                        	A.CREATE_AT,
                        	A.CREATE_BY,
                        	A.UPDATE_AT,
                        	A.UPDATE_BY,
                          A.REJECT_AT,
                          A.REJECT_BY,
                          A.REJECT_NOTE,
                        	B.CUST_REQ,
                        	B.LATAR_BELAKANG,
                        	B.TECH_INFO,
                        	B.RES_MIT,
                          A.TIM_PENGKAJI,
                        	A.STATUS,
                          (CASE WHEN (APPROVE1_AT IS NOT NULL) THEN 1 ELSE 0 END) PENDING,
                          A.DEPT_CODE,
                          A.DEPT_TEXT,
                          A.UK_CODE,
                          A.UK_TEXT UNIT_KERJA,
                        	A.APPROVE1_AT,
                          A.APPROVE1_BADGE,
                        	A.APPROVE1_BY,
                        	A.APPROVE1_JAB,
                        	A.APPROVE2_AT,
                          A.APPROVE2_BADGE,
                        	A.APPROVE2_BY,
                        	A.APPROVE2_JAB,
                          A.FILES,
                          A.FILES_AT
                        ');
        $this->db->from('MPE_PENDAMPINGAN A');
        $this->db->join("MPE_PENGAJUAN B", "A.ID_PENGAJUAN = B.ID_MPE");
        $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');

        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('A.ID', $key);

        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_foreign()
    {
        $this->db->select('
                        		PD.ID,
                        		PD.ID_PENGAJUAN,
                        		PD.NO_PENDAMPINGAN,
                        		PE.NO_PENGAJUAN,
                            PD.ID_DOK_ENG,
                            (SELECT NO_DOK_ENG FROM MPE_DOK_ENG WHERE ID=PD.ID_DOK_ENG) NO_DOK_ENG,
                            PD.PACKET_TEXT,
                        		PE.NOTIFIKASI NOTIF_NO,
                        		PE.NAMA_PEKERJAAN SHORT_TEXT,
                        		PE.MPLANT,
                        		PE.PPLANT,
                        		PE.PLANNER_GROUP,
                        		PE.PLANT_SECTION,
                        		PE.FUNCT_LOC,
                        		FL."DESC" FL_TEXT,
                        		PE.DESSTDATE START_DATE,
                        		PE.DESENDDATE START_END,
                        		PE.UK_CODE,
                        		PE.UK_TEXT,
                        		PE.PRIORITY,
                        		PE.LATAR_BELAKANG,
                        		PE.CUST_REQ,
                        		PE.TECH_INFO,
                        		PE.RES_MIT,
                        		PE.CONSTRUCT_COST,
                        		PE.ENG_COST,
                        		PE.STATUS,
                        		PD.NOTE,
                        		PE.COMPANY,
                        		PE.APPROVE_AT,
                        		PE.APPROVE_BY,
                        		PE.CREATE_AT,
                        		PE.CREATE_BY,
                        		PE.UPDATE_AT,
                        		PE.UPDATE_BY,
                        		PE.DELETE_AT,
                        		PE.DELETE_BY,
                        		PE.LOKASI,
                        		PE.WBS_CODE,
                        		PE.WBS_TEXT,
                            (CASE WHEN PD.APPROVE1_AT IS NOT NULL AND PD.APPROVE2_AT IS NULL THEN \'New\' WHEN PD.APPROVE2_AT IS NOT NULL AND PD.FILES IS NULL THEN \'Upload\' ELSE \'\' END) STAT
                          ');
        $this->db->from("MPE_PENGAJUAN PE");
        $this->db->join("MSO_FUNCLOC FL", "FL.TPLNR = PE.FUNCT_LOC", 'left');
        $this->db->join("MPE_PENDAMPINGAN PD", "PD.ID_PENGAJUAN = PE.ID_MPE", 'left');
        $this->db->where("
                          PE.DELETE_AT IS NULL
                          AND PE.STATUS = 'Approved'
                          AND FL.STATUS NOT LIKE '%DLFL%'
                          AND FL.STATUS NOT LIKE '%INAC%'
                          AND PD.STATUS = 'Approved'
                          AND PD.TIPE = 'EVATECH'
                        ");
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_pengkaji($company = '2000', $key = '50032326')
    {
        $this->hris->select('
                              muk_kode,
                              muk_short,
                              muk_nama,
                              muk_level,
                              muk_parent,
                              company,
                              muk_begda,
                              muk_endda,
                              muk_cctr,
                              muk_changed_on
                          ');
        $this->hris->from("v_unit_kerja");
        $this->hris->where("
                          muk_parent LIKE '%{$key}%'
                          AND company = '{$company}'
                        ");
        $this->hris->order_by('muk_nama', 'ASC');
        $query = $this->hris->get();

        return $query->result_array();
    }

    public function get_lead($company = '2000', $key = '')
    {
        if (!$key) {
            $this->hris->where("mjab_nama LIKE '%Engineering%'");
        } else {
            $this->hris->where("muk_kode IN ({$key})");
        }

        $this->hris->select('
                              mjab_kode,
                              mjab_nama,
                              company,
                              muk_kode,
                              mjab_emp_subgroup,
                              mjab_emp_subgroup_txt,
                              mjab_begda,
                              mjab_endda,
                              is_chief,
                              mjab_changed_on
                          ');
        $this->hris->from("v_jabatan");
        $this->hris->where("
                            company = '{$company}'
                            AND mjab_emp_subgroup = '20'
                            /*AND mjab_endda > now()*/
                          ");
        $this->hris->distinct();
        $this->hris->order_by('mjab_nama', 'ASC');
        $query = $this->hris->get();

        return $query->result_array();
    }

    function save($data)
    {
        $query = $this->db->insert('MPE_PENDAMPINGAN', $data);
        $result = false;
        if ($query) {
            $this->db->select_max('ID', 'ID');
            $result = $this->db->get('MPE_PENDAMPINGAN')->row_array();
        }
        return $result;
    }

    function updateData($tipe, $param = array())
    {
        if ($tipe == 'delete') {
            $id = $param['id'];
            unset($param['id']);
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->set('STATUS', "Approved");
            $this->db->set('APPROVE2_BADGE', NULL);
            $this->db->set('APPROVE2_BY', NULL);
            $this->db->set('APPROVE2_JAB', NULL);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_PENDAMPINGAN', $param);

            return (bool)$query;
        } else if ($tipe == 'approve') {
            $no_form = $param['no_form'];
            unset($param['no_form']);
            $this->db->where('NO_PENDAMPINGAN', $no_form);
            $query = $this->db->update('MPE_PENDAMPINGAN', $param);

            return (bool)$query;
        } else {
            $id = $param['ID'];
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            unset($param['ID']);

            $query = $this->db->update('MPE_PENDAMPINGAN', $param);

            return (bool)$query;
        }
    }

    public function insert_det($param = array())
    {
        $result = $this->db->insert_batch('MPE_DTL_PENUGASAN', $param);

        return $result;
    }

    public function update_det($param = array())
    {
        $this->db->where('DELETE_AT IS NULL');
        if (isset($param['ID_PENUGASAN'])) {
            $this->db->where('ID_PENUGASAN', $param['ID_PENUGASAN']);
            unset($param['ID_PENUGASAN']);
        }
        $result = $this->db->update('MPE_DTL_PENUGASAN', $param);

        return $result;
    }

    public function deleteDtl($param = array(), $where = array())
    {
        $id = $param['id'];
        unset($param['id']);
        $this->db->set('DELETE_AT', "CURRENT_DATE", false);
        $this->db->where('ID_PENUGASAN', $id);
        $this->db->where("DELETE_AT IS NULL");
        $query = $this->db->update('MPE_DTL_PENUGASAN', $param);

        return (bool)$query;
    }

    function getCount()
    {
        $this->db->select("NVL(MAX(CAST(SUBSTR(NO_PENDAMPINGAN,15,3) AS INTEGER))+1,1) AS URUT", FALSE);
        $this->db->from('MPE_PENDAMPINGAN');
        $this->db->where("SUBSTR(NO_PENDAMPINGAN,-44)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR(NO_PENDAMPINGAN, 12, 2)='PT'");
        $urut = $this->db->get()->row();

        return $urut->URUT;
    }

}
