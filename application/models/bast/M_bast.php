<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_bast extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    var $column = array(
        'NO_BAST', 'PACKET_TEXT', 'UK_TEXT', 'LOKASI', 'STATUS', 'CREATE_BY', 'CREATE_AT', 'ID'
    );

    function _qry($key) {
        $this->db->select('
                          A.ID,
                          A.ID_DOK_ENG,
                          A.NO_BAST,
                          A.NOTIFIKASI,
                          B.NO_DOK_ENG,
                          B.PACKET_TEXT,
                          D.UK_TEXT,
                          D.LOKASI,
                          A.HDR_TEXT,
                          A.MID_TEXT,
                          A.FTR_TEXT,
                          C.NO_PENUGASAN,
                          D.FUNCT_LOC,
                          E."DESC" FL_TEXT,
                          D.PLANT_SECTION,
                          (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=D.PLANT_SECTION AND MPLANT=D.MPLANT) DESCPSECTION,
                          C.DE_CONSTRUCT_COST,
                          C.DE_ENG_COST,
                          A.STATUS,
                          A.NOTE,
                          A.COMPANY,
                          (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=D.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                          A.APPROVE1_AT,
                          A.APPROVE1_BY,
                          A.APPROVE1_JAB,
                          A.APPROVE2_AT,
                          A.APPROVE2_BY,
                          A.APPROVE2_JAB,
                          A.REJECT_AT,
                          A.REJECT_BY,
                          A.REJECT_NOTE,
                          A.CREATE_AT,
                          A.CREATE_BY,
                          A.UPDATE_AT,
                          A.UPDATE_BY,
                          A.DELETE_AT,
                          A.DELETE_BY
                        ', FALSE);
        $this->db->from('MPE_BAST A');
        $this->db->join("MPE_DOK_ENG B", "A.ID_DOK_ENG = B.ID", 'left');
        $this->db->join("MPE_PENUGASAN C", "B.ID_PENUGASAN = C.ID", 'left');
        $this->db->join("MPE_PENGAJUAN D", "C.ID_PENGAJUAN = D.ID_MPE", 'left');
        $this->db->join("MSO_FUNCLOC E", "E.TPLNR = D.FUNCT_LOC", 'left');
        if ($key['search'] !== '') {
            $sAdd = "(
                  LOWER(A.NO_BAST) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(B.PACKET_TEXT) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(D.UK_TEXT) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(D.LOKASI) LIKE '%" . strtolower($key['search']) . "%'
                  )";
            $this->db->where($sAdd, '', FALSE);
        }
        $this->db->where('A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL AND C.DELETE_AT IS NULL AND D.DELETE_AT IS NULL');
        if (isset($key['dept_code'])) {
            // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
            $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
            $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        $order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
    }

    // function get($key){
    // 	$this->_qry($key);
    // 	$this->db->limit($key['length'], $key['start']);
    // 	$query		= $this->db->get();
    //   // echo $this->db->last_query();
    // 	$data			= $query->result();
    // 	return $data;
    // }

    function get_data($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        // echo $this->db->last_query();
        $data = $query->result();

        return $data;
    }

    function recFil($key) {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function recTot($key) {
        $key['search'] = '';
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_data_unit($key = null) {
        $this->db->select('
                          A.ID,
                          A.ID_DOK_ENG,
                          A.NO_BAST,
                          A.NOTIFIKASI,
                          B.NO_DOK_ENG,
                          B.PACKET_TEXT,
                          D.UK_TEXT,
                          D.LOKASI,
                          A.HDR_TEXT,
                          A.MID_TEXT,
                          A.FTR_TEXT,
                          C.NO_PENUGASAN,
                          D.FUNCT_LOC,
                          E."DESC" FL_TEXT,
                          D.PLANT_SECTION,
                          (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=D.PLANT_SECTION AND MPLANT=D.MPLANT) DESCPSECTION,
                          C.DE_CONSTRUCT_COST,
                          C.DE_ENG_COST,
                          A.STATUS,
                          A.NOTE,
                          A.COMPANY,
                          (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=D.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                          A.APPROVE1_AT,
                          A.APPROVE1_BY,
                          A.APPROVE1_JAB,
                          A.APPROVE1_0_AT,
                          A.APPROVE1_0_BY,
                          A.APPROVE1_0_JAB,
                          A.APPROVE2_AT,
                          A.APPROVE2_BY,
                          A.APPROVE2_JAB,
                          A.APPROVE2_0_AT,
                          A.APPROVE2_0_BY,
                          A.APPROVE2_0_JAB,
                          A.REJECT_AT,
                          A.REJECT_BY,
                          A.REJECT_NOTE,
                          A.CREATE_AT,
                          A.CREATE_BY,
                          A.UPDATE_AT,
                          A.UPDATE_BY,
                          A.DELETE_AT,
                          A.DELETE_BY,
                          A.NOTE1,
                          A.NOTE2,
                          A.NOTE1_0,
                          A.NOTE2_0
                        ', FALSE);
        $this->db->from('MPE_BAST A');
        $this->db->join("MPE_DOK_ENG B", "A.ID_DOK_ENG = B.ID", 'left');
        $this->db->join("MPE_PENUGASAN C", "B.ID_PENUGASAN = C.ID", 'left');
        $this->db->join("MPE_PENGAJUAN D", "C.ID_PENGAJUAN = D.ID_MPE", 'left');
        $this->db->join("MSO_FUNCLOC E", "E.TPLNR = D.FUNCT_LOC", 'left');
        // }
        $this->db->where('A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL AND C.DELETE_AT IS NULL AND D.DELETE_AT IS NULL');
        if (isset($key)) {
            $this->db->where('B.NO_DOK_ENG', $key);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }
    
    function get_record($key = null) {
        $this->db->select('
                          A.ID,
                          A.ID_DOK_ENG,
                          A.NO_BAST,
                          A.NOTIFIKASI,
                          B.NO_DOK_ENG,
                          B.PACKET_TEXT,
                          D.UK_TEXT,
                          D.LOKASI,
                          A.HDR_TEXT,
                          A.MID_TEXT,
                          A.FTR_TEXT,
                          C.NO_PENUGASAN,
                          D.FUNCT_LOC,
                          E."DESC" FL_TEXT,
                          D.PLANT_SECTION,
                          (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=D.PLANT_SECTION AND MPLANT=D.MPLANT) DESCPSECTION,
                          C.DE_CONSTRUCT_COST,
                          C.DE_ENG_COST,
                          A.STATUS,
                          A.NOTE,
                          A.COMPANY,
                          (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=D.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                          A.APPROVE1_AT,
                          A.APPROVE1_BY,
                          A.APPROVE1_JAB,
                          A.APPROVE1_0_AT,
                          A.APPROVE1_0_BY,
                          A.APPROVE1_0_JAB,
                          A.APPROVE2_AT,
                          A.APPROVE2_BY,
                          A.APPROVE2_JAB,
                          A.APPROVE2_0_AT,
                          A.APPROVE2_0_BY,
                          A.APPROVE2_0_JAB,
                          A.REJECT_AT,
                          A.REJECT_BY,
                          A.REJECT_NOTE,
                          A.CREATE_AT,
                          A.CREATE_BY,
                          A.UPDATE_AT,
                          A.UPDATE_BY,
                          A.DELETE_AT,
                          A.DELETE_BY,
                          A.NOTE1,
                          A.NOTE2,
                          A.NOTE1_0,
                          A.NOTE2_0
                        ', FALSE);
        $this->db->from('MPE_BAST A');
        $this->db->join("MPE_DOK_ENG B", "A.ID_DOK_ENG = B.ID", 'left');
        $this->db->join("MPE_PENUGASAN C", "B.ID_PENUGASAN = C.ID", 'left');
        $this->db->join("MPE_PENGAJUAN D", "C.ID_PENGAJUAN = D.ID_MPE", 'left');
        $this->db->join("MSO_FUNCLOC E", "E.TPLNR = D.FUNCT_LOC", 'left');
        // }
        $this->db->where('A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL AND C.DELETE_AT IS NULL AND D.DELETE_AT IS NULL');
        if (isset($key)) {
            $this->db->where('A.ID', $key);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_foreign() {
        $this->db->select('
                        		DE."ID" ID_DOK_ENG,
                            DE.NO_DOK_ENG,
                            DE.NOTIFIKASI,
                            DE.TIPE,
                            DE.PACKET_TEXT,
                            DE.COLOR,
                            (SELECT DISTINCT DE_CONSTRUCT_COST FROM MPE_PENUGASAN WHERE ID=DE.ID_PENUGASAN AND DELETE_AT IS NULL) DE_CONSTRUCT_COST,
                            (SELECT DISTINCT DE_ENG_COST FROM MPE_PENUGASAN WHERE ID=DE.ID_PENUGASAN AND DELETE_AT IS NULL) DE_ENG_COST,
                            DE.NOTE,
                            DE.STATUS,
                            DE.COMPANY,
                            DE.APPROVE1_AT,
                            DE.APPROVE1_BY,
                            DE.APPROVE1_JAB,
                            DE.APPROVE2_AT,
                            DE.APPROVE2_BY,
                            DE.APPROVE2_JAB,
                            DE.APPROVE3_AT,
                            DE.APPROVE3_BY,
                            DE.APPROVE3_JAB,
                            DE.APPROVE4_AT,
                            DE.APPROVE4_BY,
                            DE.APPROVE4_JAB,
                            DE.APPROVE5_AT,
                            DE.APPROVE5_BY,
                            DE.APPROVE5_JAB,
                            DE.APPROVE6_AT,
                            DE.APPROVE6_BY,
                            DE.APPROVE6_JAB,
                            DE.APPROVE7_AT,
                            DE.APPROVE7_BY,
                            DE.APPROVE7_JAB,
                            DE.APPROVE8_AT,
                            DE.APPROVE8_BY,
                            DE.APPROVE8_JAB,
                            DE.CREATE_AT,
                            DE.CREATE_BY,
                            DE.UPDATE_AT,
                            DE.UPDATE_BY,
                            DE.DELETE_AT,
                            DE.DELETE_BY
                          ');
        $this->db->from("MPE_DOK_ENG DE");
        $this->db->where("
                          DE.DELETE_AT IS NULL
                          AND DE.STATUS != 'Rejected'
                          AND DE.ID NOT IN (SELECT ID_DOK_ENG FROM MPE_BAST WHERE DELETE_AT IS NULL)
                        ");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        $query = $this->db->insert('MPE_BAST', $data);
        $result = false;
        if ($query) {
            $this->db->select_max('ID', 'ID');
            $result = $this->db->get('MPE_BAST')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param = array()) {
        if ($tipe == 'delete') {
            $id = $param['id'];
            unset($param['id']);
            $this->db->set('DELETE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_BAST', $param);

            return (bool) $query;
        } else if ($tipe == 'approve') {
            $id = $param['id'];
            unset($param['id']);
            unset($param['no_form']);
            // $this->db->set('APPROVE1_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_BAST', $param);

            return (bool) $query;
        } else if ($tipe == 'reject') {
            $id = $param['id'];
            unset($param['id']);
            unset($param['no_form']);
            // $this->db->set('APPROVE1_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_BAST', $param);

            return (bool) $query;
        } else {
            $id = $param['ID'];
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            unset($param['ID']);

            $query = $this->db->update('MPE_BAST', $param);
            // echo $this->db->last_query();

            return (bool) $query;
        }
    }

    function getCount() {
        $this->db->select("NVL(MAX(CAST(SUBSTR(NO_BAST,17,3) AS INTEGER))+1,1) AS URUT", FALSE);
        $this->db->from('MPE_BAST');
        $this->db->where("SUBSTR(NO_BAST,0,4)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR(NO_BAST, 14, 2)='BA'");
        $urut = $this->db->get()->row();

        return $urut->URUT;
    }

    function get_record2($key) {
        $this->db->select('
                          A.ID,
                          A.ID_DOK_ENG,
                          A.NO_BAST,
                          A.NOTIFIKASI,
                          B.NO_DOK_ENG,
                          B.PACKET_TEXT,
                          D.UK_TEXT,
                          D.LOKASI,
                          A.HDR_TEXT,
                          A.MID_TEXT,
                          A.FTR_TEXT,
                          A.STATUS,
                          A.NOTE,
                          A.COMPANY,
                          A.APPROVE1_AT,
                          A.APPROVE1_BY,
                          A.APPROVE1_JAB,
                          A.APPROVE2_AT,
                          A.APPROVE2_BY,
                          A.APPROVE2_JAB,
                          A.REJECT_AT,
                          A.REJECT_BY,
                          A.REJECT_NOTE,
                          A.CREATE_AT,
                          A.CREATE_BY,
                          A.UPDATE_AT,
                          A.UPDATE_BY,
                          A.DELETE_AT,
                          A.DELETE_BY
                        ', FALSE);
        $this->db->from('MPE_BAST A');
        $this->db->join("MPE_DOK_ENG B", "A.ID_DOK_ENG = B.ID", 'left');
        $this->db->join("MPE_PENUGASAN C", "B.ID_PENUGASAN = C.ID", 'left');
        $this->db->join("MPE_PENGAJUAN D", "C.ID_PENGAJUAN = D.ID_MPE", 'left');
        // }
        $this->db->where('A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL AND C.DELETE_AT IS NULL AND D.DELETE_AT IS NULL');
        $this->db->where('A.ID', $key);

        echo $this->db->last_query();

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function get_erf_creator($id){
      $this->db->select("D.CREATE_BY, D.APPROVE_BY");
      $this->db->from("MPE_BAST A");
      $this->db->join("MPE_DOK_ENG B", "A.ID_DOK_ENG = B.ID");
      $this->db->join("MPE_PENUGASAN C", "B.ID_PENUGASAN = C.ID");
      $this->db->join("MPE_PENGAJUAN D", "C.ID_PENGAJUAN = D.ID_MPE");
      $this->db->where("A.ID", $id);
      $query = $this->db->get();
              // echo $this->db->last_query();
      return $query->result_array();
    }

}
