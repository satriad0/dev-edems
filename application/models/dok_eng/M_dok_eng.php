<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_dok_eng extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    var $column = array(
        'ID', 'CREATE_AT', 'NO_DOK_ENG', 'NOTIFIKASI', 'NO_PENGAJUAN', 'NO_PENUGASAN', 'PACKET_TEXT', 'STATUS', 'ID'
    );

    function _qry($key) {
        $this->db->select('
                            A."ID",
                            A.ID_PENUGASAN,
                            B.NO_PENUGASAN,
                            C.NO_PENGAJUAN,
                            A.NOTIFIKASI,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,
                            (SELECT DISTINCT TIPE FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) JENIS,
                            E."DESC" FL_TEXT,
                            (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=C.PLANT_SECTION AND MPLANT=C.MPLANT) DESCPSECTION,
                        		B.OBJECTIVE,
                            A.NO_DOK_ENG,
                            A.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.COLOR,
                            A.NOMINAL,
                            A.NOTE,
                            A.STATUS,
                            A.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=C.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'Paraf\' ) LIST_PARAF,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'CRT\' ) LIST_CRT,
                                
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND REF_TABEL=\'DOK_ENG\' AND TIPE = \'TTD\') JML_APP,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND REF_TABEL=\'DOK_ENG\' AND STATE=\'Approved\') STT_APP,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE,
                            (SELECT DISTINCT ID FROM MPE_BAST WHERE ID_DOK_ENG=A.ID AND STATUS=\'Closed\' AND DELETE_AT IS NULL) ID_BAST,
                            (CASE WHEN A.NOMINAL > (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_CODE=\'ECE_LIMIT\' AND GEN_VAL=\'1\') OR KAJIAN_FILE IS NOT NULL THEN 1 ELSE 0 END) ECE_LIMIT
                        ');
        $this->db->from('MPE_DOK_ENG A');
        $this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
        $this->db->join("MPE_PENGAJUAN C", "B.ID_PENGAJUAN = C.ID_MPE AND C.STATUS='Approved'");
        $this->db->join("MSO_FUNCLOC E", "E.TPLNR = C.FUNCT_LOC", 'left');
        if ($key['search'] !== '') {
            $sAdd = "(
                  LOWER(A.NO_DOK_ENG) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.NOTIFIKASI) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(C.NO_PENGAJUAN) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(B.NO_PENUGASAN) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.PACKET_TEXT) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.STATUS) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.CREATE_AT) LIKE '%" . strtolower($key['search']) . "%'
                  )";
            $this->db->where($sAdd, '', FALSE);
        }
        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('B.DELETE_AT IS NULL');
        $this->db->where('C.DELETE_AT IS NULL');
        if (isset($key['dept_code'])) {
            $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
            $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        $order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
    }

    function get($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    function get_data($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();

        return $data;
    }

    function recFil($key) {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function recTot($key) {
        $key['search'] = '';
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }
    
    function get_data_unit($where) {
        $this->db->select('
                            A."ID",
                            A.CREATE_AT,
                            A.ID_PENUGASAN,
                            B.NO_PENUGASAN,
                            C.NO_PENGAJUAN,
                            A.NOTIFIKASI,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,
                        		B.OBJECTIVE,
                            A.NO_DOK_ENG,
                            A.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.COLOR,
                            A.NOMINAL,
                            A.NOTE,
                            A.STATUS,
                            A.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=C.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'Paraf\' ) LIST_PARAF,
                            (
                            	SELECT
                            		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                            		\':\') WITHIN GROUP(
                            	ORDER BY
                            		ID ) csv
                            	FROM
                            		MPE_APPROVAL
                            	WHERE
                            		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'CRT\' ) LIST_CRT,
                            (
                          	SELECT
                          		listagg( APP_NAME,
                          		\',\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'TTD\' AND STATE = \'Approved\' ) NAME_APP,
                            (
                          	SELECT
                          		listagg( APP_NAME,
                          		\',\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'Paraf\' AND STATE = \'Approved\' ) NAME_PARAF,
                            (
                            	SELECT
                            		listagg( APP_NAME,
                            		\',\') WITHIN GROUP(
                            	ORDER BY
                            		ID ) csv
                            	FROM
                            		MPE_APPROVAL
                            	WHERE
                            		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'CRT\' AND STATE = \'Approved\' ) NAME_CRT,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND TIPE = \'TTD\' AND DELETE_AT IS NULL) JML_APP,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.UK_CODE,
                            A.UK_TEXT,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE,
                            A.NOTE1,
                            A.NOTE2,
                            A.NOTE3,
                            A.REJECT_DATE,
                            A.REJECT_BY,
                            A.REJECT_REASON,
                            (CASE WHEN A.NOMINAL > (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_CODE=\'ECE_LIMIT\' AND GEN_VAL=\'1\') OR KAJIAN_FILE IS NOT NULL THEN 1 ELSE 0 END) ECE_LIMIT
                        ');
        $this->db->from('MPE_DOK_ENG A');
        $this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
        $this->db->join("MPE_PENGAJUAN C", "B.ID_PENGAJUAN = C.ID_MPE AND C.STATUS='Approved'");
        $this->db->where('A.DELETE_AT IS NULL AND C.DELETE_AT IS NULL');
        $this->db->where($where);

        $query = $this->db->get();

        return $query->result_array();
    }

    function get_record($where) {
        $this->db->select('
                            A."ID",
                            A.CREATE_AT,
                            A.ID_PENUGASAN,
                            B.NO_PENUGASAN,
                            C.NO_PENGAJUAN,
                            A.NOTIFIKASI,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,
                            E."DESC" FL_TEXT,
                            (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=C.PLANT_SECTION AND MPLANT=C.MPLANT) DESCPSECTION,
                        		B.OBJECTIVE,
                            A.NO_DOK_ENG,
                            A.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.COLOR,
                            A.NOMINAL,
                            A.NOTE,
                            A.STATUS,
                            A.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=C.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'Paraf\' ) LIST_PARAF,
                            (
                            	SELECT
                            		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                            		\':\') WITHIN GROUP(
                            	ORDER BY
                            		ID ) csv
                            	FROM
                            		MPE_APPROVAL
                            	WHERE
                            		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'CRT\' ) LIST_CRT,
                            (
                          	SELECT
                          		listagg( APP_NAME,
                          		\',\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'TTD\' AND STATE = \'Approved\' ) NAME_APP,
                            (
                          	SELECT
                          		listagg( APP_NAME,
                          		\',\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL AND TIPE = \'Paraf\' AND STATE = \'Approved\' ) NAME_PARAF,
                            (
                            	SELECT
                            		listagg( APP_NAME,
                            		\',\') WITHIN GROUP(
                            	ORDER BY
                            		ID ) csv
                            	FROM
                            		MPE_APPROVAL
                            	WHERE
                            		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'CRT\' AND STATE = \'Approved\' ) NAME_CRT,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND TIPE = \'TTD\' AND DELETE_AT IS NULL) JML_APP,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.UK_CODE,
                            A.UK_TEXT,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE,
                            A.NOTE1,
                            A.NOTE2,
                            A.NOTE3,
                            A.REJECT_DATE,
                            A.REJECT_BY,
                            A.REJECT_REASON,
                            A.APPROVE2_MAN,
                            A.APPROVE2_EMAIL,
                            A.APPROVE3_MAN,
                            A.APPROVE3_EMAIL,
                            A.APPROVE2_BADGE,
                            A.APPROVE3_BADGE,
                            (CASE WHEN A.NOMINAL > (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_CODE=\'ECE_LIMIT\' AND GEN_VAL=\'1\') OR KAJIAN_FILE IS NOT NULL THEN 1 ELSE 0 END) ECE_LIMIT
                        ');
        $this->db->from('MPE_DOK_ENG A');
        $this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
        $this->db->join("MPE_PENGAJUAN C", "B.ID_PENGAJUAN = C.ID_MPE AND C.STATUS='Approved'");
        $this->db->join("MSO_FUNCLOC E", "E.TPLNR = C.FUNCT_LOC", 'left');
        $this->db->where('A.DELETE_AT IS NULL AND C.DELETE_AT IS NULL');
        $this->db->where($where);

        $query = $this->db->get();

        return $query->result_array();
//        echo $this->db->last_query();
    }

    public function get_new_foreign(){
      $this->db->select("PN.ID AS ID_PENUGASAN,
    PN.NO_PENUGASAN,
    PN.OBJECTIVE,
    PN.NOTIFIKASI,
    DPE.DESKRIPSI AS PENGAJUAN");
      $this->db->from("MPE_PENUGASAN PN");
      $this->db->join("MPE_DTL_PENUGASAN DPE", "PN.ID = DPE.ID_PENUGASAN ");
      $this->db->where("PN.STATUS != 'REJECTED' 
    AND PN.DELETE_AT IS NULL 
    AND DPE.DELETE_AT IS NULL 
    AND PN.ID_PENGAJUAN IN ( SELECT ID_MPE FROM MPE_PENGAJUAN WHERE STATUS = 'Approved' ) 
    AND ( SELECT COUNT( * ) FROM MPE_DTL_PENUGASAN DTP WHERE DTP.ID_PENUGASAN = PN.ID AND DTP.DELETE_AT IS NULL ) > (
SELECT
    COUNT( * ) 
FROM
    MPE_DOK_ENG MDE 
WHERE
    MDE.ID_PENUGASAN = PN.ID 
    AND MDE.DELETE_AT IS NULL 
    AND MDE.STATUS != 'Rejected' 
    ) 
    AND ((
    PN.APPROVE0_AT IS NOT NULL 
    AND PN.UK_CODE IN ( SELECT G1.GEN_PAR1 FROM MPE_GENERAL G1 WHERE G1.GEN_TYPE = 'APP_LIMIT' AND G1.GEN_CODE = 'EAT_DE' AND G1.GEN_VAL = '1' )) 
    OR (
    PN.APPROVE1_AT IS NOT NULL 
    AND PN.UK_CODE NOT IN ( SELECT G2.GEN_PAR1 FROM MPE_GENERAL G2 WHERE G2.GEN_TYPE = 'APP_LIMIT' AND G2.GEN_CODE = 'EAT_DE' AND G2.GEN_VAL = '1' )))");
      $query = $this->db->get();
      return $query->result_array();
    }

    public function get_foreign() {
        $this->db->select('
                        		PN."ID" ID_PENUGASAN,
                        		PN.NO_PENUGASAN,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = PN.ID_PENGAJUAN AND DELETE_AT IS NULL AND APPROVE_AT IS NOT NULL) PENGAJUAN,
                        		PN.NOTIFIKASI,
                        		PN.OBJECTIVE,
                          	PN.CUST_REQ,
                          	PN.SCOPE_ENG,
                          	PN.KOMITE_PENGARAH,
                          	PN.TIM_PENGKAJI,
                          	PN.TW_KOM_PENGARAH,
                          	PN.TW_TIM_PENGKAJI,
                          	PN.STATUS,
                        		PN.COMPANY,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = PN.ID AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                        		PN.APPROVE1_AT,
                        		PN.APPROVE1_BY,
                        		PN.APPROVE2_AT,
                        		PN.APPROVE2_BY,
                        		PN.CREATE_AT,
                        		PN.CREATE_BY,
                        		PN.UPDATE_AT,
                        		PN.UPDATE_BY,
                        		PN.DELETE_AT,
                        		PN.DELETE_BY
                          ');
        $this->db->from("MPE_PENUGASAN PN");
        $this->db->where("
                          PN.DELETE_AT IS NULL
                          AND PN.STATUS != 'Rejected'
                          AND PN.ID_PENGAJUAN IN (SELECT ID_MPE FROM MPE_PENGAJUAN WHERE STATUS='Approved')
                          AND (SELECT COUNT(*) FROM MPE_DTL_PENUGASAN WHERE ID_PENUGASAN=PN.ID AND DELETE_AT IS NULL)>(SELECT COUNT(*) FROM MPE_DOK_ENG WHERE ID_PENUGASAN=PN.ID AND DELETE_AT IS NULL AND STATUS != 'Rejected')
                          AND ((PN.APPROVE0_AT IS NOT NULL AND UK_CODE IN (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_TYPE='APP_LIMIT' AND GEN_CODE='EAT_DE' AND GEN_VAL='1'))
                          OR (PN.APPROVE1_AT IS NOT NULL AND UK_CODE NOT IN (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_TYPE='APP_LIMIT' AND GEN_CODE='EAT_DE' AND GEN_VAL='1')))
                        ");
        $query = $this->db->get();
        // echo $this->db->last_query();exit();

        return $query->result_array();
    }

    public function get_dtl_foreign($f_id = null) {
        $this->db->select('
                        		DPN."ID",
                          	DPN."ID_PENUGASAN",
                          	DPN."DESKRIPSI",
                          	DPN."REMARKS",
                          	DPN."PROGRESS",
                          	DPN."START_DATE",
                          	DPN."STATUS",
                          	DPN."NOTE",
                          	DPN."COMPANY",
                          	DPN."CREATE_AT",
                          	DPN."CREATE_BY",
                          	DPN."UPDATE_AT",
                          	DPN."UPDATE_BY",
                          	DPN."DELETE_AT",
                          	DPN."DELETE_BY",
                            A.TIPE
                          ');
        $this->db->from("MPE_DTL_PENUGASAN DPN");
        $this->db->join("MPE_PENUGASAN B", "B.ID = DPN.ID_PENUGASAN", "left");
        $this->db->join("MPE_PENGAJUAN A", "A.ID_MPE = B.ID_PENGAJUAN", "left");
        $this->db->where("
                          DPN.DELETE_AT IS NULL
                          AND B.STATUS != 'Rejected'
                          AND DPN.ID_PENUGASAN = '{$f_id}'
                          AND DPN.DESKRIPSI NOT IN (SELECT PACKET_TEXT FROM MPE_DOK_ENG WHERE ID_PENUGASAN = '{$f_id}' AND DELETE_AT IS NULL AND STATUS != 'Rejected')
                          AND A.STATE = 'Active'
                        	AND (LOWER( DPN.REMARKS ) NOT LIKE '%cancel%' ESCAPE '!' OR DPN.REMARKS IS NULL)
                        ");
        $this->db->order_by('DPN.DESKRIPSI', 'ASC');
        $query = $this->db->get();

        return $query->result_array();
    }

    function save($data) {
        $query = $this->db->insert('MPE_DOK_ENG', $data);
        $result = false;
        if ($query) {
            $this->db->select_max('ID', 'REF_ID');
            $result = $this->db->get('MPE_DOK_ENG')->row_array();
        }
        return $result;
    }

    function updateData($tipe, $param = array()) {
        if ($tipe == 'delete') {
            $id = $param['ID'];
            unset($param['ID']);
            $this->db->set('DELETE_AT', "CURRENT_DATE", false);
            $this->db->set('STATUS', "Deleted");
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_DOK_ENG', $param);

            return (bool) $query;
        } else if ($tipe == 'approve') {
            $id = $param['ID'];
            unset($param['ID']);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_DOK_ENG', $param);
            return (bool) $query;
        } else if ($tipe == 'reject') {
            $id = $param['ID'];
            unset($param['ID']);
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_DOK_ENG', $param);
            return (bool) $query;
        } else {
            $id = $param['ID'];
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            unset($param['ID']);

            $query = $this->db->update('MPE_DOK_ENG', $param);

            return (bool) $query;
        }
    }

    function getMgr($id) {
        $this->db->select('GEN_CODE, GEN_DESC,
                         GEN_VAL, GEN_PAR1,
                         GEN_PAR2, GEN_PAR3,
                         GEN_PAR4');
        $this->db->from('MPE_GENERAL');
        $this->db->where('GEN_CODE', $id);
        $query = $this->db->get();
        return $query->result_array();
    }

    function getIDDOK($id) {
        $this->db->select('
                        	MPE_DOK_ENG.ID,
                        	MPE_DOK_ENG.APPROVE1_AT,
                        	MPE_DOK_ENG.APPROVE1_BY,
                        	MPE_DOK_ENG.APPROVE1_JAB,
                        	MPE_DOK_ENG.APPROVE2_AT,
                        	MPE_DOK_ENG.APPROVE2_BY,
                        	MPE_DOK_ENG.APPROVE2_JAB,
                        	MPE_DOK_ENG.APPROVE3_AT,
                        	MPE_DOK_ENG.APPROVE3_BY,
                        	MPE_DOK_ENG.APPROVE3_JAB,
                        	MPE_DOK_ENG.REJECT_DATE,
                        	MPE_DOK_ENG.REJECT_BY,
                        	MPE_DOK_ENG.REJECT_REASON,
                        	MPE_DOK_ENG.CREATE_AT,
                        	MPE_DOK_ENG.CREATE_BY,
                        	MPE_DOK_ENG.PACKET_TEXT,
                        	NVL(MPE_DOK_ENG.NOTE1,' - ') NOTE1,
                        	NVL(MPE_DOK_ENG.NOTE2,' - ') NOTE2,
                        	NVL(MPE_DOK_ENG.NOTE3,' - ') NOTE3,
                        	MPE_BAST.CREATE_AT AS BASTAT,
                        	MPE_BAST.CREATE_BY AS BASTBY,
                        	MPE_BAST.APPROVE1_AT AS BAST1AT,
                        	MPE_BAST.APPROVE1_BY AS BAST1BY,
                        	NVL(MPE_BAST.NOTE1,' - ') AS BASTNOTE1,
                        	MPE_BAST.APPROVE1_0_AT,
                        	MPE_BAST.APPROVE1_0_BY,
                        	NVL(MPE_BAST.NOTE1_0,' - ') NOTE1_0,
                        	MPE_BAST.APPROVE2_AT AS bast2AT,
                        	MPE_BAST.APPROVE2_BY AS bast2BY,
                        	NVL(MPE_BAST.NOTE2,' - ') AS BASTNOTE2,
                        	MPE_BAST.APPROVE2_0_AT,
                        	MPE_BAST.APPROVE2_0_BY,
                        	NVL(MPE_BAST.NOTE2_0,' - ') NOTE2_0,
                        	MPE_BAST.NO_BAST,
                        	MPE_BAST.REJECT_BY AS BASTREJECTAT,
                        	MPE_BAST.REJECT_AT AS BASTREJECTBY,
                        	NVL(MPE_BAST.REJECT_NOTE,' - ') AS BASTREJECTNOTE'
        );
        $this->db->from('MPE_DOK_ENG');
        $this->db->join('MPE_PENUGASAN', 'MPE_PENUGASAN.ID = MPE_DOK_ENG.ID_PENUGASAN', 'left');
        $this->db->join('MPE_PENGAJUAN', 'MPE_PENGAJUAN.ID_MPE = MPE_PENUGASAN.ID_PENGAJUAN', 'left');
        $this->db->join('MPE_BAST', 'MPE_BAST.ID_DOK_ENG = MPE_DOK_ENG.ID', 'left');
        $this->db->where("(MPE_PENGAJUAN.ID_MPE = '{$id}' OR MPE_PENGAJUAN.NOTIFIKASI LIKE '{$id}')");
        $this->db->where('(MPE_PENGAJUAN.STATE = \'Active\' OR MPE_PENGAJUAN.STATE IS NULL)');
        $this->db->where('MPE_PENGAJUAN.STATUS != \'Rejected\'');
        $this->db->where('MPE_DOK_ENG.DELETE_AT IS NULL');
        $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
        $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
        $this->db->where('MPE_BAST.DELETE_AT IS NULL');
        $query = $this->db->get();

        return $query->result_array();
    }

    function getCount() {
        $this->db->select("NVL(MAX(CAST(SUBSTR(NO_DOK_ENG,15,3) AS INTEGER))+1,1) AS URUT", FALSE);
        $this->db->from('MPE_DOK_ENG');
        $this->db->where("SUBSTR(NO_DOK_ENG,-4)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR(NO_DOK_ENG, 12, 2)='TR'");
        $urut = $this->db->get()->row();

        return $urut->URUT;
    }
    
    public function set_log_download($data){ 
        $jd     = $data['JENIS_DOKUMEN'];
        $uid    = $data['USER_ID'];
        $nu     = $data['NAMA_USER'];
        $ju     = $data['JAB_USER'];
        $uku    = $data['UK_USER'];
        $idok   = $data['ID_DOK'];
        $tp     = $data['TIPE'];
        $date_time = date('d-M-y h.s.i A');
        
        $sql = "
			INSERT INTO MPE_DOWNLOAD_FILE
				 (JENIS_DOKUMEN, USER_ID, NAMA_USER, JAB_USER, UK_USER, ID_DOK, TIPE, TIME_AT)
			VALUES 
				('$jd', $uid, '$nu', '$ju', '$uku', $idok, '$tp', '$date_time')
		";
        
		$hasil = $this->db->query($sql);
		return $hasil;
    }
    
    public function get_histori_download($idok){
        $sql = "
          SELECT TIPE, TO_CHAR(TIME_AT,'DD-MM-YYYY HH24:MI:SS') AS TIME, NAMA_USER, JAB_USER, UK_USER
          
          FROM MPE_DOWNLOAD_FILE
          WHERE ID_DOK = $idok 
              AND JENIS_DOKUMEN = 'DOK_ENG'
          ORDER BY TIME_AT ASC
        ";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

}
