<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_assign extends CI_Model
{

    var $hris;

    public function __construct()
    {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    var $column = array(
        'ID', 'ID', 'CREATE_AT', 'NO_PENUGASAN', 'NOTIFIKASI', 'NO_PENGAJUAN', 'TIPE', 'SHORT_TEXT', 'STATUS', 'DESCPSECTION', 'FL_TEXT', 'COMPANY', 'ID'
    );

    function _qry($key)
    {
        $this->db->select('
                        	A.ID,
                        	A.NO_PENUGASAN,
                        	A.NOTIFIKASI,
                        	A.ID_PENGAJUAN,
                        	B.NO_PENGAJUAN,
                        	B.NOTIFIKASI NOTIF_NO,
                        	B.NAMA_PEKERJAAN SHORT_TEXT,
                        	B.TIPE,
                        	B.UK_TEXT,
                        	B.FUNCT_LOC,
                        	C.DESC FL_TEXT,
                          B.PLANT_SECTION,
                          (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=B.PLANT_SECTION AND MPLANT=B.MPLANT) DESCPSECTION,
                        	B.LOKASI,
                        	B.COMPANY,
                          (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=B.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                        	A.CREATE_AT,
                        	A.CREATE_BY,
                        	B.CUST_REQ,
                        	B.LATAR_BELAKANG,
                        	B.TECH_INFO,
                        	B.RES_MIT,
                          TO_CHAR(A."START_DATE", \'YYYY-MM-DD\') AS "START_DATE",
                          TO_CHAR(A."END_DATE", \'YYYY-MM-DD\') AS "END_DATE",
                        	A.OBJECTIVE,
                        	A.CUST_REQ CUS_REQ,
                        	A.STATUS,
                          (CASE WHEN (APPROVE0_AT IS NOT NULL OR APPROVE1_AT IS NOT NULL) THEN (SELECT COUNT(*) FROM MPE_DOK_ENG WHERE ID_PENUGASAN=A.ID AND DELETE_AT IS NULL AND STATUS!=\'Rejected\') ELSE 1 END) PENDING,
                        	A.SCOPE_ENG,
                        	A.DE_CONSTRUCT_COST,
                        	A.DE_ENG_COST,
                        	A.LEAD_ASS,
                        	A.KOMITE_PENGARAH,
                        	A.TIM_PENGKAJI,
                        	A.TW_KOM_PENGARAH,
                        	A.TW_TIM_PENGKAJI,
                          A.DEPT_CODE,
                          A.DEPT_TEXT,
                          A.UK_CODE,
                          A.UK_TEXT UNIT_KERJA,
                        	A.APPROVE0_AT,
                        	A.APPROVE0_BADGE,
                        	A.APPROVE0_BY,
                        	A.APPROVE0_JAB,
                        	A.APPROVE1_AT,
                        	A.APPROVE1_BY,
                        	A.APPROVE1_JAB,
                        	A.APPROVE2_AT,
                        	A.APPROVE2_BY,
                        	A.APPROVE2_JAB,
                          A.LIST_PEKERJAAN,
                          A.FILES,
                          A.FILES_AT,
                          A.FILES2,
                          A.FILES2_AT,
                          A.FILES3,
                          A.FILES3_AT,
                          A.NOTE0,
                            A.NOTE
                        ');
        $this->db->from('MPE_PENUGASAN A');
        $this->db->join("MPE_PENGAJUAN B", "A.ID_PENGAJUAN = B.ID_MPE");
        $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');
        if ($key['search'] !== '') {
            $sAdd ="(
                    LOWER(A.NO_PENUGASAN) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(A.NOTIFIKASI) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(A.CREATE_AT) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.NO_PENGAJUAN) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.TIPE) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.NAMA_PEKERJAAN) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(A.STATUS) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.UK_TEXT) LIKE '%".strtolower($key['search'])."%'
                    OR LOWER(B.LOKASI) LIKE '%".strtolower($key['search'])."%'
                    
                    )";
            $this->db->where($sAdd, '', FALSE);
            // OR LOWER(COMP_TEXT) LIKE '%".strtolower($key['search'])."%'
        }
        $this->db->where('A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL');
        if (isset($key['dept_code'])) {
            // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
            $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
            $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        $order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
    }

    function get($key)
    {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    function get_data($key)
    {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
//        echo $this->db->last_query();
        $data = $query->result();

        return $data;
    }

    function recFil($key)
    {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function recTot($key)
    {
        $key['search'] = '';
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_record($key)
    {
        $this->db->select('
                            A.ID,
                            A.CREATE_AT,
                            A.NO_PENUGASAN,
                            A.NOTIFIKASI,
                            A.ID_PENGAJUAN,
                            B.NO_PENGAJUAN,
                            B.NOTIFIKASI NOTIF_NO,
                            B.NAMA_PEKERJAAN SHORT_TEXT,
                            B.TIPE,
                            B.UK_TEXT,
                            B.FUNCT_LOC,
                            C.DESC FL_TEXT,
                            B.PLANT_SECTION,
                            (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=B.PLANT_SECTION AND MPLANT=B.MPLANT) DESCPSECTION,
                            B.LOKASI,
                            B.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=B.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            B.CUST_REQ,
                            B.LATAR_BELAKANG,
                            B.TECH_INFO,
                            B.RES_MIT,
                            TO_CHAR("START_DATE", \'YYYY-MM-DD\') AS "START_DATE",
                            START_DATE AS "START_DATE_FORMAT",
                            TO_CHAR("END_DATE", \'YYYY-MM-DD\') AS "END_DATE",
                            END_DATE AS "END_DATE_FORMAT",
                            A.OBJECTIVE,
                            A.CUST_REQ CUS_REQ,
                            A.NOTE0,
                            A.NOTE,
                            A.STATUS,
                            A.SCOPE_ENG,
                            A.LEAD_ASS,
                            A.KOMITE_PENGARAH,
                            A.TIM_PENGKAJI,
                            A.TW_KOM_PENGARAH,
                            A.TW_TIM_PENGKAJI,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.UK_CODE,
                            A.UK_TEXT,
                          	A.APPROVE0_AT,
                          	A.APPROVE0_BADGE,
                          	A.APPROVE0_BY,
                          	A.APPROVE0_JAB,
                          	A.APPROVE1_AT,
                          	A.APPROVE1_BY,
                          	A.APPROVE1_JAB,
                          	A.APPROVE2_AT,
                          	A.APPROVE2_BY,
                          	A.APPROVE2_JAB,
                            A.FILES,
                            A.FILES_AT,
                            A.FILES2,
                            A.FILES2_AT,
                            A.FILES3,
                            A.FILES3_AT,
                            A.LIST_PEKERJAAN,
                            A.REJECT_DATE,
                            A.UPDATE_AT
                        ');
        $this->db->from('MPE_PENUGASAN A');
        $this->db->join("MPE_PENGAJUAN B", "A.ID_PENGAJUAN = B.ID_MPE");
        $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');

        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('A.ID', $key);

        $query = $this->db->get();
//        echo $this->db->last_query();

        return $query->result_array();
    }
    
    function get_data_unit($key)
    {
        $this->db->select('
                            A.ID,
                            A.CREATE_AT,
                            A.NO_PENUGASAN,
                            A.NOTIFIKASI,
                            A.ID_PENGAJUAN,
                            B.NO_PENGAJUAN,
                            B.NOTIFIKASI NOTIF_NO,
                            B.NAMA_PEKERJAAN SHORT_TEXT,
                            B.TIPE,
                            B.UK_TEXT,
                            B.FUNCT_LOC,
                            C.DESC FL_TEXT,
                            B.PLANT_SECTION,
                            (SELECT DESCPSECTION FROM MSO_MPLANT WHERE PSECTION=B.PLANT_SECTION AND MPLANT=B.MPLANT) DESCPSECTION,
                            B.LOKASI,
                            B.COMPANY,
                            (SELECT DISTINCT GEN_PAR4 FROM MPE_GENERAL WHERE GEN_PAR2=B.COMPANY AND GEN_TYPE=\'DEF_PGROUP\') COMP_TEXT,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            B.CUST_REQ,
                            B.LATAR_BELAKANG,
                            B.TECH_INFO,
                            B.RES_MIT,
                            TO_CHAR("START_DATE", \'YYYY-MM-DD\') AS "START_DATE",
                            TO_CHAR("END_DATE", \'YYYY-MM-DD\') AS "END_DATE",
                            A.OBJECTIVE,
                            A.CUST_REQ CUS_REQ,
                            A.NOTE0,
                            A.NOTE,
                            A.STATUS,
                            A.SCOPE_ENG,
                            A.LEAD_ASS,
                            A.KOMITE_PENGARAH,
                            A.TIM_PENGKAJI,
                            A.TW_KOM_PENGARAH,
                            A.TW_TIM_PENGKAJI,
                            A.DEPT_CODE,
                            A.DEPT_TEXT,
                            A.UK_CODE,
                            A.UK_TEXT,
                          	A.APPROVE0_AT,
                          	A.APPROVE0_BADGE,
                          	A.APPROVE0_BY,
                          	A.APPROVE0_JAB,
                          	A.APPROVE1_AT,
                          	A.APPROVE1_BY,
                          	A.APPROVE1_JAB,
                          	A.APPROVE2_AT,
                          	A.APPROVE2_BY,
                          	A.APPROVE2_JAB,
                            A.FILES,
                            A.FILES_AT,
                            A.FILES2,
                            A.FILES2_AT,
                            A.FILES3,
                            A.FILES3_AT,
                            A.LIST_PEKERJAAN,
                            A.REJECT_DATE,
                            A.UPDATE_AT
                        ');
        $this->db->from('MPE_PENUGASAN A');
        $this->db->join("MPE_PENGAJUAN B", "A.ID_PENGAJUAN = B.ID_MPE");
        $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');

        $this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('A.NO_PENUGASAN', $key);

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_foreign()
    {
        $this->db->select('
                        		PE.ID_MPE ID_PENGAJUAN,
                        		PE.NO_PENGAJUAN,
                        		PE.NOTIFIKASI NOTIF_NO,
                        		PE.NAMA_PEKERJAAN SHORT_TEXT,
                        		PE.TIPE,
                        		PE.MPLANT,
                        		PE.PPLANT,
                        		PE.PLANNER_GROUP,
                        		PE.PLANT_SECTION,
                        		PE.FUNCT_LOC,
                        		FL."DESC" FL_TEXT,
                        		PE.DESSTDATE START_DATE,
                        		PE.DESENDDATE START_END,
                        		PE.UK_CODE,
                        		PE.UK_TEXT,
                        		PE.PRIORITY,
                        		PE.LATAR_BELAKANG,
                        		PE.CUST_REQ,
                        		PE.TECH_INFO,
                        		PE.RES_MIT,
                        		PE.CONSTRUCT_COST,
                        		PE.ENG_COST,
                        		PE.STATUS,
                        		PE.COMPANY,
                        		PE.APPROVE_AT,
                        		PE.APPROVE_BY,
                        		PE.CREATE_AT,
                        		PE.CREATE_BY,
                        		PE.UPDATE_AT,
                        		PE.UPDATE_BY,
                        		PE.DELETE_AT,
                        		PE.DELETE_BY,
                        		PE.LOKASI,
                        		PE.WBS_CODE,
                        		PE.WBS_TEXT
                          ');
        $this->db->from("MPE_PENGAJUAN PE");
        $this->db->join("MSO_FUNCLOC FL", "FL.TPLNR = PE.FUNCT_LOC", 'left');
        $this->db->where("
                          PE.DELETE_AT IS NULL
                          AND PE.STATUS = 'Approved'
                          AND FL.STATUS NOT LIKE '%DLFL%'
                          AND FL.STATUS NOT LIKE '%INAC%'
                          AND PE.ID_MPE NOT IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE DELETE_AT IS NULL AND STATUS != 'Rejected')
                        ");
        $query = $this->db->get();
        return $query->result_array();
    }

    public function get_pengkaji($company = '2000', $key = '50032326')
    {
        $this->hris->select('
                              muk_kode,
                              muk_short,
                              muk_nama,
                              muk_level,
                              muk_parent,
                              company,
                              muk_begda,
                              muk_endda,
                              muk_cctr,
                              muk_changed_on
                          ');
        $this->hris->from("v_unit_kerja");
        $this->hris->where("
                          muk_parent LIKE '%{$key}%'
                          AND company = '{$company}'
                        ");
        $this->hris->order_by('muk_nama', 'ASC');
        $query = $this->hris->get();
        // echo $this->hris->last_query();

        return $query->result_array();
    }

    public function get_lead($company = '2000', $key = '')
    {
        if (!$key) {
            $this->hris->where("mjab_nama LIKE '%Engineering%'");
        } else {
            $this->hris->where("muk_kode IN ({$key})");
        }

        $this->hris->select('
                              mjab_kode,
                              mjab_nama,
                              company,
                              muk_kode,
                              mjab_emp_subgroup,
                              mjab_emp_subgroup_txt,
                              mjab_begda,
                              mjab_endda,
                              is_chief,
                              mjab_changed_on
                          ');
        $this->hris->from("v_jabatan");
        $this->hris->where("
                            company = '{$company}'
                            AND mjab_emp_subgroup = '20'
                            /*AND mjab_endda > now()*/
                          ");
        $this->hris->distinct();
        $this->hris->order_by('mjab_nama', 'ASC');
        $query = $this->hris->get();
        // echo $this->hris->last_query();

        return $query->result_array();
    }

    function save($data)
    {
        // $START_DATE = str_replace("-", "/", $data['START_DATE']);
        // $END_DATE = str_replace("-", "/", $data['END_DATE']);
        $START_DATE = date("Y-m-d", strtotime(str_replace("/", "-", $data['START_DATE'])));
        $END_DATE = date("Y-m-d", strtotime(str_replace("/", "-", $data['END_DATE'])));
        $this->db->set('START_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
        $this->db->set('END_DATE', "TO_DATE('$END_DATE','yyyy/mm/dd')", false);
        unset($data['START_DATE']);
        unset($data['END_DATE']);

        $query = $this->db->insert('MPE_PENUGASAN', $data);
        $result = false;
        if ($query) {
            $this->db->select_max('ID', 'ID');
            $result = $this->db->get('MPE_PENUGASAN')->row_array();
        }
//        echo $this->db->last_query();
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param = array())
    {
        if ($tipe == 'delete') {
            $id = $param['id'];
            unset($param['id']);
            $this->db->set('DELETE_AT', "CURRENT_DATE", false);
            $this->db->set('STATUS', "Deleted");
            $this->db->where('ID', $id);
            $query = $this->db->update('MPE_PENUGASAN', $param);

            return (bool)$query;
        } else if ($tipe == 'approve') {
            $no_form = $param['no_form'];
            unset($param['no_form']);
            $this->db->where('NO_PENUGASAN', $no_form);
            $query = $this->db->update('MPE_PENUGASAN', $param);

            return (bool)$query;
        } else {
            $id = $param['ID'];
            $START_DATE = str_replace("-", "/", $param['START_DATE']);
            $END_DATE = str_replace("-", "/", $param['END_DATE']);
            $this->db->set('START_DATE', "TO_DATE('$START_DATE','yyyy/mm/dd')", false);
            $this->db->set('END_DATE', "TO_DATE('$END_DATE','yyyy/mm/dd')", false);
            $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
            $this->db->where('ID', $id);
            unset($param['ID']);
            unset($param['START_DATE']);
            unset($param['END_DATE']);

            $query = $this->db->update('MPE_PENUGASAN', $param);
            // echo $this->db->last_query();

            return (bool)$query;
        }
    }

    public function insert_det($param = array())
    {
        $result = $this->db->insert_batch('MPE_DTL_PENUGASAN', $param);
//        echo $this->db->last_query();

        return $result;
    }
    
    public function insert_paket_pekerjaan_revisi($param = array())
    {
        $this->db->select('LIST_PEKERJAAN');
        $this->db->from('MPE_PENUGASAN');
        $this->db->where('ID', $param['ID_PENUGASAN']);
        $query = $this->db->get();
        $data = $query->row();
        
        $new_pekerjaan = array();
        $ambil_list_pekerjaan = json_decode($data->LIST_PEKERJAAN, TRUE);
        foreach ($ambil_list_pekerjaan as $key => $value) {
            array_push($new_pekerjaan, $value);
        }
        array_push($new_pekerjaan, $param['DESKRIPSI']);
        
        $list_baru = json_encode($new_pekerjaan);
        
        $data_up = array(
            'LIST_PEKERJAAN' => $list_baru
        );
        $this->db->where('ID', $param['ID_PENUGASAN']);
        $this->db->update('MPE_PENUGASAN',$data_up);
        
        $result = $this->db->insert('MPE_DTL_PENUGASAN', $param);
        return $result;
    }

    public function edit_paket_pekerjaan($param = array(), $num)
    {
        $this->db->select('LIST_PEKERJAAN');
        $this->db->from('MPE_PENUGASAN');
        $this->db->where('ID', $param['ID_PENUGASAN']);
        $query = $this->db->get();
        $data = $query->row();
        
        $new_pekerjaan = array();
        $ambil_list_pekerjaan = json_decode($data->LIST_PEKERJAAN, TRUE);

        $pp = $param["DESKRIPSI"];
        $new = array($num => $pp);
        $update_pekerjaan = array_replace($ambil_list_pekerjaan, $new);
        
        $list_baru = json_encode($update_pekerjaan);
        $data_up = array(
            'LIST_PEKERJAAN' => $list_baru
        );
        $this->db->where('ID', $param['ID_PENUGASAN']);
        $this->db->update('MPE_PENUGASAN',$data_up);

        $this->db->select('ID');
        $this->db->from('MPE_DTL_PENUGASAN');
        $this->db->where('ID_PENUGASAN', $param['ID_PENUGASAN']);
        $this->db->where('DESKRIPSI', $ambil_list_pekerjaan[$num]);
        $query = $this->db->get();
        $data = $query->row();
        
        $result = $this->db->where("ID", $data->ID)->update('MPE_DTL_PENUGASAN', $param);
        return $result;
    }

    public function delete_paket_pekerjaan($param = array(), $num){
        $this->db->select('LIST_PEKERJAAN');
        $this->db->from('MPE_PENUGASAN');
        $this->db->where('ID', $param['ID_PENUGASAN']);
        $query = $this->db->get();
        $data = $query->row();

        $new_pekerjaan = array();
        $ambil_list_pekerjaan = json_decode($data->LIST_PEKERJAAN, TRUE);

        // UPDATE TBL DTL PENUGASAN
        $this->db->select('ID');
        $this->db->from('MPE_DTL_PENUGASAN');
        $this->db->where('ID_PENUGASAN', $param['ID_PENUGASAN']);
        $this->db->where('DESKRIPSI', $ambil_list_pekerjaan[$num]);
        $query = $this->db->get();
        $data = $query->row();

        $this->db->set('DELETE_AT', "CURRENT_DATE", false);
        $result = $this->db->where("ID", $data->ID)->update('MPE_DTL_PENUGASAN');
        // END UPDATE DTL PENUGASAN

        unset($ambil_list_pekerjaan[$num]);
        // $list_baru = json_encode($ambil_list_pekerjaan);
        $data_up = array("LIST_PEKERJAAN" => '["'.implode('","', $ambil_list_pekerjaan).'"]');

        $this->db->where('ID', $param['ID_PENUGASAN']);
        $this->db->update('MPE_PENUGASAN',$data_up);

        return $result;
    }

    public function update_det($param = array())
    {
        $this->db->where('DELETE_AT IS NULL');
        if (isset($param['ID_PENUGASAN'])) {
            $this->db->where('ID_PENUGASAN', $param['ID_PENUGASAN']);
            unset($param['ID_PENUGASAN']);
        }
        $result = $this->db->update('MPE_DTL_PENUGASAN', $param);
        // echo $this->db->last_query();

        return $result;
    }

    public function deleteDtl($param = array(), $where = array())
    {
        $id = $param['id'];
        unset($param['id']);
        $this->db->set('DELETE_AT', "CURRENT_DATE", false);
        $this->db->where('ID_PENUGASAN', $id);
        $this->db->where("DELETE_AT IS NULL");
        $query = $this->db->update('MPE_DTL_PENUGASAN', $param);
        // echo $this->db->last_query();

        return (bool)$query;
    }

    function getCount()
    {
        $this->db->select("NVL(MAX(CAST(SUBSTR(NO_PENUGASAN,15,3) AS INTEGER))+1,1) AS URUT", FALSE);
        $this->db->from('MPE_PENUGASAN');
        $this->db->where("SUBSTR(NO_PENUGASAN,-44)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR(NO_PENUGASAN, 12, 2)='EA'");
        $urut = $this->db->get()->row();

        return $urut->URUT;
    }

    public function get_approver_lead_tech($tech){
        $this->db->select("*");
        $this->db->from("MPE_GENERAL");
        $this->db->where("GEN_PAR1", $tech);
        $data = $this->db->get();
        return $data->row();
    }

    public function get_no_erf($no_eat){
        $this->db->select("B.NO_PENGAJUAN AS NO_ERF");
        $this->db->from("MPE_PENUGASAN A");
        $this->db->join("MPE_PENGAJUAN B", "A.ID_PENGAJUAN = B.ID_MPE");
        $this->db->where("A.NO_PENUGASAN", $no_eat);
        $data = $this->db->get();
        return $data->row();
    }

}
