<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_user_docs extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'NO_PENGAJUAN', 'PACKET_TEXT', 'ID'
  	);

  	function _qry($key){
    		$this->db->select('
                            A."ID",
                            A.ID_PENUGASAN,
                            B.NO_PENUGASAN,
                            A.NOTIFIKASI,
                            /*(SELECT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,*/
                            C.KDLOC,
                            (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_VAL = \'1\' AND GEN_DESC = C.KDLOC AND GEN_TYPE = \'Penomoran\' AND GEN_CODE = \'Lokasi\') LOKASI,
                            C.KDAREA,
                            (SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_VAL = \'1\' AND GEN_DESC = C.KDAREA AND GEN_TYPE = \'Penomoran\' AND GEN_CODE = \'Area\') AREA,
                            (C.NO_PENGAJUAN || \' \' || C.NAMA_PEKERJAAN) ERF,
                            C.NO_PENGAJUAN,
                            C.NAMA_PEKERJAAN,
                        		B.OBJECTIVE,
                            A.NO_DOK_ENG,
                            C.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.COLOR,
                            A.NOTE,
                            A.STATUS,
                            (CASE APPROVE3_AT WHEN NULL THEN 1 ELSE (SELECT COUNT(*) FROM MPE_DOK_TEND WHERE ID_DOK_ENG=A.ID AND DELETE_AT IS NULL AND STATUS!=\'Rejected\') END) PENDING,
                            A.COMPANY,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'Paraf\' ) LIST_PARAF,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND TIPE = \'TTD\') JML_APP,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE,
                            C.APPROVE_BY AS ERF_APPROVER,
                            C.CREATE_BY AS ERF_CREATOR,
                            (SELECT ID FROM MPE_BAST WHERE ID_DOK_ENG=A.ID AND STATUS=\'Closed\' AND DELETE_AT IS NULL) ID_BAST,
                            (SELECT COUNT(*) FROM MPE_BAST WHERE ID_DOK_ENG = A."ID" AND STATUS = \'Closed\' /*AND STATE IS NOT NULL*/ AND DELETE_AT IS NULL ) ISCLOSED
                        ', false);
    		$this->db->from('MPE_DOK_ENG A');
    		$this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID",'right');
    		$this->db->join("MPE_PENGAJUAN C", "C.ID_MPE = B.ID_PENGAJUAN",'right');
    		// $this->db->join("MSO_FUNCLOC C", "C.TPLNR = B.FUNCT_LOC", 'left');
    		if($key['search']!==''){
          $sAdd ="(
                  LOWER(A.NO_DOK_ENG) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.PACKET_TEXT) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(B.NO_PENUGASAN) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.NOTIFIKASI) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(C.KDLOC) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER((SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_VAL = '1' AND GEN_DESC = C.KDLOC AND GEN_TYPE = 'Penomoran' AND GEN_CODE = 'Lokasi')) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(C.KDAREA) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER((SELECT GEN_PAR1 FROM MPE_GENERAL WHERE GEN_VAL = '1' AND GEN_DESC = C.KDAREA AND GEN_TYPE = 'Penomoran' AND GEN_CODE = 'AREA')) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(C.NAMA_PEKERJAAN) LIKE '%".strtolower($key['search'])."%'
                  )";
          $this->db->where($sAdd, '', FALSE);
    		}
    		$this->db->where('A.DELETE_AT IS NULL');
        $this->db->where('A.STATUS <> \'Rejected\'');
    		$this->db->where('B.DELETE_AT IS NULL');
    		$this->db->where('C.NO_PENGAJUAN IS NOT NULL');
    		$this->db->where('B.NO_PENUGASAN IS NOT NULL');
    		$this->db->where('C.DELETE_AT IS NULL AND (C.STATE = \'Active\' OR C.STATE IS NULL)');
        if (isset($key['dept_code'])) {
          // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
          $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        // if (isset($key['create_by'])) {
        //   $this->db->where("C.CREATE_BY = '{$key['create_by']}'");
        // }
        if (isset($key['uk_kode'])) {
          $this->db->where("C.UK_CODE = '{$key['uk_kode']}'");
        }
    		$order = $this->column[$key['ordCol']];
    		$this->db->order_by($order, $key['ordDir']);
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
      // echo $this->db->last_query();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

    function recTot($key){
      $key['search']='';
      $this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function get_record($where){
    		$this->db->select('
                            A."ID",
                            A.ID_PENUGASAN,
                            B.NO_PENUGASAN,
                            A.NOTIFIKASI,
                            (SELECT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = B.ID_PENGAJUAN) PENGAJUAN,
                        		B.OBJECTIVE,
                            A.NO_DOK_ENG,
                            A.TIPE,
                            A.ID_PACKET,
                            A.PACKET_TEXT,
                            A.COLOR,
                            A.NOTE,
                            A.STATUS,
                            A.COMPANY,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = A.ID AND DELETE_AT IS NULL AND TIPE = \'Paraf\' ) LIST_PARAF,
                          	(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = "A"."ID" AND TIPE = \'TTD\' AND DELETE_AT IS NULL) JML_APP,
                            A.APPROVE1_AT,
                            A.APPROVE1_BY,
                            A.APPROVE1_JAB,
                            A.APPROVE2_AT,
                            A.APPROVE2_BY,
                            A.APPROVE2_JAB,
                            A.APPROVE3_AT,
                            A.APPROVE3_BY,
                            A.APPROVE3_JAB,
                            A.APPROVE4_AT,
                            A.APPROVE4_BY,
                            A.APPROVE4_JAB,
                            A.APPROVE5_AT,
                            A.APPROVE5_BY,
                            A.APPROVE5_JAB,
                            A.APPROVE6_AT,
                            A.APPROVE6_BY,
                            A.APPROVE6_JAB,
                            A.APPROVE7_AT,
                            A.APPROVE7_BY,
                            A.APPROVE7_JAB,
                            A.APPROVE8_AT,
                            A.APPROVE8_BY,
                            A.APPROVE8_JAB,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY,
                            A.RKS_FILE,
                            A.RKS_AT,
                            A.RKS_STATE,
                            A.BQ_FILE,
                            A.BQ_AT,
                            A.BQ_STATE,
                            A.DRAW_FILE,
                            A.DRAW_AT,
                            A.DRAW_STATE,
                            A.ECE_FILE,
                            A.ECE_AT,
                            A.ECE_STATE,
                            A.KAJIAN_FILE,
                            A.KAJIAN_AT,
                            A.KAJIAN_STATE
                        ');
    		$this->db->from('MPE_DOK_ENG A');
    		$this->db->join("MPE_PENUGASAN B", "A.ID_PENUGASAN = B.ID");
    		$this->db->where('A.DELETE_AT IS NULL');
    		$this->db->where($where);

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
  	}

    public function get_foreign(){
        $this->db->select('
                        		PN."ID" ID_PENUGASAN,
                        		PN.NO_PENUGASAN,
                            (SELECT DISTINCT NAMA_PEKERJAAN FROM MPE_PENGAJUAN WHERE ID_MPE = PN.ID_PENGAJUAN AND DELETE_AT IS NULL AND APPROVE_AT IS NOT NULL) PENGAJUAN,
                        		PN.NOTIFIKASI,
                        		PN.OBJECTIVE,
                          	PN.CUST_REQ,
                          	PN.SCOPE_ENG,
                          	PN.KOMITE_PENGARAH,
                          	PN.TIM_PENGKAJI,
                          	PN.TW_KOM_PENGARAH,
                          	PN.TW_TIM_PENGKAJI,
                          	PN.STATUS,
                        		PN.COMPANY,
                            (
                          	SELECT
                          		listagg( APP_BADGE|| \' - \' || APP_NAME || \' - \' || APP_EMAIL || \' - \' || APP_JAB,
                          		\':\') WITHIN GROUP(
                          	ORDER BY
                          		ID ) csv
                          	FROM
                          		MPE_APPROVAL
                          	WHERE
                          		REF_ID = PN.ID AND DELETE_AT IS NULL AND TIPE = \'TTD\' ) LIST_APP,
                        		PN.APPROVE1_AT,
                        		PN.APPROVE1_BY,
                        		PN.APPROVE2_AT,
                        		PN.APPROVE2_BY,
                        		PN.CREATE_AT,
                        		PN.CREATE_BY,
                        		PN.UPDATE_AT,
                        		PN.UPDATE_BY,
                        		PN.DELETE_AT,
                        		PN.DELETE_BY
                          ');
    		$this->db->from("MPE_PENUGASAN PN");
        $this->db->where("
                          PN.DELETE_AT IS NULL
                          AND PN.APPROVE1_AT IS NOT NULL
                          AND PN.APPROVE2_AT IS NOT NULL
                          AND PN.ID_PENGAJUAN IN (SELECT ID_MPE FROM MPE_PENGAJUAN WHERE STATUS='Approved')
                        ");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_dtl_foreign($f_id = null){
        $this->db->select('
                        		DPN."ID",
                          	DPN."ID_PENUGASAN",
                          	DPN."DESKRIPSI",
                          	DPN."REMARKS",
                          	DPN."PROGRESS",
                          	DPN."START_DATE",
                          	DPN."STATUS",
                          	DPN."NOTE",
                          	DPN."COMPANY",
                          	DPN."CREATE_AT",
                          	DPN."CREATE_BY",
                          	DPN."UPDATE_AT",
                          	DPN."UPDATE_BY",
                          	DPN."DELETE_AT",
                          	DPN."DELETE_BY",
                            A.TIPE
                          ');
    		$this->db->from("MPE_DTL_PENUGASAN DPN");
        $this->db->join("MPE_PENUGASAN B", "B.ID = DPN.ID_PENUGASAN", "left");
      	$this->db->join("MPE_PENGAJUAN A", "A.ID_MPE = B.ID_PENGAJUAN", "left");
        $this->db->where("
                          DPN.DELETE_AT IS NULL
                          AND DPN.ID_PENUGASAN = '{$f_id}'
                          AND A.STATE = 'Active'
                        ");
        $query = $this->db->get();
        //echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        $query = $this->db->insert('MPE_DOK_ENG', $data);
        $result = false;
        if($query)
        {
          $this->db->select_max('ID','REF_ID');
          $result = $this->db->get('MPE_DOK_ENG')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_DOK_ENG', $param);

          return (bool) $query;
        }else  if($tipe=='approve'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_DOK_ENG', $param);
          return (bool) $query;
        }else  if($tipe=='reject'){
          $id = $param['ID'];
          unset($param['ID']);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_DOK_ENG', $param);
          return (bool) $query;
        }else {
          $id = $param['ID'];
          $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          unset($param['ID']);

          $query = $this->db->update('MPE_DOK_ENG', $param);
          // echo $this->db->last_query();

          return (bool) $query;
        }
    }

    function getMgr($id){
      $this->db->select('GEN_CODE, GEN_DESC,
                         GEN_VAL, GEN_PAR1,
                         GEN_PAR2, GEN_PAR3,
                         GEN_PAR4');
      $this->db->from('MPE_GENERAL');
      $this->db->where('GEN_CODE' , $id);
      $query = $this->db->get();
      return $query->result_array();
    }

    function getIDDOK($id){
      $this->db->select('MPE_DOK_ENG.ID,
                        MPE_DOK_ENG.APPROVE1_AT,
                        MPE_DOK_ENG.APPROVE1_BY,
                        MPE_DOK_ENG.APPROVE1_JAB,
                        MPE_DOK_ENG.APPROVE2_AT,
                        MPE_DOK_ENG.APPROVE2_BY,
                        MPE_DOK_ENG.APPROVE2_JAB,
                        MPE_DOK_ENG.APPROVE3_AT,
                        MPE_DOK_ENG.APPROVE3_BY,
                        MPE_DOK_ENG.APPROVE3_JAB,
                        MPE_DOK_ENG.REJECT_DATE,
                        MPE_DOK_ENG.REJECT_BY,
                        MPE_DOK_ENG.REJECT_REASON,
                        MPE_DOK_ENG.CREATE_AT,
                        MPE_DOK_ENG.CREATE_BY,
                        MPE_DOK_ENG.PACKET_TEXT,
                        MPE_DOK_ENG.NOTE1,
                        MPE_DOK_ENG.NOTE2,
                        MPE_DOK_ENG.NOTE3,
                        MPE_BAST.APPROVE1_AT AS BAST1AT,
                        MPE_BAST.APPROVE1_BY AS BAST1BY,
                        MPE_BAST.NOTE1 AS BASTNOTE1,
                        MPE_BAST.APPROVE1_0_AT,
                        MPE_BAST.APPROVE1_0_BY,
                        MPE_BAST.NOTE1_0,
                        MPE_BAST.APPROVE2_AT AS bast2AT,
                        MPE_BAST.APPROVE2_BY AS bast2BY,
                        MPE_BAST.NOTE2  AS BASTNOTE2,
                        MPE_BAST.APPROVE2_0_AT,
                        MPE_BAST.APPROVE2_0_BY,
                        MPE_BAST.NOTE2_0');
      $this->db->from('MPE_DOK_ENG');
      $this->db->join('MPE_PENUGASAN', 'MPE_PENUGASAN.ID = MPE_DOK_ENG.ID_PENUGASAN', 'left');
      $this->db->join('MPE_PENGAJUAN', 'MPE_PENGAJUAN.ID_MPE = MPE_PENUGASAN.ID_PENGAJUAN', 'left');
      $this->db->join('MPE_BAST', 'MPE_BAST.ID_DOK_ENG = MPE_DOK_ENG.ID', 'left');
      $this->db->where('MPE_PENGAJUAN.ID_MPE' , $id);
      $this->db->where('MPE_DOK_ENG.DELETE_AT IS NULL');
      $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
      $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
      $this->db->where('MPE_BAST.DELETE_AT IS NULL');
      $query = $this->db->get();
      //echo $this->db->last_query();
      return $query->result_array();
    }

    function getCount(){
      $this->db->distinct('ID');
      $this->db->from('MPE_DOK_ENG');
      return $this->db->count_all_results();
    }

}
