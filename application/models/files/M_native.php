<?php

class M_native extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
    }
    
    function getTypePekerjaan() {
        $this->db->select('"GEN_DESC","GEN_CODE"');
        $this->db->from('MPE_GENERAL');
        $this->db->where('GEN_VAL', '1');
        $this->db->where('GEN_TYPE', 'NTV_TYPE');
        $result = $this->db->get();

        return $result->result();
    }
    
    function getListCompany() {
        $this->db->select('"GEN_DESC","GEN_PAR1"');
        $this->db->from('MPE_GENERAL');
        $this->db->where('GEN_VAL', '1');
        $this->db->where('GEN_TYPE', 'General');
        $this->db->where('GEN_CODE', 'COMP');
        $result = $this->db->get();

        return $result->result();
    }

}
