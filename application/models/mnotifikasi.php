<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Mnotifikasi extends CI_Model {

    private $_myTable = "VMSO_CHECKLIST";

    function mget_data($param, $mplant, $status, $sapStatus, $filterDahsboard, $notif) {
        $this->db->select("*");
        $this->db->from($this->_myTable);
        $this->db->where($param);
//        echo $filterDahsboard.'s';exit;
        if ($filterDahsboard != 'today_notif') {
            if ($notif == 'yes') {
                $this->db->where('NOTIFNUMBER IS NULL', '', FALSE);
            } else {
                $this->db->where('NOTIFNUMBER IS NOT NULL', '', FALSE);
                $this->db->where('STATUS', 2);
            }

            if (count($mplant) > 0) {
                $this->db->where_in("MPLANT", $mplant);
            } else {
                $this->db->where_in("MPLANT", array('343434'));
            }
            if (count($status) > 0) {
                $this->db->where_in("STATUS", $status);
            }
//            if(count($sapStatus) > 0){
//                $this->db->where_in("SAP_STATUS",$sapStatus);
//            }
        } else {
            $date = date('d-m-Y');
            $this->db->where("to_char(NOTIFCREATED,'dd-mm-yyyy')=", "'$date'", false);
            $this->db->where_in("MPLANT", $mplant);
        }
//        $this->db->where('SAP_STATUS IN NULL','',FALSE);
        $this->db->order_by("IDAMTRANS", "DESC");
        $result = $this->db->get("");
        return $result->num_rows() > 0 ? $result->result() : false;
    }

    function set_params() {

        $col = array(
            '',
            'IDAMTRANS',
            '',
            'NOTIFNUMBER',
            '',
            '',
            'SAP_STATUS',
            'MPLANT',
            "(DESCPSECTION || '-' ||PSECTION)",
            "(DESCPGROUP || '-' ||PGROUP)",
            'EQUPTNAME',
            'ORDERNUMBER',
            'MPI',
            'PRIORITYTEXT',
            'KONDISITEXT',
            'ACTIONTEXT',
            'ABNORMAL',
            'AKTIFITAS',
            'KETERANGAN',
            'USERAD',
            'MSO_SOURCE',
            'T_INSERT2',
            '',
            ''
        );
        $session = $this->session->userdata('ses_log_id');
        if ($session['company'] == '5000' || $session['company'] == '7000' || $session['company'] == '4000') {
            $this->db->where("PSECTION IN (SELECT PSECTION FROM MSO_MAP_MPLANT WHERE USERNAME = '" . $session['username'] . "')");
        }

        if ($_POST['customSearch'] == 'true') {

            $i = 0;
            foreach ($_POST['column'] as $value) {
                if ($_POST['column'][$i] != 'default' && $_POST['filter'][$i] != 'default' && $_POST['value'][$i] != '') {
                    if ($_POST['column'][$i] == '21') {
                        $this->db->where("TO_DATE(" . $col[$_POST['column'][$i]] . ",'mm/dd/yyyy HH24:MI') " . $_POST['filter'][$i] . " TO_DATE('" . $_POST['value'][$i] . "','mm/dd/yyyy')");
                    } else {

                        $persen = (strpos($_POST['filter'][$i], 'like') !== false) ? '%' : '';
                        $this->db->where('LOWER(' . $col[$_POST['column'][$i]] . ") " . $_POST['filter'][$i] . " '$persen" . strtolower($_POST['value'][$i]) . "$persen'");
                    }
                    // echo $col[$_POST['column'][$i]] . " " . $_POST['filter'][$i] . " '$persen" . $_POST['value'][$i] . "$persen'";
                }
                $i++;
            }
        }

        if ($_POST['sSearch'] != '') {
            if ($_POST['colFilter'] == '') {
                $where = '(';
                foreach ($col as $value) {
                    if ($value !== '') {
                        $where .= 'LOWER(' . $value . ") LIKE '%" . strtolower($_POST['sSearch']) . "%' OR ";
                    }
                }
                $where = substr_replace($where, ")", -3);
                $this->db->where($where);
            } else {
                $this->db->where('LOWER(' . $col[$_POST['colFilter']] . ") LIKE '%" . strtolower($_POST['sSearch'] . "%'"));
            }
        }

        if ($_POST['ordFilter'] != '' && $_POST['ordFilter'] != '--') {
            $this->db->order_by($col[$_POST['ordFilter']], $_POST['sortFilter']);
        }
        
        if(isset($_POST['type']) && @$_POST['type']=='excel'){
            $_POST['iDisplayLength'] = 12345;
            $_POST['iDisplayStart'] = 0;
        }
    }

    function mget_data_paging($param, $mplant, $status, $sapStatus, $filterDahsboard, $notif) {

        $this->db->select($this->_myTable . ".*,ASTNR");
        $this->db->from($this->_myTable);
        $this->db->join('MSO_ORDER', 'ORDERNUMBER=AUFNR', 'left');
        $sap_status = $param['SAP_STATUS'];
        unset($param['SAP_STATUS']);

        $this->db->where($param);
        // if(!empty($sap_status)){
        $this->db->like('SAP_STATUS', $sap_status);
        // }

        if ($filterDahsboard != 'today_notif') {
            if ($notif == 'yes') {
                $this->db->where('NOTIFNUMBER IS NULL', '', FALSE);
            } else {
                $this->db->where('NOTIFNUMBER IS NOT NULL', '', FALSE);
                $this->db->where('STATUS', 2);
            }

            if (count($mplant) > 0) {
                $this->db->where_in("MPLANT", $mplant);
            } else {
                $this->db->where_in("MPLANT", array('343434'));
            }
            if (count($status) > 0) {
                $this->db->where_in("STATUS", $status);
            }
//            if(count($sapStatus) > 0){
//                $this->db->where_in("SAP_STATUS",$sapStatus);
//            }
        } else {
            $date = date('d-m-Y');
            $this->db->where("to_char(NOTIFCREATED,'dd-mm-yyyy')=", "'$date'", false);
            $this->db->where_in("MPLANT", $mplant);
        }

        $this->set_params();
//        $this->db->where('SAP_STATUS IN NULL','',FALSE);
        $this->db->order_by("IDAMTRANS", "DESC");

        $temp = clone $this->db;
        //now we run the count method on this copy
        $num_rows = $temp->count_all_results();

        
        $this->db->limit(($_POST['iDisplayLength']), $_POST['iDisplayStart'] + 1);

        $result = $this->db->get("");
        $rsl['result'] = $result->result();
        $rsl['count'] = $num_rows;
        return $result->num_rows() > 0 ? $rsl : false;
    }

    function mget_top_abnormalitas($id) {

        $this->db->select($this->_myTable . ".*,ASTNR");
        $this->db->from($this->_myTable);
        $this->db->join('MSO_ORDER', 'ORDERNUMBER=AUFNR', 'left');
        $this->db->where('"IDEQUPTMENT"', $id);
        $this->db->where('"IS_DELETE"', '0');
        switch ($this->input->post('timeOption')) {
            case 'thismonth' :
                $this->db->where("TO_CHAR(T_INSERT,'mmyyyy') = '" . date('mY') . "' ");
                break;
            case 'daterange' :
                $this->db->where("T_INSERT between TO_DATE('" . $this->input->post('sdate') . "','dd/mm/yyyy') AND TO_DATE('" . $this->input->post('edate') . "','dd/mm/yyyy') ");
                break;
            default :
                break;
        }
        $this->set_params();

        $tempdb = clone $this->db;
        //now we run the count method on this copy
        $num_rows = $tempdb->count_all_results();
        $this->db->limit(($_POST['iDisplayLength']), $_POST['iDisplayStart'] + 1);
        $result = $this->db->get("");
        $rsl['result'] = $result->result();
        $rsl['count'] = $num_rows;
        return $result->num_rows() > 0 ? $rsl : false;
    }

    function mget_top_abnormalitas_psec($mplant, $psec) {


        $this->db->select(" * FROM
                        (SELECT
                            IDAMTRANS,
                            NOTIFNUMBER,
                            SAP_STATUS,
                            DESCPSECTION,
                            DESCPGROUP,
                            PGROUP,
                            EQUPTNAME,
                            ORDERNUMBER,
                            MPI,
                            PRIORITYTEXT,
                            KONDISITEXT,
                            ACTIONTEXT,
                            ABNORMAL,
                            AKTIFITAS,
                            KETERANGAN,
                            USERAD,
                            MSO_SOURCE,
                            T_INSERT2,
                            TB0.T_INSERT,
                            TB0.MPLANT,
                            IS_DELETE,
                            CASE
                    WHEN TB0.PSECTION IS NULL THEN
                            TB1.PSECTION
                    ELSE
                            TB0.PSECTION
                    END PSECTION

                    FROM
                            VMSO_CHECKLIST TB0
                    LEFT JOIN MSO_USER TB1 ON USERAD = USERNAME ) VMSO_CHECKLIST", false);

        $this->db->where('"MPLANT"', $mplant);
        $this->db->where('"PSECTION"', $psec);
        $this->db->where('"IS_DELETE"', '0');
        switch ($this->input->post('timeOption')) {
            case 'thismonth' :
                $this->db->where("TO_CHAR(T_INSERT,'mmyyyy') = '" . date('mY') . "' ");
                break;
            case 'daterange' :
                $this->db->where("T_INSERT between TO_DATE('" . $this->input->post('sdate') . "','dd/mm/yyyy') AND TO_DATE('" . $this->input->post('edate') . "','dd/mm/yyyy') ");
                break;
            default :
                break;
        }
        $this->set_params();

        $tempdb = clone $this->db;
        //now we run the count method on this copy
        $num_rows = $tempdb->get();
        $this->db->limit(($_POST['iDisplayLength']), $_POST['iDisplayStart'] + 1);
        $result = $this->db->get("");
        $rsl['result'] = $result->result();
        $rsl['count'] = count($num_rows->result_array());
        return $result->num_rows() > 0 ? $rsl : false;
    }

    function mget_today_abnormalitas($id) {

        $this->db->select($this->_myTable . ".*,ASTNR");
        $this->db->from($this->_myTable);
        $this->db->join('MSO_ORDER', 'ORDERNUMBER=AUFNR', 'left');
        $this->db->where('"IDAMTRANS"', $id);
        $this->db->where('"IS_DELETE"', '0');
        $this->set_params();
        $tempdb = clone $this->db;
        //now we run the count method on this copy
        $num_rows = $tempdb->count_all_results();
        $this->db->limit(($_POST['iDisplayLength']), $_POST['iDisplayStart'] + 1);
        $result = $this->db->get("");
        $rsl['result'] = $result->result();
        $rsl['count'] = $num_rows;
        return $result->num_rows() > 0 ? $rsl : false;
    }

    function mget_detail_notif($id) {
        $this->db->select("*");
        $this->db->from($this->_myTable);
        $this->db->where('"IDAMTRANS"', $id);
        $result = $this->db->get("");
        return $result->num_rows() > 0 ? $result->result_array() : false;
    }

    function mget_excel($filterDb, $param, $mplant, $notif, $sSort, $vSort, $sSearch, $vSearch, $custom) {
        $col = array(
            '',
            'IDAMTRANS',
            '',
            'NOTIFNUMBER',
            '',
            '',
            'SAP_STATUS',
            'MPLANT',
            "(DESCPSECTION || '-' ||PSECTION)",
            "(DESCPGROUP || '-' ||PGROUP)",
            'EQUPTNAME',
            'ORDERNUMBER',
            'MPI',
            'PRIORITYTEXT',
            'KONDISITEXT',
            'ACTIONTEXT',
            'ABNORMAL',
            'AKTIFITAS',
            'KETERANGAN',
            'USERAD',
            'MSO_SOURCE',
            'T_INSERT2',
            '',
            ''
        );
        $col1 = array(
            'No',
            'IDTRANS',
            'Edit',
            'Notif',
            'Closed',
            'Del',
            'Status',
            'MPLANT',
            "PSECTION",
            "PGROUP",
            'Nomenclature',
            'Order',
            'MPI',
            'Priority',
            'Condition',
            'Action',
            'Abnormal',
            'Activity',
            'Remark',
            'Create By',
            'Source',
            'Create date',
            '',
            ''
        );

        $this->db->select("*");
        $this->db->from($this->_myTable);
        $this->db->where($param);
        if ($notif == 'yes') {
            $this->db->where('NOTIFNUMBER IS NULL');
        } else {
            $this->db->where('NOTIFNUMBER IS NOT NULL');
            $this->db->where('STATUS', 2);
        }
        $this->db->where_in("MPLANT", $mplant);

        if ($custom['customSearch'] == 'true') {

            $i = 0;
            foreach ($custom['column'] as $value) {
                if ($custom['column'][$i] != 'default' && $custom['filter'][$i] != 'default' && $custom['value'][$i] != '') {
                    if ($custom['column'][$i] == '21') {
                        $this->db->where("TO_DATE(" . $col[$custom['column'][$i]] . ",'mm/dd/yyyy HH24:MI') " . $custom['filter'][$i] . " TO_DATE('" . $custom['value'][$i] . "','mm/dd/yyyy')");
                    } else {

                        $persen = (strpos($custom['filter'][$i], 'like') !== false) ? '%' : '';
                        $this->db->where('LOWER(' . $col[$custom['column'][$i]] . ") " . $custom['filter'][$i] . " '$persen" . strtolower($custom['value'][$i]) . "$persen'");
                    }
                    // echo $col[$_POST['column'][$i]] . " " . $_POST['filter'][$i] . " '$persen" . $_POST['value'][$i] . "$persen'";
                }
                $i++;
            }
        }

        if ($vSearch != 'null') {
            if ($sSearch == 'null') {
                $where = '(';
                foreach ($col as $value) {
                    if ($value !== '') {
                        $where .= 'LOWER(' . $value . ") LIKE '%" . strtolower($vSearch) . "%' OR ";
                    }
                }
                $where = substr_replace($where, ")", -3);

                $this->db->where($where);
            } else {
                $this->db->where('LOWER(' . $col[$sSearch] . ") LIKE '%" . strtolower($vSearch) . "%'");
            }
        }
        if ($sSort != 'null') {
            $this->db->order_by($col[$sSort], $vSort);
        }
        $data = clone $this->db;
        $count = $this->db->count_all_results();
        $result = array();

        if ($count > 2500) {
            $jml = ceil($count / 2500);
            for ($i = 0; $i < $jml; $i++) {
                $cloning = clone $data;
                $cloning->limit(2500, 2500 * $i + 1);
                $result[] = $cloning->get();
            }
        } else {
            $result[] = $data->get();
        }
        return array(
            'data' => $result,
            'filterDb' => $filterDb,
            'count' => $count,
            'sSort' => $sSort == 'null' ? '-' : $col1[$sSort],
            'vSort' => $sSort == 'null' ? '-' : $vSort,
            'sSearch' => $vSearch == 'null' ? '-' : ($sSearch == 'null' ? 'All' : $col1[$sSearch]),
            'vSearch' => $vSearch == 'null' ? '-' : $vSearch
        );
        //$result = $this->db->get();
        //echo $this->db->last_query();
        // return $result->num_rows() > 0 ? $result->result() : false;
    }

    function get_sinlge($param) {
        $this->db->select("*");
        $this->db->from($this->_myTable);
        $this->db->where($param);
        $result = $this->db->get("");
        return $result->num_rows() > 0 ? $result->row() : false;
    }

    function msave_log_notif_created($data, $param) {
        $this->db->where($param);
        $this->db->set('NOTIFCREATED', 'SYSDATE', false);
        $this->db->update($this->_myTable, $data);
        $this->db->affected_rows();
    }

    function get_notif_number($sdate, $edate, $mplant) {
        $this->db->select("NOTIFNUMBER");
        $this->db->where('NOTIFCREATED >=', "TO_DATE('{$sdate} 00:00:00', 'yyyymmdd HH24:MI:SS')", FALSE);
        $this->db->where('NOTIFCREATED <=', "TO_DATE('{$edate} 23:59:59', 'yyyymmdd HH24:MI:SS')", FALSE);
        if ($mplant)
            $this->db->where('MPLANT', $mplant);
        $data = $this->db->get($this->_myTable);
        return $data->result_array();
    }

    function update_status($notif, $status, $order = false, $id = FALSE) {
        if ($order) {
            $this->db->set('ORDERNUMBER', $order);
        }
        if ($id) {
            $this->db->set('SYNCID', $id);
        }
        $this->db->set('SAP_STATUS', $status);
        $this->db->set('UPDATEON', 'SYSDATE', FALSE);
        $this->db->where('NOTIFNUMBER', $notif);
        $this->db->update('MSO_AMTRANSACTION');
        return 'updated-' . $this->db->affected_rows();
    }

}
