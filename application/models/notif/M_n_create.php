<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_n_create extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'ID_MPE', 'NOTIFIKASI', 'NAMA_PEKERJAAN', 'DESCPGROUP', 'DESCPSECTION', 'UK_TEXT', 'DEPT_TEXT', 'CREATE_BY', 'CREATE_AT'
  	);

  	function _qry($key){
    		$this->db->select('
                          "ID_MPE",
                          MPE_PENGAJUAN."KDLOC",
                          MPE_PENGAJUAN."KDAREA",
                          "NO_PENGAJUAN",
                          "NOTIFIKASI",
                          "REV_NO",
                          "NAMA_PEKERJAAN",
                          "UK_CODE",
                          "UK_TEXT",
                          MPE_PENGAJUAN."MPLANT",
                          DESCMPLANT,
                          "PLANT_SECTION",
                          DESCPSECTION,
                          MPE_PENGAJUAN."PPLANT",
                          DESCPLANT,
                          "PLANNER_GROUP",
                          DESCPGROUP,
                          "OBJNR",
                          "FUNCT_LOC",
                          "DESC" AS FL_TEXT,
                          "LOKASI",
                          "WBS_CODE",
                          "WBS_TEXT",
                          TO_CHAR("DESSTDATE", \'YYYY-MM-DD\') AS "DESSTDATE",
                          TO_CHAR("DESENDDATE", \'YYYY-MM-DD\') AS "DESENDDATE",
                          "PRIORITY",
                          (CASE "PRIORITY" WHEN \'1\' THEN \'Emergency\' WHEN \'2\' THEN \'High\' WHEN \'3\' THEN \'Medium\' ELSE \'Low\' END) PRIOR_TEXT,
                          "LATAR_BELAKANG",
                          "CUST_REQ",
                          "TECH_INFO",
                          "RES_MIT",
                          MPE_PENGAJUAN."DEPT_CODE",
                          MPE_PENGAJUAN."DEPT_TEXT",
                          MPE_PENGAJUAN."STATUS",
                          MPE_PENGAJUAN.FILES,
                          MPE_PENGAJUAN.FILES_AT,
                          "COMPANY",
                          "STATE",
                          "NOTE",
                          "APPROVE_AT",
                          "APPROVE_BY",
                          "APPROVE_JAB",
                          "CREATE_AT",
                          "CREATE_BY",
                          "UPDATE_AT",
                          "UPDATE_BY",
                          "DELETE_AT",
                          "DELETE_BY",
                          "REJECT_DATE",
                          "REJECT_BY"
                        ');
    		$this->db->from('MPE_PENGAJUAN');
        $this->db->join('MSO_FUNCLOC', 'MSO_FUNCLOC.TPLNR = MPE_PENGAJUAN.FUNCT_LOC','left');
        $this->db->join('MSO_MPLANT', 'MSO_MPLANT.MPLANT = MPE_PENGAJUAN.MPLANT AND MSO_MPLANT.PSECTION = MPE_PENGAJUAN.PLANT_SECTION','left');
        $this->db->join('MSO_PPLANT', 'MSO_PPLANT.PPLANT = MPE_PENGAJUAN.PPLANT AND MSO_PPLANT.PGROUP = MPE_PENGAJUAN.PLANNER_GROUP','left');
    		if($key['search']!==''){
    			$this->db->or_like('LOWER(NOTIFIKASI)', strtolower($key['search']));
          $this->db->or_like('LOWER(DESCPGROUP)', strtolower($key['search']));
    			$this->db->or_like('LOWER(UK_TEXT)', strtolower($key['search']));
    			$this->db->or_like('LOWER(NAMA_PEKERJAAN)', strtolower($key['search']));
    			$this->db->or_like('LOWER(DESCPSECTION)', strtolower($key['search']));
    			$this->db->or_like('LOWER(PRIORITY)', strtolower($key['search']));
    			$this->db->or_like('LOWER(CREATE_BY)', strtolower($key['search']));
    		}
    		// $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
    		$this->db->where('(MPE_PENGAJUAN.STATE = \'Active\' OR MPE_PENGAJUAN.STATE IS NULL)');
    		$this->db->where('(MPE_PENGAJUAN.STATUS != \'Rejected\' OR MPE_PENGAJUAN.STATUS IS NULL)');
    		$this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
        if (isset($key['dept_code'])) {
          // $this->db->where("(MPE_PENGAJUAN.CREATE_BY = '{$key['name']}' OR MPE_PENGAJUAN.CREATE_BY LIKE '{$key['username']}@%')");
          $this->db->where("MPE_PENGAJUAN.DEPT_CODE = '{$key['dept_code']}'");
        }
    		$order = $this->column[$key['ordCol']];
    		$this->db->order_by($order, $key['ordDir']);
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function recTot(){
  		$query			= $this->db->get('MPE_PENGAJUAN');
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

    function get_datarequest($id = null){
        /* TO_CHAR("CREATE_AT", \'DD-MM-YYYY\') AS "CREATE_AT",*/
        $this->db->select('"ID_MPE",
                          MPE_PENGAJUAN."KDLOC",
                          MPE_PENGAJUAN."KDAREA",
                        	"NO_PENGAJUAN",
                        	"NOTIFIKASI",
                        	"REV_NO",
                        	"NAMA_PEKERJAAN",
                        	"UK_CODE",
                        	"UK_TEXT",
                        	MPE_PENGAJUAN."MPLANT",
                        	DESCMPLANT,
                        	"PLANT_SECTION",
                        	DESCPSECTION,
                        	MPE_PENGAJUAN."PPLANT",
                        	DESCPLANT,
                        	"PLANNER_GROUP",
                        	DESCPGROUP,
                        	"OBJNR",
                        	"FUNCT_LOC",
                        	"DESC" AS FL_TEXT,
                        	"LOKASI",
                        	"WBS_CODE",
                        	"WBS_TEXT",
                        	TO_CHAR("DESSTDATE", \'YYYY-MM-DD\') AS "DESSTDATE",
                        	TO_CHAR("DESENDDATE", \'YYYY-MM-DD\') AS "DESENDDATE",
                        	"PRIORITY",
                        	"LATAR_BELAKANG",
                        	"CUST_REQ",
                        	"TECH_INFO",
                        	"RES_MIT",
                        	MPE_PENGAJUAN."STATUS",
                        	"COMPANY",
                          "STATE",
                          "NOTE",
                          MPE_PENGAJUAN.FILES,
                          MPE_PENGAJUAN.FILES_AT,
                        	"APPROVE_AT",
                        	"APPROVE_BY",
                        	"APPROVE_JAB",
                        	"CREATE_AT",
                        	"CREATE_BY",
                        	"UPDATE_AT",
                        	"UPDATE_BY",
                        	"DELETE_AT",
                        	"DELETE_BY",
                          "REJECT_DATE",
                          "REJECT_BY"');
        $this->db->from('MPE_PENGAJUAN');
        $this->db->join('MSO_FUNCLOC', 'MSO_FUNCLOC.TPLNR = MPE_PENGAJUAN.FUNCT_LOC','left');
        $this->db->join('MSO_MPLANT', 'MSO_MPLANT.MPLANT = MPE_PENGAJUAN.MPLANT AND MSO_MPLANT.PSECTION = MPE_PENGAJUAN.PLANT_SECTION','left');
        $this->db->join('MSO_PPLANT', 'MSO_PPLANT.PPLANT = MPE_PENGAJUAN.PPLANT AND MSO_PPLANT.PGROUP = MPE_PENGAJUAN.PLANNER_GROUP','left');

        if(isset($id))
        {
          $this->db->where('ID_MPE',$id);
        }

        $this->db->where('DELETE_BY',NULL);
        $this->db->order_by('CREATE_AT', 'DESC');
        $this->db->order_by('ID_MPE', 'DESC');
        $query = $this->db->get();

        return $query->result_array();
    }

    public function get_data_byFUNCLOC($param){
        $this->db->select('	"NO_PENGAJUAN", "NAMA_PEKERJAAN", "UK_CODE", "MPLANT", "PPLANT", "PLANNER_GROUP", "PLANT_SECTION", "FUNCT_LOC", "DESSTDATE", "DESENDDATE", "PRIORITY", "NOTIFIKASI", "ID_MPE", "APPROVAL_MNGR", "APPROVAL_SM", "APPROVAL_GM", "LATAR_BELAKANG", "CUST_REQ", "TECH_INFO", "RES_MIT", "STATUS", "COMPANY", "APPROVE_AT", "APPROVE_BY", "CREATE_AT", "CREATE_BY", "UPDATE_AT", "UPDATE_BY", "DELETE_AT", "DELETE_BY", "UK_TEXT", "LOKASI", "WBS_CODE", "WBS_TEXT"');
        $this->db->where('"FUNCT_LOC" like \''. $param['FUNCT_LOC'].'\' AND "DELETE_AT" IS NULL');
        $query= $this->db->get('MPE_PENGAJUAN');
        // echo $this->db->last_query();

        return $query->result_array();
    }

    public function get_seq_id(){
        $sql='select seq_mso_amtransaction.nextval From dual';
        $query = $this->db->query($sql);

        return $query->row_array();
    }

    function save($data) {
        $DSTRDATE = str_replace("-","/",$data['DESSTDATE']);
        $DENDDATE = str_replace("-","/",$data['DESENDDATE']);
        $this->db->set('DESSTDATE', "TO_DATE('$DSTRDATE','yyyy/mm/dd')", false);
        $this->db->set('DESENDDATE', "TO_DATE('$DENDDATE','yyyy/mm/dd')", false);
        unset($data['DESSTDATE']);
        unset($data['DESENDDATE']);

        $this->db->insert('MPE_PENGAJUAN', $data);
        // echo $this->db->last_query();
        return $this->db->affected_rows();
    }

    function saveStatus($data) {
        $this->db->insert('MPE_APPROVAL', $data);
        return $this->db->affected_rows();
    }

    function saveApproval($data) {
        $this->db->insert_batch('MPE_APPROVAL1', $data);
        return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['id'];
          unset($param['id']);
          $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          $this->db->where('ID_MPE', $id);
          $query = $this->db->update('MPE_PENGAJUAN', $param);


          return (bool) $query;
        }else  if($tipe=='approve'){
          $no_form = $param['no_form'];
          unset($param['no_form']);
          $this->db->set('APPROVE_AT', "CURRENT_DATE", false);
          $this->db->where('NO_PENGAJUAN', $no_form);
          $query = $this->db->update('MPE_PENGAJUAN', $param);


          return (bool) $query;
        }else if($tipe=='reject'){
          $no_form = $param['no_form'];
          unset($param['no_form']);
          print_r($nama);
          $this->db->set('REJECT_DATE', "CURRENT_DATE", false);


          $this->db->where('NO_PENGAJUAN', $no_form);
          $query = $this->db->update('MPE_PENGAJUAN', $param);
          return (bool) $query;
        }else {
          $id = $param['ID_MPE'];
          unset($param['ID_MPE']);
          $DSTRDATE = str_replace("-","/",$param['DESSTDATE']);
          $DENDDATE = str_replace("-","/",$param['DESENDDATE']);
          $this->db->set('DESSTDATE', "TO_DATE('$DSTRDATE','yyyy/mm/dd')", false);
          $this->db->set('DESENDDATE', "TO_DATE('$DENDDATE','yyyy/mm/dd')", false);
          $this->db->set('UPDATE_AT', "CURRENT_DATE", false);
          $this->db->where('ID_MPE', $id);
          $this->db->where('NOTIFIKASI', $param['NOTIFIKASI']);
          $this->db->where('REV_NO', $param['REV_NO']);
          unset($param['ID_MPE']);
          unset($param['NOTIFIKASI']);
          unset($param['REV_NO']);
          unset($param['DESSTDATE']);
          unset($param['DESENDDATE']);

          $query = $this->db->update('MPE_PENGAJUAN', $param);
          // echo $this->db->last_query();

          return (bool) $query;
        }
    }

    function getKodeUnit($where = NULL) {
        if ($where){
          $this->db->where($where);
        }
        $query = $this->db->get('MSO_MPLANT')->result();
        // echo $this->db->last_query();
        return $query;
    }

    function getKodeUnitP() {
        $query = $this->db->get('MSO_PPLANT')->result();
        return $query;
    }

    function getFunloc() {
        $query = $this->db->get('MSO_FUNCLOC')->result();
        return $query;
    }

    function getPsection($where) {
        $this->db->select('*');
        $this->db->where($where);
        $query= $this->db->get('MSO_MPLANT');
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function getPgroup($where) {
        $this->db->select('*');
        $this->db->where($where);
        $query= $this->db->get('MSO_PPLANT');
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function getFunlocMplant($q = NULL) {
        if (!$q){
          $q='';
        }
        $sql = "
                SELECT
                	*
                FROM
                	(
                SELECT
                	inner_query.*,
                	ROWNUM rnum
                FROM
                	(
                    SELECT
                    	MSO_FUNCLOC.TPLNR,
                      MSO_FUNCLOC.IWERK,
                      MSO_FUNCLOC.INGRP,
                      MSO_FUNCLOC.OBJNR,
                      MSO_FUNCLOC.\"DESC\",
                      MSO_FUNCLOC.ERDAT,
                      MSO_FUNCLOC.ERNAM,
                      MSO_FUNCLOC.AENAM,
                      MSO_FUNCLOC.ILOAN,
                      MSO_FUNCLOC.\"PARENT\",
                      MSO_FUNCLOC.STATUS,
                      MSO_FUNCLOC.HIERSTAT,
                      MSO_FUNCLOC.MPLANT,
                      ( SELECT DISTINCT DESCMPLANT FROM MSO_MPLANT WHERE MPLANT = MSO_FUNCLOC.MPLANT ) AS AS_DESCMPLANT
                    FROM
                    	MSO_FUNCLOC
                    /* INNER JOIN MSO_MPLANT ON MSO_MPLANT.MPLANT = MSO_FUNCLOC.MPLANT */
                      WHERE
                      MSO_FUNCLOC.STATUS NOT LIKE '%INAC%' ESCAPE '!'
                      AND MSO_FUNCLOC.STATUS NOT LIKE '%DLFL%' ESCAPE '!'
                      AND MSO_FUNCLOC.IWERK IS NOT NULL AND MSO_FUNCLOC.MPLANT IS NOT NULL
                      AND (UPPER(MSO_FUNCLOC.TPLNR) LIKE '%{$q}%' ESCAPE '!' OR UPPER(MSO_FUNCLOC.\"DESC\") LIKE'%{$q}%' ESCAPE '!')
                	) inner_query
                WHERE
                	ROWNUM < 11
          	   )
               ";
      // echo $sql;
      // exit;
      $query = $this->db->query($sql);
      return $query->result();
    }

    function getBiro() {
        $query = $this->db->get('MPE_LOGIN');
        return $query->result();
    }

    function getKaryawanNopeg($id) {

        $this->hris->where('MK_TGL_PENSIUN > SYSDATE', null, false);
        $this->hris->where('MK_NOPEG', $id);
        $this->hris->order_by('MK_NOPEG ASC');
        $query = $this->hris->get('M_KARYAWAN');

        return $query->row();
    }

    function _qryProgress($id){
         $this->db->select('"APPROVE_AT","APPROVE_BY",
                            CASE
                            WHEN "APPROVE_BY" IS NULL OR "APPROVE_AT" IS NULL THEN \'Pending\'
                              ELSE WHEN REJECT_DATE IS NOT NULL THEN \'Rejected\'
                                   ELSE "APPROVE_BY"
                                   END
                            END AS Status,
                            CASE
                            WHEN (SELECT COUNT(*) FROM MPE_PENUGASAN WHERE ID_PENGAJUAN = ID_MPE)>0 THEN (
                              CASE
                        			WHEN APPROVE1_AT IS NULL OR APPROVE1_BY IS NULL OR APPROVE2_AT IS NULL OR APPROVE2_BY IS NULL
                        			THEN \'not complete\' ELSE \'complete\'
                        			END
                              )
                              ELSE \'kosong\'
                            END AS StatusPenugasan,
                            CASE
                            WHEN (SELECT COUNT(*) FROM MPE_DOK_ENG WHERE ID_PENUGASAN = MPE_PENUGASAN.ID)>0 THEN
                              \'ada\' ELSE \'kosong\'
                            END AS dokstatus
                            ');
           $this->db->from('MPE_PENGAJUAN');
         $this->db->join('MPE_PENUGASAN', 'MPE_PENUGASAN.ID_PENGAJUAN = MPE_PENGAJUAN.ID_MPE', 'left');

         $this->db->where('ID_MPE',$id);
         $this->db->where('STATE','Active');
         $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
         $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
    }

    function _qryProgress2($id){
       $this->db->select('MPE_PENGAJUAN.STATUS AS STATUS1, MPE_PENUGASAN.STATUS AS STATUS2,
      CASE WHEN (SELECT COUNT(*) FROM MPE_DOK_ENG WHERE ID_PENUGASAN = MPE_PENUGASAN.ID)>0 THEN
         \'ada\' ELSE \'kosong\'
       END AS dokstatus,
       MPE_BAST.STATUS AS STATUSBAST');
       $this->db->from('MPE_PENGAJUAN');
       $this->db->join('MPE_PENUGASAN', 'MPE_PENUGASAN.ID_PENGAJUAN = MPE_PENGAJUAN.ID_MPE', 'left');
       $this->db->join('MPE_DOK_ENG', 'MPE_DOK_ENG.ID_PENUGASAN = MPE_PENUGASAN.ID', 'left');
       $this->db->join('MPE_BAST', 'MPE_BAST.ID_DOK_ENG = MPE_DOK_ENG.ID', 'left');
       $this->db->where('ID_MPE',$id);
       $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
       $this->db->where("MPE_PENGAJUAN.STATUS != 'Rejected'");
       $this->db->where('(MPE_PENGAJUAN.STATE = \'Active\' OR MPE_PENGAJUAN.STATE IS NULL)');
       $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
    }

    function getCheckApprove($id){
      $this->_qryProgress2($id);
      $query    = $this->db->get();
      $data     = $query->result_array();
      // echo $this->db->last_query();
      return $data;
    }

    function _qryLogsPengajuan($id){
         $this->db->select('MPE_PENGAJUAN.ERF_CREATE_AT, MPE_PENGAJUAN.ERF_CREATE_BY,
        MPE_PENGAJUAN.APPROVE_AT, MPE_PENGAJUAN.APPROVE_BY, MPE_PENGAJUAN.NOTE,
        MPE_PENGAJUAN.REJECT_DATE, MPE_PENGAJUAN.REJECT_BY,
        MPE_PENUGASAN.CREATE_AT AS CREATEAT2, MPE_PENUGASAN.CREATE_BY AS CREATEBY2,
        MPE_PENUGASAN.APPROVE1_AT, MPE_PENUGASAN.APPROVE1_BY,
        MPE_PENUGASAN.APPROVE2_AT, MPE_PENUGASAN.APPROVE2_BY,
        MPE_PENUGASAN.APPROVE0_AT, MPE_PENUGASAN.APPROVE0_BY,
        MPE_PENUGASAN.NOTE AS NOTE1, MPE_PENUGASAN.NOTE2 AS NOTE2, MPE_PENUGASAN.NOTE0 AS NOTE0,
        MPE_PENUGASAN.REJECT_DATE AS REJECTAT2, MPE_PENUGASAN.REJECT_BY AS REJECTBY2, MPE_PENUGASAN.REJECT_REASON');
        // MPE_DOK_ENG.APPROVE1_AT AS AT1, MPE_DOK_ENG.APPROVE1_BY AS AP1,
  			// MPE_DOK_ENG.APPROVE2_AT AS AT2, MPE_DOK_ENG.APPROVE2_BY AS AP2'
         $this->db->from('MPE_PENGAJUAN');
         $this->db->join('MPE_PENUGASAN', 'MPE_PENUGASAN.ID_PENGAJUAN = MPE_PENGAJUAN.ID_MPE', 'left');

         //$this->db->join('MPE_DOK_ENG', 'MPE_DOK_ENG.ID_PENUGASAN = MPE_PENUGASAN.ID', 'left');
         $this->db->where('ID_MPE',$id);
         $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
         $this->db->where("MPE_PENGAJUAN.STATUS != 'Rejected'");
         $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
         $this->db->where('(MPE_PENGAJUAN.STATE = \'Active\' OR MPE_PENGAJUAN.STATE IS NULL)');
    }

    function getLogPengajuan($id){
      $this->_qryLogsPengajuan($id);
      $query    = $this->db->get();
      $data     = $query->result_array();
      //echo $this->db->last_query();
      return $data;
    }

    function _qryLogsPenugasan($id){
         $this->db->select('"CREATE_AT", "CREATE_BY", "APPROVE1_AT","APPROVE1_BY","APPROVE2_AT","APPROVE2_BY"');
         $this->db->from('MPE_PENUGASAN');
         $this->db->where('ID_PENGAJUAN',$id);
         $this->db->where('MPE_PENUGASAN.DELETE_AT IS NULL');
    }

    function getLogPenugasan($id){
      $this->_qryLogsPenugasan($id);
      $query    = $this->db->get();
      $data     = $query->result_array();
      return $data;
    }

    function _qryNotifPengajuan($user){
      $this->db->select('"NOTIFIKASI", "CREATE_AT", "NAMA_PEKERJAAN","APPROVE_AT"');
      $this->db->from('MPE_PENGAJUAN');
      $this->db->where('APPROVE_BY',$user);
      $this->db->where('APPROVE_AT',NULL);
      $this->db->where('MPE_PENGAJUAN.DELETE_AT IS NULL');
    }

    function getNotification($user){
      $this->_qryNotifPengajuan($user);
      $query    = $this->db->get();$this->db->last_query();
      $data     = $query->result_array();
      return $data;
    }

    function _qrykdloc($q){
      $this->db->select('"GEN_DESC","GEN_PAR1","GEN_PAR2"');
      $this->db->from('MPE_GENERAL');
      $this->db->where('GEN_VAL','1');
      $this->db->where('GEN_CODE','LOKASI');
    }

    function getLocCode(){
     $sql = "
     SELECT GEN_DESC,GEN_PAR1,GEN_PAR2 FROM MPE_GENERAL WHERE GEN_CODE = 'Lokasi'";
     $query = $this->db->query($sql);
     return $query->result();
    }

    function getAreaCode($q){
      if (!$q){
        $q='';
      }
      $sql = "
      SELECT GEN_DESC, GEN_PAR1
        FROM MPE_GENERAL
        WHERE GEN_CODE = 'Area' AND
        GEN_VAL = 1 AND (GEN_PAR1 LIKE '%{$q}%' ESCAPE '!' OR GEN_DESC LIKE'%{$q}%' ESCAPE '!')";
    // echo $sql;
    // exit;
    $query = $this->db->query($sql);
    return $query->result();
    }

    function getCount(){
      $this->db->select("NVL(MAX(CAST(SUBSTR(NO_PENGAJUAN,17,3) AS INTEGER))+1,1) AS URUT", FALSE);
      $this->db->from('MPE_PENGAJUAN');
      $this->db->where("SUBSTR(NO_PENGAJUAN,0,4)=TO_CHAR(CURRENT_DATE, 'YYYY') AND SUBSTR(NO_PENGAJUAN, 14, 2)='EM'");
      $urut = $this->db->get()->row();

      return $urut->URUT;
      // return $this->db->row();
    }

}
