<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_dashboard extends CI_Model {

    public function __construct() {
        parent::__construct();
        // $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    function count($tipe, $from){
      $this->db->select("*");
      //$this->db->set("STATUS", "NVL(STATUS,'')",false );
      $this->db->from($from);
      $this->db->where($tipe);
      $this->db->where("to_char(CREATE_AT,'YYYY')", date("Y"));
      $this->db->where('DELETE_AT', null);
      $result = $this->db->count_all_results();
      //echo $this->db->last_query();
      return $result;
    }

    function countall($from){
      date("Y");
      $this->db->select("*");
      $this->db->from($from);
      $this->db->where("to_char(CREATE_AT,'YYYY')", date("Y"));
      $this->db->where('DELETE_AT', null);
      if ($from == 'MPE_PENGAJUAN') {
        $this->db->where("(STATE = 'Active' OR STATE IS NULL)");
      }else {
        // code...
      }

      $result = $this->db->count_all_results();
      return $result;
    }

    // function count_app($tipe, $id){
    //   if($tipe=='Paraf'){
    //     $this->db->select("*");
    //     $this->db->from('MPE_APPROVAL');
    //     $this->db->where('TIPE', $tipe);
    //     $this->db->where('REF_ID', $id);
    //     $result = $this->db->get()->row();
    //     return $result;
    // }
    //
    // function check_app($tipe, $id){
    //   if($tipe=='Paraf'){
    //     $this->db->select("*");
    //     $this->db->from('MPE_APPROVAL');
    //     $this->db->where('TIPE', $tipe);
    //     $this->db->where('REF_ID', $id);
    //     $this->db->where('UPDATE_AT', IS NOT NULL);
    //     $result = $this->db->get()->row();
    //     return $result;
    // }

}
