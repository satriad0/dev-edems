<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_slide extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

  	var $column = array(
  		'NO','SHORT_DESC', 'DESCRIPTION', 'CREATE_BY', 'CREATE_AT', 'ID'
  	);

  	function _qry($key){
    		$this->db->select('
                            ROWNUM AS NO,
                            A.ID,
                            A.TIPE,
                            A.SHORT_DESC,
                            A.DESCRIPTION,
                            A.PATHS,
                            A.NOTE,
                            A.STATUS,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY
                        ', FALSE);
    		$this->db->from('MPE_SLIDER A');
    		if($key['search']!==''){
          $sAdd ="(
                  LOWER(A.SHORT_DESC) LIKE '%".strtolower($key['search'])."%'
                  OR LOWER(A.DESCRIPTION) LIKE '%".strtolower($key['search'])."%'
                  )";
          $this->db->where($sAdd, '', FALSE);
    		}
        $this->db->where('A.DELETE_AT IS NULL');
    		$order = $this->column[$key['ordCol']];
    		$this->db->order_by($order, $key['ordDir']);
  	}

  	function get($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();
  		return $data;
  	}

  	function get_data($key){
  		$this->_qry($key);
  		$this->db->limit($key['length'], $key['start']);
  		$query		= $this->db->get();
  		$data			= $query->result();

  		return $data;
  	}

  	function recFil($key){
  		$this->_qry($key);
  		$query			= $this->db->get();
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function recTot(){
  		$query			= $this->db->get('MPE_SLIDER');
  		$num_rows		= $query->num_rows();
  		return $num_rows;
  	}

  	function get_record($where = null){
    		$this->db->select('
                            A.ID,
                            A.TIPE,
                            A.SHORT_DESC,
                            A.DESCRIPTION,
                            A.PATHS,
                            A.NOTE,
                            A.STATUS,
                            A.CREATE_AT,
                            A.CREATE_BY,
                            A.UPDATE_AT,
                            A.UPDATE_BY,
                            A.DELETE_AT,
                            A.DELETE_BY
                        ');
    		$this->db->from('MPE_SLIDER A');
    		$this->db->where('A.DELETE_AT IS NULL');
        $this->db->order_by("ID");
        if (isset($where)) {
          $this->db->where($where);
        }

        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
  	}

    public function get_foreign($id = null){
        $this->db->select('
                            ID,
                            NAME,
                            DESCRIPTION,
                            (CASE SPECIAL WHEN \'1\' THEN \'true\' ELSE \'false\' END) SPECIAL,
                            NOTE,
                            STATUS
        ');
    		$this->db->from("MPE_ROLES");
        // $this->db->where("DELETE_AT IS NULL");
        $query = $this->db->get();
        // echo $this->db->last_query();

        return $query->result_array();
    }

    function save($data) {
        $query = $this->db->insert('MPE_SLIDER', $data);
        $result = false;
        if($query)
        {
          // $this->db->select_max('id','id');
          $result = $this->db->get('MPE_SLIDER')->row_array();
        }
        return $result;
        // return $this->db->affected_rows();
    }

    function updateData($tipe, $param=array()){
        if($tipe=='delete'){
          $id = $param['id'];
          unset($param['id']);
          $this->db->set('DELETE_AT', "CURRENT_DATE", false);
          // $this->db->set('"UPDATE_AT"', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_SLIDER', $param);
          // echo $this->db->last_query();

          return (bool) $query;
        }else  if($tipe=='approve'){
          $id = $param['id'];
          unset($param['id']);
          $this->db->where('ID', $id);
          $query = $this->db->update('MPE_SLIDER', $param);

          return (bool) $query;
        }else {
          $id = $param['ID'];
          $this->db->set('"UPDATE_AT"', "CURRENT_DATE", false);
          $this->db->where('ID', $id);
          unset($param['ID']);

          $query = $this->db->update('MPE_SLIDER', $param);
          // echo $this->db->last_query();


          return (bool) $query;
        }
    }

}
