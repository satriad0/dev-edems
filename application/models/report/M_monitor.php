<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class M_monitor extends CI_Model {

    var $hris;

    public function __construct() {
        parent::__construct();
        $this->hris = $this->load->database('hris', true);
        // $this->db = $this->load->database('developer', true);
        // $this->load->database();
    }

    var $column = array(
        'RNO', 'NAMA_PEKERJAAN', 'NO_PENGAJUAN', 'NOTIFIKASI', 'R_DATE', 'DESCPSECTION', 'DESCPGROUP', 'TIPE', 'WBS_CODE', 'R_APPROVE', 'NO_PENUGASAN', 'A_DATE', 'A_APPROVE', 'A_PACKET', 'NO_DOK_ENG', 'D_DATE', 'D_APPROVE', 'NO_PENGAJUAN', 'NO_PENGAJUAN', 'NO_PENGAJUAN', 'NO_PENGAJUAN', 'NO_PENGAJUAN', 'D_REMARKS'
    );

    function _qry($key) {
        $this->db->select('
                            ROWNUM RNO,
                            R.ID_MPE,
                            R.NO_PENGAJUAN,
                            R.NOTIFIKASI,
                            R.CREATE_AT R_DATE,
                            R.MPLANT,
                            M.DESCMPLANT,
                            R.PLANT_SECTION,
                            M.DESCPSECTION,
                            R.PPLANT,
                            PP.DESCPLANT,
                            R.PLANNER_GROUP,
                            PP.DESCPGROUP,
                            R.TIPE,
                            R.WBS_CODE,
                            R.UK_TEXT,
                            R.NAMA_PEKERJAAN,
                            R.STATUS R_STATUS,
                            ( CASE WHEN R.APPROVE_AT IS NOT NULL THEN /*TO_CHAR( R.APPROVE_AT, \'DD-MM-YYYY\' )*/ R.APPROVE_AT || \' (\' || R.APPROVE_BY || \')\' END ) R_APPROVE,
                            A.NO_PENUGASAN,
                            A.LEAD_ASS,
                            A.CREATE_AT A_DATE,
                            (CASE A.APPROVE0_BY WHEN NULL THEN ( CASE WHEN A.APPROVE1_AT IS NOT NULL THEN  A.APPROVE1_AT || \' (\' || A.APPROVE1_BY || \')\' END ) ELSE ( CASE WHEN A.APPROVE0_AT IS NOT NULL THEN  A.APPROVE0_AT || \' (\' || A.APPROVE0_BY || \')\' END ) END) A_APPROVE,
                            ( CASE D.ID_PENUGASAN WHEN A.ID THEN \'Detail Design\' END ) D_STATUS,
                            ( SELECT listagg( DESKRIPSI || \' (\' || PROGRESS || \'%\' || \')\', CHR( 13 )) WITHIN GROUP( ORDER BY ID ) csv FROM MPE_DTL_PENUGASAN WHERE ID_PENUGASAN = A.ID AND DELETE_AT IS NULL ) A_PACKET,
                            D.NO_DOK_ENG,
                            D.CREATE_AT D_DATE,
                            ( SELECT listagg( APP_NAME || \' (\' || UPDATE_AT || \'\' || \')\', CHR( 13 )) WITHIN GROUP( ORDER BY ID ) csv FROM MPE_APPROVAL WHERE REF_ID = D.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL ) D_APPROVE,
                            (SELECT ROUND(AVG(PROGRESS),2) FROM MPE_DTL_PENUGASAN WHERE DELETE_AT IS NULL AND PROGRESS IS NOT NULL AND ID_PENUGASAN = A.ID) TOT_PROGRESS,
                            (CASE (SELECT COUNT(ID_DOK_ENG) FROM MPE_BAST WHERE ID_DOK_ENG=D.ID AND DELETE_AT IS NULL AND STATUS != \'Rejected\') WHEN 0 THEN \'Belum Terkirim\' ELSE \'Terkirim\' END) TRANSMITAL,
                            /*(CASE (SELECT COUNT(ID_DOK_ENG) FROM MPE_DOK_TEND WHERE ID_DOK_ENG=D.ID AND DELETE_AT IS NULL AND STATUS != \'Rejected\') WHEN 0 THEN \'Belum Terkirim\' ELSE \'Terkirim\' END) TRANSMITAL,*/
                            T.NO_DOK_TEND,
                            T.CREATE_AT T_DATE,
                            ( SELECT listagg( APP_NAME || \' (\' || UPDATE_AT || \'%\' || \')\', CHR( 13 )) WITHIN GROUP( ORDER BY ID ) csv FROM MPE_APPROVAL WHERE REF_ID = T.ID AND REF_TABEL=\'DOK_TEND\' AND DELETE_AT IS NULL ) T_APPROVE
                        ', FALSE);
        $this->db->from('MPE_PENGAJUAN R');
        $this->db->join("MPE_PENUGASAN A", "A.ID_PENGAJUAN = R.ID_MPE", 'left');
        $this->db->join("MSO_MPLANT M", "M.MPLANT=R.MPLANT AND M.PSECTION=R.PLANT_SECTION", 'left');
        $this->db->join("MSO_PPLANT PP", "PP.PPLANT=R.PPLANT AND PP.PGROUP=R.PLANNER_GROUP", 'left');
        // $this->db->join("MPE_DTL_PENUGASAN P", "P.ID_PENUGASAN = A.ID",'left');
        $this->db->join("MPE_DOK_ENG D", "D.ID_PENUGASAN = A.ID", 'left');
        $this->db->join("MPE_DOK_TEND T", "T.ID_DOK_ENG = D.ID", 'left');
        // $this->db->join("MSO_FUNCLOC C", "C.TPLNR = A.FUNCT_LOC", 'left');
        if ($key['search'] !== '') {
            $sAdd = "(
                  LOWER(R.NO_PENGAJUAN) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(R.NOTIFIKASI) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(R.NAMA_PEKERJAAN) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(R.UK_TEXT) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(R.STATUS) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(R.APPROVE_BY) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.NO_PENUGASAN) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.APPROVE1_BY) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(A.APPROVE2_BY) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(D.NO_DOK_ENG) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(D.PACKET_TEXT) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(D.NOTIFIKASI) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(CASE (SELECT COUNT(ID_DOK_ENG) FROM MPE_BAST WHERE ID_DOK_ENG=D.ID AND DELETE_AT IS NULL AND STATUS != 'Rejected') WHEN 0 THEN 'Belum Terkirim' ELSE 'Terkirim' END) LIKE '%" . strtolower($key['search']) . "%'
                  OR LOWER(T.NO_DOK_TEND) LIKE '%" . strtolower($key['search']) . "%'
                  )";
            $this->db->where($sAdd, '', FALSE);
        }
        // set range date
        if (isset($key['start_date'])) {
            $this->db->where("R.CREATE_AT >= TO_DATE('{$key['start_date']}','yyyy/mm/dd')");
        }
        if (isset($key['end_date'])) {
            $this->db->where("R.CREATE_AT <= TO_DATE('{$key['end_date']}','yyyy/mm/dd')");
        }
        if (isset($key['dept_code'])) {
            // $this->db->where("(A.CREATE_BY = '{$key['name']}' OR A.CREATE_BY LIKE '{$key['username']}@%')");
            $this->db->where("A.DEPT_CODE = '{$key['dept_code']}'");
        }
        if (isset($key['uk_code'])) {
            $this->db->where("A.UK_CODE = '{$key['uk_code']}'");
        }
        $this->db->where('(R.STATE = \'Active\' OR R.STATE IS NULL)');
        $this->db->where('R.DELETE_AT IS NULL AND A.DELETE_AT IS NULL AND D.DELETE_AT IS NULL');
        $order = $this->column[$key['ordCol']];
        $this->db->order_by($order, $key['ordDir']);
        $this->db->distinct();
    }

    function get($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }

    function get_data($key) {
        $this->_qry($key);
        $this->db->limit($key['length'], $key['start']);
        $query = $this->db->get();
        // echo $this->db->last_query();
        $data = $query->result();

        return $data;
    }

    function recFil($key) {
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function recTot($key) {
        $key['search'] = '';
        $this->_qry($key);
        $query = $this->db->get();
        $num_rows = $query->num_rows();
        return $num_rows;
    }

    function get_record() {
        $session = $this->session->userdata('logged_in');
        $info = (array) $session;
        $unit_kerja = $info['unit_kerja'];
        $role_id = $info['role_id'];
        
        if ($unit_kerja == 'Unit of Engineering I') {
            $planner_grp = '4B9';
        } else if ($unit_kerja == 'Unit of Engineering II') {
            $planner_grp = '4C0';
        } else if ($unit_kerja == 'Unit of Engineering III') {
            $planner_grp = '4C1';
        } else if ($unit_kerja == 'Department of Design & Engineering') {
            $planner_grp = '311';
        }

        if (!empty($planner_grp) && $role_id != 5) {
            $params = "AND R.PLANNER_GROUP = '$planner_grp'";
        } else {
            $params = "";
        }
        $this->db->select('ROWNUM RNO,
                            R.ID_MPE,
                            R.NO_PENGAJUAN,
                            R.NOTIFIKASI,
                            R.CREATE_AT R_DATE,
                            R.MPLANT,
                            M.DESCMPLANT,
                            R.PLANT_SECTION,
                            M.DESCPSECTION,
                            R.PPLANT,
                            PP.DESCPLANT,
                            R.PLANNER_GROUP,
                            PP.DESCPGROUP,
                            R.TIPE,
                            R.WBS_CODE,
                            R.UK_TEXT,
                            R.NAMA_PEKERJAAN,
                            R.STATUS R_STATUS,
                            ( CASE WHEN R.APPROVE_AT IS NOT NULL THEN /*TO_CHAR( R.APPROVE_AT, \'DD-MM-YYYY\' )*/ R.APPROVE_AT || \' (\' || R.APPROVE_BY || \')\' END ) R_APPROVE,
                            A.NO_PENUGASAN,
                            A.LEAD_ASS,
                            A.CREATE_AT A_DATE,
                            (CASE WHEN A.APPROVE0_BY IS NULL THEN ( CASE WHEN A.APPROVE1_AT IS NOT NULL THEN  A.APPROVE1_AT || \' (\' || A.APPROVE1_BY || \')\' END ) ELSE ( CASE WHEN A.APPROVE0_AT IS NOT NULL THEN  A.APPROVE0_AT || \' (\' || A.APPROVE0_BY || \')\' END ) END) A_APPROVE,
                            ( CASE D.ID_PENUGASAN WHEN A.ID THEN \'Detail Design\' END ) D_STATUS,
                            ( SELECT listagg( DESKRIPSI || \' (\' || PROGRESS || \'%\' || \')\', CHR( 13 )) WITHIN GROUP( ORDER BY ID ) csv FROM MPE_DTL_PENUGASAN WHERE ID_PENUGASAN = A.ID AND DELETE_AT IS NULL ) A_PACKET,
                            D.NO_DOK_ENG,
                            D.CREATE_AT D_DATE,
                            ( SELECT listagg( APP_NAME || \' (\' || UPDATE_AT || \'%\' || \')\', CHR( 13 )) WITHIN GROUP( ORDER BY ID ) csv FROM MPE_APPROVAL WHERE REF_ID = D.ID AND REF_TABEL=\'DOK_ENG\' AND DELETE_AT IS NULL ) D_APPROVE,
                            (SELECT AVG(PROGRESS) FROM MPE_DTL_PENUGASAN WHERE DELETE_AT IS NULL AND PROGRESS IS NOT NULL AND ID_PENUGASAN = A.ID) TOT_PROGRESS,
                            (CASE (SELECT COUNT(ID_DOK_ENG) FROM MPE_BAST WHERE ID_DOK_ENG=D.ID AND DELETE_AT IS NULL AND STATUS != \'Rejected\') WHEN 0 THEN \'Belum Terkirim\' ELSE \'Terkirim\' END) TRANSMITAL,
                            /*(CASE (SELECT COUNT(ID_DOK_ENG) FROM MPE_DOK_TEND WHERE ID_DOK_ENG=D.ID AND DELETE_AT IS NULL AND STATUS != \'Rejected\') WHEN 0 THEN \'Belum Terkirim\' ELSE \'Terkirim\' END) TRANSMITAL,*/
                            T.NO_DOK_TEND,
                            T.CREATE_AT T_DATE,
                            ( SELECT listagg( APP_NAME || \' (\' || UPDATE_AT || \'%\' || \')\', CHR( 13 )) WITHIN GROUP( ORDER BY ID ) csv FROM MPE_APPROVAL WHERE REF_ID = T.ID AND REF_TABEL=\'DOK_TEND\' AND DELETE_AT IS NULL ) T_APPROVE
                        ', FALSE);
        $this->db->from('MPE_PENGAJUAN R');
        $this->db->join("MPE_PENUGASAN A", "A.ID_PENGAJUAN = R.ID_MPE", 'left');
        $this->db->join("MSO_MPLANT M", "M.MPLANT=R.MPLANT AND M.PSECTION=R.PLANT_SECTION", 'left');
        $this->db->join("MSO_PPLANT PP", "PP.PPLANT=R.PPLANT AND PP.PGROUP=R.PLANNER_GROUP", 'left');
        // $this->db->join("MPE_DTL_PENUGASAN P", "P.ID_PENUGASAN = A.ID",'left');
        $this->db->join("MPE_DOK_ENG D", "D.ID_PENUGASAN = A.ID", 'left');
        $this->db->join("MPE_DOK_TEND T", "T.ID_DOK_ENG = D.ID", 'left');
//        $this->db->where('R.DELETE_AT IS NULL AND A.DELETE_AT IS NULL AND D.DELETE_AT IS NULL ' . $params);
        $this->db->where('R.DELETE_AT IS NULL '.$params);
        $this->db->distinct();

        $query = $this->db->get();
//        echo $this->db->last_query();

        return $query->result_array();
    }

    function get_count() {
        $query = $this->db->query("
                                  SELECT
                                  	(
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_PENGAJUAN
                                  	WHERE
                                  		DELETE_AT IS NULL AND STATE = 'Active' ) J_ERF,
                                  	(
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_PENUGASAN
                                  	WHERE
                                  		DELETE_AT IS NULL ) J_EAT,
                                    (
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_DTL_PENUGASAN  A, MPE_PENUGASAN B
                                  	WHERE
                                      to_char(B.CREATE_AT,'YYYY') = '" . date("Y") . "' AND
                                      A.ID_PENUGASAN = B.ID AND B.REJECT_DATE IS NULL AND
                                      A.DELETE_AT IS NULL AND B.DELETE_AT IS NULL ) J_PROG,
                                  	(
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_DOK_ENG
                                  	WHERE
                                  		DELETE_AT IS NULL ) J_DOK,
                                    (
                                  	SELECT
                                  		COUNT(*)
                                  	FROM
                                  		MPE_BAST
                                  	WHERE
                                  		DELETE_AT IS NULL ) J_BAST
                                  FROM
                                  	dual
                                    ");
        // echo $this->db->last_query();

        return $query->result_array();
    }

}
