<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_createnotif extends CI_Model {

    private $apd_mat_list = 'SHE_APD_SAP_MATERIAL';

    function __construct(){
        parent::__construct();
        require_once APPPATH."/third_party/sapclasses/sap.php";
    }

    public function get_codemat_apd(){
        $sql = "SELECT * FROM {$this->apd_mat_list} ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function coba($post){
        $sap = new SAPConnection();
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('Z_ZAPPSD_SELECT_TRUK2');  // ON Dev

            if ($fce == TRUE) {

                     $fce->XPARAM["NOPOLISI"] = $post; //Sales Organization
                     $fce->XDATA_APP["NMORG"] = "7000"; //Unit of measure of delivery unit

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->RETURN_DATA->Reset();
                  while ($fce->RETURN_DATA->Next()) {
                     $sapPrint[] = ($fce->RETURN_DATA->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }
        return $sapPrint[0]['NO_EXPEDITUR'];
    }

    function createnotif($param){
        // print_r($param['codefuncloc']);
        $sap = new SAPConnection();
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }

        $fce = $sap->NewFunction('Z_ZCPM_RFC_NOTIFCREATE');
         // ON Dev
            // print_r($param);
            // echo "<pre>";
            // print_r($param);
            // echo "</pre>";
            // exit;
            if ($fce == TRUE) {
                $fce->I_NOTIF_TYPE = $param['notiftype'];
                $fce->I_LOGINNAME = $param['loginsap'];
                $fce->I_NOTIFHEADER["FUNCT_LOC"] = $param['codefuncloc'];
                $fce->I_NOTIFHEADER["SHORT_TEXT"] = $param['header'];
                $fce->I_NOTIFHEADER["PRIORITY"] = $param['priority'];
                $fce->I_NOTIFHEADER["DESSTDATE"] = $param['datestart'];
                $fce->I_NOTIFHEADER["DESSTTIME"] = $param['timestart'];
                $fce->I_NOTIFHEADER["DESENDDATE"] = $param['dateend'];
                $fce->I_NOTIFHEADER["DESENDTM"] = $param['timeend'];
                $fce->I_NOTIFHEADER["REPORTEDBY"] = $param['reporter'];
                $fce->I_NOTIFHEADER["PLANPLANT"] = $param['planplant'];
                $fce->I_NOTIFHEADER["PLANGROUP"] = $param['plangroup'];
                $fce->I_NOTIFHEADER["MAINTPLANT"] = $param['maintplant'];
                $fce->I_BDCCHG["BEBER"] = $param['BEBER'];
                // echo "<pre>";
                // print_r($fce);
                // echo "</pre>";
                // exit;

                // print_r($param);

                $fce->I_VEHICLEDETAIL["ZZPENUMPANG"] = '0000';
                $fce->I_VEHICLEDETAIL["ZZJAMDIMINTA"] = '00:00:00';

                // $fce->T_LONGTEXTS->row["OBJTYPE"] = '';
                // $fce->T_LONGTEXTS->row["OBJKEY"] = '00000000';
                // $fce->T_LONGTEXTS->row["FORMAT_COL"] = '';
                $fce->T_LONGTEXTS->row["TEXT_LINE"] = $param['tech_info'];
                // $fce->T_LONGTEXTS->row["TEXT_LINE"] = $param['description'];
                $fce->T_LONGTEXTS->Append($fce->T_LONGTEXTS->row);

                // print_r($fce);
                // $fce->I_NOTIFHEADER->row["REFOBJECTTYPE"] = '';
                // $fce->I_NOTIFHEADER->row["REFOBJECTKEY"] = '';
                // $fce->I_NOTIFHEADER->row["REFRELTYPE"] = '';
                // $fce->I_NOTIFHEADER->row["EQUIPMENT"] = '';
                // $fce->I_NOTIFHEADER->row["FUNCT_LOC"] = $param['codefuncloc'];
                // $fce->I_NOTIFHEADER->row["ASSEMBLY"] = '';
                // $fce->I_NOTIFHEADER->row["SERIALNO"] = '';
                // $fce->I_NOTIFHEADER->row["MATERIAL"] = '';
                // $fce->I_NOTIFHEADER->row["DIVISION"] = '';
                // $fce->I_NOTIFHEADER->row["SALES_ORG"] = '';
                // $fce->I_NOTIFHEADER->row["DISTR_CHAN"] = '';
                // $fce->I_NOTIFHEADER->row["SALES_OFFI"] = '';
                // $fce->I_NOTIFHEADER->row["SALES_GRP"] = '';
                // $fce->I_NOTIFHEADER->row["SHORT_TEXT"] = $param['header'];
                // $fce->I_NOTIFHEADER->row["PRIORITY"] = $param['priority'];
                // $fce->I_NOTIFHEADER->row["DESSTDATE"] = $param['datestart'];
                // $fce->I_NOTIFHEADER->row["DESSTTIME"] = $param['timestart'];
                // $fce->I_NOTIFHEADER->row["DESENDDATE"] = $param['dateend'];
                // $fce->I_NOTIFHEADER->row["DESENDTM"] = $param['timeend'];
                // $fce->I_NOTIFHEADER->row["DEVICEDATA"] = '';
                // $fce->I_NOTIFHEADER->row["PM_WKCTR"] = '00000000';
                // $fce->I_NOTIFHEADER->row["PURCH_NO_C"] = '';
                // $fce->I_NOTIFHEADER->row["PURCH_DATE"] = '';
                // $fce->I_NOTIFHEADER->row["PLANPLANT"] = '';
                // $fce->I_NOTIFHEADER->row["PLANGROUP"] = '';
                // $fce->I_NOTIFHEADER->row["BREAKDOWN"] = '';
                // $fce->I_NOTIFHEADER->row["STRMLFNDATE"] = '';
                // $fce->I_NOTIFHEADER->row["STRMLFNTIME"] = '00:00:00';
                // $fce->I_NOTIFHEADER->row["REPORTEDBY"] = $param['reporter'];
                // $fce->I_NOTIFHEADER->row["NOTIF_DATE"] = '';
                // $fce->I_NOTIFHEADER->row["NOTIFTIME"] = '00:00:00';
                // $fce->I_NOTIFHEADER->row["CODE_GROUP"] = '';
                // $fce->I_NOTIFHEADER->row["CODING"] = '';
                // $fce->I_NOTIFHEADER->row["DOC_NUMBER"] = '';
                // $fce->I_NOTIFHEADER->row["ITM_NUMBER"] = '000000';
                // $fce->I_NOTIFHEADER->row["ENDMLFNDATE"] = '';
                // $fce->I_NOTIFHEADER->row["ENDMLFNTIME"] = '00:00:00';
                // $fce->I_NOTIFHEADER->row["SCENARIO"] = '';
                // $fce->I_NOTIFHEADER->row["ASSEMBLY_EXTERNAL"] = '';
                // $fce->I_NOTIFHEADER->row["ASSEMBLY_GUID"] = '';
                // $fce->I_NOTIFHEADER->row["ASSEMBLY_VERSION"] = '';
                // $fce->I_NOTIFHEADER->row["MATERIAL_EXTERNAL"] = '';
                // $fce->I_NOTIFHEADER->row["MATERIAL_GUID"] = '';
                // $fce->I_NOTIFHEADER->row["MATERIAL_VERSION"] = '';
                // $fce->I_NOTIFHEADER->row["MAINTPLANT"] = '';
                // $fce->I_NOTIFHEADER->row["MAINTLOC"] = '';
                // $fce->I_NOTIFHEADER->row["MAINTROOM"] = '';

                // print_r($fce);
                // $fce->I_NOTIFHEADER->row["FUNCT_LOC"] = $param['codefuncloc'];
                // $fce->I_NOTIFHEADER->row["SHORT_TEXT"] = $param['header'];
                // $fce->I_NOTIFHEADER->row["PRIORITY"] = $param['priority'];
                // $fce->I_NOTIFHEADER->row["DESSTDATE"] = $param['datestart'];
                // $fce->I_NOTIFHEADER->row["DESSTTIME"] = $param['timestart'];
                // $fce->I_NOTIFHEADER->row["DESENDDATE"] = $param['dateend'];
                // $fce->I_NOTIFHEADER->row["DESENDTM"] = $param['timeend'];
                // $fce->I_NOTIFHEADER->row["REPORTEDBY"] = $param['reporter'];
                // $fce->I_NOTIFHEADER->Append($fce->I_NOTIFHEADER->row);


                // print_r($param['codefuncloc']);
                // print_r($fce->I_NOTIFHEADER->row);


                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    // print_r($param);
                    // $fce->O_NOTIFHEADER_EXPORT->Reset();
                    // $sapPrint['HEADER'] = $fce->O_NOTIFHEADER_EXPORT;

                    $sapPrint['HEADER'] = $fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"];

                    // print_r($fce);
                    $fce->T_RETURN->Reset();
                    while ($fce->T_RETURN->Next()) {
                        $sapPrint['CONTENT'] = ($fce->T_RETURN->row);
                    }

                    // $fce->T_RETURN->Reset();
                    // $msg = "";
                    // //$hasil = array();
                    // $hasil['HEADER'] = $fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"];
                    // while ($fce->T_RETURN->Next()) {
                    //     $msg = $fce->T_RETURN->row["MESSAGE"];
                    //     $NOTIF_NO = $fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"];
                    //
                    //     $hasil['CONTENT'] = ($fce->T_RETURN->row);
                    // }
                    // $ressss = array("NOTIFNUMBER" => $fce->O_NOTIFHEADER_EXPORT["NOTIF_NO"],
                    //     "SAP_STATUS" => "OSNO",
                    //     "NOTIFCREATEBY" => $session['username'],
                    //     "PGROUP" => $fce->O_NOTIFHEADER_EXPORT['PLANGROUP'],
                    //     "PSECTION" => $_POST['BEBER'],
                    //     "IDAMTRANS" => $_POST["IDAMTRANS"]);
                }

                $fce->Close();
                $sap->Close();
            }

        return $sapPrint;
    }

}
