<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_funcloc extends CI_Model {   

    private $apd_mat_list = 'SHE_APD_SAP_MATERIAL';

    function __construct(){
        parent::__construct();
        require_once APPPATH."/third_party/sapclasses/sap.php";
    }

    public function get_codemat_apd(){
        $sql = "SELECT * FROM {$this->apd_mat_list} ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function coba($post){
        $sap = new SAPConnection();
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('Z_ZAPPSD_SELECT_TRUK2');  // ON Dev

            if ($fce == TRUE) {

                     $fce->XPARAM["NOPOLISI"] = $post; //Sales Organization
                     $fce->XDATA_APP["NMORG"] = "7000"; //Unit of measure of delivery unit

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->RETURN_DATA->Reset();
                  while ($fce->RETURN_DATA->Next()) {
                     $sapPrint[] = ($fce->RETURN_DATA->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }
        return $sapPrint[0]['NO_EXPEDITUR'];
    }

    function get_funcloclist($comp,$compst,$tobject){
        $sap = new SAPConnection();
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('ZCPM_GET_LIST_FUNCLOC');  // ON Dev
            // print_r($fce);
            if ($fce == TRUE) {

                // $fce->P_SOBKZ = '';
                // $fce->P_LAYOUT = '/AH';
                // $fce->P_VARIANT = '';

                $compcode = $comp;

                if ($comp == 7000 || $comp == 5000 || $comp == 2000) {
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = 2000;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = 5000;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = 7000;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = 3000;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = 4000;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = 6000;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                }else{
                    $fce->R_BUKRS->row["SIGN"] = 'I';
                    $fce->R_BUKRS->row["OPTION"] = 'EQ';
                    $fce->R_BUKRS->row["LOW"] = $compcode;
                    $fce->R_BUKRS->row["HIGH"] = '';
                    $fce->R_BUKRS->Append($fce->R_BUKRS->row);
                }
                // echo $compcode;

                if ($compst) {
                    # code...

                    $fce->R_STRNO->row["SIGN"] = 'I';
                    $fce->R_STRNO->row["OPTION"] = 'CP';
                    $fce->R_STRNO->row["LOW"] = $compst.'*';
                    $fce->R_STRNO->row["HIGH"] = '';
                    $fce->R_STRNO->Append($fce->R_STRNO->row);

                }

                if ($tobject) {
                    # code...
                    $fce->R_CATEGORY->row["SIGN"] = 'I';
                    $fce->R_CATEGORY->row["OPTION"] = 'EQ';
                    $fce->R_CATEGORY->row["LOW"] = $tobject;
                    $fce->R_CATEGORY->row["HIGH"] = '';
                    $fce->R_CATEGORY->Append($fce->R_CATEGORY->row);

                }




                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->T_FUNCLOC->Reset();
                    while ($fce->T_FUNCLOC->Next()) {
                        $sapPrint[] = ($fce->T_FUNCLOC->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }

        return $sapPrint;
    }

}
