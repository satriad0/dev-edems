<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_updatenotif extends CI_Model {

    private $apd_mat_list = 'SHE_APD_SAP_MATERIAL';

    function __construct(){
        parent::__construct();
        require_once APPPATH."/third_party/sapclasses/sap.php";
    }

    public function get_codemat_apd(){
        $sql = "SELECT * FROM {$this->apd_mat_list} ";

        $query = $this->db->query($sql);

        return $query->result_array();
    }

    function coba($post){
        $sap = new SAPConnection();
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }
        $fce = $sap->NewFunction('Z_ZAPPSD_SELECT_TRUK2');  // ON Dev

            if ($fce == TRUE) {

                     $fce->XPARAM["NOPOLISI"] = $post; //Sales Organization
                     $fce->XDATA_APP["NMORG"] = "7000"; //Unit of measure of delivery unit

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    $fce->RETURN_DATA->Reset();
                  while ($fce->RETURN_DATA->Next()) {
                     $sapPrint[] = ($fce->RETURN_DATA->row);
                    }
                }
                $fce->Close();
                $sap->Close();
            }
        return $sapPrint[0]['NO_EXPEDITUR'];
    }

    function rejectnotif($param){
        // print_r($param['codefuncloc']);
        // print_r($param);

        $sap = new SAPConnection();
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }

        $fce = $sap->NewFunction('Z_ZCPM_RFC_NOTIFCHANGE_1');  // ON Dev
            // print_r($param);

            if ($fce == TRUE) {

                $fce->I_NUMBER = $param['NO_NOTIF_SAP'];
                $fce->I_DEFAULTUPDFLAG = 'X';
                $fce->I_LOGINNAME = 'BNURFAHRI';

                // print_r($param);

                $fce->I_NOTIFHEADER["FUNCT_LOC"] = $param['FUNCTIONAL_LOC'];
                $fce->I_NOTIFHEADER["SHORT_TEXT"] = $param['TITTLE'];
                $fce->I_NOTIFHEADER["PRIORITY"] = $param['PRIORITY'];
                $fce->I_NOTIFHEADER["REPORTEDBY"] = $param['REPORTER'];
                $fce->I_NOTIFHEADER["PLANPLANT"] = $param['PLANPLANT'];
                $fce->I_NOTIFHEADER["PLANGROUP"] = $param['PLANGROUP'];
                $fce->I_NOTIFHEADER["MAINTPLANT"] = $param['MPLANT'];

                // print_r($param);

                $fce->T_USR_STATUS->row["STATUS_INT"] = '';
                $fce->T_USR_STATUS->row["STATUS_EXT"] = 'REJE';
                $fce->T_USR_STATUS->row["LANGU"] = '';
                $fce->T_USR_STATUS->row["LANGU_ISO"] = '';
                $fce->T_USR_STATUS->row["STATUS_IND"] = '';
                $fce->T_USR_STATUS->Append($fce->T_USR_STATUS->row);

                $fce->SYSTSTAT["LANGU"] = 'EN';
                $fce->SYSTSTAT["LANGUISO"] = 'EN';
                $fce->SYSTSTAT["REFDATE"] = '31.12.2017';
                $fce->SYSTSTAT["REFTIME"] = '10:00:00';
                // $fce->SYSTSTAT->Append($fce->SYSTSTAT->row);

                 $fce->I_FLDLFL = 'X';
                // print_r($param);

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    // print_r($param);

                    $sapPrint['HEADER'] = $fce->O_NOTIFHEADER_EXPORT;

                    // print_r($fce);
                    $fce->T_RETURN->Reset();
                    while ($fce->T_RETURN->Next()) {
                        $sapPrint['CONTENT'] = ($fce->T_RETURN->row);
                    }
                }

                $fce->Close();
                $sap->Close();
            }

        return $sapPrint;
    }

    function closenotif($param){
        // print_r($param['codefuncloc']);
        // print_r($param);

        $sap = new SAPConnection();
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }

        $fce = $sap->NewFunction('Z_ZCPM_RFC_NOTIFCHANGE_1');  // ON Dev
            // print_r($param);

            if ($fce == TRUE) {

                $fce->I_NUMBER = $param['NO_NOTIF_SAP'];
                $fce->I_DEFAULTUPDFLAG = 'X';
                $fce->I_LOGINNAME = 'BNURFAHRI';

                // print_r($param);

                $fce->I_NOTIFHEADER["FUNCT_LOC"] = $param['FUNCTIONAL_LOC'];
                $fce->I_NOTIFHEADER["SHORT_TEXT"] = $param['TITTLE'];
                $fce->I_NOTIFHEADER["PRIORITY"] = $param['PRIORITY'];
                $fce->I_NOTIFHEADER["REPORTEDBY"] = $param['REPORTER'];
                $fce->I_NOTIFHEADER["PLANPLANT"] = $param['PLANPLANT'];
                $fce->I_NOTIFHEADER["PLANGROUP"] = $param['PLANGROUP'];
                $fce->I_NOTIFHEADER["MAINTPLANT"] = $param['MPLANT'];

                // print_r($param);

                // $fce->T_USR_STATUS->row["STATUS_INT"] = '';
                // $fce->T_USR_STATUS->row["STATUS_EXT"] = 'REJE';
                // $fce->T_USR_STATUS->row["LANGU"] = '';
                // $fce->T_USR_STATUS->row["LANGU_ISO"] = '';
                // $fce->T_USR_STATUS->row["STATUS_IND"] = '';
                // $fce->T_USR_STATUS->Append($fce->T_USR_STATUS->row);

                $fce->SYSTSTAT["LANGU"] = 'EN';
                $fce->SYSTSTAT["LANGUISO"] = 'EN';
                $fce->SYSTSTAT["REFDATE"] = '31.12.2017';
                $fce->SYSTSTAT["REFTIME"] = '10:00:00';
                // $fce->SYSTSTAT->Append($fce->SYSTSTAT->row);

                 $fce->I_FLCO = 'X';
                // print_r($param);

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    // print_r($param);

                    $sapPrint['HEADER'] = $fce->O_NOTIFHEADER_EXPORT;

                    // print_r($fce);
                    $fce->T_RETURN->Reset();
                    while ($fce->T_RETURN->Next()) {
                        $sapPrint['CONTENT'] = ($fce->T_RETURN->row);
                    }
                }

                $fce->Close();
                $sap->Close();
            }

        return $sapPrint;
    }

    function updatenotif($param){
        // print_r($param['codefuncloc']);
        // print_r($param);

        $sap = new SAPConnection();
        // $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataDev.conf");
        $sap->Connect(APPPATH."/third_party/sapclasses/logon_dataCloning.conf");

        if ($sap->GetStatus() == SAPRFC_OK) {
            $sap->Open();

            }
        else{
                echo "Cannot Connect to SAP ERR : " . $sap->GetStatusText();
            exit;
        }

        $fce = $sap->NewFunction('Z_ZCPM_RFC_NOTIFCHANGE_1');  // ON Dev
            print_r($fce);

            if ($fce == TRUE) {

                $fce->I_NUMBER = $param['NOTIFIKASI'];
                $fce->I_DEFAULTUPDFLAG = 'X';
                $fce->I_LOGINNAME = $param['USERNAME'];

                                                    echo "<pre>";
                                                    print_r($param);
                                                    print_r($fce);
                                                    echo "</pre>";

                // $fce->I_NOTIFHEADER["FUNCT_LOC"] = $param['FUNCT_LOC'];
                $fce->I_NOTIFHEADER["SHORT_TEXT"] = $param['NAMA_PEKERJAAN'];
                $fce->I_NOTIFHEADER["PRIORITY"] = $param['PRIORITY'];
                $fce->I_NOTIFHEADER["REPORTEDBY"] = $param['USERNAME'];
                $fce->I_NOTIFHEADER["PLANPLANT"] = $param['PLANT_SECTION'];
                $fce->I_NOTIFHEADER["PLANGROUP"] = $param['PLANNER_GROUP'];
                $fce->I_NOTIFHEADER["MAINTPLANT"] = $param['MPLANT'];
                $fce->T_LONGTEXTS->row["TEXT_LINE"] = $param['TECH_INFO'];
                $fce->T_LONGTEXTS->Append($fce->T_LONGTEXTS->row);

                // print_r($param);

                // $fce->T_USR_STATUS->row["STATUS_INT"] = '';
                // $fce->T_USR_STATUS->row["STATUS_EXT"] = 'REJE';
                // $fce->T_USR_STATUS->row["LANGU"] = '';
                // $fce->T_USR_STATUS->row["LANGU_ISO"] = '';
                // $fce->T_USR_STATUS->row["STATUS_IND"] = '';
                // $fce->T_USR_STATUS->Append($fce->T_USR_STATUS->row);

                // $fce->SYSTSTAT["LANGU"] = 'EN';
                // $fce->SYSTSTAT["LANGUISO"] = 'EN';
                // $fce->SYSTSTAT["REFDATE"] = '31.12.2017';
                // $fce->SYSTSTAT["REFTIME"] = '10:00:00';
                // // $fce->SYSTSTAT->Append($fce->SYSTSTAT->row);

                //  $fce->I_FLDLFL = 'X';
                // print_r($param);

                $fce->Call();
                if ($fce->GetStatus() == SAPRFC_OK) {
                    // print_r($param);

                    $sapPrint['HEADER'] = $fce->O_NOTIFHEADER_EXPORT;

                    // print_r($fce);
                    $fce->T_RETURN->Reset();
                    while ($fce->T_RETURN->Next()) {
                        $sapPrint['CONTENT'] = ($fce->T_RETURN->row);
                    }
                }

                $fce->Close();
                $sap->Close();
            }
            echo "<pre>";
            print_r($param);
            print_r($fce);
            echo "</pre>";

        return $sapPrint;
    }
}
