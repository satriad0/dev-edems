<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of sapTime
 *
 * @author User
 */
class sapTime {
    //put your code here 2011-08-10T00:00:00
    public function convertExtJStoSAPDate($date) {
        $hari = substr($date, 8, 2);
        $bulan = substr($date, 5, 2);
        $tahun = substr($date, 0, 4);
        $sapDate = $hari.".".$bulan.".".$tahun;
        return $sapDate;
    }
    public function convertExtJStoSAPDate2($date) {
        $hari = substr($date, 8, 2);
        $bulan = substr($date, 5, 2);
        $tahun = substr($date, 0, 4);
        $sapDate = $tahun.$bulan.$hari;
        return $sapDate;
    }
    public function convertSAPDateToUIDate($date) {
        $hari = substr($date, 6, 2);
        $bulan = substr($date, 4, 2);
        $tahun = substr($date, 0, 4);
        $sapDate = $hari.".".$bulan.".".$tahun;
        return $sapDate;
    }
    public function convertSAPTimeToUITime($time) {
        //182350
        $jam = substr($time, 0, 2);
        $menit = substr($time, 2, 2);
        $detik = substr($time, 4, 2);
        $sapTime = $jam.":".$menit.":".$detik;
        return $sapTime;
    }
    public function convertEasyUItoSAPDate($date) {
        if(strlen($date) == 9) $date = '0'.$date; 
        $hari = substr($date, 3, 2);
        $bulan = substr($date, 0, 2);
        $tahun = substr($date, 6, 4);
        $sapDate = $tahun.$bulan.$hari;
        return $sapDate;
    }
    public function convertToSAPTime($time) {
        $jam = substr($time, 0, 2);
        $menit = substr($time, 3, 2);
        $detik = substr($time, 6, 2);
        $sapTime = $jam.$menit.$detik;
        return $sapTime;
    }
    //21.09.2011
    public function convertToSAPDate($date) {
        $hari = substr($date, 0, 2);
        $bulan = substr($date, 3, 2);
        $tahun = substr($date, 6, 4);
        $sapDate = $tahun.$bulan.$hari;
        return $sapDate;
    }
}

?>
