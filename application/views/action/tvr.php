<?php ?>

<!DOCTYPE html>
<html>

    <head>
        <base href="<?php echo base_url(); ?>" />
        <meta name='viewport' content='width=device-width'>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
        <title>e-DEMS | <?php echo $TITLE; ?></title>
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link href="<?= base_url(); ?>assets/css/others/sweetalert/sweetalert.css" rel="stylesheet">
        <style type='text/css'>
            /* -------------------------------------
                        INLINED WITH https://putsmail.com/inliner
                    ------------------------------------- */

            /* -------------------------------------
                        RESPONSIVE AND MOBILE FRIENDLY STYLES
                    ------------------------------------- */

            @media only screen and (max-width: 620px) {
                table[class=body] h1 {
                    font-size: 28px !important;
                    margin-bottom: 10px !important;
                }
                table[class=body] p,
                table[class=body] ul,
                table[class=body] ol,
                table[class=body] td,
                table[class=body] span,
                table[class=body] a {
                    font-size: 16px !important;
                }
                table[class=body] .wrapper,
                table[class=body] .article {
                    padding: 10px !important;
                }
                table[class=body] .content {
                    padding: 0 !important;
                }
                table[class=body] .container {
                    padding: 0 !important;
                    width: 100% !important;
                }
                table[class=body] .main {
                    border-left-width: 0 !important;
                    border-radius: 0 !important;
                    border-right-width: 0 !important;
                }
                table[class=body] .btn table {
                    width: 100% !important;
                }
                table[class=body] .btn a {
                    width: 100% !important;
                }
                table[class=body] .img-responsive {
                    height: auto !important;
                    max-width: 100% !important;
                    width: auto !important;
                }
            }

            /* -------------------------------------
                        PRESERVE THESE STYLES IN THE HEAD
                    ------------------------------------- */

            @media all {
                .ExternalClass {
                    width: 100%;
                }
                .ExternalClass,
                .ExternalClass p,
                .ExternalClass span,
                .ExternalClass font,
                .ExternalClass td,
                .ExternalClass div {
                    line-height: 100%;
                }
                .apple-link a {
                    color: inherit !important;
                    font-family: inherit !important;
                    font-size: inherit !important;
                    font-weight: inherit !important;
                    line-height: inherit !important;
                    text-decoration: none !important;
                }
                .btn-primary table td:hover {
                    background-color: #34495e !important;
                }
                .btn-primary a:hover {
                    background-color: #34495e !important;
                    border-color: #34495e !important;
                }
            }

            tbody tr td:last-child{
                text-align: left;
            }

            tbody tr td {
                color: #112833;
            }

            p{
                color: #112833;
            }

            .footer{
                left: 0px;
                background: #f6f6f6;
                border-top: none;
            }

            html {
                height: 100%;
            }
            body {
                min-height: 100%;
            }
        </style>
    </head>

    <body class='' style='background-color:#f6f6f6;font-family:sans-serif;-webkit-font-smoothing:antialiased;font-size:14px;line-height:1.4;margin:0;padding:0;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;text-align:left'>
        <table border='0' cellpadding='0' cellspacing='0' class='body' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background-color:#f6f6f6;width:100%;'>
            <tr>
                <td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
                <td class='container' style='font-family:sans-serif;font-size:14px;vertical-align:top;display:block;max-width:580px;padding:10px;width:580px;Margin:0 auto !important;'>
                    <div class='content' style='box-sizing:border-box;display:block;Margin:0 auto;max-width:580px;padding:10px;'>
                        <!-- START CENTERED WHITE CONTAINER -->
                        <span class='preheader' style='color:transparent;display:none;height:0;max-height:0;max-width:0;opacity:0;overflow:hidden;mso-hide:all;visibility:hidden;width:0;'>This is preheader text. Some clients will show this text as a preview.</span>
                        <table border='0' cellpadding='0' cellspacing='0' class='body' style='background-color:#f0f0f0; text-align:left; border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                            <tr>
                                <th width='20%' align='center'><img src='media/logo/Logo_SI.png' style='width:50%'></th>
                                <td width='60%' align='center'>
                                    <h4>
                                        Tindak Lanjut <?php echo $SHORT; ?>
                                        <br />
                                        e-DEMS
                                    </h4>
                                </td>
                                <th width='20%' align='center'><img src='media/CoE.png' style='width:50%'></th>
                            </tr>
                        </table>
                        <table border="0" class='main' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;background:#fff;0px 0px 0px 5px;width:100%;'>
                            <!-- START MAIN CONTENT AREA -->
                            <tr>
                                <td class='wrapper' style='font-family:sans-serif;font-size:14px;vertical-align:top;box-sizing:border-box;padding:20px;'>
                                    <table border='1' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                                        <tr>
                                            <td style='font-family:sans-serif;font-size:14px;vertical-align:top;border:0"' colspan="2">
                                                <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>
                                        <center>
                                            <h4><u><b></u><?php echo $TITLE; ?></u></b></b></h4></center>
                                        </p>
                                        <tr ><td colspan="2" style="text-align:center;border:0"><b>General Information</b></td></tr>
                                        <tr>
                                            <td><b>No. </b></td>
                                            <td><?php echo "  " . $NO; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Nama Pekerjaan </b></td>
                                            <td><?php echo "  " . $PACKET; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Unit Kerja Peminta </b></td>
                                            <td><?php echo "  " . $DESCPSECTION; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Unit Kerja Lokasi Pekerjaan </b></td>
                                            <td><?php echo "  " . $FL_TEXT; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Company </b></td>
                                            <td><?php echo "  " . $COMP_TEXT; ?></td>
                                        </tr>
                                    </table>
                                    <table border='1' cellpadding='0' cellspacing='0' style='margin-top: 10px;border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                                    </table>
                                    <?php
                                    if ($FILES) {
                                        // code...
                                        echo "<table border='1' cellpadding='0' cellspacing='0' style='margin-top: 10px;border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                            <tr><td colspan='2' style='text-align:center'><b>Lampiran  </td></tr>
                            <tr>
                              <td><b>File Pendukung </b></td>
                              <td style='text-align:center'><a rel='noopener noreferrer' target='_blank' href='{$FILES}'>View</a></td>
                            </tr>
                          </table>";
                                    }
                                    ?>



                                    <br />


                                    <form id="approve-aprroval">
                                        <input type="text" class="form-control input-sm" style="display:none" name="START_DATE" id="NAMA" value="<?php echo $NAMA; ?>" ></input>
                                        <?php if ($STATUS == 'In Approval') { ?>
                                            <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Surat permintaan ini sebagai informasi untuk melakukan tindak lanjut dokumen, klik 'Approve' untuk setuju.</p>
                                            <div class="form-actions" style="align-content:right;width:100%;text-align:right">
                                                <a href='#' id="btn-reject" data-toggle="modal" data-target="#myModal2"> <i class="fa fa-check"></i> Approve </a>
                                                &nbsp
                                                <a href='#' style="color:red" id="btn-reject" data-toggle="modal" data-target="#myModal"><i class="fa fa-close"></i>  Reject </a>
                                                <!-- <button type="button" class="btn btn-sm btn-inverse btn-link" id="btn-reject" data-toggle="modal" data-target="#myModal">Reject</button> -->
                                            </div>
                                        <?php } else { ?>
                                            <div class="form-actions" style="align-content:right;width:100%;text-align:right">
                                                <?php if ($REJECTED != '') { ?>
                                                    <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Dokumen <u><?php echo $TITLE; ?></u> telah ditolak.</p>
                                                <?php } elseif ($APPROVE1_BY == $NAMA && $APPROVE1_AT == '') { ?>
                                                    <div class="form-actions" style="align-content:right;width:100%;text-align:right">
                                                        <a href='#' id="btn-reject" data-toggle="modal" data-target="#myModal2"> <i class="fa fa-check"></i> Approve </a>
                                                        &nbsp
                                                        <a href='#' style="color:red" id="btn-reject" data-toggle="modal" data-target="#myModal"><i class="fa fa-close"></i>  Reject </a>
                                                        <!-- <button type="button" class="btn btn-sm btn-inverse btn-link" id="btn-reject" data-toggle="modal" data-target="#myModal">Reject</button> -->
                                                    </div>
                                                <?php } elseif ($APPROVE2_BY == $NAMA && $APPROVE2_AT == '') { ?>
                                                    <div class="form-actions" style="align-content:right;width:100%;text-align:right">
                                                        <a href='#' id="btn-reject" data-toggle="modal" data-target="#myModal2"> <i class="fa fa-check"></i> Approve </a>
                                                        &nbsp
                                                        <a href='#' style="color:red" id="btn-reject" data-toggle="modal" data-target="#myModal"><i class="fa fa-close"></i>  Reject </a>
                                                        <!-- <button type="button" class="btn btn-sm btn-inverse btn-link" id="btn-reject" data-toggle="modal" data-target="#myModal">Reject</button> -->
                                                    </div>
                                                    <!-- <button type="button" class="btn btn-sm btn-inverse btn-link" id="btn-reject" data-toggle="modal" data-target="#myModal">Reject</button> -->
                                                <?php } else { ?>
                                                    <div class="form-actions" style="align-content:right;width:100%;text-align:right">


                                                        <!-- <button type="button" class="btn btn-sm btn-inverse btn-link" id="btn-reject" data-toggle="modal" data-target="#myModal">Reject</button> -->
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </form>
                                    <!-- <p style='font-family:sans-serif;font-size:14px;font-weight:normal;margin:0;Margin-bottom:15px;'>Hormat kami.</p> -->
                                </td>
                            </tr>
                        </table>
                </td>
            </tr>
            <!-- END MAIN CONTENT AREA -->
        </table>
        <!-- START FOOTER -->
        <div class='footer' style='clear:both;padding-top:10px;text-align:center;width:100%;'>
            <table border='0' cellpadding='0' cellspacing='0' style='border-collapse:separate;mso-table-lspace:0pt;mso-table-rspace:0pt;width:100%;'>
                <tr style="align-content:center">
                    <td class='content-block' style='font-family:sans-serif;font-size:14px;vertical-align:top;color:#999999;font-size:12px;text-align:center;'>
                        <span class='apple-link' style='color:#999999;font-size:12px;text-align:center;'>
                            e-DEMS, <a style='color:#999999;font-size:12px;' href='http://e-dems.semenindonesia.com'>Electronic - Design Engineering Management System</a>
                            <br>
                            Copyright by <a style='color:#999999;font-size:12px;' href='http://sisi.id/'>PT.SISI</a>
                        </span>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
        <!-- END FOOTER -->
        <!-- END CENTERED WHITE CONTAINER -->
    </div>
</td>
<td style='font-family:sans-serif;font-size:14px;vertical-align:top;'>&nbsp;</td>
</tr>
</table>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form id="reject-aprroval">
                <div class="modal-header" style="text-align:center">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Tindak Lanjut <?php echo $SHORT; ?></b></h4>
                    <h4 class=""><b>e-DEMS</b></h4>
                </div>
                <div class="modal-body" style="width:100%">
                    <p>Deskripsi alasan reject</p>
                    <textarea class="form-control" rows="5" id="REASON" placeholder="(max. 255 karakter)" maxlength="255"></textarea>
                    <input type="text" class="form-control input-sm" style="display:none" name="START_DATE" id="NAMA" value="<?php echo $NAMA; ?>" ></input>
                </div>
                <div class="modal-footer">
                    <button id="test-reject" type="submit" class="btn btn-default" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form id="approve">
                <div class="modal-header" style="text-align:center">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Tindak Lanjut <?php echo $SHORT; ?></b></h4>
                    <h4 class=""><b>e-DEMS</b></h4>
                </div>
                <div class="modal-body" style="width:100%">
                    <p>Deskripsi Note Approve</p>
                    <textarea class="form-control" rows="5" id="REASON2" placeholder="(max. 255 karakter)" maxlength="255"></textarea>
                    <input type="text" class="form-control input-sm" style="display:none" name="START_DATE" id="key" value="<?php echo $d; ?>" ></input>
                </div>
                <div class="modal-footer">
                    <button id="test" type="submit" class="btn btn-default" data-dismiss="modal">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
        </div>
        </form>
    </div>
</div>

<!-- ======default Jquery====== -->
<script src="<?= base_url(); ?>js/action/tvr.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
<script src="<?= base_url(); ?>assets/js/others/sweetalert/sweetalert.min.js"></script>
</body>
</html>
<?php ?>
