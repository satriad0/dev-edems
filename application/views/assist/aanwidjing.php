<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Assign Task</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Assign Task</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form">
                        <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab">
                            <span class="hidden-sm-up"><i class="ti-home"></i></span> 
                            <span class="hidden-xs-down">List <?= $tittle ?></span>
                        </a>
                    </li>
                    <li class="nav-item nw-form">
                        <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab">
                            <span class="hidden-sm-up"><i class="ti-user"></i></span>
                            <span class="hidden-xs-down">Create <?= $tittle ?></span>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_awr" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                        <!-- <th>No</th> -->
                                        <th style="text-align: center;" >NO.</th>
                                        <th></th>
                                        <th style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO AANWIDJING</th>
                                        <th style="text-align: center;" >NO NOTIFIKASI</th>
                                        <th style="text-align: center;" >NO DOC. ENG.</th>
                                        <th style="text-align: center;" >NAMA PEKERJAAN</th>
                                        <th style="text-align: center;" >STATUS</th>
                                        <th style="text-align: center;" >UK PEMINTA (P. SECTION)</th>
                                        <th style="text-align: center;" >UNIT KERJA LOKASI PEKERJAAN</th>
                                        <th style="text-align: center;" >COMPANY</th>
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">
                        <?php
                        $lvl = 1;
                        $disSM = '';
                        $disMan = 'style="display:none;"';
                        if (isset($MGR_APP)) {
                            $disMan = '';
                            $disSM = 'style="display:none;"';
                            $lvl = 0;
                        }
                        ?>
                        <input type="hidden" class="form-control input-sm" name="LVL" id="LVL"
                               value="<?php echo $lvl; ?>" readonly=""/>

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-6">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">General Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6 col-no">
                                                <div class="form-group">
                                                    <label for="NO_PENDAMPINGAN" class="control-label">No. EAT <span
                                                            class="text-danger" readonly required>*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NO_PENDAMPINGAN"
                                                           id="NO_PENDAMPINGAN" value="<?php echo "{$no}"; ?>" readonly
                                                           required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;">
                                                    <label for="ID" class="control-label">ID Penugasan</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID"
                                                           readonly=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_PENGAJUAN" class="control-label">Pekerjaan <span
                                                            class="text-danger">*</span></label>
                                                <!-- <select name="ID_PENGAJUAN" id="ID_PENGAJUAN" class="form-control form-control-sm select-sm" data-title="Pilih Erf..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="ID_PENGAJUAN" id="ID_PENGAJUAN"
                                                            class="chosen input-sm select2" style="width:100%;"
                                                            required>
                                                                <?php
                                                                foreach ($foreigns as $list) {
                                                                    echo "<option value='" . $list["ID_PENGAJUAN"] . "' title='" . $list["NO_DOK_ENG"] . " - " . $list["NOTIF_NO"] . "' data-subtext='" . $list["NO_PENGAJUAN"] . "' data-id_dok='" . $list["ID_DOK_ENG"] . "' data-id='" . $list["ID"] . "' data-no='" . $list["NO_DOK_ENG"] . "' data-packet='" . $list["PACKET_TEXT"] . "' data-notif='" . $list["NOTIF_NO"] . "' data-flcode='" . $list["FUNCT_LOC"] . "' data-fltext='" . $list["FL_TEXT"] . "' data-shorttext='" . $list["SHORT_TEXT"] . "' data-uktext='" . $list["UK_TEXT"] . "' data-custreq='" . $list["CUST_REQ"] . "' data-lokasi='" . $list["LOKASI"] . "' data-const='" . $list["CONSTRUCT_COST"] . "' data-eng='" . $list["ENG_COST"] . "' data-stat='" . $list["STAT"] . "' >" . "[{$list['NO_DOK_ENG']}] " . $list["PACKET_TEXT"] . "</option>";
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_ERF" class="control-label">No. Dok. Eng. </label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_ERF"
                                                           id="NO_ERF" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIF_NO" class="control-label">No. Notifikasi </label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIF_NO"
                                                           id="NOTIF_NO" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SHORT_TEXT" class="control-label">Pekerjaan <span
                                                            class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm select-sm"
                                                           name="SHORT_TEXT" id="SHORT_TEXT"
                                                           placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="PACKET_TEXT" class="control-label">Packet Pekerjaan <span
                                                            class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm select-sm"
                                                           name="PACKET_TEXT" id="PACKET_TEXT"
                                                           placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="UK_TEXT" class="control-label">Unit Kerja Peminta</label>
                                                    <input type="textarea" class="form-control input-sm" name="UK_TEXT"
                                                           id="UK_TEXT" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="FL_TEXT" class="control-label">Unit Kerja Lokasi Pekerjaan</label>
                                                    <input type="textarea" class="form-control input-sm" name="FL_TEXT"
                                                           id="FL_TEXT" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="HDR_TEXT" class="control-label">Header Text
                                                        <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="HDR_TEXT"
                                                              id="HDR_TEXT" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;" ><?php echo $ASSIST_TEXT->GEN_PAR1; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="MID_TEXT" class="control-label">Middle Text
                                                        <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="MID_TEXT"
                                                              id="MID_TEXT" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;" ><?php echo $ASSIST_TEXT->GEN_PAR2; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="FTR_TEXT" class="control-label">Footer Text
                                                        <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="FTR_TEXT"
                                                              id="FTR_TEXT" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;" ><?php echo $ASSIST_TEXT->GEN_PAR3; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="NOTE" class="control-label">Note</label>
                                                    <textarea class="form-control tarea-sm disabled" rows="5" name="NOTE"
                                                              id="NOTE" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;" disabled ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--/span-->
                                    <div class="col-md-6" >
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title"> Others</h5>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-md-12">
                                            <div class="form-group">
                                                <?php
                                                $inputs = '<fieldset><label for="TIM_PENGKAJI" class="control-label">Tim Pengkaji<br /></label>';
                                                $inputs .= '<ul class="checkbox-columns">';

                                                $row = 1;
                                                foreach ($PENGKAJI as $subject) {
                                                    $select = 'checked';
                                                    if (count($PENGKAJI) > 1) {
                                                        $select = '';
                                                    }
                                                    $inputs .= '<li><label><input type="checkbox" name="PENGKAJI[]" value="' . $subject->GEN_DESC . '" ' . $select . ' > ' . $subject->GEN_DESC . '</input></label></li>';
                                                }

                                                $inputs .= '</ul>';
                                                $inputs .= '</fieldset>';

                                                echo $inputs;
                                                ?>
                                            </div>
                                        </div>
                                        <div class="div_app">
                                            <div class="row disMan" <?php echo $disMan; ?> >
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="MANAGER" class="control-label">Pilih Approval Manager
                                                            <span class="text-danger">*</span></label>
                                                            <!-- <select name="MANAGER" id="MANAGER" class="chosen input-sm select2" style="width:100%;" required></select> -->
                                                        <select class="chosen-select select2" name="MANAGER" id='MANAGER'
                                                                style="width:100%;" required>
                                                                    <?php
                                                                    foreach ($MANAGER as $list) {
                                                                        echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->JAB_TEXT}' >[ {$list->NOBADGE} ] {$list->NAMA} - {$list->JAB_TEXT}</option>";
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row disSM" <?php echo $disSM; ?>>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="SM_APP" class="control-label">Pilih Approval SM <span
                                                                class="text-danger">*</span></label>
                                                        <select class="chosen-select select2" name="SM_APP" id='SM_APP'
                                                                style="width:100%;" required>
                                                                    <?php
                                                                    foreach ($BIRO as $list) {
                                                                        echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->JAB_TEXT}' >[ {$list->NOBADGE} ] {$list->NAMA} - {$list->JAB_TEXT}</option>";
                                                                    }
                                                                    ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row div_file" style="display:none;" >
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES" class="control-label">Select File Assistance
                                                        (*.pdf) <span class="text-danger">*</span> </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES" id="FILES"
                                                           accept="application/pdf"/>
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri" rel="noopener noreferrer" target="_blank"><i
                                                            id="file_name"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php
                                echo "{$short_tittle}-write";
                                echo " {$short_tittle}-modify";
                                ?>" id="btn-save"><i class="fa fa-check"></i> Save
                                </button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <div id="uploadModal" class="modal fade" tabindex="-1" role="dialog"  aria-hidden="true" >
        <div class="modal-dialog modal-lg modal-md modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title title_ukur">File Upload Aanwijzing Assist Task</h4>
                </div>
                <form id="fm-reject">
                    <div class="form-body">

                        <div class="modal-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <p><b>Nama Pekerjaan:</b> <span id="label_np"> </span></p>
                                                    <hr>
                                                    <br>
                                                </div>
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group" id="file_uploaded">
                                                    </div>
                                                    <div class="form-group" id="form_upload">
                                                        <div class="col-md-12">
                                                            <label for="assist_file" class="control-label" id="label_up_form"><span class="text-danger" >*</span> Please upload file.. (*.pdf) </label>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <input type="hidden" name="id_in" id="id_in"/>
                                                            <input type="hidden" name="nm_dok" id="nm_dok"/>
                                                            <input type="file" class="form-control" name="assist_file" id="assist_file" accept="application/pdf" required />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary btn-canc-reject" data-dismiss="modal" aria-hidden="true" id="btnHidel123" onclick="hideShowModal('uploadModal')">Cancel</button>
                            <button class="btn btn-primary btn-conf-reject" id="act_upl123">Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- =============================== END MODAL ERF =============================== -->

    </div>
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/assist/ass_aanwidjing.js"></script>
