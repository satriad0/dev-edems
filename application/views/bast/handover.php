
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Transmital</span></a> </li>
                    <li class="nav-item nw-form" style="display:none;"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Create Transmital</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_list" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>

                                    <tr id="tb_tasnmital_tr_search">
                                        <td></td>
                                        <th style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO. TRANSMITAL</th>
                                        <th style="text-align: center;" >NAMA PAKET PEKERJAAN</th>
                                        <th style="text-align: center;" >UNIT KERJA PEMINTA</th>
                                        <th style="text-align: center;" >UNIT KERJA LOKASI PEKERJAAN</th>
                                        <th style="text-align: center;" >COMPANY</th>
                                        <th style="text-align: center;" >STATUS</th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <td style="text-align: center;" ></td>
                                    </tr>

                                    <tr>
                                        <th></th>
                                        <th style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO. TRANSMITAL</th>
                                        <th style="text-align: center;" >NAMA PAKET PEKERJAAN</th>
                                        <th style="text-align: center;" >UNIT KERJA PEMINTA</th>
                                        <th style="text-align: center;" >UNIT KERJA LOKASI PEKERJAAN</th>
                                        <th style="text-align: center;" >COMPANY</th>
                                        <th style="text-align: center;" >STATUS</th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-12">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">General Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_BAST" class="control-label">No Transmital <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NO_BAST" id="NO_BAST" value="<?php echo "{$no}"; ?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID Transmital</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_DOK_ENG" class="control-label">Nama Paket <span class="text-danger">*</span></label>
                                                    <!-- <select name="ID_DOK_ENG" id="ID_DOK_ENG" class="form-control form-control-sm select-sm" data-title="Pilih Packet..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="ID_DOK_ENG" id="ID_DOK_ENG" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($foreigns as $list) {
                                                            echo "<option value='" . $list['ID_DOK_ENG'] . "' title='" . $list['ID_DOK_ENG'] . " - " . $list['ID_DOK_ENG'] . "' data-subtext='" . $list["NO_DOK_ENG"] . "' data-no='" . $list["NO_DOK_ENG"] . "' data-notif='" . $list["NOTIFIKASI"] . "' data-packet='" . $list["PACKET_TEXT"] . "' data-const='" . $list["DE_CONSTRUCT_COST"] . "' data-eng='" . $list["DE_ENG_COST"] . "' >" . $list["NO_DOK_ENG"] . " - " . $list["PACKET_TEXT"] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_DOK_ENG" class="control-label">No. Paket</label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">No. Notifikasi</label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="PACKET_TEXT" class="control-label">Nama Paket</label>
                                                    <input type="textarea" class="form-control input-sm" name="PACKET_TEXT" id="PACKET_TEXT" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="HDR_TEXT" class="control-label" required>Header Text <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="HDR_TEXT" id="HDR_TEXT" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="MID_TEXT" class="control-label" required>Middle Text <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="MID_TEXT" id="MID_TEXT" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="FTR_TEXT" class="control-label" required>Footer Text <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="FTR_TEXT" id="FTR_TEXT" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="DE_CONSTRUCT_COST" class="control-label">Biaya Konstruksi <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control input-sm" name="DE_CONSTRUCT_COST" id="DE_CONSTRUCT_COST" maxlength="255" required />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="DE_ENG_COST" class="control-label">Biaya Engineering <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control input-sm" name="DE_ENG_COST" id="DE_ENG_COST" maxlength="255" required />
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <!--/span-->
                                    <!-- <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <h5 class="card-title"> Upload Document</h5>
                                          <hr>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="RKS_FILE" class="control-label"><u>Select RKS File </u></label>
                                            <div class="col-md-12">
                                              <input type="file" name="RKS_FILE" id="RKS_FILE" />
                                            </div>
                                            <div class="col-md-12">
                                              <a id="rks_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="rks_name"></i></a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="BQ_FILE" class="control-label"><u>Select BQ File </u></label>
                                            <div class="col-md-12">
                                              <input type="file" name="BQ_FILE" id="BQ_FILE" />
                                            </div>
                                            <div class="col-md-12">
                                              <a id="bq_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="bq_name"></i></a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="DRAW_FILE" class="control-label"><u>Select Drawing File </u></label>
                                            <div class="col-md-12">
                                              <input type="file" name="DRAW_FILE" id="DRAW_FILE" />
                                            </div>
                                            <div class="col-md-12">
                                              <a id="draw_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="draw_name"></i></a>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="ECE_FILE" class="control-label"><u>Select ECE File </u></label>
                                              <div class="col-md-12">
                                                <input type="file" name="ECE_FILE" id="ECE_FILE" />
                                              </div>
                                              <div class="col-md-12">
                                                <a id="ece_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="ece_name"></i></a>
                                              </div>
                                            </div>
                                          </div>
                                      </div>
  
  
                                    </div> -->
                                    <!--/span-->
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                        echo " {$short_tittle}-modify"; ?>" style="display:none;" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url(); ?>js/bast/ba_handover.js"></script>
