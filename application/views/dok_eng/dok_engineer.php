
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <?php $sess = $this->session->userdata("logged_in"); ?>

        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <div class="cssload-loading" id="loading">
                  <i></i>
                  <i></i>
                  <i></i>
                </div> -->
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List <?= $tittle ?></span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Create <?= $tittle ?></span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_dok_eng" class="table display table-bordered table-striped" style="width:100%;" >
                                <thead>
                                    <tr id="tb_dok_eng_tr_search">
                                        <td></td>
                                        <th style="text-align: center;" >tanggal</th>
                                        <th style="text-align: center;" >no. doc eng</th>
                                        <th style="text-align: center;" >no. notifikasi</th>
                                        <th style="text-align: center;" >no. erf</th>
                                        <th style="text-align: center;" >no. eat</th>
                                        <th style="text-align: center;" >nama paket pekerjaan</th>
                                        <!-- <th>Progress</th> -->
                                        <th style="text-align: center;" >status</th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <td style="text-align: center;" ></td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO. DOC. ENG.</th>
                                        <th style="text-align: center;" >NO. NOTIFIKASI</th>
                                        <th style="text-align: center;" >NO. ERF</th>
                                        <th style="text-align: center;" >NO. EAT</th>
                                        <th style="text-align: center;" >NAMA PAKET PEKERJAAN</th>
                                        <!-- <th>Progress</th> -->
                                        <th style="text-align: center;" >STATUS</th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <input type="hidden" id="id_role" value="<?= $sess['role_id'] ?>" name="">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-6">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">General Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_DOK_ENG" class="control-label">No Dokumen <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" value="<?php echo "{$no}"; ?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID Dokumen</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_PENUGASAN" class="control-label">EAT <span class="text-danger">*</span></label>
                                                    <!-- <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="form-control form-control-sm select-sm selectpicker" data-title="Pilih Erf..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <!-- <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="form-control form-control-sm select-sm" data-title="Pilih Eat..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($foreigns as $list) {
                                                            echo "<option value='" . $list['ID_PENUGASAN'] . "' title='" . $list['NO_PENUGASAN'] . ' - ' . $list['OBJECTIVE'] . "' data-subtext='" . $list['NO_PENGAJUAN'] . "' data-no='" . $list['NO_PENUGASAN'] . "' data-notif='" . $list['NOTIFIKASI'] . "' data-pengajuan='" . $list['PENGAJUAN'] . "' data-object='" . $list['OBJECTIVE'] . "' >" . $list['NO_PENUGASAN'] . " - " . $list['PENGAJUAN'] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_PENUGASAN" class="control-label">EAT No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_PENUGASAN" id="NO_PENUGASAN" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">Notifikasi No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                              <div class="form-group">
                                                <label for="PENGAJUAN" class="control-label">Deskripsi ERF.</label>
                                                <input type="textarea" class="form-control input-sm" name="PENGAJUAN" id="PENGAJUAN" readonly />
                                              </div>
                                            </div> -->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="OBJECTIVE" class="control-label">Tujuan yang Diharapkan EAT</label>
                                                    <input type="textarea" class="form-control input-sm" name="OBJECTIVE" id="OBJECTIVE" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_PACKET" class="control-label">Pilih Paket <span class="text-danger">*</span></label>
                                                    <select name="ID_PACKET" id="ID_PACKET" class="form-control form-control-sm select-sm" data-title="Pilih Packet..." data-show-subtext="true" data-live-search="true" required></select>
                                                    <!-- <select name="ID_PACKET" id="ID_PACKET" class="chosen input-sm select2" required></select> -->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="NOMINAL" class="control-label">Nilai ECE <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="NOMINAL" id="NOMINAL" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="display:none;" >
                                                <div class="form-group">
                                                    <label for="PACKET_TEXT" class="control-label">Nama Paket <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="PACKET_TEXT" id="PACKET_TEXT" placeholder="(max. 255 karakter)" maxlength="255" style="resize:none;" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" style="display:none;" >
                                                <div class="form-group">
                                                    <label for="PROGRESS" class="control-label">Progress <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control input-sm" name="PROGRESS" id="PROGRESS" placeholder="(digits: 10,20,30,..)" min="0" max="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <!-- <div class="form-group">
                                                  <label for="NOTIFIKASI" class="control-label">Notifikasi No.</label>
                                                  <input type="textarea" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" placeholder="Data tak ditemukan" readonly />
                                                </div>
                                              </div> -->
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Approval Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Dibuat oleh (Pelaksana/Supervisor/Manager) <small>pilih beberapa, jika paket gabungan</small>  <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 paraf" id='CRT_LIST' multiple style="width:100%;" tabindex="4" required >
                                                        <?php
                                                        foreach ($LIST_CRT as $list) {
                                                            echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Diverifikasi oleh (Supervisor & Manager) <small>pilih beberapa, jika paket gabungan</small>  <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 paraf" id='PARAF_LIST' multiple style="width:100%;" tabindex="4" required >
                                                        <?php
                                                        foreach ($LIST_MNG as $list) {
                                                            echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Disetujui oleh (SM Pengkaji)  <small>pilih beberapa, jika paket gabungan</small> <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 approval" id='APP_LIST' multiple style="width:100%;" tabindex="4" required >
                                                        <?php
                                                        foreach ($LIST_APP as $list) {
                                                            echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" id="HDE_LINE_APP" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Disetujui oleh (GM of DE) <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 approval" id='HDE_LIST' style="width:100%;" tabindex="4" required>
                                                        <?php
                                                        foreach ($LIST_HDE as $list) {
                                                            echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <!--/span  style="display: none;" -->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Documents</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="COLOR" class="control-label">Warna Cover Dokumen <span class="text-danger">*</span></label>
                                                    <select name="COLOR" id="COLOR" class="chosen input-sm select2" style="width:100%;" required>
                                                        <option value='Abu-abu' title='Abu-abu' data-kddis='A' > Abu-abu - Gabungan</option>
                                                        <option value='Merah' title='Merah' data-kddis='E' > Merah - Electrical </option>
                                                        <option value='Kuning' title='Kuning' data-kddis='C' > Kuning - Sipil </option>
                                                        <option value='Biru' title='Biru' data-kddis='M' > Biru - Mechanical </option>
                                                        <option value='Biru' title='Hijau' data-kddis='P' > Hijau - Proses </option>
                                                        <option value='Biru' title='Orange' data-kddis='T' > Orange - Peng. Teknologi </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h5 class="card-title"> Upload Document</h5>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div id="capex">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="RKS_FILE" class="control-label"><u>Select TOR File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" class="detail-input" name="RKS_FILE" id="RKS_FILE" accept="application/pdf" onchange="previewDocument(this)" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="rks_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="rks_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="BQ_FILE" class="control-label"><u>Select BQ File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" class="detail-input" name="BQ_FILE" id="BQ_FILE" accept="application/pdf" onchange="previewDocument(this)" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="bq_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="bq_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="ECE_FILE" class="control-label"><u>Select ECE File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" class="detail-input" name="ECE_FILE" id="ECE_FILE" accept="application/pdf" onchange="previewDocument(this)" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="ece_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="ece_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="DRAW_FILE" class="control-label"><u>Select Drawing File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" class="detail-input" name="DRAW_FILE" id="DRAW_FILE" accept="application/pdf" onchange="previewDocument(this)" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="draw_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="draw_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="kajian">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="KAJIAN_FILE" class="control-label"><u>Select Kajian File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" class="kajian-input" name="KAJIAN_FILE" id="KAJIAN_FILE" accept="application/pdf" onchange="previewDocument(this)" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="kajian_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="kajian_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Convert PDF Version</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a rel="noopener noreferrer" target="_blank" href="https://docupub.com/pdfconvert/"><i class="menu-icon fa fa-retweet"></i>&nbsp;&nbsp;Klik untuk membutuhkan bantuan dalam konversi pdf dari file scanner (Drawing). </a>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Compress PDF</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a rel="noopener noreferrer" target="_blank" href="https://docupub.com/pdfcompress/"><i class="menu-icon fa fa-laptop"></i>&nbsp;&nbsp;Klik untuk membutuhkan bantuan dalam compress pdf dari file scanner (Drawing). </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                        echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- modal preview document -->
    <div class="modal fade" id="preview_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="height: 700px">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Preview File</h5>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="secret_doc">
                    <input type="hidden" id="secret_doc_name">
                    <div id="preview_body">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal" id="preview_cancel">Cancel</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="preview_close">Upload</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade showing" id="preview_modal_download_req" tabindex="-1" role="dialog" aria-hidden="true" >
        <div class="modal-dialog modal-lg" role="download_req" >
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"  style="font-weight:bold; text-align:center;">Histori Download Engineering Documents</h5>
                </div>
                <div class="modal-body" >
                    <center><h6 id="title_mod_download">Dokumen Engineering</h6></center>
                    <hr>
                    <div id="download_bd">


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-hidden="true" onClick="hideShowModal('preview_modal_download_req')">Close</button>
                </div>
            </div> 
        </div>
    </div>
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url(); ?>js/dok_eng/de_engineer.js"></script>
