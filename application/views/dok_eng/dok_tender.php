
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <div class="cssload-loading" id="loading">
                  <i></i>
                  <i></i>
                  <i></i>
                </div> -->
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Tender Documents</span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Create Tender Documents</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_dok_tend" class="table display table-bordered table-striped" style="width:100%;" >
                                <thead>
                                    <tr id="tb_tender_tr_search">
                                        <td></td>
                                        <th style="text-align: center;" ids="c1"></th>
                                        <th style="text-align: center;" ids="c1"></th>
                                        <th style="text-align: center;" ids="c1"></th>
                                        <th style="text-align: center;" ids="c1"></th>
                                        <th style="text-align: center;" ids="c1"></th>
                                        <th style="text-align: center;" ids="c1"></th>
                                        <!-- <th>Progress</th> -->
                                        <th style="text-align: center;" ids="c1"></th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <td style="text-align: center;" ></td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO. DOC. TENDER.</th>
                                        <th style="text-align: center;" >NO. NOTIFIKASI</th>
                                        <th style="text-align: center;" >NO. ERF</th>
                                        <th style="text-align: center;" >NO. DOC. ENG.</th>
                                        <th style="text-align: center;" >NAMA PAKET PEKERJAAN</th>
                                        <!-- <th>Progress</th> -->
                                        <th style="text-align: center;" >STATUS</th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-6">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">General Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6 trn-no">
                                                <div class="form-group">
                                                    <label for="NO_DOK_TEND" class="control-label">No Dokumen Tender <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NO_DOK_TEND" id="NO_DOK_TEND" value="<?php echo "{$no}"; ?>" readonly required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID Dokumen Tender</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID_PACKET" class="control-label">ID Packet</label>
                                                    <input type="text" class="form-control input-sm" name="ID_PACKET" id="ID_PACKET" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_DOK_ENG" class="control-label">Document Engineering <span class="text-danger">*</span></label>
                                                    <select name="ID_DOK_ENG" id="ID_DOK_ENG" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($foreigns as $list) {
                                                            echo "<option value='" . $list['ID'] . "' title='" . $list['NO_DOK_ENG'] . ' - ' . $list['PACKET_TEXT'] . "' data-subtext='" . $list['NO_DOK_ENG'] . "' data-no='" . $list['NO_DOK_ENG'] . "' data-notif='" . $list['NOTIFIKASI'] . "' data-idpacket='" . $list['ID_PACKET'] . "' data-packet='" . $list['PACKET_TEXT'] . "' data-rks='" . $list['RKS_FILE'] . "' data-bq='" . $list['BQ_FILE'] . "' data-draw='" . $list['DRAW_FILE'] . "' data-ece='" . $list['ECE_FILE'] . "' >" . $list['NO_DOK_ENG'] . " - " . $list['PACKET_TEXT'] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_DOK_ENG" class="control-label">No. Document Engineering</label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">Notifikasi No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                              <div class="form-group">
                                                <label for="PENGAJUAN" class="control-label">Deskripsi ERF.</label>
                                                <input type="textarea" class="form-control input-sm" name="PENGAJUAN" id="PENGAJUAN" readonly />
                                              </div>
                                            </div> -->
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="PACKET_TEXT" class="control-label">Nama Paket <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="PACKET_TEXT" id="PACKET_TEXT" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="ID_PACKET" class="control-label">Pilih Paket <span class="text-danger">*</span></label>
                                              <select name="ID_PACKET" id="ID_PACKET" class="form-control form-control-sm select-sm" data-title="Pilih Packet..." data-show-subtext="true" data-live-search="true" required></select>
                                            </div>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="NOMINAL" class="control-label">Nominal Dokumen <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="NOMINAL" id="NOMINAL" required />
                                                </div>
                                            </div>
                                        </div>


                                    </div>


                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Documents</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="COLOR" class="control-label">Warna Cover Dokumen <span class="text-danger">*</span></label>
                                                    <select name="COLOR" id="COLOR" class="chosen input-sm select2" style="width:100%;" required>
                                                        <option value='Abu-abu' title='Abu-abu' > Abu-abu - Gabungan</option>
                                                        <option value='Merah' title='Merah' > Merah - Electrical </option>
                                                        <option value='Kuning' title='Kuning' > Kuning - Mechanical </option>
                                                        <option value='Biru' title='Biru' > Biru - Sipil </option>
                                                        <option value='Biru' title='Hijau' > Hijau - Proses </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h5 class="card-title"> Upload Document</h5>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div id="capex">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="RKS_FILE" class="control-label"><u> TOR File </u></label>
                                                        <div class="col-md-12" style="display:none;">
                                                            <input type="file" class="detail-input" name="RKS_FILE" id="RKS_FILE" accept="application/pdf" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="rks_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="rks_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="BQ_FILE" class="control-label"><u> BQ File </u></label>
                                                        <div class="col-md-12" style="display:none;">
                                                            <input type="file" class="detail-input" name="BQ_FILE" id="BQ_FILE" accept="application/pdf" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="bq_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="bq_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="ECE_FILE" class="control-label"><u> ECE File </u></label>
                                                        <div class="col-md-12" style="display:none;">
                                                            <input type="file" class="detail-input" name="ECE_FILE" id="ECE_FILE" accept="application/pdf" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="ece_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="ece_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="DRAW_FILE" class="control-label"><u> Drawing File </u></label>
                                                        <div class="col-md-12" style="display:none;">
                                                            <input type="file" class="detail-input" name="DRAW_FILE" id="DRAW_FILE" accept="application/pdf" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="draw_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="draw_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="display:none;">
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="kajian">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group" style="display:none;">
                                                        <label for="KAJIAN_FILE" class="control-label"><u>Select Kajian File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" class="kajian-input" name="KAJIAN_FILE" id="KAJIAN_FILE" accept="application/pdf" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="kajian_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="kajian_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                    <!--/span-->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Approval Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-2 col-sm-2">
                                              <div class="form-group">
                                                <label class="control-label" for="status">Tipe</label>
                                                <select name="status" id="in_type_mail" class="chosen-select select2" style="width:100%;" >
                                                  <option value="CREATED" selected="">Dibuat</option>
                                                  <option value="VERF">Diverifikasi</option>
                                                  <option value="APPROVE">Disetujui</option>
                                                </select>
                                              </div>
                                            </div>
                                            <div class="col-md-8 col-sm-8">
                                              <div class="form-group">
                                                <label class="control-label" for="status">Pegawai</label>
                                                <select class="form-control selectpicker" data-live-search="true" id="in_mail" data-title="Pilih pegawai..." multiple></select>
                                              </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                              <div class="form-group">
                                                <br />
                                                <br />
                                                <button type="button" class="btn btn-sm btn-success" id="tampung_email_btn"><i class="fa fa-plus"></i> Tampung</button>
                                              </div>
                                            </div>
                                            <div class="col-sm-12"><br></div>
                                            <div class="col-sm-12">
                                              <div class="table-responsive">
                                                <table class="table table-striped table-bordered display compact" id="tb_mail">
                                                  <thead>
                                                    <th width="50">Tipe</th>
                                                    <th>Email</th>
                                                    <th width="50">Opsi</th>
                                                  </thead>
                                                </table>
                                              </div>
                                            </div>
                                            <div class="col-sm-12">
                                            </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Dibuat oleh (Eselon 3/4 unit kerja peminta)  <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 paraf" id='CRT_LIST' style="width:100%;" tabindex="4" multiple required >
                                                        <?php
                                                        foreach ($LIST_ESL3 as $list) { //LIST_CTR
                                                            // echo '<option value="" ></option>';
                                                            // echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                            echo '<option value="' . $list->NOBADGE . ' - ' . $list->NAMA . ' - ' . $list->EMAIL . ' - ' . $list->JAB_TEXT . '" >[' . $list->NOBADGE . '] ' . $list->NAMA . ' ' . $list->JAB_TEXT . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Direview oleh (Eselon 2/3 unit kerja peminta)  <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 paraf" id='PARAF_LIST' style="width:100%;" tabindex="4" multiple required >
                                                        <?php
                                                        foreach ($LIST_ESL2 as $list) { //LIST_MNG
                                                            echo '<option value="' . $list->NOBADGE . ' - ' . $list->NAMA . ' - ' . $list->EMAIL . ' - ' . $list->JAB_TEXT . '" >[' . $list->NOBADGE . '] ' . $list->NAMA . ' ' . $list->JAB_TEXT . '</option>';
                                                            // echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Disetujui oleh (Eselon 1 unit kerja peminta) <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 approval" id='APP_LIST' style="width:100%;" tabindex="4" multiple required >
                                                        <?php
                                                        foreach ($LIST_ESL1 as $list) {   //LIST_APP
                                                            echo '<option value="' . $list->NOBADGE . ' - ' . $list->NAMA . ' - ' . $list->EMAIL . ' - ' . $list->JAB_TEXT . '" >[' . $list->NOBADGE . '] ' . $list->NAMA . ' ' . $list->JAB_TEXT . '</option>';
                                                            // echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="display:none;">
                                                <div class="form-group app-gm">
                                                    <label class="control-label">Disetujui oleh (General Manager) <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 approval" id='GM_LIST' style="width:100%;" tabindex="4" >
                                                        <?php
                                                        foreach ($LIST_GM as $list) {
                                                            echo '<option value="' . $list->NOBADGE . ' - ' . $list->NAMA . ' - ' . $list->EMAIL . ' - ' . $list->JAB_TEXT . '" >[' . $list->NOBADGE . '] ' . $list->NAMA . ' ' . $list->JAB_TEXT . '</option>';
                                                            // echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >[{$list->NOBADGE}] {$list->NAMA} {$list->JAB_TEXT}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                        echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url(); ?>js/dok_eng/doc_tender.js"></script>
