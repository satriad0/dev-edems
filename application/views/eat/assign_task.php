<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Assign Task</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Assign Task</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"><a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span><span class="hidden-xs-down">List <?= $tittle ?></span></a></li>
                    <li class="nav-item nw-form"><a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span><span class="hidden-xs-down">Create <?= $tittle ?></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_eat" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr id="tb_eat_tr_search">
                                        <td width="16"></td>
                                        <td></td>
                                        <th width="40px" style="text-align: center;" >tanggal</th>
                                        <th style="text-align: center;" >no. eat</th>
                                        <th style="text-align: center;" >no. notifikasi</th>
                                        <th style="text-align: center;" >no. erf</th>
                                        <th style="text-align: center;" >jenis</th>
                                        <th style="text-align: center;" >nama pekerjaan</th>
                                        <th style="text-align: center;" >status</th>
                                        <th style="text-align: center;" >uk peminta (p. section)</th>
                                        <th style="text-align: center;" >unit kerja lokasi pekerjaan</th>
                                        <!-- <th>COST CENTER</th> -->
                                        <th>company</th> 
                                        <td width="60px"></td>   
                                    </tr>
                                    <tr>
                                        <!-- <th>No</th> -->
                                        <th style="text-align: center;" >NO.</th>
                                        <th></th>
                                        <th style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO. EAT</th>
                                        <th style="text-align: center;" >NO NOTIFIKASI</th>
                                        <th style="text-align: center;" >NO ERF</th>
                                        <th style="text-align: center;" >JENIS</th>
                                        <th style="text-align: center;" >NAMA PEKERJAAN</th>
                                        <th style="text-align: center;" >STATUS</th>
                                        <!-- <th style="text-align: center;" >LEAD ASS. TECH.</th> -->
                                        <th style="text-align: center;" >UK PEMINTA (P. SECTION)</th>
                                        <th style="text-align: center;" >UNIT KERJA LOKASI PEKERJAAN</th>
                                        <th style="text-align: center;" >COMPANY</th>
                                        <!-- <th style="text-align: center;" >PEMBUAT</th> -->
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">
                        <?php
                        $lvl = 1;
                        $disSM = '';
                        $disMan = 'style="display:none;"';
                        if (isset($MGR_APP)) {
                            $disMan = '';
                            $disSM = 'style="display:none;"';
                            $lvl = 0;
                        }
                        ?>
                        <input type="hidden" class="form-control input-sm" name="LVL" id="LVL"
                               value="<?php echo $lvl; ?>" readonly=""/>

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-6">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">General Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6 col-no">
                                                <div class="form-group">
                                                    <label for="NO_PENUGASAN" class="control-label">No. EAT <span
                                                            class="text-danger" readonly required>*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NO_PENUGASAN"
                                                           id="NO_PENUGASAN" value="<?php echo "{$no}"; ?>" readonly
                                                           required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;">
                                                    <label for="ID" class="control-label">ID Penugasan</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID"
                                                           readonly=""/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_PENGAJUAN" class="control-label">ERF <span
                                                            class="text-danger">*</span></label>
                                                <!-- <select name="ID_PENGAJUAN" id="ID_PENGAJUAN" class="form-control form-control-sm select-sm" data-title="Pilih Erf..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="ID_PENGAJUAN" id="ID_PENGAJUAN"
                                                            class="chosen input-sm select2" style="width:100%;"
                                                            required>
                                                                <?php
                                                                foreach ($foreigns as $list) {
                                                                    echo "<option value='" . $list["ID_PENGAJUAN"] . "' title='" . $list["NO_PENGAJUAN"] . " - " . $list["NOTIF_NO"] . "' data-subtext='" . $list["NO_PENGAJUAN"] . "' data-no='" . $list["NO_PENGAJUAN"] . "' data-notif='" . $list["NOTIF_NO"] . "' data-tipe='" . $list["TIPE"] . "' data-flcode='" . $list["FUNCT_LOC"] . "' data-fltext='" . $list["FL_TEXT"] . "' data-shorttext='" . $list["SHORT_TEXT"] . "' data-uktext='" . $list["UK_TEXT"] . "' data-custreq='" . $list["CUST_REQ"] . "' data-lokasi='" . $list["LOKASI"] . "' data-const='" . $list["CONSTRUCT_COST"] . "' data-eng='" . $list["ENG_COST"] . "' >" . $list["SHORT_TEXT"] . "</option>";
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_ERF" class="control-label">No. ERF </label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_ERF"
                                                           id="NO_ERF" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="NOTIF_NO" class="control-label">No. Notifikasi </label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIF_NO"
                                                           id="NOTIF_NO" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="TIPE" class="control-label">Jenis Dokumen </label>
                                                    <input type="textarea" class="form-control input-sm" name="TIPE"
                                                           id="TIPE" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SHORT_TEXT" class="control-label">Nama Pekerjaan <span
                                                            class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm select-sm"
                                                           name="SHORT_TEXT" id="SHORT_TEXT"
                                                           placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="UK_TEXT" class="control-label">Unit Kerja Peminta</label>
                                                    <input type="textarea" class="form-control input-sm" name="UK_TEXT"
                                                           id="UK_TEXT" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="FL_TEXT" class="control-label">Unit Kerja Lokasi Pekerjaan</label>
                                                    <input type="textarea" class="form-control input-sm" name="FL_TEXT"
                                                           id="FL_TEXT" placeholder="Data tak ditemukan" readonly/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="LEAD_ASS" class="control-label" required>Lead of Tech.
                                                        Ass. <span class="text-danger">*</span></label>
                                                    <select name="LEAD_ASS" id="LEAD_ASS"
                                                            class="form-control form-control-sm select-sm"
                                                            data-title="Pilih Assistant..." data-show-subtext="true"
                                                            data-live-search="true" required>
                                                                <?php
                                                                echo "<option value >Please select lead...</option>";
                                                                foreach ($LEAD_ASS as $list) {
                                                                    $select = 'selected';
                                                                    $sJab = '';
                                                                    if (isset($list->GEN_PAR3)) {
                                                                        $sJab = " [{$list->GEN_PAR3}]";
                                                                    }
                                                                    if (count($LEAD_ASS) > 1) {
                                                                        $select = '';
                                                                    }
                                                                    echo "<option value='" . $list->GEN_DESC . $sJab . "' {$select} >" . $list->GEN_DESC . $sJab . "</option>";
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="unit" class="control-label">Target Date <span
                                                            class="text-danger">*</span></label>
                                                    <div class="input-daterange input-group">
                                                        <input type="text" class="form-control input-sm"
                                                               name="START_DATE" id="START_DATE" placeholder="dd/mm/yyyy" required>
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i
                                                                    class="ti-minus"></i></span>
                                                        </div>
                                                        <input type="text" class=" form-control input-sm"
                                                               name="END_DATE" id="END_DATE" placeholder="dd/mm/yyyy" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="OBJECTIVE" class="control-label">Tujuan yang Diharapkan
                                                        <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="OBJECTIVE"
                                                              id="OBJECTIVE" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;" required></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="CUST_REQ" class="control-label">Customer Request</label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="CUST_REQ"
                                                              id="CUST_REQ" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SCOPE_ENG" class="control-label">Engineering
                                                        Scope</label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="SCOPE_ENG"
                                                              id="SCOPE_ENG" placeholder="(max. 2000 karakter)"
                                                              maxlength="2000" style="resize:none;"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="DE_CONSTRUCT_COST" class="control-label">Est. Biaya
                                                        Pekerjaan <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm"
                                                           name="DE_CONSTRUCT_COST" id="DE_CONSTRUCT_COST"
                                                           maxlength="255" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="DE_ENG_COST" class="control-label">Est. Biaya
                                                        Engineering <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="DE_ENG_COST"
                                                           id="DE_ENG_COST" maxlength="255" required/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title"> Tim Design & Engineering</h5>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="KOMITE_PENGARAH" class="control-label">Komite Pengarah
                                                        <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm select-sm"
                                                           name="KOMITE_PENGARAH" id="KOMITE_PENGARAH"
                                                           value="<?php print_r($DEPT); //print_r($DEPT->JAB_TEXT);  ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-md-12">
                                                <div class="form-group">
                                                    <!-- <label for="TIM_PENGKAJI" class="control-label">Tim Pengkaji<br /></label> -->
                                                    <?php
                                                    $inputs = '<fieldset><label for="TIM_PENGKAJI" class="control-label">Tim Design & Engineering<br /></label>';
                                                    $inputs .= '<ul class="checkbox-columns">';

                                                    $row = 1;
                                                    foreach ($PENGKAJI as $subject) {
                                                        $select = 'checked';
                                                        if (count($PENGKAJI) > 1) {
                                                            $select = '';
                                                        }
                                                        // $inputs .= '<li><label><input type="checkbox" name="PENGKAJI[]" value="' . $subject['muk_nama'] . '"/>' . $subject['muk_nama'] . '</label></li>';
                                                        $inputs .= '<li><label><input type="checkbox" name="PENGKAJI[]" value="' . $subject->GEN_DESC . '" ' . $select . ' > ' . $subject->GEN_DESC . '</input></label></li>';
                                                    }

                                                    $inputs .= '</ul>';
                                                    $inputs .= '</fieldset>';

                                                    echo $inputs;
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="TW_KOM_PENGARAH" class="control-label">Tugas & Wewenang
                                                        Komite Pengarah</label>
                                                    <textarea class="form-control tarea-sm" rows="5"
                                                              name="TW_KOM_PENGARAH" id="TW_KOM_PENGARAH"
                                                              placeholder="(max. 2000 karakter)" maxlength="2000"
                                                              style="resize:none;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row"  style="display:none;" >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="TW_TIM_PENGKAJI" class="control-label">Tugas & Wewenang
                                                        TIM Pengkaji</label>
                                                    <textarea class="form-control tarea-sm" rows="5"
                                                              name="TW_TIM_PENGKAJI" id="TW_TIM_PENGKAJI"
                                                              placeholder="(max. 2000 karakter)" maxlength="2000"
                                                              style="resize:none;"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <div id="form-add-todo" class="form-add-todo">
                                                            <label>Nama Kajian/Paket Pekerjaan  <span class="text-danger">*</span></label>
                                                            <div class="form-group form_list_pekerjaan">
                                                                <div class="input-group">
                                                                    <input placeholder="(max. 255 karakter)" type="text"
                                                                           class="form-control input-sm new-todo-item-pekerjaan"
                                                                           id="new-todo-item-pekerjaan"
                                                                           name="pekerjaan[]" required>
                                                                    <span class="btn-group">
                                                                        <button type="button" class="btn btn-sm btn-info"
                                                                                id="add-todo-item-pekerjaan"> <i class="fa fa-plus"></i></button>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- <form id="form-todo-list"> -->
                                                    <ul id="todo-list-pekerjaan" class="todo-list"></ul>
                                                    <!-- </form> -->
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row" id="paket_pekerjaan_revisi">

                                        </div>

                                        <div class="row disMan" <?php echo $disMan; ?> >
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="MANAGER" class="control-label">Pilih Approval Manager
                                                        <span class="text-danger">*</span></label>
                                                    <!-- <select name="MANAGER" id="MANAGER" class="chosen input-sm select2" style="width:100%;" required></select> -->
                                                    <select class="chosen-select select2" name="MANAGER" id='MANAGER'
                                                            style="width:100%;" required>
                                                                <?php
                                                                foreach ($MANAGER as $list) {
                                                                    echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->JAB_TEXT}' >[ {$list->NOBADGE} ] {$list->NAMA} - {$list->JAB_TEXT}</option>";
                                                                }
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row disSM" <?php echo $disSM; ?>>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SM_APP" class="control-label">Pilih Approval SM <span
                                                            class="text-danger">*</span></label>
                                                    <select class="chosen-select select2" name="SM_APP" id='SM_APP'
                                                            style="width:100%;" required>
                                                                <?php
                                                                echo "<option value='{$BIRO->NOBADGE} - {$BIRO->NAMA} - {$BIRO->JAB_TEXT}' >[ {$BIRO->NOBADGE} ] {$BIRO->NAMA} - {$BIRO->JAB_TEXT}</option>";
                                                                ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES" class="control-label">Upload Schedule Pekerjaan(*.pdf) </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES" id="FILES"
                                                           accept="application/pdf" onchange="previewDocument(this)"/>
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri" rel="noopener noreferrer" target="_blank"><i
                                                            id="file_name"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES2" class="control-label">Upload Notulen Rapat(*.pdf) </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES2" id="FILES2"
                                                           accept="application/pdf" onchange="previewDocument(this)"/>
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri2" rel="noopener noreferrer" target="_blank"><i
                                                            id="file_name2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES3" class="control-label">Upload Data Pendukung Lainnya(*.pdf) </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES3" id="FILES3"
                                                           accept="application/pdf" onchange="previewDocument(this)"/>
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri3" rel="noopener noreferrer" target="_blank"><i
                                                            id="file_name3"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>



                                    </div>
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                                echo " {$short_tittle}-modify";
                                                                ?>" id="btn-save"><i class="fa fa-check"></i> Save
                                </button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- modal preview document -->
    <div class="modal fade" id="preview_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content" style="height: 700px">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Preview File</h5>
                </div>
                <div class="modal-body">
                    <div class="row" id="preview_body"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" id="preview_close">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit_to_do_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Nama Kajian/Paket Pekerjaan</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="container-fluid">
                            <div class="col-md-12">
                                <form>
                                    <div class="form-group">
                                        <label for="TO_DO_EDIT" class="control-label">Nama Kajian/Paket Pekerjaan</label>
                                        <input type="hidden" name="TO_DO_NUMBER" id="TO_DO_NUMBER">
                                        <input type="text" class="form-control" name="TO_DO_EDIT" id="TO_DO_EDIT" maxlength="255" style="width: 100%" required/>
                                    </div>
                                </form>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm btn-success" id="act_edit_to_do">Save Paket</button>
                    <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal" id="edit_to_do_close">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->

<script src="<?= base_url();?>js/eat/eat_assign.js"></script>
