
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <div class="cssload-loading" id="loading">
                  <i></i>
                  <i></i>
                  <i></i>
                </div> -->
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                  <!-- <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">Filing Documents</span></a> </li> -->
                  <!-- <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">New Engineering Documents</span></a> </li> -->
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_filing" class="table display table-bordered table-striped" style="width:100%;" >
                                <thead>
                                    <tr>
                                      <!-- <th>Form no</th> -->
                                        <th style="text-align: center;" >AREA</th>
                                        <!-- <th style="text-align: center;" >LOKASI</th> -->
                                        <!-- <th style="text-align: center;" >ERF DESC.</th> -->
                                        <th style="text-align: center;" >PACKET NAME</th>
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_DOK_ENG" class="control-label">No Dokumen <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" value="<?php echo "{$no}"; ?>" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID Dokumen</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_PENUGASAN" class="control-label">EAT <span class="text-danger">*</span></label>
                                                    <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($foreigns as $list) {
                                                            echo "<option value='" . $list['ID_PENUGASAN'] . "' title='" . $list['NO_PENUGASAN'] . ' - ' . $list['OBJECTIVE'] . "' data-subtext='" . $list['NO_PENGAJUAN'] . "' data-no='" . $list['NO_PENUGASAN'] . "' data-notif='" . $list['NOTIFIKASI'] . "' data-pengajuan='" . $list['PENGAJUAN'] . "' data-object='" . $list['OBJECTIVE'] . "' >" . $list['NO_PENUGASAN'] . " - " . $list['PENGAJUAN'] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_PENUGASAN" class="control-label">EAT No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_PENUGASAN" id="NO_PENUGASAN" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">Notifikasi No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PENGAJUAN" class="control-label">Deskripsi ERF.</label>
                                                    <input type="textarea" class="form-control input-sm" name="PENGAJUAN" id="PENGAJUAN" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="OBJECTIVE" class="control-label">Objective EAT</label>
                                                    <input type="textarea" class="form-control input-sm" name="OBJECTIVE" id="OBJECTIVE" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ID_PACKET" class="control-label">Pilih Paket <span class="text-danger">*</span></label>
                                                    <select name="ID_PACKET" id="ID_PACKET" class="form-control form-control-sm select-sm" data-title="Pilih Packet..." data-show-subtext="true" data-live-search="true" required></select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12" style="display:none;" >
                                                <div class="form-group">
                                                    <label for="PACKET_TEXT" class="control-label">Nama Paket <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="PACKET_TEXT" id="PACKET_TEXT" placeholder="(max. 255 karakter)" maxlength="255" style="resize:none;" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" style="display:none;" >
                                                <div class="form-group">
                                                    <label for="PROGRESS" class="control-label">Progress <span class="text-danger">*</span></label>
                                                    <input type="number" class="form-control input-sm" name="PROGRESS" id="PROGRESS" placeholder="(digits: 10,20,30,..)" min="0" max="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Approval Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Manager <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 paraf" id='PARAF_LIST' multiple style="width:100%;" tabindex="4">
                                                        <?php
                                                        foreach ($LIST_MNG as $list) {
                                                            echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >{$list->NOBADGE} - {$list->NAMA}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">SM Pengkaji <span class="text-danger">*</span></label>
                                                    <select class="chosen-select select2 approval" id='APP_LIST' multiple style="width:100%;" tabindex="4">
                                                        <?php
                                                        foreach ($LIST_APP as $list) {
                                                            echo "<option value='{$list->NOBADGE} - {$list->NAMA} - {$list->EMAIL} - {$list->JAB_TEXT}' >{$list->NOBADGE} - {$list->NAMA}</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Documents</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div id="capex">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="RKS_FILE" class="control-label"><u>Select RKS File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" name="RKS_FILE" id="RKS_FILE" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="rks_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="rks_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="BQ_FILE" class="control-label"><u>Select BQ File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" name="BQ_FILE" id="BQ_FILE" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="bq_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="bq_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="DRAW_FILE" class="control-label"><u>Select Drawing File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" name="DRAW_FILE" id="DRAW_FILE" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="draw_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="draw_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="ECE_FILE" class="control-label"><u>Select ECE File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" name="ECE_FILE" id="ECE_FILE" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="ece_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="ece_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="kajian">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="KAJIAN_FILE" class="control-label"><u>Select Kajian File </u></label>
                                                        <div class="col-md-12">
                                                            <input type="file" name="KAJIAN_FILE" id="KAJIAN_FILE" />
                                                        </div>
                                                        <div class="col-md-12">
                                                            <a id="kajian_uri" rel="noopener noreferrer" target="_blank"><i class="m-t-12" id="kajian_name"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-sm btn-success" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/eng/filing_eng.js"></script>
