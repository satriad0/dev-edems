
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Progress Engineering</span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">New Progress Engineering</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_progress" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                      <!-- <th>No</th> -->
                                        <th style="text-align: center;" >NO. EAT</th>
                                        <th style="text-align: center;" >Notification</th>
                                        <th style="text-align: center;" >NAMA PAKET PEKERJAAN</th>
                                        <th style="text-align: center;" >START DATE</th>
                                        <th style="text-align: center;" >END DATE</th>
                                        <th style="text-align: center;" >REMARKS</th>
                                        <th style="text-align: center;" >PROGRESS PEKERJAAN</th>
                                        <!-- <th>Status</th> -->
                                        <!-- <th style="text-align: center;" >CREATED BY</th> -->
                                        <!-- <th style="text-align: center;" >CREATED AT</th> -->
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-12">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">General Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                              <div class="form-group" style="display:none;" >
                                                <label for="NO_DOK_ENG" class="control-label">No Dokumen <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" required/>
                                              </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID Progress</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID_PENUGASAN" class="control-label">ID EAT</label>
                                                    <input type="text" class="form-control input-sm" name="ID_PENUGASAN" id="ID_PENUGASAN" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="ID_PENUGASAN" class="control-label">EAT <span class="text-danger">*</span></label>
                                              <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="form-control form-control-sm select-sm" data-title="Pilih Eat..." data-show-subtext="true" data-live-search="true" required>
                                        <?php
                                        // foreach ($FOREIGN as $list) {
                                        //     echo "<option value='" . $list["ID_PENGAJUAN"] . "' title='" . $list["NO_PENGAJUAN"] . " - " . $list["NOTIF_NO"] . "' $list-subtext='" . $list["NO_PENGAJUAN"] . "' $list-no='" . $list["NO_PENGAJUAN"] . "' $list-notif='" . $list["NOTIF_NO"] . "' $list-flcode='" . $list["FUNCT_LOC"] . "' $list-fltext='" . $list["FL_TEXT"] . "' $list-shorttext='" . $list["SHORT_TEXT"] . "' $list-uktext='" . $list["UK_TEXT"] . "' >" . $list["SHORT_TEXT"] . "</option>";
                                        // }
                                        ?>
                                              </select>
                                            </div>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NO_PENUGASAN" class="control-label">EAT No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NO_PENUGASAN" id="NO_PENUGASAN" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">Notifikasi No.</label>
                                                    <input type="textarea" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" style="display:none;">
                                                <div class="form-group">
                                                    <label for="OBJECTIVE" class="control-label">Objective EAT</label>
                                                    <input type="textarea" class="form-control input-sm" name="OBJECTIVE" id="OBJECTIVE" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="DESKRIPSI" class="control-label">Nama Paket Pekerjaan</label>
                                                    <input type="textarea" class="form-control input-sm" name="DESKRIPSI" id="DESKRIPSI" readonly />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" >
                                                <div class="form-group" >
                                                    <label for="unit" class="control-label">Update Date</label>
                                                    <input type="date" class="form-control input-sm" name="UPDATE_DATE" id="UPDATE_DATE" >
                                                </div>
                                            </div>
                                            <div class="col-md-6" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="unit" class="control-label">Target Date</label>
                                                    <div class="input-daterange input-group" >
                                                        <input type="date" class="form-control input-sm" name="START_DATE" id="START_DATE" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-minus"></i></span>
                                                        </div>
                                                        <input type="date" class=" form-control input-sm" name="END_DATE" id="END_DATE" >
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <li class="nav-item dropdown mega-dropdown pull-right" style="position:absolute;"> <a class="nav-link dropdown-toggle text-muted  " style="margin-left:21%; padding-top:inherit;" title="Panduan Teknis Pengisian Progress" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-comment"></i></a>
                                                        <div class="dropdown-menu animated zoomIn">
                                                            <ul class="mega-dropdown-menu row">
                                                                <li class="guide-kaj col-lg-12 col-xlg-12 m-b-30" >
                                                                    <h6 class="text-info m-b-20">Petunjuk pengisian progress engineering (<code>Kajian Teknis</code>)</h6>
                                                                    <hr>
                                                                    <!-- <h2 style="text-align: center;margin-bottom: 30px">Data Buku Dengan CodeIgniter & DataTables</h2> -->
                                                                    <div class="table-responsive m-t-25">
                                                                        <table id="table_id" class="compact dataTable row-border" cellspacing="0" width:"100%">
                                                                            <thead>
                                                                                <tr class="alert alert-success" >
                                                                                    <th style="text-align:right;"><small>No</small></th>
                                                                                    <th><small>Status</small></th>
                                                                                    <th style="text-align:right;"><small>Bobot</small></th>
                                                                                    <th style="text-align:left;"><small>Deskripsi</small></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $no = 1;
                                                                                foreach ($guide_kajian as $data) {
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td class="text-primary" style="text-align:right;"><small><?php echo $no++; ?></small></td>
                                                                                        <td class="text-warning" ><small><?php echo $data->GEN_PAR3; ?></small></td>
                                                                                        <td style="text-align:right;"><code><?php echo $data->GEN_PAR2; ?> %</code></td>
                                                                                        <td class="text-primary" style="text-align:left;"><small><?php echo $data->GEN_DESC; ?></small></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </li>
                                                                <li class="guide-ded col-lg-12 col-xlg-12 m-b-30" >
                                                                    <h6 class="text-info m-b-20">Petunjuk pengisian progress engineering (<code>Detaill Design</code>)</h6>
                                                                    <hr>
                                                                    <!-- <h2 style="text-align: center;margin-bottom: 30px">Data Buku Dengan CodeIgniter & DataTables</h2> -->
                                                                    <div class="table-responsive m-t-25" style="max-height:380px;">
                                                                        <table id="table_id" class="compact dataTable row-border" cellspacing="0" width:"100%">
                                                                            <thead>
                                                                                <tr class="alert alert-success" >
                                                                                    <th style="text-align:right;"><small>No</small></th>
                                                                                    <th><small>Status</small></th>
                                                                                    <th style="text-align:right;"><small>Bobot</small></th>
                                                                                    <th style="text-align:left;"><small>Deskripsi</small></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <?php
                                                                                $no = 1;
                                                                                foreach ($guide_detail as $data) {
                                                                                    ?>
                                                                                    <tr>
                                                                                        <td class="text-primary" style="text-align:right;"><small><?php echo $no++; ?></small></td>
                                                                                        <td class="text-warning" ><small><?php echo $data->GEN_PAR3; ?></small></td>
                                                                                        <td style="text-align:right;"><code><?php echo $data->GEN_PAR2; ?> %</code></td>
                                                                                        <td class="text-primary" style="text-align:left;"><small><?php echo $data->GEN_DESC; ?></small></td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <label for="PROGRESS" class="control-label">Progress Pekerjaan <span class="text-danger">*</span> </label>
                                                    <input type="number" class="form-control input-sm" name="PROGRESS" id="PROGRESS" placeholder="(digits: 26,50,..%) Max. 94%"  step="any" min="26" max="94" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="REMARKS" class="control-label">Remarks <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="REMARKS" id="REMARKS" placeholder="(max. 2000 karakter)" maxlength="255" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="tb_log" class="control-label">History </label>
                                                    <div class="table-responsive">
                                                        <table id="tb_log" class="table display compact table-bordered" style="width:100%;">
                                                            <thead>
                                                                <tr>
                                                                    <th style="text-align: center;" >No</th>
                                                                    <th style="text-align: center;" >Progress</th>
                                                                    <th style="text-align: center;" >Last Update</th>
                                                                    <th style="text-align: center;" >Remarks</th>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label for="REMARKS" class="control-label">Optional Files (*.pdf) </label>
                                                        <input class="multifile m-b-15" type="file" name="FILES[]" id="FILES" style="width:100%;" accept="application/pdf" />
                                                    </div>
                                                </div>
                                                <div class="form-group file">
                                                    <div class="col-md-12">
                                                        <ul id="filelist" class="todo-list"></ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="REMARKS" class="control-label">Optional Files (*.pdf) </label>
                                              <input class="multifile m-b-15" type="file" name="FILES[]" id="FILES" style="width:100%;" accept="application/pdf" />
                                            </div>
                                          </div>
                                          <div class="form-group file">
                                            <div class="col-md-12">
                                              <ul id="filelist" class="todo-list"></ul>
                                            </div>
                                          </div>
                                        </div> -->

                                    </div>

                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                                                echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======plugin Jquery====== -->
<script src="<?= base_url();?>assets/js/lib/jquery-multiple-file/jquery.multifile.js"></script>
<script src="<?= base_url();?>assets/js/lib/jquery-multiple-file/jquery.multifile.preview.js"></script>
<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/eng/en_progress.js"></script>
