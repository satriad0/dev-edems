<style>
    /* The Modal (background) */

    #fade {
        display: none;
        position:absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: #ababab;
        z-index: 9999;
        -moz-opacity: 0.8;
        opacity: .70;
        filter: alpha(opacity=80);
    }

    #modal {
        display: none;
        position: absolute;
        top: 45%;
        left: 45%;
        width: 160px;
        height: 160px;
        padding:30px 15px 0px;
        border: 3px solid #ababab;
        box-shadow:1px 1px 10px #ababab;
        border-radius:20px;
        background-color: white;
        z-index: 10000;
        text-align:center;
        overflow: auto;
        overflow-y:auto;
    }
    .bootstrap-select.show>.dropdown-menu>.dropdown-menu {
        display: block;
    }

    .bootstrap-select > .dropdown-menu > .dropdown-menu li.hidden{
        display:none;
    }

    .bootstrap-select > .dropdown-menu > .dropdown-menu li a{
        display: block;
        width: 100%;
        padding: 3px 1.5rem;
        clear: both;
        font-weight: 400;
        color: #292b2c;
        text-align: inherit;
        white-space: nowrap;
        background: 0 0;
        border: 0;
    }

    .modal-dialog{
        width: 1000px;
    }
</style>

<!-- Page wrapper  -->
<div class="page-wrapper" id="page_in">
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List <?= $tittle ?></span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Create <?= $tittle ?></span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">

                        <div class="table-responsive m-t-25">
                            <table id="tb_erf2" name="tb_erf2" class="table display table-bordered table-striped" style="width:100%;" >
                                <thead>
                                    <tr id="tb_erf2_tr_search">
                                        <td width="16"></td>
                                        <th width="40px" style="text-align: center;" ids="c1">tanggal</th>
                                        <th style="text-align: center;" ids="c1">no. erf</th>
                                        <th style="text-align: center;" ids="c1">no. notifikasi</th>
                                        <th style="text-align: center;" ids="c1">nama pekerjaan</th>
                                        <th style="text-align: center;" ids="c1">prioritas</th>
                                        <th style="text-align: center;" ids="c1">status</th>
                                        <th style="text-align: center;" ids="c1">uk peminta (p. section)</th>
                                        <th style="text-align: center;" ids="c1">uk peminta (fungsional lokasi)</th>
                                        <th style="text-align: center;" ids="c1">company</th> 
                                        <!-- <th>COST CENTER</th> -->
                                        <th ids="c1">pembuat</th> 
                                        <td width="60px"></td>   
                                    </tr>
                                    <tr>
                                        <th width="16"></th>
                                        <th width="40px" style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NO. ERF</th>
                                        <th style="text-align: center;" >NO. NOTIFIKASI</th>
                                        <th style="text-align: center;" >NAMA PEKERJAAN</th>
                                        <th style="text-align: center;" >PRIORITAS</th>
                                        <th style="text-align: center;" >STATUS</th>
                                        <th style="text-align: center;" >UK PEMINTA (P. SECTION)</th>
                                        <th style="text-align: center;" >UK PEMINTA (FUNGSIONAL LOKASI)</th>
                                        <th style="text-align: center;" >COMPANY</th>
                                        <!-- <th>COST CENTER</th> -->
                                        <th>PEMBUAT</th>
                                        <th width="60px">OPTIONS</th>
                                    </tr>

                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">
                        <input type="textarea" class="form-control input-sm select-sm" name="STATUS" id="STATUS" placeholder="Data tak ditemukan" style="display:none;" readonly />
                        <input type="textarea" class="form-control input-sm select-sm" name="DEF_PLANT" id="DEF_PLANT" placeholder="Data tak ditemukan" style="display:none;" value="<?php echo "{$kpgroup->GEN_PAR1} - {$kpgroup->GEN_PAR7}"; ?>" readonly />
                        <input type="textarea" class="form-control input-sm select-sm" name="ENG_FEE" id="ENG_FEE" placeholder="Data tak ditemukan" style="display:none;" value="<?php echo "{$eng_fee->GEN_PAR1}"; ?>" readonly />
                        <select name="DEF_PGROUP" id="DEF_PGROUP" class="form-control form-control-sm select-sm" placeholder="Pilih planner group.." style="display:none;" readonly required>
                            <?php
                            echo "<option value='" . $kpgroup->GEN_CODE . "' title='" . $kpgroup->GEN_CODE . " - " . $kpgroup->GEN_DESC . "' selected >" . $kpgroup->GEN_CODE . " - " . $kpgroup->GEN_DESC . "</option>";
                            ?>
                        </select>

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">General Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="KDLOC" class="control-label">Nama Lokasi <span class="text-danger">*</span></label>
                                                    <!-- <select name="KDLOC" id="KDLOC" class="form-control form-control-sm select-sm" data-title="Pilih Erf..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="KDLOC" id="KDLOC" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($locations as $list) {
                                                            echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="KDAREA" class="control-label">Nama Area<span class="text-danger">*</span></label>
                                                    <!-- <div class="col-md-12 col-lg-12"> -->
                                                    <select name="KDAREA" id="KDAREA" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($areas as $list) {
                                                            echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' >" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="KDAREA" class="control-label">Bidang Disiplin<span class="text-danger">*</span></label>
                                                    <!-- <div class="col-md-12 col-lg-12"> -->
                                                    <select name="KDDIS" id="KDDIS" class="chosen input-sm select2 disabled" style="width:100%;" disabled required >
                                                        <?php
                                                        foreach ($disiplin as $list) {
                                                            echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_PAR1 . "' data-tipe='{$list->GEN_PAR2}' data-par1='{$list->GEN_PAR3}' data-par2='{$list->GEN_PAR4}'  >" . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                    <!-- </div> -->
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="IDMPE" class="control-label">ID MPE</label>
                                                    <input type="text" class="form-control input-sm" name="IDMPE" id="IDMPE" readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display:none;" >
                                                <div class="form-group" >
                                                    <!-- <div class="form-group" style="display:none;" > -->
                                                    <label for="REV_NO" class="control-label">Rev.</label>
                                                    <input type="text" class="form-control input-sm" name="REV_NO" id="REV_NO" readonly="" value="0"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;" id="col-pengajuan" >
                                            <div class="col-md-6" >
                                                <div class="form-group">
                                                    <label for="NOPENGAJUAN" class="control-label">No ERF. <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm"  name="NOPENGAJUAN" id="NOPENGAJUAN" value="<?php echo "{$no}"; ?>" readonly="" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">No. Notifikasi</label>
                                                    <input type="text" class="form-control input-sm notif-el" name="NOTIFIKASI" id="NOTIFIKASI" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SHORT_TEXT" class="control-label">Nama Pekerjaan <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm notif-el" name="SHORT_TEXT" id="SHORT_TEXT" placeholder="(max. 255 karakter)" maxlength="255" required/>
                                                </div>
                                            </div>
                                            <div class="col-md-6 ">
                                                <div class="form-group">
                                                    <label for="SHORT_TEXT" class="control-label">Tipe ERF<span class="text-danger">*</span></label>
                                                    <select name="JENIS" id="JENIS" class="form-control form-control-sm select-sm select2" data-title="Erf Type..." style="width:100%;" required>
                                                        <option value="Detail Design">Detail Design Engineering</option>
                                                        <option value="Kajian Teknis">Kajian Engineering</option>

                                                        <?php
                                                        $info = $this->session->userdata;
                                                        $info = $info['logged_in'];
                                                        if ($info['uk_kode'] == '50032326' or $info['dept_code'] == '50032326') {
                                                            ?>
                                                            <option value="Konsultan Engineering">Konsultan Engineering</option>
                                                            <option value="Pembelian Barang">Pembelian Barang</option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PRIORITY" class="control-label">Prioritas</label>
                                                    <select name="PRIORITY" id="PRIORITY" class="form-control form-control-sm select-sm select2 disabled" style="width:100%;" disabled >
                                                        <option value="3" selected>MEDIUM</option>
                                                        <option value="1">EMERGENCY</option>
                                                        <option value="2">HIGH</option>
                                                        <option value="4">LOW</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="FUNCT_LOC" class="control-label">Unit Kerja Peminta (Fungsional Lokasi) <span class="text-danger">*</span></label>
                                                    <!-- <select name="FUNCT_LOC" id="FUNCT_LOC" class="form-control select-sm selectpicker" data-title="Pilih functional location..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="FUNCT_LOC" id="FUNCT_LOC" class="chosen input-sm select2 notif-el" style="width:100%;" required>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="FL_CODE" class="control-label">FL CODE</label>
                                                    <input type="text" class="form-control input-sm" name="FL_CODE" id="FL_CODE" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="SWERK" class="control-label">Lokasi Plant Unit Kerja Peminta <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="SWERK" id="SWERK" placeholder="Data tak ditemukan" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="BEBER" class="control-label">Unit Kerja Peminta (Plant Section) <span class="text-danger">*</span></label>
                                                    <select name="BEBER" id="BEBER" class="form-control form-control-sm select-sm select2 notif-el" style="width:100%;" placeholder="Pilih section.." required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PLANPLANT" class="control-label">Lokasi Plant Unit Kerja Tujuan <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm select-sm" name="PLANPLANT" id="PLANPLANT" placeholder="Data tak ditemukan" value="<?php echo "{$pgroup->GEN_PAR1} - {$pgroup->GEN_PAR7}"; ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PLANGROUP" class="control-label">Unit Kerja Tujuan <span class="text-danger">*</span></label>
                                                    <select name="PLANGROUP" id="PLANGROUP" class="form-control form-control-sm select-sm select2" style="width:100%;" placeholder="Pilih planner group.." required>
                                                        <?php
                                                        // echo $pgroup->GEN_PAR1;
                                                        foreach ($apgroup as $list) {
                                                            $id_dis = ($list->GEN_CODE == "499" ? "id499" : "other");
                                                            // echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "</option>";
                                                            echo "<option id='" . $id_dis . "' value='" . $list->GEN_CODE . "' title='" . $list->GEN_CODE . " - " . $list->GEN_DESC . "' data-kode='{$list->STATUS}' >" . $list->GEN_CODE . " - " . $list->GEN_DESC . "</option>";
                                                        }
                                                        // echo "<option value='" . $pgroup->GEN_CODE . "' title='" . $pgroup->GEN_CODE . " - " . $pgroup->GEN_DESC . "' selected >" . $pgroup->GEN_CODE . " - " . $pgroup->GEN_DESC . "</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ASP0" class="control-label">Aspek Lingkungan #1<span class="text-danger">*</span></label>
                                                    <select name="ASP0" id="ASP0" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($aspect0 as $list) {
                                                            echo "<option value='" . $list->GEN_PAR1 . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ASP1" class="control-label">Aspek Lingkungan #2<span class="text-danger">*</span></label>
                                                    <select name="ASP1" id="ASP1" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($aspect1 as $list) {
                                                            echo "<option value='" . $list->GEN_PAR1 . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ASP2" class="control-label">Aspek Lingkungan #3<span class="text-danger">*</span></label>
                                                    <select name="ASP2" id="ASP2" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($aspect2 as $list) {
                                                            echo "<option value='" . $list->GEN_PAR1 . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ASP3" class="control-label">Aspek Lingkungan #4<span class="text-danger">*</span></label>
                                                    <select name="ASP3" id="ASP3" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($aspect3 as $list) {
                                                            echo "<option value='" . $list->GEN_PAR1 . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_PAR1 . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6" style="display:none;" >
                                                <div class="form-group">
                                                    <label for="LOKASI" class="control-label">Detail Lokasi Pekerjaan</label>
                                                    <input type="text" class="form-control input-sm" name="LOKASI" id="LOKASI" placeholder="(max. 255 karakter)" maxlength="255" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ASP_OTHER" class="control-label">Aspek Lingkungan Lainnya</label>
                                                    <input type="text" class="form-control input-sm" name="ASP_OTHER" id="ASP_OTHER" placeholder="(max. 255 karakter)" maxlength="255" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row wbs-view">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="WBS_CODE" class="control-label">Kode Proyek</label>
                                                    <input type="text" class="form-control input-sm" name="WBS_CODE" id="WBS_CODE" placeholder="(max. 255 karakter)" maxlength="255" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="WBS_TEXT" class="control-label">Nama Proyek</label>
                                                    <input type="text" class="form-control input-sm" name="WBS_TEXT" id="WBS_TEXT" placeholder="(max. 255 karakter)" maxlength="255" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="unit" class="control-label">Tanggal Rencana (Permintaan-Penyelesaian)</label>
                                                    <div class="input-daterange input-group" >
                                                        <input type="text" class="form-control input-sm notif-el" name="DESSTDATE" id="DESSTDATE" placeholder="dd/mm/yyyy" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-minus"></i></span>
                                                        </div>
                                                        <input type="text" class=" form-control input-sm notif-el" name="DESENDDATE" id="DESENDDATE" placeholder="dd/mm/yyyy" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title"> Spesifikasi</h5>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 wbs-view">
                                                <div class="form-group">
                                                    <label for="CONSTRUCT_COST" class="control-label">Est. Biaya Pekerjaan </label>
                                                    <input type="text" class="form-control input-sm money" name="CONSTRUCT_COST" id="CONSTRUCT_COST" maxlength="255" />
                                                    <!-- <input type="number" value="1000" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="currency" id="CONSTRUCT_COST" /> -->
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ENG_COST" class="control-label">Est. Biaya Engineering <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm money" name="ENG_COST" id="ENG_COST" maxlength="255" required />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Pilih Approval (Senior Manager/ General Manager)  <span class="text-danger">*</span></label>
                                                    <select name="APP_LIST" id="APP_LIST" class="chosen input-sm select2" style="width:100%;" required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="LATAR_BELAKANG" class="control-label">Latar Belakang & Permasalahan <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="LATAR_BELAKANG" id="LATAR_BELAKANG" placeholder="(max. 2000 char)" maxlength="2000" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="CUST_REQ" class="control-label">Permintaan yang diharapkan <span class="text-danger">*</span></label>
                                                    <div class="opt-view">
                                                        <?php
                                                        $inputs = '<fieldset class=""><label for="TIM_PENGKAJI" class="control-label"></label>';
                                                        $inputs .= '<ul class="checkbox-columns">';

                                                        $row = 1;
                                                        foreach ($opt_cust as $subject) {
                                                            $select = 'checked';
                                                            if (count($opt_cust) > 1) {
                                                                $select = '';
                                                            }
                                                            // $inputs .= '<li><label><input type="checkbox" name="PENGKAJI[]" value="' . $subject['muk_nama'] . '"/>' . $subject['muk_nama'] . '</label></li>';
                                                            $inputs .= '<li><label><input type="checkbox" name="OPT_CUST" value="' . $subject->GEN_DESC . '" ' . $select . ' > ' . $subject->GEN_DESC . '</input></label></li>';
                                                        }

                                                        $inputs .= '</ul>';
                                                        $inputs .= '</fieldset>';

                                                        echo $inputs;
                                                        ?>
                                                        <input type="checkbox" class="opt-lain" value="Lain-lain" > Lain-lain</input>
                                                    </div>
                                                    <textarea class="form-control tarea-sm" rows="5" name="CUST_REQ" id="CUST_REQ" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="TECH_INFO" class="control-label">Informasi Teknis <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="TECH_INFO" id="TECH_INFO" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12"  style="display:none;">
                                                <div class="form-group">
                                                    <label for="RES_MIT" class="control-label">Resiko & Mitigasi <span class="text-danger">*</span></label>
                                                    <textarea class="form-control tarea-sm" rows="5" name="RES_MIT" id="RES_MIT" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" ></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <!--                                    <div class="col-md-1">
                                                                            <div class="col-md-12">
                                                                                <h5 class="card-title"> Preview</h5>
                                                                                <hr>
                                                                            </div>
                                                                            <div class="col-md-6" style="margin-right: 0px">
                                                                                <div class="form-group">
                                                                                    <div class="col-md-12">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <a class="btnPrevFiles1 btn btn-xs btn-info" title="Preview"><i class="fa fa-eye" style="color: white"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group file">
                                                                                    <div class="col-md-12">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="col-md-12">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <a class="btnPrevFiles2 btn btn-xs btn-info" title="Preview"><i class="fa fa-eye" style="color: white"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group file">
                                                                                    <div class="col-md-12">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-6">
                                                                                <div class="form-group">
                                                                                    <div class="col-md-12">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                    <div class="col-md-12">
                                                                                        <a class="btnPrevFiles3 btn btn-xs btn-info" title="Preview"><i class="fa fa-eye" style="color: white"></i></a>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="form-group file">
                                                                                    <div class="col-md-12">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>-->

                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <h5 class="card-title"> Lampiran</h5>
                                            <hr>
                                        </div>
                                        <div class="col-md-6" style="margin-right: 0px">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES" class="control-label">Upload Feasibility Study (FS) </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES" id="FILES" onchange="previewDocument(this)" />
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri" rel="noopener noreferrer" target="_blank"><i id="file_name"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES2" class="control-label">Upload Notulen Rapat </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES2" id="FILES2" onchange="previewDocument(this)" />
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri2" rel="noopener noreferrer" target="_blank"><i id="file_name2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="FILES3" class="control-label">Upload Data Pendukung Lainnya </label>
                                                </div>
                                                <div class="col-md-12">
                                                    <input type="file" name="FILES3" id="FILES3" onchange="previewDocument(this)" />
                                                </div>
                                            </div>
                                            <div class="form-group file">
                                                <div class="col-md-12">
                                                    <a id="file_uri3" rel="noopener noreferrer" target="_blank"><i id="file_name3"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php
                                echo "{$short_tittle}-write";
                                echo " {$short_tittle}-modify";
                                ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->


        <!-- ================================= MODAL ERF ================================= -->
        <div id="reject_modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" >
            <div class="modal-dialog modal-lg modal-md modal-sm">
                <div class="modal-content">
                    <form id="fm-reject" >
                        <div class="form-body">
                            <div class="modal-header">
                                <center>
                                    <h4 class="modal-title title_ukur">File Reject</h4>
                                    <small class="font-bold"></small>
                                </center>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12" style="display:none;" >
                                                        <div class="form-group" >
                                                            <label for="tmpID" class="control-label">ID MPE</label>
                                                            <input type="text" class="form-control input-sm" name="tmpID" id="tmpID" readonly="" />
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <label for="REJECT_FILE" class="control-label"><span class="text-danger">*</span> Please upload file.. (*.pdf) </label>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <input type="file" class="form-control" name="REJECT_FILE" id="REJECT_FILE" accept="application/pdf" required />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary btn-canc-reject" data-dismiss="modal" >Cancel</button>
                                <button type="submit" class="btn btn-primary btn-conf-reject">Reject</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <!-- =============================== END MODAL ERF =============================== -->

        </div>

        <div class="modal fade" id="preview_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="height: 700px">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Preview File</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row" id="preview_body">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="preview_close">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade showing" id="preview_modal_progres_req" tabindex="-1" role="dialog" aria-hidden="true" >
            <div class="modal-dialog modal-lg" role="progres_req" >
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"  style="font-weight:bold; text-align:center;">Detail Progres Engineering Request</h5>
                    </div>
                    <div class="modal-body" >
                        <center><h6 id="title_mod_engreq">Progress Engineering</h6></center>
                        <hr>
                        <div id="prog_request_bd">


                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-hidden="true" id="modal_progres_req_close">Close</button>
                    </div>
                </div> 
            </div>
        </div>


        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- ======default Jquery====== -->
<!-- All Jquery -->
<!-- <script src="assets/js/lib/jquery/jquery.min.js"></script> -->
<!-- Bootstrap tether Core JavaScript -->
<!-- <script src="assets/js/lib/bootstrap/js/popper.min.js"></script> -->
<!-- <script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script> -->
<!-- slimscrollbar scrollbar JavaScript -->
<!-- <script src="assets/js/jquery.slimscroll.js"></script> -->
<!--Menu sidebar -->
<!-- <script src="assets/js/sidebarmenu.js"></script> -->
<!--stickey kit -->
<!-- <script src="assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script> -->
<!--Custom JavaScript -->
<!-- <script src="assets/js/custom.min.js"></script> -->
<!-- ====end default Jquery==== 

<script src="https://code.jquery.com/jquery-3.5.1.js"></script> 
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script> -->
<script src="<?= base_url();?>js/erf/erf_request.js"></script>
