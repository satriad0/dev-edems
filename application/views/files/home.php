<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/basic.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/dropzone.min.css">
<style>
    /* The Modal (background) */
    #fade {
        display: none;
        position:absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: #ababab;
        z-index: 9999;
        -moz-opacity: 0.8;
        opacity: .70;
        filter: alpha(opacity=80);
    }

    #modal {
        display: none;
        position: absolute;
        top: 45%;
        left: 45%;
        width: 160px;
        height: 160px;
        padding:30px 15px 0px;
        border: 3px solid #ababab;
        box-shadow:1px 1px 10px #ababab;
        border-radius:20px;
        background-color: white;
        z-index: 10000;
        text-align:center;
        overflow: auto;
        overflow-y:auto;
    }
    .bootstrap-select.show>.dropdown-menu>.dropdown-menu {
        display: block;
    }

    .bootstrap-select > .dropdown-menu > .dropdown-menu li.hidden{
        display:none;
    }

    .bootstrap-select > .dropdown-menu > .dropdown-menu li a{
        display: block;
        width: 100%;
        padding: 3px 1.5rem;
        clear: both;
        font-weight: 400;
        color: #292b2c;
        text-align: inherit;
        white-space: nowrap;
        background: 0 0;
        border: 0;
    }
    .dropzone {
        margin-top: 10px;
        border: 2px dashed #0087F7;
    }
</style>

<!-- Page wrapper  -->
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List <?= $tittle ?></span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Create <?= $tittle ?></span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <div class="table-responsive m-t-25">
                            <table id="tb_native" name="tb_native" class="table display table-bordered table-striped" style="width:100%;" >
                                <thead>
                                    <tr id="tb_nativ_tr_search">
                                        <td width="16"></td>
                                        <th width="40px" style="text-align: center;" ids="c1">TANGGAL</th>
                                        <th style="text-align: center;" ids="c1">NAMA FOLDER</th>
                                        <th style="text-align: center;" ids="c1">NAMA PEKERJAAN</th>
                                        <th style="text-align: center;" ids="c1">TIPE KONTEN</th>
                                        <th style="text-align: center;" ids="c1">COMPANY</th>
                                        <th ids="c1">PEMBUAT</th>
                                        <td width="60px"></td>
                                    </tr>
                                    <tr>
                                        <td width="16"></td>
                                        <th width="40px" style="text-align: center;" >TANGGAL</th>
                                        <th style="text-align: center;" >NAMA FOLDER</th>
                                        <th style="text-align: center;" >NAMA PEKERJAAN</th>
                                        <th style="text-align: center;" >TIPE KONTEN</th>
                                        <th style="text-align: center;" >COMPANY</th>
                                        <th>PEMBUAT</th>
                                        <th width="60px">OPTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tab-pane p-20" id="new-form" role="tabpanel">
                        <form id="fm-new" enctype="multipart/form-data">
                            <div class="form-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">General Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="TYPE_FILE" class="control-label">Tipe Pekerjaan<span class="text-danger">*</span></label>
                                                    <input type="hidden" class="form-control input-sm notif-el" name="ID_NATIVE_DOC" id="ID_NATIVE_DOC"/>
                                                    <select name="TYPE_FILE" id="TYPE_FILE" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($type as $list) {
                                                            echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_DESC . " - " . $list->GEN_CODE . "' >" . $list->GEN_DESC . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="COMPANY" class="control-label">Company<span class="text-danger">*</span></label>
                                                    <select name="COMPANY" id="COMPANY" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($comp as $list) {
                                                            echo "<option value='" . $list->GEN_PAR1 . "-" . $list->GEN_DESC . "' >" . $list->GEN_PAR1 . " - " . $list->GEN_DESC . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-lg-4">
                                                <div class="form-group">
                                                    <label for="unit" class="control-label">Tanggal Dokumen<span class="text-danger">*</span></label>
                                                    <div class="input-daterange input-group" >
                                                        <input type="date" class="form-control input-sm notif-el" name="DOCDATE" id="DOCDATE" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="FOLDER_NAME" class="control-label">Nama Folder <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm notif-el" name="FOLDER_NAME" id="FOLDER_NAME" placeholder="(max. 255 karakter)" maxlength="255" required/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="PEKERJAAN" class="control-label">Nama Pekerjaan <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm notif-el" name="PEKERJAAN" id="PEKERJAAN" placeholder="(max. 255 karakter)" maxlength="255" required/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Files Section</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div>
                                            <label for="UPLOADFILE" class="control-label">Anda bisa mengupload beberapa file sekaligus (maksimal 100MB per file)</label>
                                            <div class="dropzone" name="UPLOADFILE">
                                                <div class="dz-message">
                                                    <h3> Klik atau Drop file anda disini</h3>
                                                </div>
                                            </div>
                                            <hr/>
                                            <ul id="list-native-docs" class="todo-list"></ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php
                                echo "{$short_tittle}-write";
                                echo " {$short_tittle}-modify";
                                ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<script src="<?= base_url();?>assets/js/dropzone.min.js"></script>
<script src="<?= base_url();?>js/files/native_files.js"></script>