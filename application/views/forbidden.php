
        <!-- Page wrapper  -->
        <div class="page-wrapper">
            <!-- Bread crumb -->
            <!-- <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Engineering Documents</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Engineering Documents</li>
                    </ol>
                </div>
            </div> -->
            <!-- End Bread crumb -->
            <!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content --><div class="card">
                  <div class="card-body">
                    <div class="middle-box text-center animated fadeInDown">
                        <h1>403</h1>
                        <h3 class="font-bold">Forbidden</h3>

                        <div class="error-desc">
                            Sorry, you don't have permission to access this page. Please contact administrator to view more..
                        </div>
                    </div>
                  </div>
                </div>
                <!-- End PAge Content -->
            </div>
            <!-- End Container fluid  -->
            <!-- footer -->
            <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI). Template designed by <a href="https://colorlib.com">Colorlib</a></footer>
            <!-- End footer -->
        </div>
        <!-- End Page wrapper  -->
    </div>
    <!-- End Wrapper -->

    <!-- ======default Jquery====== -->
    <script src="js/others/users.js"></script>
