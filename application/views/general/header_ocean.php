<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// $this->load->helper('url');
// $autoload['libraries'] = array('database', 'session');
// $session = $this->session->userdata('ses_log_id');
// $session = $this->session->userdata();
// $session = $this->session->userdata('logged_in');
// $info = (array)$session;
// if(empty($info['username'])){
//  redirect('login');
// }
// echo "<pre>";
// print_r((array)($session));
// echo "</pre>";
$info = $this->session->userdata;
$info = $info['logged_in'];
?>
<html lang="en">

    <head>
        <base href="<?php echo base_url(); ?>" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="media/png" sizes="16x14" href="<?= base_url();?>media/CoE.png">
        <title>e-DEMS | <?php echo $tittle; ?></title>

        <link href="<?= base_url();?>assets/css/lib/chartist/chartist.min.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/lib/owl.carousel.min.css" rel="stylesheet" />
        <link href="<?= base_url();?>assets/css/lib/owl.theme.default.min.css" rel="stylesheet" />
        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url();?>assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
        <!-- <link href="<?= base_url();?>assets/css/lib/bootstrap/bootstrap3.min.css" rel="stylesheet"> -->
        <!-- Custom CSS -->
        <link href="<?= base_url();?>assets/css/helper.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">
        <!-- selectpicker -->
        <link href="<?= base_url();?>assets/css/others/bootstrap-select/ajax-bootstrap-select.min.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/bootstrap-select/bootstrap-select.css" rel="stylesheet">
        <!-- Add on -->
        <link href="<?= base_url();?>assets/css/others/sweetalert/sweetalert.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/ladda/ladda-themeless.min.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/chosen/chosen.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/select2/select2.min.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/select2/select2-bootstrap.min.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/switch/switch.css" rel="stylesheet">
        <link href="<?= base_url();?>assets/css/others/vmc-simple-slide/vmvc.min.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/datatables/datatables.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/datatables/DataTables-1.10.18/css/fixedColumns.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/progress.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/cssload-loading.css"> -->
        <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/css-load-gear.css">
        <!-- <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/css-gear.Sass"> -->
        <link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/vertical_center.css">




        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
        <!--[if lt IE 9]>
        <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    </head>

    <body class="fix-sidebar fix-header ">
        <!-- Preloader - style you can find in spinners.css -->
        <div class="preloader" id="ReloadData">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <div style="display:none;" >
            <input type="text" class="form-control input-sm" name="page_short_title" id="page_short_title" value="<?php echo $short_tittle; ?>" readonly />
            <input type="text" class="form-control input-sm" name="page_state" id="page_state" value="<?php echo $page_state; ?>" readonly />
        </div>
        <!-- <div class="outer" style="z-index:10; position: fixed; display: none;" id="loading">
          <div class="middle">
            <div class="inner">
              <div class="cssload-loading">
                <i></i>
                <i></i>
                <i></i>
              </div>
              <br />
              <br />
              <h6 style="text-align: center;">loading...</h6>
            </div>
          </div>
        </div> -->
        <div class="outer" style="z-index:10; position: fixed; display: none;" id="loading">
            <div class="middle">
                <div class="inner">
                    <center>
                        <img src="<?= base_url();?>media/logo/CoE-150.gif" class="img-responsive" style="max-width: 20%;" alt="homepage" class="dark-logo" />
                        <label class="form-group"><h6 style="text-align: center;">loading...</h6></label>
                    </center>
                </div>
            </div>
        </div>
        <!-- <div class="outer" style="z-index:10; position: fixed; display: none;" id="loading">
          <div class="middle">
            <div class="inner">
              <div class="cssload-cssload-wrap2">
                    <div class="cssload-wrap">
                            <div class="cssload-overlay"></div>
    
                            <div class="cssload-cogWheel cssload-one">
                            <div class="cssload-cog cssload-one"></div>
                            <div class="cssload-cog cssload-two"></div>
                            <div class="cssload-cog cssload-three"></div>
                            <div class="cssload-cog cssload-four"></div>
                            <div class="cssload-cog cssload-five"></div>
                            <div class="cssload-center"></div>
                            </div>
    
                            <div class="cssload-cogWheel cssload-two">
                            <div class="cssload-cog cssload-one"></div>
                            <div class="cssload-cog cssload-two"></div>
                            <div class="cssload-cog cssload-three"></div>
                            <div class="cssload-cog cssload-four"></div>
                            <div class="cssload-cog cssload-five"></div>
                            <div class="cssload-center"></div>
                            </div>
                    </div>
              </div>
              <br />
              <br />
              <br />
              <br />
              <br />
              <h6 style="text-align: center;z-index:40;">loading...</h6>
            </div>
          </div>
        </div> -->
        <div class="header">
            <nav class="navbar top-navbar navbar-expand-md navbar-light ocean-themes">
                <!-- Logo -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="welcome">
                        <!-- Logo icon -->
                        <img src="<?= base_url();?>media/CoE.png" class="img-responsive" style="max-width: 33%;" alt="homepage" class="dark-logo" />
                        <!--End Logo icon -->
                        <!-- Logo text -->
                        <span><img src="<?= base_url();?>media/edems.png" style="max-width: 33%;" alt="homepage" class="dark-logo" /></span>
                    </a>
                </div>
                <!-- End Logo -->
                <div class="navbar-collapse">
                    <!-- toggle and nav items -->
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <!-- This is  -->
                        <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted  " href="javascript:void(0)"><i class="mdi mdi-menu color-white"></i></a> </li>
                        <li class="nav-item m-l-10"> <a class="nav-link sidebartoggler hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-menu color-white"></i></a> </li>
                        <li class="nav-item m-l-10"> <h3 class="text-white"><?php echo $tittle; ?></h3> </li>
                    </ul>
                    <!-- User profile and search -->
                    <ul class="navbar-nav my-lg-0">
                        <li class="nav-item hidden-sm-down search-box"> <a class="nav-link hidden-sm-down text-muted  " href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search" id="app-search" style="display: none;">
                                <input type="text" class="form-control search-glb text-white" id="search-glb" name="search-glb" placeholder="Search by notification no..." autocomplete="off" > <a class="srh-btn text-white"><i class="fa fa-close"></i></a> </form>
                        </li>
                    </ul>
                    <li class="div-search nav-item dropdown" style="position:fixed;margin-top:1.8%;">
                        <div class="div-search dropdown-menu dropdown-menu-right mailbox animated zoomIn" style="width:1050px;">
                            <div class="div-search" id="search-content"></div>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a rel="noopener noreferrer" class="btn btn-sm btn-outline-primary" target="_blank" href="https://drive.google.com/drive/folders/1iu_VvSTHwOIhTQ2E8cA1ofPErJA8oDq_" title="User Manual" style="padding-right: 15px;" ><i class="fa fa-external-link"></i> Download User Manual</a>
                    </li>
                    <li class="nav-item ">
                        <p id="online" style="padding-left: 15px;padding-right: 15px;" > </p>
                    </li>


                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-muted text-muted  " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell color-white"></i>
                        <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                        </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" style="right:0; left:auto">
                                <ul>
                                    <li>
                                        <div class="drop-title">Notifications</div>
                                    </li>
                                    <li style="overflow: visible;">
                                        <div class="slimScrollDiv" style="position: relative; overflow: visible hidden; width: auto; height: 250px;">
                                          <div name="message-center" id="notification" class="message-center" style="overflow: auto; width: auto; height: 250px;">
    
                                          </div>
                                        <div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                        <div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                      </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li> -->

                    <ul class="navbar-nav my-lg-0">
                        <!-- Log out -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-white btn-info btn-flat"><small> <?php echo $info['name']; ?></small></a>
                            <!-- <a class="nav-link dropdown-toggle text-muted btn-dark btn-flat" href="auth/logout"><i class="fa fa-out"></i><small> Logout</small></a> -->
                        </li>
                    </ul>

                </div>
            </nav>
        </div>
        <!-- End header header -->

        <!-- Left Sidebar  -->
        <div class="left-sidebar ocean-themes-side">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav-ocean">
                    <ul id="sidebarnav">
                        <?php
                        // build roles
                        if (isset($roles)) {
                            // code...
                            $sIndex = '';
                            $sdiv = "
                            <li class='nav-devider-ocean'></li>
                            <li class='nav-label-ocean'>Home</li>";
                            foreach ($roles as $list) {
                                foreach ($list['action'] as $key => $value) {
                                    // code...
                                    if ($list['form_id'] == $value['ref_id']) {
                                        // code...
                                        if ($sIndex != $list['group']) {
                                            // code...
                                            $sIndex = $list['group'];
                                            echo "
                                    <li class='nav-devider-ocean'></li>
                                    <li class='nav-label-ocean'>{$sIndex}</li>";
                                        }
                                        $spntext = '';
                                        if (isset($list['count']) && $list['count'] > 0) {
                                            $spntext = "<span class='label label-rouded {$list['code']}-sp label-{$list['spancol']} pull-right' style='right: 1px;' > {$list['count']}</span>";
                                        }
                                        echo "<li style='position: relative;' > <a href='{$list['uri']}' aria-expanded='false' ><i class='fa {$list['icon']}'></i><span class='hide-menu' >{$list['desc']} {$spntext}</span></a>
                                  </li>";
                                    }
                                }
                            }
                        }
                        ?>
                        <!-- <li class="nav-devider"></li>
                        <li class="nav-label">Home</li>
                        <li> <a href="welcome" aria-expanded="false"><i class="fa fa-tachometer"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
                        <li class="nav-label">Apps</li>
                        <li> <a href="erf/request" aria-expanded="false"><i class="fa fa-envelope"></i><span class="hide-menu">Engineering Request Form</span></a>
                        </li>
                        <li> <a href="eat/assign" aria-expanded="false"><i class="fa fa-wpforms"></i><span class="hide-menu">Engineering Assign Task</span></a>
                        </li>
                        <li> <a href="eng/progress" aria-expanded="false"><i class="fa fa-clock-o"></i><span class="hide-menu">Progress Engineering</span></a>
                        </li>
                        <li> <a href="dok_eng/documents" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Engineering Documents</span></a>
                        </li>
                        <li> <a href="bast/handover" aria-expanded="false"><i class="fa fa-suitcase"></i><span class="hide-menu">BAST</span></a>
                        </li>
                        <li class="nav-label">Reports</li>
                        <li> <a href="report/monitor" aria-expanded="false"><i class="fa fa-list-alt"></i><span class="hide-menu">Monitoring</span></a>
                        </li> -->
                        <li class="nav-devider"></li>
                        <li> <a href="auth/logout" aria-expanded="false"><i class="fa fa-sign-out"></i><span class="hide-menu">Logout</span></a>
                        </li>
                        <!-- <li> <a class="has-arrow  " href="dok_eng/rks" aria-expanded="false"><i class="fa fa-book"></i><span class="hide-menu">Engineering Documents <span class="label label-rouded label-success pull-right">4</span></span></a>
                              <ul aria-expanded="false" class="collapse">
                                  <li><a href="dok_eng/rks">RKS</a></li>
                                  <li><a href="dok_eng/drawing">Drawing</a></li>
                                  <li><a href="dok_eng/bq">BQ</a></li>
                                  <li><a href="dok_eng/ece">ECE</a></li>
                              </ul>
                        </li> -->
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>
        <!-- End Left Sidebar  -->

    </div>
    <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?= base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/jquery-redirect/jquery.redirect.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?= base_url();?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?= base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?= base_url();?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?= base_url();?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?= base_url();?>assets/js/custom.min.js"></script>

    <script src="<?= base_url();?>assets/js/lib/datatables/datatables.min.js"></script>
    <script src="<?= base_url();?>assets/datatables/DataTables-1.10.18/js/dataTables.fixedColumns.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <script src="<?= base_url();?>assets/js/lib/datatables/datatables-init.js"></script>

    <!-- Chosen -->
    <script src="<?= base_url();?>assets/js/others/chosen/chosen.jquery.js"></script>
    <!-- selectpicker -->
    <script src="<?= base_url();?>assets/js/others/bootstrap-select/ajax-bootstrap-select.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/bootstrap-select/bootstrap-select.js"></script>
    <script src="<?= base_url();?>assets/js/others/validate/jquery.validate.min.js"></script>
    <!-- add on -->
    <script src="<?= base_url();?>assets/js/others/ladda/spin.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/ladda/ladda.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/ladda/ladda.jquery.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/sweetalert/sweetalert.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/priceformat/jquery.priceFormat.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/select2/select2.full.min.js"></script>
    <script src="<?= base_url();?>assets/js/others/vmc-simple-slide/vmc.simple.slide.min.js"></script>
    <!-- general script -->
    <script src="<?= base_url();?>js/mpe_general.js"></script>
    <script type="text/javascript" charset="utf8" src="<?= base_url();?>assets/datatables/datatables.js"></script>


<!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->
