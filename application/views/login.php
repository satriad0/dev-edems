<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- <base href="http://10.15.5.150/dev/MPE2/" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>MPE Web Application</title>
        <script type="text/javascript" src="jquery-2.1.4.min.js"></script>
        <link rel="icon" href="media/mpe_logo.png" type="image/png" sizes="16x16"> -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x14" href="<?= base_url(); ?>media/CoE.png">
        <title>eDMS-DE | <?php echo $tittle; ?></title>
        <!-- Bootstrap Core CSS -->
        <link href="<?= base_url(); ?>assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?= base_url(); ?>assets/css/helper.css" rel="stylesheet">
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet">

    </head>
    <style>
        .navbar-custom {
            background-color: #0000005c;
            color: #fff;
        }
        .navbar-fixed-top {
            top: 0;
            border-width: 0 0 1px;
        }
        .navbar-fixed-bottom, .navbar-fixed-top {
            border-radius: 0;
        }
        .navbar-fixed-bottom, .navbar-fixed-top {
            position: fixed;
            right: 0;
            left: 0;
            z-index: 1030;
        }
        .navbar {
            border-radius: 4px;
        }
        .navbar {
            position: relative;
            min-height: 50px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-top-width: 1px;
            border-right-width: 1px;
            border-bottom-width: 1px;
            border-left-width: 1px;
        }
        .container{
            width: 100%;
        }
        h1{
            margin-top: 6%;
            color: #52525a;
        }
        .img-logo {
            max-width: 50% !important;
            margin: auto !important;
            margin-top: 50px;
            margin-bottom: 50px;
        }
        .h1, h1 {
            font-size: 50px;
            font-weight: 600;
        }
        .h3, h3{
            font: 400 30px/0.8 'Orbitron', sans-serif;
            color: #000;;
            text-shadow: 4px 4px 3px rgba(0,0,0,0.1);
        }
        .bg-mso{

            background: url('media/bg_monitor.jpg') no-repeat center center fixed;
            -moz-background-size: cover;
            -webkit-background-size: cover;
            -o-background-size: cover;
            background-size: cover;

        }
        a {
            color: #c0c0c0;
            text-decoration: none;
        }
        .panel {
            margin-bottom: 20px;
            background-color: #ffffff47;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
            box-shadow: 0 1px 1px rgba(0,0,0,.05);
        }
        .panel-default>.panel-heading {
            color: #333;
            background-color: #c2c2c224;
            border-color: #ad9c9c00;
        }

        .panel-heading {
            padding: 10px 15px;
            border-bottom: 1px solid transparent;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
        }
        .nav>li>a {
            background-color: #5cb85cab;
            border-color: #4cae4ca6;
            margin:10px;
            padding: 4px;
            width: 100px;
        }


        ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
            color:    #696969 !important;
        }
        :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color:    #696969 !important;
            opacity:  1;
        }
        ::-moz-placeholder { /* Mozilla Firefox 19+ */
            color:    #696969 !important;
            opacity:  1;
        }
        :-ms-input-placeholder { /* Internet Explorer 10-11 */
            color:    #696969 !important;
        }
        ::-ms-input-placeholder { /* Microsoft Edge */
            color:    #696969 !important;
        }

        ::placeholder { /* Most modern browsers support this now. */
            color:    #696969 !important;
        }

        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            /*background-color: red;*/
            color: black;
            text-align: center;
        }
        .nav>li>a:focus, .nav>li>a:hover {
            text-decoration: none;
            background-color: #107232c4;
        }
        body {
            background: url('media/bg_monitor.jpg');
            background-repeat: no-repeat;
            height: 100%;

            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            -moz-background-size: cover;
            -webkit-background-size: cover;
            -o-background-size: cover;
        }
    </style>
    <!-- <body style="background: url('media/bg_monitor.jpg');"> -->
    <body class="fix-header fix-sidebar mini-sidebar navbar-custom navbar-fixed-top">
        <!-- Preloader - style you can find in spinners.css -->
        <div class="preloader" style="display: none;">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"></circle> </svg>
        </div>
        <!-- loading style -->
        <div class="outer" style="z-index:10; display: none;" id="loading">
            <div class="middle">
                <div class="inner">
                    <center>
                        <img src="media/logo/CoE-150.gif" class="img-responsive" style="max-width: 20%;" alt="homepage" class="dark-logo" />
                        <label class="form-group"><h6 style="text-align: center;">loading...</h6></label>
                    </center>
                </div>
            </div>
        </div>
        <!-- Main wrapper  -->
        <div id="main-wrapper">
            <div class="unix-login">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="login-content card" style="background: rgba(255, 255, 255, 0.58) none repeat scroll 0 0;box-shadow: 0px 0px 0px 5px rgba( 255,255,255,0.4 ), 0px 4px 20px rgba( 0,0,0,0.33 );border-radius: 5px;" >
                                <div class="login-form" style="background: rgba(255, 255, 255, 0.58) none repeat scroll 0 0;" >

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle text-muted pull-right " href="#" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-gears"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right mailbox animated zoomIn" aria-labelledby="2">
                                            <ul>
                                                <li>
                                                    <div class="drop-title"><h6>Themes List</h6></div>
                                                </li>
                                                <li>
                                                    <div class="message-center">
                                                      <!-- <p><center>Themes</center></p>
                                                      <hr> -->
                                                        <div class="cc-selector-2">
                                                            <?php
                                                            $sCheck = '';
                                                            foreach ($themes as $key => $list) {
                                                                // if ($key==0) $sCheck='checked';
                                                                // else $sCheck = '';
                                                                echo "<br />";
                                                                // echo "<div class='row'>";
                                                                echo "<input id='{$list->GEN_CODE}' type='radio' name='themes' value='{$list->GEN_CODE}' {$sCheck} />";
                                                                echo "<label class='drinkcard-cc' for='{$list->GEN_CODE}' title='{$list->GEN_DESC}' style = 'background-image:url({$list->GEN_PAR1});border: 1px solid #ccc!important;'></label>";
                                                                // echo "</div>";
                                                            }
                                                            ?>
                                                        </div>
                                                        <!-- <hr>
                                                        <p><small>reload this page to clear selection.</small></p> -->
                                                    </div>
                                                </li>
                                                <li>
                                                    <p class="nav-link text-center">reload this page to clear selection.</i> </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <h3><center>
                                            Login
                                        </center></h3>
                                    <hr />
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6" align="center">
                                            <h6> e-DEMS</h6>
                                            <h6><small> Electronic - Design Engineering Management System</small></h6>
                                            <!-- <img src="media/mpe_logo.png" class="img-responsive img-logo" sizes="19x20"> -->
                                            <img src="media/CoE.png" class="img-responsive" style="margin:auto;max-width: 80%;">
                                            <div class="row">
                                                <div class="col-lg-12" style="margin-bottom:5%">
                                                    <img src="<?= base_url(); ?>media/semen_padang.png"  style="margin:auto;max-width: 15%;" >
                                                    <img src="<?= base_url(); ?>media/semen_tonasa.png"  style="margin:auto;max-width: 15%;margin-left: 10%"  >
                                                    <img src="<?= base_url(); ?>media/semen_gresik.png"  style="margin:auto;max-width: 15%;margin-left: 10%"  >
                                                    <img src="<?= base_url(); ?>media/thanglong.png"  style="margin:auto;max-width: 15%;margin-left: 10%"  >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 vertical-center">
                                            <form id="fm-login">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="username" placeholder="Username" required >
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="password" placeholder="Password" required >
                                                </div>
                                                <div class="form-group">
                                                    <div class="error"></div>
                                                    <!-- <button type="submit" onclick="login_mpe()" class="btn btn-primary btn-flat">Login</button> -->
                                                    <div class="col-sm-offset-2 col-sm-10" style="/*display:none;*/" >
                                                        <div class="checkbox" >
                                                            <label>
                                                                <input type="checkbox" id="remember" value="true" > Remember me
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary btn-flat">Login</button>
                                                </div>
                                            </form>
                                            <p style="text-align: center;"> <small>Created by PT. SISI &copy; 2018</small> </p>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                    </div>
                                    <!-- <div class="row">
                                      <div class="col-lg-12">
                                        <p class="m-t" style="text-align: center;"> <small>Created by PT. SISI &copy; 2017</small> </p>
                                      </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- End Wrapper -->
        <!-- All Jquery -->
        <script src="<?= base_url(); ?>assets/js/lib/jquery/jquery.min.js"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="<?= base_url(); ?>assets/js/lib/bootstrap/js/popper.min.js"></script>
        <script src="<?= base_url(); ?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="<?= base_url(); ?>assets/js/jquery.slimscroll.js"></script>
        <!--Menu sidebar -->
        <script src="<?= base_url(); ?>assets/js/sidebarmenu.js"></script>
        <!--stickey kit -->
        <script src="<?= base_url(); ?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
        <!--Custom JavaScript -->
        <script src="<?= base_url(); ?>assets/js/custom.min.js"></script>

        <script src="<?= base_url(); ?>js/mpe_login.js"></script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
        
          gtag('config', 'UA-23581568-13');
        </script> -->
    </body>

</html>
