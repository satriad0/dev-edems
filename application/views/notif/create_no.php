<style>
    /* The Modal (background) */

    #fade {
        display: none;
        position:absolute;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: #ababab;
        z-index: 9999;
        -moz-opacity: 0.8;
        opacity: .70;
        filter: alpha(opacity=80);
    }

    #modal {
        display: none;
        position: absolute;
        top: 45%;
        left: 45%;
        width: 160px;
        height: 160px;
        padding:30px 15px 0px;
        border: 3px solid #ababab;
        box-shadow:1px 1px 10px #ababab;
        border-radius:20px;
        background-color: white;
        z-index: 10000;
        text-align:center;
        overflow: auto;
        overflow-y:auto;
    }
    .bootstrap-select.show>.dropdown-menu>.dropdown-menu {
        display: block;
    }

    .bootstrap-select > .dropdown-menu > .dropdown-menu li.hidden{
        display:none;
    }

    .bootstrap-select > .dropdown-menu > .dropdown-menu li a{
        display: block;
        width: 100%;
        padding: 3px 1.5rem;
        clear: both;
        font-weight: 400;
        color: #292b2c;
        text-align: inherit;
        white-space: nowrap;
        background: 0 0;
        border: 0;
    }
</style>

<!-- Page wrapper  -->
<div class="page-wrapper">
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Notification</span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">Create Notification</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">

                        <div class="table-responsive m-t-25">
                            <table id="tb_erf2" name="tb_erf2" class="table display table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="16"></th>
                                        <!-- <th>Form no</th> -->
                                        <th>No. Noftifikasi</th>
                                        <th>Deskripsi</th>
                                        <th>P. Group</th>
                                        <th>P. Section</th>
                                        <th>Unit Kerja</th>
                                        <!-- <th>Department</th> -->
                                        <!-- <th>Priority</th> -->
                                        <th>Pembuat</th>
                                        <th width="40px">Tanggal</th>
                                        <th width="60px">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">General Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6 col-lg-6">
                                              <div class="form-group">
                                                <label for="KDLOC" class="control-label">Location code <span class="text-danger">*</span></label>
                                                <select name="KDLOC" id="KDLOC" class="chosen input-sm select2" style="width:100%;" required>
                                            <?php
                                            // foreach ($locations as $list) {
                                            //   echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' {$sAdd} >" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "</option>";
                                            // }
                                            ?>
                                                  </select>
                                              </div>
                                            </div> -->
                                            <!-- <div class="col-md-6 col-lg-6">
                                              <div class="form-group">
                                                <label for="KDAREA" class="control-label">Area Code<span class="text-danger">*</span></label>
                                                  <select name="KDAREA" id="KDAREA" class="chosen input-sm select2" style="width:100%;" required>
                                            <?php
                                            // foreach ($areas as $list) {
                                            //   echo "<option value='" . $list->GEN_DESC . "' title='" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "' >" . $list->GEN_DESC . " - " . $list->GEN_PAR1 . "</option>";
                                            // }
                                            ?>
                                                  </select>
                                              </div>
                                            </div> -->
                                            <!-- <div class="col-md-6">
                                              <div class="form-group">
                                                <label for="NOPENGAJUAN" class="control-label">No ERF. <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control input-sm"  name="NOPENGAJUAN" id="NOPENGAJUAN" readonly="" required/>
                                              </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="NOTIFIKASI" class="control-label">No. Notifikasi</label>
                                                    <input type="text" class="form-control input-sm" name="NOTIFIKASI" id="NOTIFIKASI" readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PRIORITY" class="control-label">Prioritas</label>
                                                    <select name="PRIORITY" id="PRIORITY" class="form-control form-control-sm select-sm">
                                                        <option value="1">EMERGENCY</option>
                                                        <option value="2">HIGH</option>
                                                        <option value="3" selected>MEDIUM</option>
                                                        <option value="4">LOW</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="IDMPE" class="control-label">ID MPE</label>
                                                    <input type="text" class="form-control input-sm" name="IDMPE" id="IDMPE" readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-md-3" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="REV_NO" class="control-label">Rev.</label>
                                                    <input type="text" class="form-control input-sm" name="REV_NO" id="REV_NO" readonly="" value="0"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SHORT_TEXT" class="control-label">Nama Pekerjaan <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="SHORT_TEXT" id="SHORT_TEXT" placeholder="(max. 40 karakter)" maxlength="40" required/>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-6 ">
                                              <div class="form-group">
                                                <label for="SHORT_TEXT" class="control-label">ERF Type<span class="text-danger">*</span></label>
                                                <select name="JENIS" id="JENIS" class="form-control form-control-sm select-sm" data-title="Erf Type..." data-show-subtext="true" data-live-search="true" required>
                                                    <option value="Capex">Capex</option>
                                                    <option value="Kajian">Kajian</option>
                                                </select>
                                              </div>
                                            </div> -->
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="FUNCT_LOC" class="control-label">Lokasi Fungsional <span class="text-danger">*</span></label>
                                                    <!-- <select name="FUNCT_LOC" id="FUNCT_LOC" class="form-control select-sm selectpicker" data-title="Pilih functional location..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="FUNCT_LOC" id="FUNCT_LOC" class="chosen input-sm select2" style="width:100%;" required>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="FL_CODE" class="control-label">FL CODE</label>
                                                    <input type="text" class="form-control input-sm" name="FL_CODE" id="FL_CODE" readonly="" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="SWERK" class="control-label">Maintenance Plant <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="SWERK" id="SWERK" placeholder="Data tak ditemukan" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="BEBER" class="control-label">Unit Kerja Peminta <span class="text-danger">*</span></label>
                                                    <select name="BEBER" id="BEBER" class="form-control form-control-sm select-sm" placeholder="Pilih section.." required>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PLANPLANT" class="control-label">Lokasi Plant <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm select-sm" name="PLANPLANT" id="PLANPLANT" placeholder="Data tak ditemukan" value="<?php echo "{$pgroup->GEN_PAR1} - {$pgroup->GEN_PAR7}"; ?>" readonly />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="PLANGROUP" class="control-label">Unit Kerja Tujuan <span class="text-danger">*</span></label>
                                                    <select name="PLANGROUP" id="PLANGROUP" class="form-control form-control-sm select-sm" placeholder="Pilih planner group.." readonly required>
                                                        <?php
                                                        echo $pgroup->GEN_PAR1;
                                                        echo "<option value='" . $pgroup->GEN_CODE . "' title='" . $pgroup->GEN_CODE . " - " . $pgroup->GEN_DESC . "' selected >" . $pgroup->GEN_CODE . " - " . $pgroup->GEN_DESC . "</option>";
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="LOKASI" class="control-label">Lokasi</label>
                                              <input type="text" class="form-control input-sm" name="LOKASI" id="LOKASI" placeholder="(max. 255 karakter)" maxlength="255" />
                                            </div>
                                          </div>
                                        </div> -->
                                        <!-- <div class="row">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="WBS_CODE" class="control-label">Code WBS</label>
                                              <input type="text" class="form-control input-sm" name="WBS_CODE" id="WBS_CODE" placeholder="(max. 255 karakter)" maxlength="255" />
                                            </div>
                                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="WBS_TEXT" class="control-label">Description WBS</label>
                                              <input type="text" class="form-control input-sm" name="WBS_TEXT" id="WBS_TEXT" placeholder="(max. 255 karakter)" maxlength="255" />
                                            </div>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="unit" class="control-label">Rencana Tanggal (Permintaan-Penyelesaian)</label>
                                                    <div class="input-daterange input-group" >
                                                        <input type="date" class="form-control input-sm" name="DESSTDATE" id="DESSTDATE" >
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="ti-minus"></i></span>
                                                        </div>
                                                        <input type="date" class=" form-control input-sm" name="DESENDDATE" id="DESENDDATE" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!--/span-->
                                    <!-- <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <h5 class="card-title"> Specification</h5>
                                          <hr>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="LATAR_BELAKANG" class="control-label">Latar Belakang <span class="text-danger">*</span></label>
                                            <textarea class="form-control tarea-sm" rows="5" name="LATAR_BELAKANG" id="LATAR_BELAKANG" placeholder="(max. 2000 char)" maxlength="2000" style="resize:none;" required ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="CUST_REQ" class="control-label">Customer Request <span class="text-danger">*</span></label>
                                            <textarea class="form-control tarea-sm" rows="5" name="CUST_REQ" id="CUST_REQ" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="TECH_INFO" class="control-label">Technical Info <span class="text-danger">*</span></label>
                                            <textarea class="form-control tarea-sm" rows="5" name="TECH_INFO" id="TECH_INFO" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="form-group">
                                            <label for="RES_MIT" class="control-label">Resiko & Mitigasi <span class="text-danger">*</span></label>
                                            <textarea class="form-control tarea-sm" rows="5" name="RES_MIT" id="RES_MIT" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;" required ></textarea>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="form-group">
                                          <div class="col-md-12">
                                            <label for="FILES" class="control-label">Select File Pendukung (*.pdf) </label>
                                          </div>
                                          <div class="col-md-12">
                                            <input type="file" name="FILES" id="FILES" accept="application/pdf" />
                                          </div>
                                        </div>
                                        <div class="form-group file">
                                          <div class="col-md-12">
                                            <a id="file_uri" rel="noopener noreferrer" target="_blank"><i id="file_name"></i>
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div> -->
                                    <!--/span-->
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                        echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->


        <!-- ================================= MODAL ERF ================================= -->
        <div id="tmp_modal" class="modal fade" role="dialog" style="padding-top:20%;" >
            <div class="modal-dialog modal-lg modal-md modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <center>
                            <h4 class="modal-title title_ukur"></h4>
                            <!-- <small class="font-bold">PT. Semen Indonesia</small> -->
                        </center>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <!-- <form action="#"> -->
                                <!-- <div class="form-body"> -->
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <h8 class="card-title">Header Info</h8>
                                            <hr>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-6" style="display:none;" >
                                                <div class="form-group" >
                                                    <label for="tmpID" class="control-label">ID MPE</label>
                                                    <input type="text" class="form-control input-sm" name="tmpID" id="tmpID"  readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="tmpNo" class="control-label">No ERF</label>
                                                    <input type="text" class="form-control input-sm"  name="tmpNo" id="tmpNo" disabled >
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <label for="tmpNotif" class="control-label">No Notifikasi</label>
                                                    <input type="text" class="form-control input-sm"  name="tmpNotif" id="tmpNotif" disabled />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12" id="tablog" class="table-responsive">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- </div> -->
                                <!-- </form> -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="closeModal('tmp_modal')"><i class="fa fa-times"></i> Tutup</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- =============================== END MODAL ERF =============================== -->

    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- ======default Jquery====== -->
<!-- All Jquery -->
<!-- <script src="assets/js/lib/jquery/jquery.min.js"></script> -->
<!-- Bootstrap tether Core JavaScript -->
<!-- <script src="assets/js/lib/bootstrap/js/popper.min.js"></script> -->
<!-- <script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script> -->
<!-- slimscrollbar scrollbar JavaScript -->
<!-- <script src="assets/js/jquery.slimscroll.js"></script> -->
<!--Menu sidebar -->
<!-- <script src="assets/js/sidebarmenu.js"></script> -->
<!--stickey kit -->
<!-- <script src="assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script> -->
<!--Custom JavaScript -->
<!-- <script src="assets/js/custom.min.js"></script> -->
<!-- ====end default Jquery==== -->


<script src="<?= base_url();?>js/notif/ntf_create.js"></script>
