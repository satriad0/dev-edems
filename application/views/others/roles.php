
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Roles</span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">New Roles</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_roles" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                      <!-- <th>No</th> -->
                                      <!-- <th>No. EAT</th> -->
                                        <th width='20%' style="text-align: center;" >NAME</th>
                                        <th width='40%' style="text-align: center;" >DESCRIPTION</th>
                                        <th width='10%' style="text-align: center;" >SPECIAL</th>
                                        <!-- <th>Note</th> -->
                                        <th width='5%' style="text-align: center;" >CREATED BY</th>
                                        <th width='5%' style="text-align: center;" >CREATED AT</th>
                                        <th width='10%' style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Roles Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                              <div class="form-group" style="display:none;" >
                                                <label for="NO_DOK_ENG" class="control-label">No Dokumen <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" required/>
                                              </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID ROLES</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-6">
                                              <div class="form-group" style="display:none;" >
                                                <label for="ID_PENUGASAN" class="control-label">ID EAT</label>
                                                <input type="text" class="form-control input-sm" name="ID_PENUGASAN" id="ID_PENUGASAN" readonly="" />
                                              </div>
                                            </div> -->
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="ID_PENUGASAN" class="control-label">EAT <span class="text-danger">*</span></label>
                                              <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="form-control form-control-sm select-sm" data-title="Pilih Eat..." data-show-subtext="true" data-live-search="true" required>
                                        <?php
                                        // foreach ($FOREIGN as $list) {
                                        //     echo "<option value='" . $list["ID_PENGAJUAN"] . "' title='" . $list["NO_PENGAJUAN"] . " - " . $list["NOTIF_NO"] . "' $list-subtext='" . $list["NO_PENGAJUAN"] . "' $list-no='" . $list["NO_PENGAJUAN"] . "' $list-notif='" . $list["NOTIF_NO"] . "' $list-flcode='" . $list["FUNCT_LOC"] . "' $list-fltext='" . $list["FL_TEXT"] . "' $list-shorttext='" . $list["SHORT_TEXT"] . "' $list-uktext='" . $list["UK_TEXT"] . "' >" . $list["SHORT_TEXT"] . "</option>";
                                        // }
                                        ?>
                                              </select>
                                            </div>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="NAME" class="control-label">Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NAME" id="NAME" required />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="DESCRIPTION" class="control-label">Description <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="DESCRIPTION" id="DESCRIPTION" required />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="SPECIAL" class="control-label">Special Access <span class="text-danger">*</span></label>
                                                    <div class="switch">
                                                        <div class="onoffswitch">
                                                            <input type="checkbox" checked="" class="onoffswitch-checkbox special" id="SPECIAL">
                                                            <label class="onoffswitch-label" for="SPECIAL">
                                                                <span class="onoffswitch-inner"></span>
                                                                <span class="onoffswitch-switch"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Permision</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="table-responsive m-t-25">
                                            <div class="form-group">
                                                <table id="tb_forms" class="table display table-bordered table-striped" style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                          <!-- <th>Group</th>
                                                          <th>Form Name</th>
                                                          <th style="text-align:center;">Type</th> -->
                                                            <th>Group</th>
                                                            <th>Form Name</th>
                                                            <th>Permission</th>
                                                            <th>Description</th>
                                                        </tr>
                                                    </thead>
                                                    <!-- <tfoot>
                                                    <tr>
                                                    <th>Form Name</th>
                                                    <th>Group</th>
                                                    <th style="text-align:center;">Type</th>
                                                      </tr>
                                                    </tfoot> -->
                                                    <tbody>
                                                        <?php
                                                        // foreach ($forms as $list) {
                                                        //   echo "<tr>";
                                                        //   echo "<td>{$list['GEN_PAR1']}</td>";
                                                        //   echo "<td>{$list['GEN_DESC']}</td>";
                                                        //   echo "<td><ul style='text-align:left;'>";
                                                        //   foreach ($list['ACTION'] as $value) {
                                                        //     echo "<li><label><input type='checkbox' name='cekval[]' value='{$list['ID']}-{$value['ID']}' title='{$value['GEN_PAR5']}' > {$value['GEN_DESC']}</label></li>";
                                                        //   }
                                                        //   echo "</ul></td>";
                                                        //   // echo "<td><center><input type='checkbox' name='cekval[]' value='{$list['ID']}' > View</center></td>";
                                                        //   echo"</tr>";
                                                        //   // echo "<tr>";
                                                        //   // echo "<td>{$list->GEN_PAR1}</td>";
                                                        //   // echo "<td>{$list->GEN_DESC}</td>";
                                                        //   // echo "<td><center><input type='checkbox' name='cekval[]' value='{$list->ID}' > View</center></td>";
                                                        //   // echo"</tr>";
                                                        // }
                                                        foreach ($forms as $list) {
                                                            // echo "<td><ul style='text-align:left;'>";
                                                            foreach ($list['ACTION'] as $value) {
                                                                echo "<tr>";
                                                                // echo "<li><label><input type='checkbox' name='cekval[]' value='{$list['ID']}-{$value['ID']}' title='{$value['GEN_PAR5']}' > {$value['GEN_DESC']}</label></li>";
                                                                echo "<td>{$list['GEN_PAR1']}</td>";
                                                                echo "<td>{$list['GEN_DESC']}</td>";
                                                                echo "<td><input type='checkbox' name='cekval[]' value='{$list['ID']}-{$value['ID']}' title='{$value['GEN_PAR5']}' > {$value['GEN_DESC']}</td>";
                                                                echo "<td style='text-align: left;'>{$value['GEN_PAR5']}</td>";
                                                                echo"</tr>";
                                                            }
                                                            // echo "</ul></td>";
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                        echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                    <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/others/roles.js"></script>
