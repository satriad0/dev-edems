
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Slide</span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">New Slide</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_slides" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                      <!-- <th>No</th> -->
                                      <!-- <th>No. EAT</th> -->
                                        <th style="text-align: center;" >NO</th>
                                        <th style="text-align: center;" >CAPTION</th>
                                        <th style="text-align: center;" >DESCRIPTION</th>
                                        <th style="text-align: center;" >CREATE BY</th>
                                        <th style="text-align: center;" >CREATE AT</th>
                                        <th style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Roles Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                              <div class="form-group" style="display:none;" >
                                                <label for="NO_DOK_ENG" class="control-label">No Dokumen <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" required/>
                                              </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID Slide</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="TIPE" class="control-label">Tipe</label>
                                                    <select class="chosen-select select2" name="TIPE" id='TIPE' style="width:100%;" required>
                                                        <option value="running" selected >Running Picture</option>
                                                        <option value="split" >Split</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="SHORT_DESC" class="control-label">Caption</label>
                                                    <input type="text" class="form-control input-sm" name="SHORT_DESC" id="SHORT_DESC" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="DESCRIPTION" class="control-label">Description</label>
                                                    <!-- <input type="textarea" class="form-control input-sm" name="DESCRIPTION" id="DESCRIPTION" placeholder="(max. 2000 Karakter)" /> -->
                                                    <textarea class="form-control tarea-sm" rows="5" name="DESCRIPTION" id="DESCRIPTION" placeholder="(max. 2000 karakter)" maxlength="2000" style="resize:none;"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="FILES" class="control-label">Select Picture (1140x375 px) </label><br />
                                                    <input type="file" name="FILES" id="FILES" accept="image/*" /><br /><br />
                                                    <a id="file_uri" rel="noopener noreferrer" target="_blank"><i id="file_name"></i>
                                                    </a>
                                                    <!-- file_extension|audio/*|video/*|image/*|media_type -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/others/slides.js"></script>
