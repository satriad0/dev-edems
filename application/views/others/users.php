
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content --><div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <ul class="nav nav-tabs customtab2" id="tabs" role="tablist">
                    <li class="nav-item lf-form"> <a class="nav-link active" data-toggle="tab" href="#list-form" id="ul-list" role="tab"><span class="hidden-sm-up"><i class="ti-home"></i></span> <span class="hidden-xs-down">List Users</span></a> </li>
                    <li class="nav-item nw-form"> <a class="nav-link" data-toggle="tab" href="#new-form" id="ul-new" role="tab"><span class="hidden-sm-up"><i class="ti-user"></i></span> <span class="hidden-xs-down">New User</span></a> </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active show" id="list-form" role="tabpanel">
                        <!-- <div>
                          <button type="button" class="btn btn-success btn-sm m-b-5 m-l-5" onClick="openModal('tmp_modal')"><i class="ti-plus"></i> Tambah</button>
                        </div> -->
                        <div class="table-responsive m-t-25">
                            <table id="tb_roles" class="table display table-bordered table-striped" style="width:100%;">
                                <thead>
                                    <tr>
                                      <!-- <th>No</th> -->
                                      <!-- <th>No. EAT</th> -->
                                        <th width='20%' style="text-align: center;" >NAME</th>
                                        <th width='20%' style="text-align: center;" >EMAIL</th>
                                        <th width='10%' style="text-align: center;" >ROLES</th>
                                        <!-- <th>Note</th> -->
                                        <th width='10%' style="text-align: center;" >POSITION</th>
                                        <th width='15%' style="text-align: center;" >WORK UNIT</th>
                                        <th width='15%' style="text-align: center;" >DEPARTMENT</th>
                                        <th width='5%' style="text-align: center;" >COMPANY</th>
                                        <th width='5%' style="text-align: center;" >OPTIONS</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane p-20" id="new-form" role="tabpanel">

                        <form id="fm-new">
                            <div class="form-body">
                                <div class="row">
                                    <!-- /span -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h8 class="card-title">Roles Info</h8>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                              <div class="form-group" style="display:none;" >
                                                <label for="NO_DOK_ENG" class="control-label">No Dokumen <span class="text-danger">*</span></label>
                                                <input type="text" class="form-control input-sm" name="NO_DOK_ENG" id="NO_DOK_ENG" required/>
                                              </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group" style="display:none;" >
                                                    <label for="ID" class="control-label">ID USERS</label>
                                                    <input type="text" class="form-control input-sm" name="ID" id="ID" readonly="" />
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-6">
                                              <div class="form-group" style="display:none;" >
                                                <label for="ID_PENUGASAN" class="control-label">ID EAT</label>
                                                <input type="text" class="form-control input-sm" name="ID_PENUGASAN" id="ID_PENUGASAN" readonly="" />
                                              </div>
                                            </div> -->
                                        </div>
                                        <!-- <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="ID_PENUGASAN" class="control-label">EAT <span class="text-danger">*</span></label>
                                              <select name="ID_PENUGASAN" id="ID_PENUGASAN" class="form-control form-control-sm select-sm" data-title="Pilih Eat..." data-show-subtext="true" data-live-search="true" required>
                                        <?php
                                        // foreach ($FOREIGN as $list) {
                                        //     echo "<option value='" . $list["ID_PENGAJUAN"] . "' title='" . $list["NO_PENGAJUAN"] . " - " . $list["NOTIF_NO"] . "' $list-subtext='" . $list["NO_PENGAJUAN"] . "' $list-no='" . $list["NO_PENGAJUAN"] . "' $list-notif='" . $list["NOTIF_NO"] . "' $list-flcode='" . $list["FUNCT_LOC"] . "' $list-fltext='" . $list["FL_TEXT"] . "' $list-shorttext='" . $list["SHORT_TEXT"] . "' $list-uktext='" . $list["UK_TEXT"] . "' >" . $list["SHORT_TEXT"] . "</option>";
                                        // }
                                        ?>
                                              </select>
                                            </div>
                                          </div>
                                        </div> -->
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="BADGE" class="control-label">Badge <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="BADGE" id="BADGE" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="form-group">
                                                    <label for="NAME" class="control-label">Name <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control input-sm" name="NAME" id="NAME" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="EMAIL" class="control-label">Email <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="EMAIL" id="EMAIL" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="ROLE_ID" class="control-label">Roles <span class="text-danger">*</span></label>
                                                    <!-- <select name="ID_PENGAJUAN" id="ID_PENGAJUAN" class="form-control form-control-sm select-sm" data-title="Pilih Erf..." data-show-subtext="true" data-live-search="true" required> -->
                                                    <select name="ROLE_ID" id="ROLE_ID" class="chosen input-sm select2" style="width:100%;" required>
                                                        <?php
                                                        foreach ($roles as $list) {
                                                            echo "<option value='" . $list["ID"] . "' title='" . $list["NAME"] . "' >" . $list["NAME"] . "</option>";
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="POS_TEXT" class="control-label">Position <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="POS_TEXT" id="POS_TEXT" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="UNIT_KERJA" class="control-label">Work Unit <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="UNIT_KERJA" id="UNIT_KERJA" readonly required />
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label for="DEPT_TEXT" class="control-label">Department <span class="text-danger">*</span></label>
                                                    <input type="textarea" class="form-control input-sm" name="DEPT_TEXT" id="DEPT_TEXT" readonly required />
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-sm btn-success <?php echo "{$short_tittle}-write";
                                                        echo " {$short_tittle}-modify"; ?>" id="btn-save"> <i class="fa fa-check"></i> Save</button>
                                <button type="button" class="btn btn-sm btn-inverse" id="btn-cancel">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/others/users.js"></script>
