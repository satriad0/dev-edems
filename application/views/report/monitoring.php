
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <!-- <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Engineering Documents</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Engineering Documents</li>
            </ol>
        </div>
    </div> -->
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="card">
            <div class="card-body">
                <!-- <h4 class="card-title">Data Engineering Request Form</h4> -->
                <!-- <h6 class="card-subtitle">Data Engineering Request Form</h6> -->
                <!-- Nav tabs -->
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5">
                        <label class="control-label">Date Range</label>
                        <div class="input-daterange input-group" >
                            <input type="date" class="form-control input-sm" name="START_DATE" id="START_DATE" >
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ti-minus"></i></span>
                            </div>
                            <input type="date" class=" form-control input-sm" name="END_DATE" id="END_DATE" >
                        </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7">
                        <button class="btn btn-sm btn-success pull-right" type="button" id="toPdf"><i class="fa fa-file-text"></i>&nbsp;to Pdf</button>
                        <button class="btn btn-sm btn-info pull-right" type="button" id="toExcel"><i class="fa fa-file-excel-o"></i>&nbsp;to Excel</button>
                    </div>
                </div>
                <div class="table-responsive m-t-10">
                    <table id="tb_list" class="table display table-bordered table-striped" style="width:100%;" >
                        <thead>
                            <tr>
                                <th rowspan="2">NO</th>
                                <th style="text-align: center;" colspan="9">ENGINEERING REQUEST FORM (ERF)</th>
                                <th style="text-align: center;" colspan="4">ENGINEERING ASSIGN TASK (EAT)</th>
                                <th style="text-align: center;" colspan="3">DOKUMEN ENGINEERING</th>
                                <th style="text-align: center;" rowspan="2">(Σ) PROGRESS</th>
                                <th style="text-align: center;" rowspan="2">TRANSMITAL</th>

                                <th style="text-align: center;" rowspan="2">DURATION (DAYS)</th>
                                <th style="text-align: center;" rowspan="2">STATUS</th>
                                <th style="text-align: center;" colspan="3">DOKUMEN TENDER</th>
                            </tr>
                            <tr>
                                <th style="text-align: center;" >DESKRIPSI</th>
                                <th style="text-align: center;" >NO ERF</th>
                                <th style="text-align: center;" >NO NOTIF.</th>
                                <th style="text-align: center;" >TANGGAL</th>
                                <th style="text-align: center;" >UK PEMINTA</th>
                                <th style="text-align: center;" >UK TUJUAN</th>
                                <th style="text-align: center;" >JENIS</th>
                                <th style="text-align: center;" >NO PROYEK</th>
                                <th style="text-align: center;" >STATUS APPROVAL</th>
                                <th style="text-align: center;" >NO EAT</th>
                                <th style="text-align: center;" >TANGGAL</th>
                                <th style="text-align: center;" >STATUS APPROVAL</th>
                                <th style="text-align: center;" >NAMA PEKERJAAN</th>
                                <th style="text-align: center;" >NO DE</th>
                                <th style="text-align: center;" >TANGGAL</th>
                                <th style="text-align: center;" >STATUS APPROVAL</th>
                                <th style="text-align: center;" >NO TENDER</th>
                                <th style="text-align: center;" >TANGGAL</th>
                                <th style="text-align: center;" >STATUS APPROVAL</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!-- End PAge Content -->
        </div>


        <!-- <div class="col-lg-6"> -->
        <div class="card <?php echo "{$short_tittle}-total"; ?>">
            <div class="card-body">
                <h4 class="card-title">Total Kajian Enginering <?php echo date('Y'); ?></h4>
                <div id="morris-bar-chart"></div>
            </div>
        </div>
        <!-- </div> -->

        <!-- End Container fluid  -->
        <!-- footer -->
        <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
        <!-- End footer -->
    </div>
    <!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->

<!-- ======Graph Jquery====== -->
<script src="<?= base_url();?>assets/js/lib/morris-chart/raphael-min.js"></script>
<script src="<?= base_url();?>assets/js/lib/morris-chart/morris.js"></script>

<!-- ======default Jquery====== -->
<script src="<?= base_url();?>js/report/rpt_monitor.js"></script>
