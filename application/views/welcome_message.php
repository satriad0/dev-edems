<!-- <script src="assets/js/lib/highchart/highcharts.js"></script> -->
<link href="<?= base_url(); ?>assets/css/others/okaslider/oka_slider_model.css" rel="stylesheet">
<script src="<?= base_url(); ?>assets/js/others/okaslider/oka_slider_model.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/highchart/highcharts2.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/highchart/highcharts-modules-exporting.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/highchart/highcharts-drilldown.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/highchart/highcharts-data.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/highchart/highcharts-more.js"></script>
<!-- <script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script> -->
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row <?php echo "{$short_tittle}-slide"; ?>">
            <!-- <div class="col-md-12">
                <div class="card">
                  <div class="card-body" >
                    <div class="box" id="vmc-slide">
                      <ul>
            <?php
            // if (count($slides)>0) {
            //   foreach ($slides as $list) {
            //     echo "<!--{$list['PATHS']} -->";
            //     echo "<li";
            //     if (!empty($list['SHORT_DESC'])) {
            //       echo " title='{$list['SHORT_DESC']}'";
            //     }
            //     echo ">";
            //     if (!empty($list['PATHS'])) {
            //       echo " <img src='{$list['PATHS']}'>";
            //     }else {
            //       echo " <img src='media/upload/img/slide/no-slide.png'>";
            //     }
            //     echo "</li>";
            //   }
            // }
            ?>
                      </ul>
                    </div>
                  </div>
                </div>
            </div> -->
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body" >
                        <div class="slider_model">
                            <div class="slider_model_box">
                                <?php
                                if (count($slides) > 0) {
                                    foreach ($slides as $list) {
                                        if (!empty($list['PATHS'])) {
                                            echo " <img src='{$list['PATHS']}' title='{$list['SHORT_DESC']}' alt='{$list['DESCRIPTION']}' >";
                                        } else {
                                            echo " <img src='media/upload/img/slide/no-slide375.jpg' title='Empty' alt='No Image' >";
                                            // echo " <img src='media/upload/img/slide/no-slide.png' title='Empty' alt='No Image' >";
                                        }
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 <?php echo "{$short_tittle}-widget-erf"; ?>">
                <div class="card bg-danger p-20">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-envelope f-s-30"></i></span>
                            <!-- <span><img src="media/icon/envelope.png" class="img-responsive" style="max-width: 20%;" alt="homepage" class="dark-logo" /></span> -->
                        </div>
                        <div class="media-body media-text-right">
                            <h4 class="color-white">Engineering Request Form</h4>
                            <hr></hr>
                        </div>

                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Total Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="erfall"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Open Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="erfopen"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">In Progress Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="erfprog"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Closed Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="erfclose"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Rejected Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="erfreject"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 <?php echo "{$short_tittle}-widget-eat"; ?>">
                <div class="card bg-yellow p-20">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-wpforms f-s-30"></i></span>
                            <!-- <span><img src="media/icon/dokumen.png" class="img-responsive" style="max-width: 20%;" alt="homepage" class="dark-logo" /></span> -->
                        </div>
                        <div class="media-body media-text-right">
                            <h4 class="color-white">Engineering Assign Task</h4>
                            <hr></hr>
                        </div>

                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Total Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="eattotal"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Open Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="eatopen"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">In Progress Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="eatprog"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Closed Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="eatclose"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Rejected Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="eatreject"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 <?php echo "{$short_tittle}-widget-docs"; ?>">
                <div class="card bg-green p-20">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-book f-s-30"></i></span>
                            <!-- <span><img src="media/icon/archive.png" class="img-responsive" style="max-width: 20%;" alt="homepage" class="dark-logo" /></span> -->
                        </div>
                        <div class="media-body media-text-right">
                            <h4 class="color-white">Documents Engineering</h4>
                            <hr></hr>
                        </div>

                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Total Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="doctotal"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Open Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="docopen"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">In Progress Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="docprog"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Closed Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="docclose"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Rejected Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="docreject"></h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 <?php echo "{$short_tittle}-widget-bast"; ?>">
                <div class="card bg-primary p-20">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="fa fa-suitcase f-s-30"></i></span>
                            <!-- <span><img src="media/icon/suitcase.png" class="img-responsive" style="max-width: 20%;" alt="homepage" class="dark-logo" /></span> -->
                        </div>
                        <div class="media-body media-text-right">
                            <h4 class="color-white">BAST Documents</h4>
                            <hr></hr>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Total Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="basttotal"></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Open Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="bastopen" class=""></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">In Progress Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="bastprog"class=""></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Closed Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="bastclose" class=""></h5>
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <h5 class="color-white">Rejected Documents</h5>
                        </div>
                        <div class="media-body media-text-right">
                            <h5 class="color-white" id="bastreject"></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-lg-7 <?php //echo "{$short_tittle}-work-stat";   ?>">
                <div class="card">
                  <div class="card-title" style="text-align: center"> <h4><b>Work Status <?php // echo date("Y");   ?></b></b></h4></div>
                  <div class="card-body" >
                    <div id="hg-chart"></div>
                  </div>
                </div>
            </div> -->
            <div class="col-md-7 <?php echo "{$short_tittle}-graph-stat"; ?>">
                <div class="card">
                    <div class="card-body" >
                        <div id="chart-stat"></div>
                    </div>
                </div>
            </div>
            <!-- /# column -->
            <div class="col-lg-5 <?php echo "{$short_tittle}-work-prog"; ?>">
                <div class="card">
                    <div class="card-title" style="text-align: center"> <h4><b>Engineering on Progress</b></h4></div>
                    <div class="card-body" >
                        <div class="table-responsive recent-comment"  style="height:360px">
                            <table id="tb_prog" class="display table-hover" style="width:100%">
                                <thead style="display:none">
                                        <!-- <tr>

                                                <th>Progress</th>
                                        </tr> -->
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-md-7 <?php echo "{$short_tittle}-graph-notif"; ?>">
                <div class="card">
                    <div class="card-body" >
                        <div id="chart-notif"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 <?php echo "{$short_tittle}-graph-erf"; ?>">
                <div class="card">
                    <div class="card-body" >
                        <div id="chart-erf"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-7 <?php echo "{$short_tittle}-work-doc"; ?>">
                <div class="card">
                    <div class="card-body" >
                        <div id="chart-all"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-5 <?php echo "{$short_tittle}-graph-date"; ?>">
                <div class="card">
                    <div class="card-body" >
                        <div id="chart-date"></div>
                    </div>
                </div>
            </div>
        </div>

        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->
    <!-- footer -->
    <footer class="footer" style="margin-left: 75px;"> © 2018 PT.Sinergi Informatika Semen Indonesia (SISI)</footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- ======default Jquery====== -->
<!-- All Jquery -->
<!-- <script src="assets/js/lib/jquery/jquery.min.js"></script> -->
<!-- Bootstrap tether Core JavaScript -->
<!-- <script src="assets/js/lib/bootstrap/js/popper.min.js"></script>
<script src="assets/js/lib/bootstrap/js/bootstrap.min.js"></script> -->
<!-- slimscrollbar scrollbar JavaScript -->
<!-- <script src="assets/js/jquery.slimscroll.js"></script> -->
<!--Menu sidebar -->
<!-- <script src="assets/js/sidebarmenu.js"></script> -->
<!--stickey kit -->
<!-- <script src="assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script> -->
<!--Custom JavaScript -->
<!-- <script src="assets/js/custom.min.js"></script> -->
<!-- ====end default Jquery==== -->


    <!-- <script src="assets/js/lib/datamap/d3.min.js"></script> -->
    <!-- <script src="assets/js/lib/datamap/topojson.js"></script> -->
<script src="<?= base_url(); ?>assets/js/lib/morris-chart/raphael-min.js"></script>
<script src="<?= base_url(); ?>assets/js/lib/morris-chart/morris.js"></script>
<!-- <script src="assets/js/lib/datamap/datamap-init.js"></script> -->

    <!-- <script src="assets/js/lib/weather/jquery.simpleWeather.min.js"></script> -->
    <!-- <script src="assets/js/lib/weather/weather-init.js"></script> -->
    <!-- <script src="assets/js/lib/owl-carousel/owl.carousel.min.js"></script> -->
    <!-- <script src="assets/js/lib/owl-carousel/owl.carousel-init.js"></script> -->
<script src="<?= base_url(); ?>js/welcome.js"></script>
<!-- <script src="js/report/rpt_monitor.js"></script> -->
<!-- <script src="assets/js/lib/chartist/chartist.min.js"></script> -->
<!-- <script src="assets/js/lib/chartist/chartist-plugin-tooltip.min.js"></script> -->
<!-- <script src="assets/js/lib/chartist/chartist-init.js"></script> -->
