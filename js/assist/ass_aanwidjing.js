var oTable;

$(document).ready(function() {
  // getForeign();
  if ($('#ID_PENGAJUAN').val()) {
    onForeign();
  }
  $('.col-no').hide();
  tb_assign();
  // generateDoc();
  $('.select2').select2();

  $('input[name^=DE_CONSTRUCT_COST],input[name^=DE_ENG_COST]').priceFormat({
    prefix: '',
    centsSeparator: '.',
    centsLimit: 0,
    thousandsSeparator: ',',
    clearOnEmpty: false,
  });
});

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_assign() {
  oTable = $('#tb_awr').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'assist/aanwidjing/data_list',
      type: "POST"
    },
    columns: [{
        "data": "RNUM",
        "sClass": "right"
      }, {
        "className": 'details-control',
        "orderable": false,
        "data": null,
        "defaultContent": ''
      }, {
        "data": "CREATE_AT",
        "width": 75
      }, {
        "data": "NO_PENDAMPINGAN"
      },
      {
        "data": "NOTIFIKASI"
      },
      {
        "data": "NO_DOK_ENG"
      },
      {
        "data": "PACKET_TEXT"
      },
      {
        "data": "STATUS",
        "mRender": function(row, data, index) {
          if (index['STATUS'].indexOf('Approved') != -1) {
            return '<center><p style="color: green">' + index['STATUS'] + ' </p></center> ';
          } else if (index['STATUS'].indexOf('Rejected') != -1) {
            return '<center><p style="color: red">' + index['STATUS'] + ' </p></center>';
          } else {
            return '<center><p style="color: blue">' + index['STATUS'] + ' </p></center>';
            // return '';
          }
        }
      },
      {
        "data": "UK_TEXT"
      },
      {
        "data": "LOKASI"
      },
      {
        "data": "COMP_TEXT"
      },
      {
        "data": "ID",
        "width": 70,
        "mRender": function(row, data, index) {
          sresend = '';
          if ((!index['APPROVE2_AT'] && index['APPROVE2_BY']) || (!index['APPROVE1_AT'] && index['APPROVE1_BY'])) {
            sresend = '<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
          }
          sdis = '';
          if (page_state.indexOf(page_short + '-delete=1') == -1 || index['STATUS'].indexOf('In Approval') == -1) {
            sdis = 'disabled';
          }
          if (page_state.indexOf('special_access=false') == -1 && page_state.indexOf(page_short + '-delete=1') != -1) {
            sdis = ';'
          }
          if (index['STATUS'].indexOf('Approved') != -1 && page_state.indexOf(page_short + '-spc-del=1') != -1) {
            sdis = '';
          }
          sexp = '';
          if (page_state.indexOf(page_short + '-export=1') == -1) {
            sexp = ' style="display:none;"';
          }
          return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download (Download Lampiran -> di tombol view)"' + sexp + ' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></button>' + sresend + '<button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button><button class="btnUpl btn btn-xs btn-warning" data-toggle="modal" data-target="#uploadModal" title="Upload"><i class="fa fa-upload"></i></button></div></center>';
        }
      },
    ],
    "order": [
      [0, "desc"]
    ],
    // "createdRow": function(row, data, dataIndex) {
    //   if (Number(data['PENDING']) < 1) {
    //     $('td', row).addClass("color-danger text-danger");
    //   }
    //
    // }
  });

  $('#tb_awr tbody').on('click', 'td.details-control', function() {
    var tr = $(this).closest('tr');
    var row = oTable.row(tr);
    var data = row.data();

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {

      var html = '';
      var d = row.data();
      var data;
      var approveAt = '';
      var status = '';
      var i = 0;

      html += '<div class="container" style="width:1000px;height:400px;overflow-y:scroll">' +
        '<table class="table display table-bordered table-striped" id="tableprogress">' +
        '<thead>' +
        '<tr>' +
        '<th>No</th>' +
        '<th>Type</th>' +
        '<th>Status</th>' +
        '<th>Position</th>' +
        '<th>Date</th>' +
        '<th>By</th>' +
        '<th style="width:100px">Note</th>' +
        '</tr>' +
        '</thead>' +
        '<tbody>';
      html += '<tr>' +
        '<td>' + (i + 1) + '</td>' +
        '<td>Created</td>' +
        '<td>Approved</td>' +
        '<td>' + data.UK_TEXT + '</td>' +
        //if(item.UPDATE_AT != null) {
        '<td>' + data.CREATE_AT + '</td>' +
        '<td>' + data.CREATE_BY + '</td>' +
        '<td style="width:280px">-</td>' +
        '</tr>';
      // reject 1
      if (data.APPROVE1_BY && !data.APPROVE2_BY && data.REJECT_AT) {
        html += '<tr>' +
          '<td>' + (i + 1) + '</td>' +
          '<td>TTD</td>';
        if (data.REJECT_AT) html += '<td>Rejected</td>';
        else html += '<td>-</td>';
        html += '<td>' + data.APPROVE1_JAB + '</td>';
        note = '-';
        if (data.REJECT_NOTE) {
          note = data.REJECT_NOTE;
        }
        html += '<td>' + data.REJECT_AT + '</td>' +
          '<td>' + data.REJECT_BY + '</td>' +
          '<td style="width:280px">' + note + '</td>';
        html += '</tr>';
      } else {
        i++;
        html += '<tr>' +
          '<td>' + (i + 1) + '</td>' +
          '<td>TTD</td>';
        if (data.APPROVE1_AT) html += '<td>Approved</td>';
        else html += '<td>Pending</td>';
        html += '<td>' + data.APPROVE1_JAB + '</td>';
        var note = '-';
        if (data.APPROVE1_BY) {
          if (data.NOTE1) {
            note = data.NOTE1;
          }
          html += '<td>' + data.APPROVE1_AT + '</td>' +
            '<td>' + data.APPROVE1_BY + '</td>' +
            '<td style="width:280px">' + note + '</td>';
        }
      }
      // reject 2
      if (data.APPROVE1_BY && data.APPROVE2_BY && data.REJECT_AT) {
        html += '<tr>' +
          '<td>' + (i + 1) + '</td>' +
          '<td>TTD</td>';
        if (data.REJECT_AT) html += '<td>Rejected</td>';
        else html += '<td>-</td>';
        html += '<td>' + data.APPROVE2_JAB + '</td>';
        note = '-';
        if (data.REJECT_NOTE) {
          note = data.REJECT_NOTE;
        }
        html += '<td>' + data.REJECT_AT + '</td>' +
          '<td>' + data.REJECT_BY + '</td>' +
          '<td style="width:280px">' + note + '</td>';
        html += '</tr>';
      } else {
        // approval DE
        if (data.APPROVE2_BY) {
          html += '<tr>' +
            '<td>' + (i + 1) + '</td>' +
            '<td>TTD</td>';
          if (data.APPROVE2_AT) html += '<td>Approved</td>';
          else html += '<td>Pending</td>';
          html += '<td>' + data.APPROVE2_JAB + '</td>';
          note = '-';
          if (data.NOTE2) {
            note = data.NOTE2;
          }
          html += '<td>' + data.APPROVE2_AT + '</td>' +
            '<td>' + data.APPROVE2_BY + '</td>' +
            '<td style="width:280px">' + note + '</td>';
          html += '</tr>';
        }
      }
      // upload file
      if (data.APPROVE2_BY && data.FILES) {
        arFile = data.FILES.split('/');

        html += '<tr>' +
          '<td>' + (i + 1) + '</td>' +
          '<td>File</td>';
        if (data.FILES_AT) html += '<td>Uploaded</td>';
        else html += '<td>-</td>';
        html += '<td>Design Engineering</td>';
        note = '-';
        if (data.FILES) {
          note = '<a href="' + data.FILES + '" rel="noopener noreferrer" target="_blank" ><i class="fa fa-file-pdf-o"></i> ' + arFile[(arFile.length - 1)] + '</a>';
        }
        html += '<td>' + data.FILES_AT + '</td>' +
          '<td>' + data.UPDATE_BY + '</td>' +
          '<td style="width:280px">' + note + '</td>';
        html += '</tr>';
      }

      html +=
        '</tbody>' +
        '</table>' +
        '</div>';
      // console.log(html);
      row.child(html).show();
      tr.addClass('shown');

      return html;
    }
  });
}

function getForeign() {
  $.ajax({
    type: 'POST',
    url: 'assist/aanwidjing/foreign',
    success: function(json) {
      json = $.parseJSON(json);
      $('#ID_PENGAJUAN').html('');
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          // foreigns += ('<option value >Please select...</option>');
          $.each(json['data'], function(i, data) {
            foreigns += ('<option value="' + data['ID_PENGAJUAN'] + '" title="' + data['NO_PENGAJUAN'] + ' - ' + data['NOTIF_NO'] + '" data-id="' + data['ID'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-id_dok="' + data['ID_DOK_ENG'] + '" data-no="' + data['NO_PENGAJUAN'] + '" data-packet="' + data['PACKET_TEXT'] + '" data-notif="' + data['NOTIF_NO'] + '" data-flcode="' + data['FUNCT_LOC'] + '" data-fltext="' + data['FL_TEXT'] + '" data-shorttext="' + data['SHORT_TEXT'] + '" data-uktext="' + data['UK_TEXT'] + '" data-custreq="' + data['CUST_REQ'] + '" data-lokasi="' + data['LOKASI'] + '" data-const="' + data['CONSTRUCT_COST'] + '" data-eng="' + data['ENG_COST'] + '" data-stat="' + data['STAT'] + '" >' + data['SHORT_TEXT'] + '</option>');
          });
        } else {
          foreigns = ('<option value >Empty...</option>');
        }
        $('#ID_PENGAJUAN').html(foreigns);
        $('#ID_PENGAJUAN').select2();
        onForeign();
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function onForeign() {
  $('#ID').val($('#ID_PENGAJUAN :selected').attr('data-id'));
  $('#NO_PENDAMPINGAN').val($('#ID_PENGAJUAN :selected').attr('data-trnno'));
  $('#NO_ERF').val($('#ID_PENGAJUAN :selected').attr('data-no'));
  $('#NOTIF_NO').val($('#ID_PENGAJUAN :selected').attr('data-notif'));
  $('#SHORT_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-shorttext'));
  $('#PACKET_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-packet'));
  $('#UK_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-uktext'));
  $('#FL_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-lokasi'));
  // $('#OBJECTIVE').val($('#ID_PENGAJUAN :selected').attr('data-fltext'));
  $('#CUST_REQ').val($('#ID_PENGAJUAN :selected').attr('data-custreq'));
  $('#DE_CONSTRUCT_COST').val(toCurrency($('#ID_PENGAJUAN :selected').attr('data-const')));
  $('#DE_ENG_COST').val(toCurrency($('#ID_PENGAJUAN :selected').attr('data-eng')));

  var stat = $('#ID_PENGAJUAN :selected').attr('data-stat');
  if (stat == 'New') {
    $('.div_app').show();
    $('.div_file').hide();
  } else if (stat == 'Upload') {
    $('.div_app').hide();
    $('.div_file').show();
  }
}

$('#ID_PENGAJUAN').on('change', function() {
  onForeign();
})

function select_content_change(selecttag, urlselect, placeholder) {
  $('' + selecttag).select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: placeholder,
    theme: 'bootstrap',
    ajax: {
      dataType: 'json',
      url: urlselect,
      // data: data,
      delay: 50,
      type: 'POST',
      data: function(params) {
        return {
          search: params.term
          // search: params.term,
          // additional_data: data
        }
      },
      processResults: function(data, page) {
        console.log(data);
        return {
          results: $.map(data['data'], function(obj) {
            console.log(obj);
            if (obj.NOBADGE) {
              // var objectcode = '000'+obj.ID_DATA+'';
              // var stdcode = objectcode.slice(-3);

              return {
                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA,
                // photo: obj.photo
              };
            } else {
              return {
                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA,
                // photo: obj.photo
              };
            }

          })
        };
      },
    }
  });
  $('' + selecttag).trigger('change');
}

function load_selected_option(key, id, text) {
  $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
  $(key).select2('data', {
    id: id,
    label: text
  });
  $(key).trigger('change'); // Notify any JS components that the value changed
}

function reset_form(reset = null) {
  $('#ID').val('');
  $('#NO_PENDAMPINGAN').val('');
  $('#NO_ERF').val('');
  $('#NOTIF_NO').val('');
  $('#SHORT_TEXT').val('');
  $('#UK_TEXT').val('');
  $('#FL_TEXT').val('');
  $('#NOTE').val('');
  $('#FILES').val('');

  generateDoc();
  getForeign();

  $('.col-no').hide();
  $("#FILES").prop('required', false);
  $("#btn-save").removeClass("disabled");
  $("#btn-save").prop("disabled", false);
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('.select2').val('').trigger('change');

  $('#ul-new').text('Create Aanwidjing Task');
  $('#btn-save').show();
  $('#file_name').removeClass('fa fa-file-pdf-o');
  $('#file_uri').attr('href', '');
  $('#file_name').text('');
  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }
}

// export
$(document).on('click', ".btnExport", function() {
  var data = oTable.row($(this).parents('tr')).data();
  window.location.assign('assist/aanwidjing/export_pdf/' + data['ID']);
});

$("#btn-cancel").on("click", function() {
  reset_form();
});

// event resend mail
$(document).on('click', ".btnResend", function() {
  $('.title_ukur').text('Resend Email');
  var data = oTable.row($(this).parents('tr')).data();

  var form_data = new FormData();
  form_data.append('ID', data['ID']);

  $.ajax({
    url: 'assist/aanwidjing/resend', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Resend Email : NOT Success! " + error,
        type: "error"
      });
    },
    success: function(json) {
      if (json['status'] == 200) {
        swal({
          title: "Email was sent.",
          type: "success"
        });
        // reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
});

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();

  $('#ID').val(data['ID']);
  $('#NO_PENDAMPINGAN').val(data['NO_PENDAMPINGAN']);
  $('.col-no').show();
  if (data['ID_PENGAJUAN']) {
    $('#ID_PENGAJUAN').html('<option selected value="' + data['ID_PENGAJUAN'] + '" title="' + data['NO_PENGAJUAN'] + ' - ' + data['NOTIF_NO'] + '" data-id="' + data['ID'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-id_dok="' + data['ID_DOK_ENG'] + '" data-no="' + data['NO_PENGAJUAN'] + '" data-packet="' + data['PACKET_TEXT'] + '" data-notif="' + data['NOTIF_NO'] + '" data-flcode="' + data['FUNCT_LOC'] + '" data-fltext="' + data['FL_TEXT'] + '" data-shorttext="' + data['SHORT_TEXT'] + '" data-uktext="' + data['UK_TEXT'] + '" data-stat="' + data['STAT'] + '" >' + data['SHORT_TEXT'] + '</option>');
  }
  $('#NO_ERF').val(data['NO_PENGAJUAN']);
  $('#NOTIF_NO').val(data['NOTIF_NO']);
  $('#SHORT_TEXT').val(data['SHORT_TEXT']);
  $('#UK_TEXT').val(data['UK_TEXT']);
  $('#FL_TEXT').val(data['FL_TEXT']);
  $('#NOTE').val(data['NOTE']);
  $('#HDR_TEXT').val(data['HDR_TEXT']);
  $('#MID_TEXT').val(data['MID_TEXT']);
  $('#FTR_TEXT').val(data['FTR_TEXT']);
  if (data['TIM_PENGKAJI'] && data['TIM_PENGKAJI']!='[]') {
    var arpengkaji = eval(data['TIM_PENGKAJI']);
    $(':checkbox ').each(function(i) {
      if (data['TIM_PENGKAJI'].indexOf('"' + $(this).val() + '"') != -1) {
        $(this).attr('checked', 'checked');
      }
    });
  }

  if ($('#LVL').val() == '0') {
    $('.disMan').show();
    $('.disSM').hide();
    $('#MANAGER').val(data['APPROVE2_BADGE'] + " - " + data['APPROVE2_BY'] + " - " + data['APPROVE2_JAB']).trigger('change');
  } else {
    $('.disMan').hide();
    $('.disSM').show();
    $('#SM_APP').val(data['APPROVE2_BADGE'] + " - " + data['APPROVE2_BY'] + " - " + data['APPROVE2_JAB']).trigger('change');
  }
  if (data['FILES']) {
    var aFiles = data['FILES'].split('/');

    $('#file_uri').attr('href', data['FILES']);
    $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#file_name').addClass('fa fa-file-pdf-o');
  }

  $('.select2').select2({
    tags: true
  });
  $('.selectpicker').selectpicker('refresh');
  setActiveTab('ul-new', 'new-form');
  $('#ul-new').text('View Aanwidjing Task');

  if (data['APPROVE2_AT'] && data['FILES']) {
    $('#btn-save').hide();
    $('.div_app').show();
    $('.div_file').show();
  } else if (data['APPROVE2_AT'] && !data['FILES']) {
    $('#ul-new').text('Upload Aanwidjing Task');
    $('.div_app').hide();
    $('.div_file').show();
    $("#FILES").prop('required', true);
  } else {
    $('.div_app').show();
    $('.div_file').hide();
  }
  if (page_state.indexOf(page_short + '-modify=1') == -1) {
    $('.' + page_short + '-modify').hide();
  }
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").prop("disabled", true);
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  no_pendampingan = $('#NO_PENDAMPINGAN').val();
  id_pengajuan = $('#ID_PENGAJUAN').val();
  notifikasi = $('#NOTIF_NO').val();
  no_erf = $('#NO_ERF').val();
  short_text = $('#SHORT_TEXT').val();
  hdr_text = $('#HDR_TEXT').val();
  mid_text = $('#MID_TEXT').val();
  ftr_text = $('#FTR_TEXT').val();
  note = $('#NOTE').val();
  strpath = document.getElementById('FILES').files[0];
  strfile = $('#FILES').attr('data-url');
  var val = [];
  $(':checkbox:checked ').each(function(i) {
    val[i] = $(this).val();
  });
  tim_pengkaji = JSON.stringify(val);

  var stat = $('#ID_PENGAJUAN :selected').attr('data-stat');
  var app = '';
  lvl = $('#LVL').val();
  if (stat == 'New') {
    if (lvl == '0') {
      app = $('#MANAGER').val().split(" - ");
    } else {
      app = $('#SM_APP').val().split(" - ");
    }
  }

  var formURL = "";

  var form_data = new FormData();
  form_data.append('ID', id);
  form_data.append('NO_PENDAMPINGAN', no_pendampingan);
  form_data.append('ID_PENGAJUAN', id_pengajuan);
  form_data.append('NOTIFIKASI', notifikasi);
  form_data.append('NO_ERF', no_erf);
  form_data.append('SHORT_TEXT', short_text);
  form_data.append('HDR_TEXT', hdr_text);
  form_data.append('MID_TEXT', mid_text);
  form_data.append('FTR_TEXT', ftr_text);
  form_data.append('TIM_PENGKAJI', tim_pengkaji);
  // form_data.append('NOTE', note);
  if (app) {
    form_data.append('APPROVE2_BADGE', app[0]);
    form_data.append('APPROVE2_BY', app[1]);
    form_data.append('APPROVE2_JAB', app[2]);
  }
  if (strpath) {
    form_data.append('STRPATH', strpath);
  } else {
    if (strfile) {
      form_data.append('FILES', strfile);
    }
  }

  var uri = 'create';
  if (id) {
    uri = 'update';
  }

  $.ajax({
    url: 'assist/aanwidjing/' + uri, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
    },
    success: function(json) {
      if (json['status'] == 200) {
        tb_assign();
        swal({
          title: "Data Saved!\n" + no_pendampingan,
          type: "success"
        });
        reset_form('yes');
      } else {
        swal({
          title: "Data Save : NOT Success! ",
          type: "error"
        });
      }
      $("#btn-save").prop("disabled", false);
      $("#btn-save").removeClass("disabled");
    }
  });

  $("#btn-save").prop("disabled", false);
  $("#btn-save").removeClass("disabled");
  return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['NO_PENDAMPINGAN'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'assist/aanwidjing/delete/' + data['ID'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_assign();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});

function generateDoc() {
  var tahun;
  var lokasi;
  var Nomor;
  $.ajax({
    type: 'POST',
    url: 'assist/aanwidjing/generateNoDoc',
    //data:{'id':id},
    success: function(data) {
      // console.log("test " + data);
      try {
        data = JSON.parse(data);
        Nomor = data.nomor;
        tahun = data.tahun;

        $("#NO_PENDAMPINGAN").val("/" + "/" + "/" + "PT/" + Nomor + "/" + "/" + tahun);
      } catch (e) {
        alert(e);
      } finally {

      }

    },
    error: function() {
      console.log("error");
    }
  });
}
