var oTable;

$(document).ready(function () {
    // getForeign();
    if ($('#ID_PENUGASAN').val()) {
        onForeign();
        onChange();
    }
    tb_dok_eng();
    
    // Setup - add a text input to each footer cell
    $('#tb_dok_eng_tr_search th').each( function () {
        var title = $(this).text();
        $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
    } ); 
     
    
    $('.select2').select2();

    $('#NO_DOK_ENG').hide();
    $('input[name^=NOMINAL]').priceFormat({
        prefix: '',
        centsSeparator: '.',
        centsLimit: 0,
        thousandsSeparator: ',',
        clearOnEmpty: false,
    });
});

function generateDoc() {
    var tahun;
    var lokasi;
    var Nomor;
    $.ajax({
        type: 'POST',
        url: 'dok_eng/documents/generateNoDoc',
        success: function (data) {
            try {
                data = JSON.parse(data);
                Nomor = data.nomor;
                tahun = data.tahun;

                $("#NO_DOK_ENG").val("/" + "/" + "/" + "TR/" + Nomor + "/" + "/" + tahun);

            } catch (e) {
                alert(e);
            } finally {

            }

        },
        error: function () {
            console.log("error");
        }
    });
}

function setActiveTab(ul_id = null, content_id = null) {
    $(".nav-link").removeClass("active show");
    $('#' + ul_id).addClass('active show');
    $(".tab-pane").removeClass("active show");
    $('#' + content_id).addClass('active show');
}

function tb_dok_eng() {
    oTable = $('#tb_dok_eng').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: 'dok_eng/documents/data_list',
            type: "POST"
        },
        initComplete: function () {
            
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
        columns: [{
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "CREATE_AT",
                "width": 75
            },
            {
                "data": "NO_DOK_ENG"
            },
            {
                "data": "NOTIFIKASI"
            },
            {
                "data": "NO_PENGAJUAN"
            },
            {
                "data": "NO_PENUGASAN"
            },
            {
                "data": "PACKET_TEXT"
            },
            {
                "data": "STATUS",
                "mRender": function (row, data, index) {
                    if (index['STATUS'].indexOf('Approved') != -1) {
                        return '<center><p style="color: green">' + index['STATUS'] + ' </p></center> ';
                    } else if (index['STATUS'].indexOf('Rejected') != -1) {
                        return '<center><p style="color: red">' + index['STATUS'] + ' </p></center>';
                    } else {
                        return '<center><p style="color: blue">' + index['STATUS'] + ' </p></center>';
                    }
                }
            },
            {
                "data": "ID",
                "width": 70,
                "mRender": function (row, data, index) {
                    var role = $("#id_role").val();
                    sresend = '';
                    if (index['STATUS'] != 'Rejected' && ((!index['APPROVE3_AT'] && index['ECE_LIMIT'] == '1') || (index['STATUS'].indexOf('SM') == -1 && index['ECE_LIMIT'] == '0'))) {
                        sresend = '<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
                    }
                    srks = '';
                    if (index['RKS_FILE'] && page_state.indexOf(page_short + '-exp-tr=1') != -1 && !index['KAJIAN_FILE']) {
                        if( index['STATUS'].indexOf('SM') != -1 || index['APPROVE3_AT'] || role == 2 || role == 3 || role == 5){
                            srks = '<button class="btnExpPc btn btn-xs btn-success" data-toggle="tooltip" data-tipe="TR" dt_file="Dokumen TOR(RKS)" style="background-color: #00a69c;" title="Download TOR" ><i class="fa fa-download"></i> T </button>';
                        }
                    }
                    sbq = '';
                    if (index['BQ_FILE'] && page_state.indexOf(page_short + '-exp-bq=1') != -1 && !index['KAJIAN_FILE']) {
                        if( index['STATUS'].indexOf('SM') != -1 || index['APPROVE3_AT'] || role == 2 || role == 3 || role == 5){
                        sbq = '<button class="btnExpPc btn btn-xs btn-warning" data-toggle="tooltip" data-tipe="BQ" dt_file="Dokumen BQ" title="Download BQ" ><i class="fa fa-download"></i> B </button>';
                        }
                    }
                    sdraw = '';
                    if (index['DRAW_FILE'] && page_state.indexOf(page_short + '-exp-draw=1') != -1 && !index['KAJIAN_FILE']) {
                        if( index['STATUS'].indexOf('SM') != -1 || index['APPROVE3_AT'] || role == 2 || role == 3 || role == 5){
                            sdraw = '<button class="btnExpPc btn btn-xs btn-info" data-toggle="tooltip" data-tipe="DRW" dt_file="Dokumen Drawing" style="background-color: #288cff;" title="Download Drawing" ><i class="fa fa-download"></i> D </button>';
                        }
                    }
                    sece = '';
                    if (index['ECE_FILE'] && page_state.indexOf(page_short + '-exp-ece=1') != -1 && !index['KAJIAN_FILE']) {
                        if( index['STATUS'].indexOf('SM') != -1 || index['APPROVE3_AT'] || role == 2 || role == 3 || role == 5){
                            sece = '<button class="btnExpPc btn btn-xs btn-danger" data-toggle="tooltip" data-tipe="EC" dt_file="Dokumen ECE" style="background-color: #c00c16;" title="Download ECE" ><i class="fa fa-download"></i> E </button>';
                        }
                    }
                    skj = '';
                    if (index['KAJIAN_FILE'] && page_state.indexOf(page_short + '-exp-kaj=1') != -1) {
                        if( index['STATUS'].indexOf('SM') != -1 || index['APPROVE3_AT'] || role == 2 || role == 3 || role == 5){
                            skj = '<button class="btnExpPc btn btn-xs btn-primary" data-toggle="tooltip" data-tipe="KJ" dt_file="Dokumen Kajian" style="background-color: #a85232;" title="Download Kajian" ><i class="fa fa-download"></i> K </button>';
                        }
                    }
                    sdis = '';
                    if (page_state.indexOf(page_short + '-delete=1') == -1 || index['STATUS'].indexOf('In Approval') == -1) {
                        sdis = 'disabled';
                    }
                    if (page_state.indexOf(page_short + '-spc-del=1') == -1) {
                        sdis = '';
                    }
                    strm = '';
                    if (index['ID_BAST']) {
                        strm = '<button class="btnExportBast btn btn-xs btn-white" data-toggle="tooltip" data-tipe="TF" dt_file="Dokumen Transmital" title="Download Transmital" ><i class="fa fa-download"></i> TF </button>';
                    }
                    sexp = '';
                    if (page_state.indexOf(page_short + '-export=1') == -1) {
                        sexp = ' style="display:none;"';
                    }
                    sbgroup2 = '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download"' + sexp + ' ><i class="fa fa-download"></i></button>' + srks + sdraw + sbq + sece + skj + strm + '</div></center>';
                    sbgroup = '';
                    return '<center><div style="float: none;" class="btn-group">' + sbgroup + '<button class="btnLogDownload btn btn-xs btn-info" data-toggle="tooltip"  title="Histori Download" ><i class="fa fa-history"></i></button><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></button>' + sresend + '<button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button></div></center>' + sbgroup2;
                }
            },
        ],
        "order": [
            [0, "desc"]
        ],
        "createdRow": function (row, data, dataIndex) {
            // if (Number(data['PENDING']) < 1) {
            if (!data['ID_BAST']) {
                $('td', row).addClass("color-danger text-danger");
            }
        }
    });

    $('#tb_dok_eng tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = oTable.row(tr);

        if (row.child.isShown()) {
            row.child.hide();
            tr.removeClass('shown');
        } else {

            var html = '';
            var d = row.data();
            var data;
            var approveAt = '';
            var status = '';
            var i = 0;

            html += '<div class="container" style="width:1000px;height:400px;overflow-y:scroll">' +
                    '<table class="table display table-bordered table-striped" id="tableprogress">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>No</th>' +
                    '<th>Type</th>' +
                    '<th>Status</th>' +
                    '<th>Bureau</th>' +
                    '<th>Date</th>' +
                    '<th>By</th>' +
                    '<th style="width:100px">Note</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>';

            //paraf
            $.ajax({
                url: 'dok_eng/documents/get_app',
                data: {
                    id: d.ID,
                    tipe: 'CRT'
                },
                dataType: 'json',
                type: 'POST',
                beforeSend: function () {},
                success: function (result) {
                    data = result;
                    $.map(data, function (item, index) {
                        var date = item.UPDATE_AT;
                        if (!date) {
                            date = '-';
                        }
                        if (item.NOTE == null) {
                            item.NOTE = '-';
                        }
                        html += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td>Created</td>' +
                                '<td>' + item.STATUS + '</td>' +
                                '<td>' + item.APP_JAB + '</td>' +
                                '<td>' + date + '</td>' +
                                '<td>' + item.APP_NAME + '</td>' +
                                '<td style="width:280px">' + item.NOTE + '</td>' +
                                '</tr>';
                        i++;
                    });
                    return html;
                },
                error: function () {

                },
                complete: function () {
                    $.ajax({
                        url: 'dok_eng/documents/get_paraf',
                        data: {
                            id: d.ID
                        },
                        dataType: 'json',
                        type: 'POST',
                        beforeSend: function () {},
                        success: function (result) {
                            data = result;
                            $.map(data, function (item, index) {
                                var date = item.UPDATE_AT;
                                if (!date) {
                                    date = '-';
                                }
                                if (item.NOTE == null) {
                                    item.NOTE = '-';
                                }
                                html += '<tr>' +
                                        '<td>' + (i + 1) + '</td>' +
                                        '<td>Paraf</td>' +
                                        '<td>' + item.STATUS + '</td>' +
                                        '<td>' + item.APP_JAB + '</td>' +
                                        '<td>' + date + '</td>' +
                                        '<td>' + item.APP_NAME + '</td>' +
                                        '<td style="width:280px">' + item.NOTE + '</td>' +
                                        '</tr>';
                                i++;
                            });
                            return html;
                        },
                        error: function () {

                        },
                        complete: function () {
                            //TTD
                            $.ajax({
                                url: 'dok_eng/documents/get_ttd',
                                data: {
                                    id: d.ID
                                },
                                dataType: 'json',
                                type: 'POST',
                                beforeSend: function () {},
                                success: function (result) {
                                    data = result;
                                    $.map(data, function (item, index) {
                                        if (item.UPDATE_AT == null) {
                                            item.UPDATE_AT = '-';
                                        }

                                        if (item.NOTE == null) {
                                            item.NOTE = '-';
                                        }
                                        html += '<tr>' +
                                                '<td>' + (i + 1) + '</td>' +
                                                '<td>TTD</td>' +
                                                '<td>' + item.STATUS + '</td>' +
                                                '<td>' + item.APP_JAB + '</td>' +
                                                '<td>' + item.UPDATE_AT + '</td>' +
                                                '<td>' + item.APP_NAME + '</td>' +
                                                '<td style="width:280px">' + item.NOTE + '</td>' +
                                                '</tr>';
                                        i++;
                                    });
                                    return html;
                                },
                                error: function () {

                                },
                                complete: function () {
                                    $.ajax({
                                        url: 'dok_eng/documents/get_ttd_smgm',
                                        data: {
                                            id: d.ID
                                        },
                                        dataType: 'json',
                                        type: 'POST',
                                        beforeSend: function () {},
                                        success: function (result) {
                                            data = result;
                                            $.map(data, function (item, index) {
                                                if (item.APPROVE1_AT == null) {
                                                    item.APPROVE1_AT = '-';
                                                }
                                                if (item.APPROVE2_AT == null) {
                                                    item.APPROVE2_AT = '-';
                                                }
                                                if (item.APPROVE3_AT == null) {
                                                    item.APPROVE3_AT = '-';
                                                }
                                                if (item.NOTE1 == null) {
                                                    item.NOTE1 = '-';
                                                }
                                                if (item.NOTE2 == null) {
                                                    item.NOTE2 = '-';
                                                }
                                                if (item.NOTE3 == null) {
                                                    item.NOTE3 = '-';
                                                }

                                                if (item.APPROVE3_AT != null && item.APPROVE3_AT != '-') {
                                                    html += '<tr>' +
                                                            '<td>' + (i + i) + '</td>' +
                                                            '<td>TTD Department</td>' +
                                                            '<td>Approved</td>' +
                                                            '<td>' + item.APPROVE3_JAB + '</td>' +
                                                            '<td>' + item.APPROVE3_AT + '</td>' +
                                                            '<td>' + item.APPROVE3_BY + '</td>' +
                                                            '<td style="width:280px"> ' + item.NOTE3 + '</td>' +
                                                            '</tr>';
                                                } else if (item.REJECT_BY != null) {
                                                    html += '<tr>' +
                                                            '<td>' + (i + 1) + '</td>' +
                                                            '<td>TTD Department</td>' +
                                                            '<td>Rejected</td>' +
                                                            '<td>' + item.APPROVE3_JAB + '</td>' +
                                                            '<td>' + item.REJECT_DATE + '</td>' +
                                                            '<td>' + item.REJECT_BY + '</td>' +
                                                            '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                                                            '</tr>';
                                                } else {
                                                    if (item.ECE_LIMIT == '1') {
                                                        html += '<tr>' +
                                                                '<td>' + (i + 1) + '</td>' +
                                                                '<td>TTD Department</td>' +
                                                                '<td>null</td>' +
                                                                '<td>' + item.APPROVE3_JAB + '</td>' +
                                                                '<td>' + item.APPROVE3_AT + '</td>' +
                                                                '<td>' + item.APPROVE3_BY + '</td>' +
                                                                '<td style="width:280px">' + item.NOTE3 + '</td>' +
                                                                '</tr>';
                                                    }
                                                }

                                            });
                                            return html;
                                        },
                                        error: function () {

                                        },
                                        complete: function () {
                                            html +=
                                                    '</tbody>' +
                                                    '</table>' +
                                                    '</div>';
                                            row.child(html).show();
                                            tr.addClass('shown');
                                            return html;
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });

        }
    });
}

function getForeign() {
    $.ajax({
        type: 'POST',
        url: 'dok_eng/documents/foreign',
        success: function (json) {
            json = $.parseJSON(json);
            $('#ID_PENUGASAN').html('');
            foreigns = '';
            if (json) {
                if (json['status'] == 200) {
                    $.each(json['data'], function (i, data) {
                        sAdd = '';
                        if (i == 0) {
                            sAdd = 'Selected';
                        }
                        foreigns += ('<option value="' + data['ID_PENUGASAN'] + '" title="' + data['NO_PENUGASAN'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENUGASAN'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" ' + sAdd + ' >' + data['NO_PENUGASAN'] + ' - ' + data['PENGAJUAN'] + '</option>');
                    });
                } else {
                    foreigns = ('<option value >Empty...</option>');
                }
                $('#ID_PENUGASAN').html(foreigns);
                $('.select2').select2();
                onForeign();
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

function getForeignDtl(f_id = null) {
    $.ajax({
        type: 'POST',
        url: 'dok_eng/documents/foreign_dtl/' + f_id,
        success: function (json) {
            json = $.parseJSON(json);
            $('#ID_PACKET').html('');
            foreigns = '';
            if (json) {
                if (json['status'] == 200) {
                    foreigns = ('<option value >Please select...</option>');
                    $.each(json['data'], function (i, data) {
                        foreigns += ('<option value="' + data['ID'] + '" >' + data['DESKRIPSI'] + '</option>');
                        if (/detail/i.test(data['TIPE'])) {
                            var e = document.getElementById("kajian");
                            var f = document.getElementById("capex");
                            //console.log(e);
                            e.style.display = "none";
                            f.style.display = "block";
                        } else {
                            var e = document.getElementById("capex");
                            var f = document.getElementById("kajian");
                            //console.log(e);
                            e.style.display = "none";
                            f.style.display = "block";
                        }
                    });
                } else {
                    foreigns = ('<option value >Empty...</option>');
                }
                $('#ID_PACKET').html(foreigns);
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

function select_content_change(selecttag, urlselect, placeholder, data) {
    $('' + selecttag).select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: placeholder,
        theme: 'bootstrap',
        ajax: {
            dataType: 'json',
            url: urlselect,
            data: data,
            delay: 50,
            type: 'POST',
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                console.log(data);
                return {
                    results: $.map(data['data'], function (obj) {
                        console.log(obj);
                        if (obj.ID) {
                            return {
                                id: obj.ID,
                                text: obj.DESKRIPSI,
                            };
                        } else {
                            return {
                                id: obj.ID,
                                text: obj.DESKRIPSI,
                            };
                        }

                    })
                };
            },
        }
    });
}

function load_selected_option(key, id, text) {
    $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
    $(key).select2('data', {
        id: id,
        label: text
    });
    $(key).trigger('change'); // Notify any JS components that the value changed
}

function onForeign() {
    $('#NO_PENUGASAN').val($('#ID_PENUGASAN :selected').attr('data-no'));
    $('#NOTIFIKASI').val($('#ID_PENUGASAN :selected').attr('data-notif'));
    $('#PENGAJUAN').val($('#ID_PENUGASAN :selected').attr('data-pengajuan'));
    $('#OBJECTIVE').val($('#ID_PENUGASAN :selected').attr('data-object'));
    getForeignDtl($('#ID_PENUGASAN :selected').val());
    onChange();
}

function onChange() {
    var no_dok = $("#NO_DOK_ENG").val();
    var arr = no_dok.split("/");
    var pengajuan = $("#NO_PENUGASAN").val();
    var kddis = $('#COLOR :selected').attr('data-kddis');
    // console.log(pengajuan);
    var arr2 = pengajuan.split("/");
    $("#NO_DOK_ENG").val(arr2[0] + "/" + arr2[1] + "/" + kddis + arr2[2].substring(1, 2) + "/" + arr[3] + "/" + arr[4] + "/" + arr2[5] + "/" + arr[6]);
}

$('#ID_PENUGASAN').on('change', function () {
    onForeign();
    onChange();
})

$('#COLOR').on('change', function () {
    onChange();
})

function reset_form(reset = null) {
    $('#ID').val('');
    $('#NO_DOK_ENG').val('');
    $('#NO_DOK_ENG').hide();
    $('#ID_PENUGASAN').html('');
    $('#NO_PENUGASAN').val('');
    $('#NOTIFIKASI').val('');
    $('#PENGAJUAN').val('');
    $('#OBJECTIVE').val('');
    $('#ID_PACKET').html('');
    $('#PACKET_TEXT').val('');
    $('#NOMINAL').val('');
    $('#COLOR').val('Blank');

    // RKS
    $('#RKS_FILE').val('');
    $('#rks_uri').attr('href', '');
    $('#rks_name').removeClass('fa fa-file-pdf-o');
    $('#rks_name').text('');

    // BQ
    $('#BQ_FILE').val('');
    $('#bq_uri').attr('href', '');
    $('#bq_name').text('');
    $('#bq_name').removeClass('fa fa-file-pdf-o');

    // Drawing
    $('#DRAW_FILE').val('');
    $('#draw_uri').attr('href', '');
    $('#draw_name').text('');
    $('#draw_name').removeClass('fa fa-file-pdf-o');

    // ECE
    $('#ECE_FILE').val('');
    $('#ece_uri').attr('href', '');
    $('#ece_name').removeClass('fa fa-file-pdf-o');
    $('#ece_name').text('');

    generateDoc();
    getForeign();

    $("#APP_LIST").val('');
    $("#APP_LIST").removeAttr('disabled');
    $("#PARAF_LIST").val('');
    $("#PARAF_LIST").removeAttr('disabled');
    $("#CRT_LIST").val('');
    $("#CRT_LIST").removeAttr('disabled');
    $('.select2').select2();

    $("#btn-save").removeClass("disabled");
    $("#btn-save").prop("disabled", false);
    $("#btn-save").html(' <i class="fa fa-check"></i> Save');
    $('.selectpicker').selectpicker('refresh');
    $('#ul-new').text('Create Engineering Documents');
    $('#btn-save').show();

    if (reset) {
        setActiveTab('ul-list', 'list-form');
}

}

$("#btn-cancel").on("click", function () {
    reset_form();
});

// export
$(document).on('click', ".btnExport", function (e) {
    e.preventDefault();
    var data = oTable.row($(this).parents('tr')).data();

    window.location.assign('dok_eng/documents/export_pdf/' + data['ID']);
    //set_log_download(id_dok, tipe);
});

// export
$(document).on('click', ".btnExpPc", function (e) {
    e.preventDefault();
    var data = oTable.row($(this).parents('tr')).data();
    console.log($(this).attr('data-tipe'));
    sAdd = '';
    if ($(this).attr('data-tipe') != 'All') {
        sAdd = '/' + $(this).attr('data-tipe');
    }
    window.location.assign('eng/filing/export_pdf/' + data['ID'] + sAdd);
    var tipe = $(this).attr('dt_file');
    setTimeout( set_log_download(data['ID'], tipe), 20000);
});

// export transmital
$(document).on('click', ".btnExportBast", function (e) {
    e.preventDefault();
    var data = oTable.row($(this).parents('tr')).data();
    window.location.assign('bast/handover/export_pdf/' + data['ID_BAST']);
    var tipe = $(this).attr('dt_file');
    setTimeout( set_log_download(data['ID'], tipe), 20000);
    
    //console.log(data);
});

// set log download
function set_log_download(id_dok, tipe){
    var form_data = new FormData();
    form_data.append('jd', 'DOK_ENG');
    form_data.append('idok', id_dok);
    form_data.append('tp', tipe);
    
    $.ajax({
        url: 'dok_eng/documents/set_log_download', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (json) {
           if (json['status'] == 200) {
                swal({
                    title: "Histori Download "+tipe+" Tersimpan.",
                    type: "success"
                });
                reset_form('yes');
            }
        }
    });
}

// get log download
$(document).on('click', ".btnLogDownload", function (e) {
    e.preventDefault();
    var data = oTable.row($(this).parents('tr')).data();
    console.log(data);
    
    var title = "("+data['NO_DOK_ENG']+") "+data['PACKET_TEXT'];
    $('#title_mod_download').text(title);
    
    var setHtml = '';
    
    $.ajax({
        url: 'dok_eng/documents/getHistoriDownload', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        type: 'POST',
        data: {
            'idok': data['ID']
        },
        success: function (json) {
            
                setHtml += '<div class="table-responsive"> ' +
                                '<table id="log_down" class="table display compact table-bordered" style="width:100%; font-size: 10px;"> '+
                                    '<thead> '+
                                        '<tr> '+
                                            '<th style="text-align: center;" >Waktu</th> '+
                                            '<th style="text-align: center;" >Tipe</th> '+
                                            '<th style="text-align: center;" >Nama</th> '+
                                            '<th style="text-align: center;" >Jabatan</th> '+
                                            '<th style="text-align: center;" >Unit Kerja</th> '+
                                        '</tr> '+
                                    '</thead> '+
                                    '<tbody> ';
                                    
                                    $.each(json['data'], function (j, item2) {
                                        setHtml += '<tr >'+
                                                        '<td style="text-align: center;">'+item2['TIME']+'</td>'+
                                                        '<td style="text-align: left;"> &nbsp;'+item2['TIPE']+' </td>'+
                                                        '<td style="text-align: left;"> &nbsp;'+item2['NAMA_USER']+'</td>'+
                                                        '<td style="text-align: left;"> &nbsp;'+item2['JAB_USER']+'</td>'+
                                                        '<td style="text-align: left;"> &nbsp;'+item2['UK_USER']+'</td>'+
                                                    '</tr>';      
                                    });  
                setHtml +=          '</tbody> '+
                                '</table> '+
                            '</div>';
                
            
           
            $('#download_bd').html(setHtml);
             $('#log_down').dataTable();
        }
    });
    openModal('preview_modal_download_req');
});

function openModal(id = null) {
    $('#' + id).modal('show');
    $('#' + id).show();
    $('.modal-backdrop').show();
}

function closeModal(id = null) {
    $('#' + id).hide();
    $('.modal-backdrop').hide();
}

// event resend mail
$(document).on('click', ".btnResend", function () {
    $('.title_ukur').text('Resend Email');
    var data = oTable.row($(this).parents('tr')).data();

    var form_data = new FormData();
    form_data.append('ID', data['ID']);

    $.ajax({
        url: 'dok_eng/documents/resend', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Resend Email : NOT Success! " + error,
                type: "error"
            });
        },
        success: function (json) {
            if (json['status'] == 200) {
                swal({
                    title: "Email was sent.",
                    type: "success"
                });
                reset_form('yes');
            }
            $("#btn-save").removeClass("disabled");
        }
    });
});

// view
$(document).on('click', ".btnEdit", function () {
    var data = oTable.row($(this).parents('tr')).data();
    $('#ID').val(data['ID']);
    $('#NO_DOK_ENG').val(data['NO_DOK_ENG']);
    $('#NO_DOK_ENG').show();
    if (data['ID_PENUGASAN']) {
        $('#ID_PENUGASAN').html('<option value="' + data['ID_PENUGASAN'] + '" title="' + data['NO_PENUGASAN'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENUGASAN'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" >' + data['NO_PENUGASAN'] + ' - ' + data['PENGAJUAN'] + '</option>');
    }
    $('#NO_PENUGASAN').val(data['NO_PENUGASAN']);
    $('#NOTIFIKASI').val(data['NOTIFIKASI']);
    $('#PENGAJUAN').val(data['PENGAJUAN']);
    $('#OBJECTIVE').val(data['OBJECTIVE']);
    if (data['ID_PACKET']) {
        $('#ID_PACKET').html('<option value="' + data['ID_PACKET'] + '" >' + data['PACKET_TEXT'] + '</option>');
    }
    $('#PACKET_TEXT').val(data['PACKET_TEXT']);
    $('#NOMINAL').val(toCurrency(data['NOMINAL']));
    $('#COLOR').val(data['COLOR']);

    // LIST APP
    if (data['LIST_APP']) {
        var aApp = data['LIST_APP'].split(':');

        $('#APP_LIST').val(aApp);
        if (data['STATUS'] != 'In Approval') {
            $('#APP_LIST').attr('disabled', 'disabled');
            ;
        }
    }
    // LIST Paraf
    if (data['LIST_PARAF']) {
        var aPar = data['LIST_PARAF'].split(':');

        $('#PARAF_LIST').val(aPar);
        if (data['STATUS'] != 'In Approval') {
            $('#PARAF_LIST').attr('disabled', 'disabled');
            ;
        }
    }

    // LIST Created
    if (data['LIST_CRT']) {
        var aCrt = data['LIST_CRT'].split(':');

        $('#CRT_LIST').val(aCrt);
        if (data['STATUS'] != 'In Approval') {
            $('#CRT_LIST').attr('disabled', 'disabled');
            ;
        }
    }

    // RKS
    if (data['RKS_FILE']) {
        var aFiles = data['RKS_FILE'].split('/');

        $('#rks_uri').attr('href', data['RKS_FILE']);
        $('#rks_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#rks_name').addClass('fa fa-file-pdf-o');
    }
    // BQ
    if (data['BQ_FILE']) {
        var aFiles = data['BQ_FILE'].split('/');

        $('#bq_uri').attr('href', data['BQ_FILE']);
        $('#bq_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#bq_name').addClass('fa fa-file-pdf-o');
    }
    // Drawing
    if (data['DRAW_FILE']) {
        var aFiles = data['DRAW_FILE'].split('/');

        $('#draw_uri').attr('href', data['DRAW_FILE']);
        $('#draw_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#draw_name').addClass('fa fa-file-pdf-o');
    }
    // ECE
    if (data['ECE_FILE']) {
        var aFiles = data['ECE_FILE'].split('/');

        $('#ece_uri').attr('href', data['ECE_FILE']);
        $('#ece_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#ece_name').addClass('fa fa-file-pdf-o');
    }
    // Kajian
    if (data['KAJIAN_FILE']) {
        var aFiles = data['KAJIAN_FILE'].split('/');

        $('#kajian_uri').attr('href', data['KAJIAN_FILE']);
        $('#kajian_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#kajian_name').addClass('fa fa-file-pdf-o');
    }

    if (data['STT_APP'] > 0) {
        $('#btn-save').hide();
    }

    if (/detail/i.test(data['JENIS'])) {
        var e = document.getElementById("kajian");
        var f = document.getElementById("capex");
        //console.log(e);
        e.style.display = "none";
        f.style.display = "block";
    } else {
        var e = document.getElementById("capex");
        var f = document.getElementById("kajian");
        //console.log(e);
        e.style.display = "none";
        f.style.display = "block";
    }

    $('.select2').select2();
    $('.selectpicker').selectpicker('refresh');
    setActiveTab('ul-new', 'new-form');
    $('#ul-new').text('View Engineering Task');
    if (page_state.indexOf(page_short + '-modify=1') == -1) {
        $('.' + page_short + '-modify').hide();
    }
});

// save
$("#fm-new").submit(function (e) {
    e.preventDefault();
    $("#btn-save").prop("disabled", true);
    $("#btn-save").addClass("disabled");
    
    val_gmde = $('#HDE_LIST').val();
    if (val_gmde) {
        gm_de = val_gmde;
    }

    id = $('#ID').val();
    no_dok_eng = $('#NO_DOK_ENG').val();
    tipe = 'All';
    id_penugasan = $('#ID_PENUGASAN').val();
    notifikasi = $('#NOTIFIKASI').val();
    id_packet = $('#ID_PACKET').val();
    packet_text = $('#ID_PACKET :selected').text();
    nominal = toNumber($('#NOMINAL').val());
    color = $('#COLOR').val();
    rkspath = document.getElementById('RKS_FILE').files[0];
    rksfile = $('#RKS_FILE').attr('data-url');
    bqpath = document.getElementById('BQ_FILE').files[0];
    bqfile = $('#BQ_FILE').attr('data-url');
    drawpath = document.getElementById('DRAW_FILE').files[0];
    drawfile = $('#DRAW_FILE').attr('data-url');
    ecepath = document.getElementById('ECE_FILE').files[0];
    ecefile = $('#ECE_FILE').attr('data-url');
    kajianpath = document.getElementById('KAJIAN_FILE').files[0];
    kajianfile = $('#KAJIAN_FILE').attr('data-url');
    tmp_crt = $('#APP_CRT').val();
    tmp_app = $('#APP_LIST').val();
    tmp_paraf = $('#PARAF_LIST').val();

    var formURL = "";

    var form_data = new FormData();
    form_data.append('ID', id);
    form_data.append('NO_DOK_ENG', no_dok_eng);
    form_data.append('TIPE', tipe);
    form_data.append('ID_PENUGASAN', id_penugasan);
    form_data.append('NOTIFIKASI', notifikasi);
    form_data.append('ID_PACKET', id_packet);
    form_data.append('PACKET_TEXT', packet_text);
    form_data.append('COLOR', color);
    form_data.append('NOMINAL', nominal);
    form_data.append('APPROVE3_BY', gm_de);
    // RKS
    if (rkspath) {
        form_data.append('RKSPATH', rkspath);
    } else {
        if (rksfile) {
            form_data.append('RKSFILE', rksfile);
        }
    }
    // BQ
    if (bqpath) {
        form_data.append('BQPATH', bqpath);
    } else {
        if (bqfile) {
            form_data.append('BQFILE', bqfile);
        }
    }
    // Drawing
    if (drawpath) {
        form_data.append('DRAWPATH', drawpath);
    } else {
        if (drawfile) {
            form_data.append('DRAWFILE', drawfile);
        }
    }
    // ECE
    if (ecepath) {
        form_data.append('ECEPATH', ecepath);
    } else {
        if (ecefile) {
            form_data.append('ECEFILE', ecefile);
        }
    }
    // Kajian
    if (kajianpath) {
        form_data.append('KAJIANPATH', kajianpath);
    } else {
        if (ecefile) {
            form_data.append('KAJIANFILE', kajianfile);
        }
    }
    // var list_app = [];
    // var index = 1;
    // $('#APP_LIST :selected').each(function(i, selectedElement) {
    //   tmpApp = $(selectedElement).val().split(' - ');
    //   if (i < 8) {
    //     form_data.append('APPROVE' + index + '_BY', tmpApp[1]);
    //     form_data.append('APPROVE' + index + '_JAB', tmpApp[3]);
    //   }
    //   index++;
    // });
    form_data.append('LIST_APP', $('#APP_LIST').val().join(':'));
    form_data.append('LIST_PARAF', $('#PARAF_LIST').val().join(':'));
    form_data.append('LIST_CRT', $('#CRT_LIST').val().join(':'));


    var uri = 'create';
    if (id) {
        uri = 'update';
    }

    $.ajax({
        url: 'dok_eng/documents/' + uri, // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Data Save : NOT Success! " + error,
                type: "error"
            });
            $("#btn-save").removeClass("disabled");
        },
        success: function (json) {
            if (json['status'] == 200) {
                tb_dok_eng();
                swal({
                    title: "Data Saved!\n" + no_dok_eng,
                    type: "success"
                });
                reset_form('yes');
            }
            $("#btn-save").removeClass("disabled");
        }
    });
    return false;
});

// delete
$(document).on('click', ".btnDel", function () {
    var data = oTable.row($(this).parents('tr')).data();
    swal({
        title: "Are you sure?",
        text: "Data (" + data['NO_DOK_ENG'] + ") will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Delete",
        confirmButtonClass: "btn-danger",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    url = 'dok_eng/documents/delete/' + data['ID'];
                    $.ajax({
                        url: url, // point to server-side PHP script
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'get',
                        error: function (xhr, status, error) {
                            swal("Error", "Your data failed to delete!", "error");
                        },
                        success: function (json) {
                            if (json['status'] == 200) {
                                tb_dok_eng();
                                swal("Deleted!", "Your data has been deleted.", "success");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Deletion of data canceled", "error");
                }
            });
});

/* Terkait task noo 32
//ECE 5 M
$(document).on('change',"#NOMINAL", function (){
    var val_ECE = $('#NOMINAL').val().replace(/,/g, '');
    var select = $('#HDE_LIST');
    var ECE = parseFloat(val_ECE);
    if (ECE >= 5000000000) {
        $('#HDE_LINE_APP').show();
        select.attr('required', true);
    } else {
        $('#HDE_LINE_APP').hide();
        select.attr('required', false);
    }
});
*/

$('#preview_close').click(function(){
    hideShowModal('preview_modal');
});

$('#preview_cancel').click(function(){
    var doc = $("#secret_doc").val();
    var doc_name = $("#secret_doc_name").val();

    $("#"+doc).val("");
    hideShowModal('preview_modal');
    $.ajax({
        url: 'dok_eng/documents/unlink_preview_document',
        data: {
            "document": doc_name,
        },
        method: 'POST',
        success: function (json) {

        }
    });
});

var modState = 0;
function previewDocument(input){
    $("#secret_doc").val("");
    $("#secret_doc_name").val("");

    var form = input.name;
    var crt = $("#CRT_LIST").val();
    var paraf = $("#PARAF_LIST").val();
    var app = $("#APP_LIST").val();
    var hde = $("#HDE_LIST").val();

    if(crt == "" || paraf == "" || app == "" || hde == ""){
        var r = confirm("Approval info masih belum diisi ? Apakah ingin lanjut ?");
        if(r == true){
            upload_file_preview(form);
        } else {
            $("#"+input.id).val("");
        }
    } else {
        upload_file_preview(form);
    }
}

function upload_file_preview(form){
    var form_data = new FormData($("#fm-new")[0]);
    form_data.append("DOC", form);
    form_data.append("NO_PENUGASAN", $("#NO_PENUGASAN").val());
    form_data.append("CRT_LIST", $('#CRT_LIST').val().join(':'));
    form_data.append("PARAF_LIST", $('#PARAF_LIST').val().join(':'));
    form_data.append("APP_LIST", $('#APP_LIST').val().join(':'));
    form_data.append("HDE_LIST", $('#HDE_LIST').val());
    $.ajax({
        "url": "dok_eng/documents/preview_document_upload",
        "async": true,
        "crossDomain": true,
        "cache": false,
        "processData": false,
        "contentType": false,
        "type": "POST",
        "data": form_data,
        "mimeType": "multipart/form-data",
        error: function (xhr, status, error) {
            swal("Error", "Cant upload pdf file!", "error");
        },
        success: function (json) {
            var data = JSON.parse(json);
            console.log(data);
            if(data.status == "success"){
                $("#secret_doc").val(form);
                $("#secret_doc_name").val(data.file_name);
                _OBJECT_URL = data.data;
                var pdf = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="assets/pdfviewer/web/viewer.html?file=' + _OBJECT_URL+ '" scrolling="no"></iframe>';
                $('#preview_body').html(pdf);

                if (modState < 1) {
                    $('#preview_modal').modal('show');   
                } else {
                    hideShowModal('preview_modal');
                }
                modState += 1;
            } else {
                swal("Error", data.message, "error");
            }
            
            
        }
    });
}