var oTable;

$(document).ready(function() {
    tb_assign();
    // Setup - add a text input to each footer cell
    $('#tb_eat_tr_search th').each( function () {
        var title = $(this).text();
        $(this).html( '<input id="search-table" style="width:100%;" type="text" placeholder="Search '+title+'" />' );
    } ); 
    
  $("#START_DATE, #END_DATE").datepicker({
        autoclose: true,
        format:'dd/mm/yyyy'
    });

  if ($('#ID_PENGAJUAN').val()) {
    onForeign();
    onChange();
  }
 
  setDefault();
  list_dinamis();
  $('.select2').select2();

  if ($("#SM_APP").is(":visible")) {
    $("#SM_APP").prop('required', false);
    $("#MANAGER").prop('required', true);
  }else {
    $("#SM_APP").prop('required', true);
    $("#MANAGER").prop('required', false);
  }

  $('input[name^=DE_CONSTRUCT_COST],input[name^=DE_ENG_COST]').priceFormat({
    prefix: '',
    centsSeparator: '.',
    centsLimit: 0,
    thousandsSeparator: ',',
    clearOnEmpty: false,
  });
});

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_assign() {
  oTable = $('#tb_eat').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'eat/assign/data_list',
      type: "POST"
    },
    initComplete: function () {
       // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
    },
    columns: [{
        "data": "RNUM",
        "sClass": "right"
      }, {
        "className": 'details-control',
        "orderable": false,
        "data": null,
        "defaultContent": ''
      }, {
        "data": "CREATE_AT",
        "width": 75
      }, {
        "data": "NO_PENUGASAN"
      },
      {
        "data": "NOTIFIKASI"
      },
      {
        "data": "NO_PENGAJUAN"
      },
      {
        "data": "TIPE"
      },
      {
        "data": "SHORT_TEXT"
      },
      {
        "data": "STATUS",
        "mRender": function(row, data, index) {
          if (index['STATUS'].indexOf('Approved') != -1) {
            return '<center><p style="color: green">' + index['STATUS'] + ' </p></center> ';
          } else if (index['STATUS'].indexOf('Rejected') != -1) {
            return '<center><p style="color: red">' + index['STATUS'] + ' </p></center>';
          } else {
            return '<center><p style="color: blue">' + index['STATUS'] + ' </p></center>';
          }
        }
      },
      {
        "data": "UK_TEXT"
      },
      {
        "data": "LOKASI"
      },
      {
        "data": "COMP_TEXT"
      },
      {
        "data": "ID",
        "width": 70,
        "mRender": function(row, data, index) {
          sresend = '';
          if (!index['APPROVE0_AT'] && !index['APPROVE1_AT']) {
            sresend = '<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
          }
          sdis = '';
          if (page_state.indexOf(page_short + '-delete=1') == -1 || index['STATUS'].indexOf('For Approval') == -1) {
            sdis = 'disabled style="display:none;" '; 
            //penggantian button atribute disabled menjadi hide [Task no.20] 
          }
          if (page_state.indexOf('special_access=false') == -1 && Number(index['PENDING'])<1 && page_state.indexOf(page_short + '-delete=1') != -1) {
            sdis = '';
          }
          if (Number(index['PENDING'])<1 && page_state.indexOf(page_short + '-spc-del=1') != -1) {
            sdis = '';
          }
          sexp = '';
          if (page_state.indexOf(page_short + '-export=1') == -1) {
            sexp = ' style="display:none;"';
          }
          return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download (Download Lampiran -> di tombol view)"' + sexp + ' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></button>' + sresend + '<button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button></div></center>';
        }
      },
    ],
    "order": [
      [0, "desc"]
    ],
    "createdRow": function(row, data, dataIndex) {
      // if (Number(data['PENDING']) < 1) {
      if (data['STATUS'] == 'For Approval') {
        $('td', row).addClass("color-danger text-danger");
      }
    }
  });

  $('#tb_eat tbody').on('click', 'td.details-control', function() {
    var tr = $(this).closest('tr');
    var row = oTable.row(tr);
    var data = row.data();

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {

      var html = '';
      var d = row.data();
      var data;
      var approveAt = '';
      var status = '';
      var i = 0;
      var fprogress = 12;

      html += '<div class="container"><ul class="progressbar">';
      $.ajax({
        url: 'erf/request/getApproval',
        data: {
          id: d.ID_PENGAJUAN
        },
        dataType: 'json',
        type: 'POST',
        success: function (result) {
          var datas = result;
          $.map(datas, function(item, index){
            pengajuan = item.STATUS1;
            penugasan = item.STATUS2;
            if (item.STATUS1 === 'Open') {
              status = 'Pending';
            } else if (item.STATUS1 === 'Approved') {
              status = 'active';
              fprogress = 12.5;
            } else {
              status = 'reject';
            }

            if (item.STATUS2 === 'In Approval') {
              statuspenugasan = 'pending';
            } else if (item.STATUS2 === 'Approved Biro.' || item.STATUS2 === 'SM Approved') {
              statuspenugasan = 'active';
              fprogress = 25;
            } else if (item.STATUS2 === 'Approved Mgr.' || item.STATUS2 === 'MGR Approved') {
              statuspenugasan = 'active';
              fprogress = 25;
            } else if (item.STATUS2 === 'Rejected') {
              statuspenugasan = 'reject';
            } else {
              statuspenugasan = '';
            }

            if (item.DOKSTATUS === 'ada') {
              statusdok = 'active';
              statuspenugasan = 'active';
              fprogress = parseFloat(item.STS_PRGS);
            } else {
              statusdok = '';
            }

            if (item.STATUSBAST === 'Closed') {
              statusbast = 'active';
            } else if (item.STATUSBAST === 'Rejected') {
              statuspenugasan = 'reject';
            } else {
              statusbast = '';
            }

            if(item.REMAINING_DAYS < 0){
              remaining_days = item.REMAINING_DAYS+" Remaining Days";
            } else if(item.REMAINING_DAYS > 0) {
              remaining_days = item.REMAINING_DAYS+" Days Ago";
            } else {
              remaining_days = "";
            }

            html += '<li class="'+statuspenugasan+'">Assign Task</li>';
            html += '<li class="'+statusdok+'">Engineering Documents</li>';
            html += '<li class="'+statusbast+'">Finish</li>';

            if(data.NOTE == null){
            	var notes = data.NOTE0;
            } else {
            	var notes = data.NOTE;
            }

            html += '</ul><button type="button" id="btn_progres_req" class="btn btn-dark btn-outline btn-rounded btn-sm" style="cursor:default;">' + parseFloat(fprogress).toFixed(2) + ' %<br/>'+remaining_days+'</button>' +
              '</div><div class="container"><table class="table display table-bordered table-striped" id="tableprogress">' +
                '<thead>' +
                '<tr>' +
                '<th>No</th>' +
                '<th>Type</th>' +
                '<th>Status</th>' +
                '<th>Work Unit</th>' +
                '<th>Date</th>' +
                '<th>By</th>' +
                '<th style="width:100px">Note</th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>';
              html += '<tr>' +
                '<td>' + (i + 1) + '</td>' +
                '<td>Created</td>' +
                '<td>Approved</td>' +
                '<td>' + data.UK_TEXT + '</td>' +
                //if(item.UPDATE_AT != null) {
                '<td>' + data.CREATE_AT + '</td>' +
                '<td>' + data.CREATE_BY + '</td>' +
                '<td style="width:280px">-</td>' +
                '</tr>';
              i++;
              html += '<tr>' +
                '<td>' + (i + 1) + '</td>' +
                '<td>TTD</td>';
              if (data.APPROVE0_AT || data.APPROVE1_AT) html += '<td>Approved</td>';
              else html += '<td>Pending</td>';
              
              //html += '<td>' + data.UK_TEXT + '</td>';
              
              var note = '-';
              if (data.APPROVE0_BY) {
                if (data.NOTE0) {
                  note = data.NOTE0;
                }
                html += '<td>' + data.APPROVE0_JAB + '</td>' +
                  '<td>' + data.APPROVE0_AT + '</td>' +
                  '<td>' + data.APPROVE0_BY + '</td>' +
                  '<td style="width:280px">' + note + '</td>';
              } else {
                if (data.NOTE) {
                  note = data.NOTE;
                } 
                html += '<td>' + data.APPROVE1_JAB + '</td>' +
                  '<td>' + data.APPROVE1_AT + '</td>' +
                  '<td>' + data.APPROVE1_BY + '</td>' +
                  '<td style="width:280px">' + note + '</td>';
              }
              html += '</tr>';

              html +=
                '</tbody>' +
                '</table>' +
                '</div>';
              row.child(html).show();
              tr.addClass('shown');
              return html;

          });
        }
      });


      
    }
  });
}


function setDefault() {
  $('.col-no').hide();
  $('#TIM_PENGKAJI').val('1.Office of Engineering\n2. Design Engineering Integration');
  $('#TW_KOM_PENGARAH').val('1. Memberi arahan kepada tim pengkaji terkait biaya dan waktu penyelesaian..\n2. Memberi masukan perihal rencana..\n3. Membuat keputusan yang bersifat strategis terhadap kajian engineering.\n');
  $('#TW_TIM_PENGKAJI').val('1. Mengumpulkan data-data, informasi dan masukan yang diperlukan tim\n2. Melakukan koordinasi dengan unit yang terkait\n3. Melakukan analisa terhadap rencana disain\n4. Membuat dokumen engineering\n5. Membuat Laporan kepada Komite Pengarah\n');
}

function setAtasan() {
  $.ajax({
    type: 'POST',
    url: 'eat/assign/atasan/dept',
    success: function(json) {
      json = $.parseJSON(json);
      if (json) {
        if (json['status'] == 200) {
          $('#KOMITE_PENGARAH').val(json['data']['JAB_TEXT']);
        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function list_dinamis() {

  $("#add-todo-item-pekerjaan").on('click', function(e) {
    e.preventDefault();
    addTodoItem_pekerjaan()
  });

  $("#new-todo-item-pekerjaan").on('keypress', function(e) {
    if (e.which == 13) {
      e.preventDefault();
      addTodoItem_pekerjaan()
    }
  });


  $("#todo-list-pekerjaan").on('click', '.todo-item-delete', function(e) {
    var item = this;
    var k = $(this).data("k");
    var ktem = $(this).data("ktem");
    var ID = $("#ID").val();
    var r = confirm("Apakah Anda Yakin, menghapus data?");
    if (r == true) {
      if(ID != ""){
        $.ajax({
          type: 'POST',
          url: 'eat/assign/delete_paket_pekerjaan',
          data: {
            "id_eat" : ID,
            "num" : k,
            "pp" : ktem
          },
          error: function(xhr, status, error) {
            swal({
              title: "Data Save : NOT Success! " + error,
              type: "error"
            });
          },
          success: function(json) {
            deleteTodoItem(e, item);
            // alert('Revisi paket pekerjaan ('+desk+') berhasil dibuat.');
            // hideShowModal('edit_to_do_modal');
            // $(".pekerjaan"+num).html(desk + "<i style='float:right; cursor:pointer' class='todo-item-delete fa fa-times'></i>");
          }
        });
      } else {
        deleteTodoItem(e, item); //DELETE BEFORE EAT APPROVE
      }         
    }
    
    
  })

}

function addTodoItem_pekerjaan() {
  var todoItem = $("#new-todo-item-pekerjaan").val();
  if (todoItem) {
    $("#todo-list-pekerjaan").append("<li class='pekerjaan'>" + todoItem + "<i style='float:right; cursor:pointer' class='todo-item-delete fa fa-times'></i></li>");
  } else {
    swal({
      title: "Field kosong!",
      text: "Field pekerjaan tidak boleh kosong."
    });
  }
  $("#new-todo-item-pekerjaan").val("");
  $("#new-todo-item-pekerjaan").prop('required',false);
}

function deleteTodoItem(e, item) {
  e.preventDefault();
  $(item).parent().fadeOut('slow', function() {
    $(item).parent().remove();
  });
  if ($('ul#items li').length < 1) {
    $("#new-todo-item-pekerjaan").prop('required',true);
  }
}

function getForeign() {
  $.ajax({
    type: 'POST',
    url: 'eat/assign/foreign',
    success: function(json) {
      json = $.parseJSON(json);
      $('#ID_PENGAJUAN').html('');
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          foreigns += ('<option value >Please select...</option>');
          $.each(json['data'], function(i, data) {
            foreigns += ('<option value="' + data['ID_PENGAJUAN'] + '" title="' + data['NO_PENGAJUAN'] + ' - ' + data['NOTIF_NO'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENGAJUAN'] + '" data-notif="' + data['NOTIF_NO'] + '" data-flcode="' + data['FUNCT_LOC'] + '" data-fltext="' + data['FL_TEXT'] + '" data-shorttext="' + data['SHORT_TEXT'] + '" data-uktext="' + data['UK_TEXT'] + '" data-custreq="' + data['CUST_REQ'] + '" data-lokasi="' + data['LOKASI'] + '" data-const="' + data['CONSTRUCT_COST'] + '" data-eng="' + data['ENG_COST'] + '" >' + data['SHORT_TEXT'] + '</option>');
          });
        } else {
          foreigns = ('<option value >Empty...</option>');
        }
        $('#ID_PENGAJUAN').html(foreigns);
        onForeign();
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function onForeign() {
  $('#NO_ERF').val($('#ID_PENGAJUAN :selected').attr('data-no'));
  $('#NOTIF_NO').val($('#ID_PENGAJUAN :selected').attr('data-notif'));
  $('#TIPE').val($('#ID_PENGAJUAN :selected').attr('data-tipe'));
  $('#SHORT_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-shorttext'));
  $('#UK_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-uktext'));
  $('#FL_TEXT').val($('#ID_PENGAJUAN :selected').attr('data-lokasi'));
  // $('#OBJECTIVE').val($('#ID_PENGAJUAN :selected').attr('data-fltext'));
  $('#CUST_REQ').val($('#ID_PENGAJUAN :selected').attr('data-custreq'));
  $('#DE_CONSTRUCT_COST').val(toCurrency($('#ID_PENGAJUAN :selected').attr('data-const')));
  $('#DE_ENG_COST').val(toCurrency($('#ID_PENGAJUAN :selected').attr('data-eng')));
}

function onChange() {
  var no_penugasan = $("#NO_PENUGASAN").val();
  var arr = no_penugasan.split("/");
  var pengajuan = $("#NO_ERF").val();
  var arr2 = pengajuan.split("/");
  $("#NO_PENUGASAN").val(arr2[0] + "/" + arr2[1] + "/" + arr2[2] + "/" + arr[3] + "/" + arr[4] + "/" + arr2[5] + "/" + arr[6]);
}

$('#ID_PENGAJUAN').on('change', function() {
  onForeign();
  onChange();
})

function select_content_change(selecttag, urlselect, placeholder) {
  $('' + selecttag).select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: placeholder,
    theme: 'bootstrap',
    ajax: {
      dataType: 'json',
      url: urlselect,
      // data: data,
      delay: 50,
      type: 'POST',
      data: function(params) {
        return {
          search: params.term
        }
      },
      processResults: function(data, page) {
        console.log(data);
        return {
          results: $.map(data['data'], function(obj) {
            console.log(obj);
            if (obj.NOBADGE) {
              return {
                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA,
              };
            } else {
              return {
                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA,
              };
            }

          })
        };
      },
    }
  });
  $('' + selecttag).trigger('change');
}

function load_selected_option(key, id, text) {
  $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
  $(key).select2('data', {
    id: id,
    label: text
  });
  $(key).trigger('change'); // Notify any JS components that the value changed
}

function reset_form(reset = null) {
  $('#ID').val('');
  $('#NO_PENUGASAN').val('');
  $('#ID_PENGAJUAN').html('');
  $('#NO_ERF').val('');
  $('#NOTIF_NO').val('');
  $('#TIPE').val('');
  $('#SHORT_TEXT').val('');
  $('#UK_TEXT').val('');
  $('#FL_TEXT').val('');
  $('#START_DATE').val('');
  $('#END_DATE').val('');
  $('#OBJECTIVE').val('');
  $('#CUST_REQ').val('');
  $('#SCOPE_ENG').val('');
  $('#LEAD_ASS').val('');
  $('#KOMITE_PENGARAH').val('');
  $(':checkbox ').each(function(i) {
    $(this).removeAttr('checked');
  });
  $('#TW_KOM_PENGARAH').val('');
  $('#TW_TIM_PENGKAJI').val('');
  $('#FILES').val('');
  $('#FILES2').val('');
  $('#FILES3').val('');
  $("#todo-list-pekerjaan").html('');
  $('#paket_pekerjaan_revisi').html('');

  setDefault();
  generateDoc();

  $('.col-no').hide();
  $("#new-todo-item-pekerjaan").prop('required',true);
  $("#btn-save").removeClass("disabled");
  $("#btn-save").prop("disabled", false);
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('.select2').val('').trigger('change');

  $('#ul-new').text('Create Engineering Task');
  $('#btn-save').show();
  $('#file_name').removeClass('fa fa-file-pdf-o');
  $('#file_uri').attr('href', '');
  $('#file_name').text('');
  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }
}

// export
$(document).on('click', ".btnExport", function() {
  var data = oTable.row($(this).parents('tr')).data();
  window.location.assign('eat/assign/export_pdf/' + data['ID']);
});

$("#btn-cancel").on("click", function() {
  reset_form();
});

// event resend mail
$(document).on('click', ".btnResend", function() {
  $('.title_ukur').text('Resend Email');
  var data = oTable.row($(this).parents('tr')).data();


  var form_data = new FormData();
  form_data.append('ID', data['ID']);

  $.ajax({
    url: 'eat/assign/resend', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Resend Email : NOT Success! " + error,
        type: "error"
      });
    },
    success: function(json) {
      if (json['status'] == 200) {
        swal({
          title: "Email was sent.",
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
});

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();
  $("#todo-list-pekerjaan").html('');
  $('#ID').val(data['ID']);
  $('#NO_PENUGASAN').val(data['NO_PENUGASAN']);
  $('.col-no').show();
  if (data['ID_PENGAJUAN']) {
    $('#ID_PENGAJUAN').html('<option selected value="' + data['ID_PENGAJUAN'] + '" title="' + data['NO_PENGAJUAN'] + ' - ' + data['NOTIF_NO'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENGAJUAN'] + '" data-notif="' + data['NOTIF_NO'] + '" data-tipe="' + data['TIPE'] + '" data-flcode="' + data['FUNCT_LOC'] + '" data-fltext="' + data['FL_TEXT'] + '" data-shorttext="' + data['SHORT_TEXT'] + '" data-uktext="' + data['UK_TEXT'] + '" >' + data['SHORT_TEXT'] + '</option>');
  }
  $('#NO_ERF').val(data['NO_PENGAJUAN']);
  $('#NOTIF_NO').val(data['NOTIF_NO']);
  $('#TIPE').val(data['TIPE']);
  $('#SHORT_TEXT').val(data['SHORT_TEXT']);
  $('#UK_TEXT').val(data['UK_TEXT']);
  $('#FL_TEXT').val(data['FL_TEXT']);
  var start_date = new Date(data['START_DATE']);
  $('#START_DATE').datepicker("setDate",start_date.getDate()+"/"+start_date.getMonth()+"/"+start_date.getFullYear());
  var end_date = new Date(data['END_DATE']);
  $('#END_DATE').datepicker("setDate",end_date.getDate()+"/"+end_date.getMonth()+"/"+end_date.getFullYear());
  $('#OBJECTIVE').val(data['OBJECTIVE']);
  $('#CUST_REQ').val(data['CUS_REQ']);
  $('#SCOPE_ENG').val(data['SCOPE_ENG']);
  $('#DE_CONSTRUCT_COST').val(toCurrency(data['DE_CONSTRUCT_COST']));
  $('#DE_ENG_COST').val(toCurrency(data['DE_ENG_COST']));
  $('#LEAD_ASS').html('<option selected value="' + data['LEAD_ASS'] + '" title="' + data['LEAD_ASS'] + '"  >' + data['LEAD_ASS'] + '</option>');
  $('#KOMITE_PENGARAH').val(data['KOMITE_PENGARAH']);
  var arpengkaji = eval(data['TIM_PENGKAJI']);
  $(':checkbox ').each(function(i) {
    if (data['TIM_PENGKAJI'].indexOf('"' + $(this).val() + '"') != -1) {
      $(this).attr('checked', 'checked');
    }
  });
  $('#TW_KOM_PENGARAH').val(data['TW_KOM_PENGARAH']);
  $('#TW_TIM_PENGKAJI').val(data['TW_TIM_PENGKAJI']);
  if (data['APPROVE0_BY']) {
    $('.disMan').show();
    $('.disSM').hide();
    load_selected_option('#MANAGER', data['APPROVE0_BADGE'] + " - " + data['APPROVE0_BY'] + " - " + data['APPROVE0_JAB'], "[ " + data['APPROVE0_BADGE'] + " ] - " + data['APPROVE0_BY']);
  }
  if (data['APPROVE1_BY']) {
    $('.disMan').hide();
    $('.disSM').show();
  }
  if (data['FILES']) {
    var aFiles = data['FILES'].split('/');

    $('#file_uri').attr('href', data['FILES']);
    $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#file_name').addClass('fa fa-file-pdf-o');
  }
  if (data['FILES2']) {
    var aFiles = data['FILES2'].split('/');

    $('#file_uri2').attr('href', data['FILES2']);
    $('#file_name2').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#file_name2').addClass('fa fa-file-pdf-o');
  }
  if (data['FILES3']) {
    var aFiles = data['FILES3'].split('/');

    $('#file_uri3').attr('href', data['FILES3']);
    $('#file_name3').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#file_name3').addClass('fa fa-file-pdf-o');
  }

  list_pekerjaan = eval(data['LIST_PEKERJAAN']);
  $('.form_list_pekerjaan').show();
  $.each(list_pekerjaan, function(k, ktem) {
    if (ktem) {
      $("#todo-list-pekerjaan").append("<li class='pekerjaan"+k+"' ><span id='to-do-list-update' data-k='"+k+"' data-ktem='"+ktem+"' style='cursor:pointer;' >" + ktem + "</span><i style='float:right; cursor:pointer' data-k='"+k+"' data-ktem='"+ktem+"' class='todo-item-delete fa fa-times'></i></li>");
    }
  });
  
  // penambahan paket pekerjaan revisi;
  
  var setHtml = '';
  
  setHtml += '<div class="col-md-12"> ' +
              '<div class="form-group"> '+
                '<div class="form-group"> '+
                  '<div id="form-add-todo" class="form-add-todo"> '+
                    '<label>Buat Revisi Kajian/Paket Pekerjaan  <span class="text-danger">*</span></label> '+
                                                           ' <div class="form-group form_list_pekerjaan_revisi">'+
                                                              '  <div class="input-group"> '+
                                                              '<input type="hidden" id="id_eat_ref" value="'+data['ID']+'">'+ 
                                                                  '  <input placeholder="(max. 255 karakter)" type="text" class="form-control input-sm new-todo-item-pekerjaan-revisi" id="new-todo-item-pekerjaan-revisi" name="pekerjaan_revisi" required> '+
                                                                  '<span class="btn-group">'+
                                                        '<button type="button" class="btn btn-sm btn-danger" id="add-todo-item-pekerjaan-revisi"> <i class="fa fa-plus"></i></button>'+
                                                      '</span>'+
                                                          '      </div>'+
                                                           ' </div>'+
                                                        '</div>'+
                                                   ' </div>'+
                                                    
                                                '</div>'+
                                            '</div>';
                                            
  $('#paket_pekerjaan_revisi').html(setHtml);
  
  // akhir

  if (list_pekerjaan.length) {
    $("#new-todo-item-pekerjaan").prop('required',false);
  }else {
    $("#new-todo-item-pekerjaan").prop('required',true);
  }

  $('.select2').select2({
    tags: true
  });
  $('.selectpicker').selectpicker('refresh');
  setActiveTab('ul-new', 'new-form');
  $('#ul-new').text('View Engineering Task');

  if (data['APPROVE0_AT'] || data['APPROVE1_AT']) {
    $('#btn-save').hide();
  }
  if (page_state.indexOf(page_short + '-modify=1') == -1) {
    $('.' + page_short + '-modify').hide();
  }
});

$(document).on("click", "#to-do-list-update", function(e){
  $("#TO_DO_EDIT").val($(this).data("ktem"));
  $("#TO_DO_NUMBER").val($(this).data("k"));
  if(modState < 1){
    $("#edit_to_do_modal").modal("show");
  } else {
    hideShowModal('edit_to_do_modal');
  }
  modState += 1;
});

$('#edit_to_do_close').click(function(){
    hideShowModal('edit_to_do_modal');
});

$(document).on('click', "#act_edit_to_do", function () {
  var id_eat = $('#id_eat_ref').val(); 
  var num = $('#TO_DO_NUMBER').val();
  var desk = $('#TO_DO_EDIT').val();

  $.ajax({
    type: 'POST',
    url: 'eat/assign/edit_paket_pekerjaan',
    data: {
      "id_eat" : id_eat,
      "num" : num,
      "pp" : desk
    },
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
    },
    success: function(json) {
      alert('Revisi paket pekerjaan ('+desk+') berhasil dibuat.');
      hideShowModal('edit_to_do_modal');
      $(".pekerjaan"+num).html(desk + "<i style='float:right; cursor:pointer' class='todo-item-delete fa fa-times'></i>");
    }
  });
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").prop("disabled", true);
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  no_penugasan = $('#NO_PENUGASAN').val();
  id_pengajuan = $('#ID_PENGAJUAN').val();
  notifikasi = $('#NOTIF_NO').val();
  no_erf = $('#NO_ERF').val();
  short_text = $('#SHORT_TEXT').val();
  lokasi = $('#FL_TEXT').val();
  start_date = $('#START_DATE').val();
  end_date = $('#END_DATE').val();
  objective = $('#OBJECTIVE').val();
  cust_req = $('#CUST_REQ').val();
  scope_eng = $('#SCOPE_ENG').val();
  de_construct_cost = toNumber($('#DE_CONSTRUCT_COST').val());
  de_eng_cost = toNumber($('#DE_ENG_COST').val());
  lead_ass = $('#LEAD_ASS').val();
  komite_pengarah = $('#KOMITE_PENGARAH').val();
  var val = [];
  $(':checkbox:checked ').each(function(i) {
    val[i] = $(this).val();
  });
  tim_pengkaji = JSON.stringify(val);
  tw_kom_pengarah = $('#TW_KOM_PENGARAH').val();
  tw_tim_pengkaji = $('#TW_TIM_PENGKAJI').val();
  strpath = document.getElementById('FILES').files[0];
  strfile = $('#FILES').attr('data-url');
  strpath2 = document.getElementById('FILES2').files[0];
  strfile2 = $('#FILES2').attr('data-url');
  strpath3 = document.getElementById('FILES3').files[0];
  strfile3 = $('#FILES3').attr('data-url');
  var list_pekerjaan = [];
  $(".pekerjaan").each(function() {
    ppekerjaan = $(this).text();
    if (ppekerjaan) {
      list_pekerjaan.push(ppekerjaan);
    }
  });
  lvl = $('#LVL').val();
  app_mgr = $('#MANAGER').val().split(" - ");
  app_sm = $('#SM_APP').val().split(" - ");

  var formURL = "";

  if (list_pekerjaan.length>0) {
    var form_data = new FormData();
    form_data.append('ID', id);
    form_data.append('NO_PENUGASAN', no_penugasan);
    form_data.append('ID_PENGAJUAN', id_pengajuan);
    form_data.append('NOTIFIKASI', notifikasi);
    form_data.append('NO_ERF', no_erf);
    form_data.append('SHORT_TEXT', short_text);
    form_data.append('LOKASI', lokasi);
    form_data.append('START_DATE', start_date);
    form_data.append('END_DATE', end_date);
    form_data.append('OBJECTIVE', objective);
    form_data.append('CUST_REQ', cust_req);
    form_data.append('SCOPE_ENG', scope_eng);
    form_data.append('DE_CONSTRUCT_COST', de_construct_cost);
    form_data.append('DE_ENG_COST', de_eng_cost);
    form_data.append('LEAD_ASS', lead_ass);
    form_data.append('KOMITE_PENGARAH', komite_pengarah);
    form_data.append('TIM_PENGKAJI', tim_pengkaji);
    form_data.append('TW_KOM_PENGARAH', tw_kom_pengarah);
    form_data.append('TW_TIM_PENGKAJI', tw_tim_pengkaji);
    if (strpath) {
      form_data.append('STRPATH', strpath);
    } else {
      if (strfile) {
        form_data.append('FILES', strfile);
      }
    }
    if (strpath2) {
      form_data.append('STRPATH2', strpath2);
    } else {
      if (strfile2) {
        form_data.append('FILES2', strfile2);
      }
    }
    if (strpath3) {
      form_data.append('STRPATH3', strpath3);
    } else {
      if (strfile3) {
        form_data.append('FILES', strfile3);
      }
    }
    form_data.append('LIST_PEKERJAAN', JSON.stringify(list_pekerjaan));
    form_data.append('LVL', lvl);
    if (lvl == '1') {
      form_data.append('APPROVE1_BADGE', app_sm[0]);
      form_data.append('APPROVE1_BY', app_sm[1]);
      form_data.append('APPROVE1_JAB', app_sm[2]);
    } else {
      form_data.append('APPROVE0_BADGE', app_mgr[0]);
      form_data.append('APPROVE0_BY', app_mgr[1]);
      form_data.append('APPROVE0_JAB', app_mgr[2]);
    }

    var uri = 'create';
    if (id) {
      uri = 'update';
    }

    $.ajax({
      url: 'eat/assign/' + uri, // point to server-side PHP script
      dataType: 'json', // what to expect back from the PHP script, if anything
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      error: function(xhr, status, error) {
        swal({
          title: "Data Save : NOT Success! " + error,
          type: "error"
        });
      },
      success: function(json) {
        if (json['status'] == 200) {
          tb_assign();
          swal({
            title: "Data Saved!\n" + no_penugasan,
            type: "success"
          });
          reset_form('yes');
        } else {
          swal({
            title: "Data Save : NOT Success! ",
            type: "error"
          });
        }
        $("#btn-save").prop("disabled", false);
        $("#btn-save").removeClass("disabled");
      }
    });
  }else {
    swal({
      title: "List Pekerjaan Null! (pastikan untuk menekan tombol +)",
      type: "error"
    });
  }
  $("#btn-save").prop("disabled", false);
  $("#btn-save").removeClass("disabled");
  return false;
});

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['NO_PENUGASAN'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'eat/assign/delete/' + data['ID'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_assign();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});

function generateDoc() {
  var tahun;
  var lokasi;
  var Nomor;
  $.ajax({
    type: 'POST',
    url: 'eat/assign/generateNoDoc',
    success: function(data) {
      try {
        data = JSON.parse(data);
        Nomor = data.nomor;
        tahun = data.tahun;

        $("#NO_PENUGASAN").val("/" + "/" + "/" + "EA/" + Nomor + "/" + "/" + tahun);

      } catch (e) {
        alert(e);
      } finally {

      }

    },
    error: function() {
      console.log("error");
    }
  });
}
$('#preview_close').click(function(){
    hideShowModal('preview_modal');
});

var modState = 0;
function previewDocument(input){
    var file = input.files[0];
    var mime_types = [ 'application/pdf' ];
    if(mime_types.indexOf(file.type) == -1) {
        alert('Error : Incorrect file type');
        return;
    }
    _OBJECT_URL = URL.createObjectURL(file)
    var pdf = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="assets/pdfviewer/web/viewer.html?file=' + _OBJECT_URL+ '" scrolling="no"></iframe>';
    $('#preview_body').html(pdf);

    if (modState < 1) {
        $('#preview_modal').modal('show');   
    } else {
        hideShowModal('preview_modal');
    }
    modState += 1;
}

$(document).on("change", "#LEAD_ASS", function(e){
	var tech = $(this).val();
	if(tech == "SM of Technology Development"){
		$.ajax({
			type: 'POST',
			url: 'eat/assign/get_approver_lead_tech',
			data: {
				"tech" : tech
			},
			success: function(response) {
				var data = JSON.parse(response).data;
				$("#MANAGER").val(data.GEN_CODE +" - "+data.GEN_DESC+" - "+data.GEN_PAR1).change();
			}
		});
	}
});

$(document).on("change", "#MANAGER", function(e){
	console.log($(this).val());
});

$(document).on('click', "#add-todo-item-pekerjaan-revisi", function () {
    //e.preventDefault();
    var id_eat = $('#id_eat_ref').val(); 
    var desk = $('#new-todo-item-pekerjaan-revisi').val();
    
    // console.log(id_eat);
    // console.log(desk);
    
        $.ajax({
			type: 'POST',
			url: 'eat/assign/buat_revisi_pekerjaan',
			data: {
				"id_eat" : id_eat,
                "pp" : desk
			},
            error: function(xhr, status, error) {
              swal({
                title: "Data Save : NOT Success! " + error,
                type: "error"
              });
            },
			success: function(json) {
              alert('Revisi paket pekerjaan ('+desk+') berhasil dibuat.');
              //reset_form('yes');
            }
		});
    //location.reload();
});