var oTable;

$(document).ready(function() {
  // getForeign();
  if ($('#ID_PENUGASAN').val()) {
    onForeign();
  }
  tb_docs();
  $('.select2').select2();
  // generateDoc();

  // setAtasan();
  // reloadtabel();
});

function generateDoc(){
  var tahun;
  var lokasi;
  var Nomor;
  $.ajax({
          type:'POST',
          url:'dok_eng/documents/generateNoDoc',
                    //data:{'id':id},
          success:function(data){
              console.log("test " + data);
              try {
                data = JSON.parse(data);
                Nomor = data.nomor;
                tahun = data.tahun;

                $("#NO_DOK_ENG").val(tahun+"/"+"/"+"/"+"TR/"+Nomor);

              } catch (e) {
                alert(e);
              } finally {

              }

          },error: function() {
              console.log("error");
          }
        });
}

function setActiveTab(ul_id = null, content_id = null) {
  $(".nav-link").removeClass("active show");
  $('#' + ul_id).addClass('active show');
  $(".tab-pane").removeClass("active show");
  $('#' + content_id).addClass('active show');
}

function tb_docs() {
  oTable = $('#tb_docs').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'eng/docs/data_list',
      type: "POST"
    },
    columns: [
      // {
      //   "className": 'details-control',
      //   "orderable": false,
      //   "data": null,
      //   "defaultContent": ''
      // },
      {
        "data": "ERF"
      },
      {
        "data": "PACKET_TEXT",
        "mRender": function(row, data, index) {
          return ' - ' + index['PACKET_TEXT'];
        }
      },
      {
        "data": "ID",
        "width": 70,
        "mRender": function(row, data, index) {
          var creator_erf = index["ERF_CREATOR"];
          var approver_erf = index["ERF_APPROVER"];
          var role_login = $("#role_login").val();;
          var user_login = $("#user_name_login").val();
          console.log(creator_erf+"="+approver_erf);

          srks='';
          sdis = 'disabled';
          if (index['ISCLOSED']!=='0') {
            sdis = '';
          }
          if (index['RKS_FILE'] && page_state.indexOf(page_short+'-exp-tr=1') != -1 && !index['KAJIAN_FILE']) {
            srks='<button class="btnExport btn btn-xs btn-success '+ sdis +'" data-toggle="tooltip" data-tipe="TR" style="background-color: #00a69c;" title="Download TOR" '+ sdis +' ><i class="ti ti-download"></i> T </button>';
          }
          sbq='';
          if (index['BQ_FILE'] && page_state.indexOf(page_short+'-exp-bq=1') != -1 && !index['KAJIAN_FILE']) {
            sbq='<button class="btnExport btn btn-xs btn-warning '+ sdis +'" data-toggle="tooltip" data-tipe="BQ" title="Download BQ" '+ sdis +' ><i class="ti ti-download"></i> B </button>';
          }
          sdraw='';
          if (index['DRAW_FILE'] && page_state.indexOf(page_short+'-exp-draw=1') != -1 && !index['KAJIAN_FILE']) {
            sdraw='<button class="btnExport btn btn-xs btn-info '+ sdis +'" data-toggle="tooltip" data-tipe="DRW" style="background-color: #288cff;" title="Download Drawing" '+ sdis +' ><i class="ti ti-download"></i> D </button>';
          }
          sece='';
          if (index['ECE_FILE'] && page_state.indexOf(page_short+'-exp-ece=1') != -1 && !index['KAJIAN_FILE']) {
            if(creator_erf == user_login || approver_erf == user_login || role_login == 2 || role_login == 5 || role_login == 3){
              sece='<button class="btnExport btn btn-xs btn-danger '+ sdis +'" data-toggle="tooltip" data-tipe="EC" style="background-color: #c00c16;" title="Download ECE" '+ sdis +' ><i class="ti ti-download"></i> E </button>';
            }
          }
          skj = '';
          if (index['KAJIAN_FILE'] && page_state.indexOf(page_short + '-exp-kaj=1') != -1) {
            skj = '<button class="btnExport btn btn-xs btn-primary" data-toggle="tooltip" data-tipe="KJ" style="background-color: #a85232;" title="Download Kajian" ><i class="fa fa-download"></i> K </button>';
          }
          strm = '';
          if (index['ID_BAST']) {
            // strm = '<li class="btnExportBast" style="height: 20px;padding-top: 0px !important;font-size: 10px;cursor: pointer;" ><a>Transmital</a></li>';
            strm = '<button class="btnExportBast btn btn-xs btn-white" data-toggle="tooltip" data-tipe="TF" title="Download Transmital" ><i class="fa fa-download"></i> TF </button>';
            // strm = '<button class="btnExportBast btn btn-xs btn-white" data-toggle="tooltip" data-tipe="TF" title="Download Transmital" ><i class="ti ti-download"></i> F </button>';
          }
          sexp = '';
          sbgroup2 = '';
          if (page_state.indexOf(page_short+'-exp-group=1') == -1) {
            sexp = ' style="display:none;"';
          }
          sbgroup2 = '<button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" data-tipe="All" title="Download"' + sexp + ' ><i class="fa fa-download"></i></button>';
          return '<center><div style="float: none;" class="btn-group">'+sbgroup2+srks+sbq+sdraw+sece+skj+strm+'</div></center>';
          // sbgroup = '<div class="btn-group '+ sdis +'"'+sexp+' '+ sdis +'><button type="button" class="btn btn-xs btn-dark dropdown-toggle '+ sdis +'" data-toggle="dropdown" aria-expanded="false" '+ sdis +'><span class="caret"></span><i class="fa fa-download"></i></button><ul class="dropdown-menu" role="menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 40px, 0px); top: 0px; left: 0px; will-change: transform;"><li class="btnExport" data-tipe="All" style="height: 20px;padding-top: 0px !important;font-size: 10px;cursor: pointer;" ><a >All</a></li>'+strm+'</ul></div>';
          // return '<center><div style="float: none;" class="btn-group">'+sbgroup+srks+sbq+sdraw+sece+'</div></center>';
          // return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark '+ sdis +'" data-toggle="tooltip" data-tipe="All" title="Export All"'+sexp+' '+ sdis +' ><i class="fa fa-download"></i> </button>'+srks+sbq+sdraw+sece+'</div></center>';
        }
      },
    ],
    "createdRow": function(row, data, dataIndex) {
      if (Number(data['PENDING']) < 1 && /detail/i.test(data['TIPE'])) {
        $('td', row).addClass("color-danger text-danger");
      }

    },
    columnDefs: [
        { "visible": false, "targets": 0 }
    ],
    drawCallback: function (settings) {
        var api = this.api();
        var rows = api.rows({ page: 'current' }).nodes();
        var last = null;

        api.column(0, { page: 'current' }).data().each(function (group, i) {

            if (last !== group) {

                $(rows).eq(i).before(
                    '<tr class="group"><td colspan="6" style="/*background-color:rgba(116, 96, 238,0.2);*/font-weight:700;color:#006232;text-align:left;">' + ' ' + group  + '</td></tr>'
                );

                last = group;
            }
        });
    }
  });

  $('#tb_docs tbody').on('click', 'td.details-control', function() {
    var tr = $(this).closest('tr');
    var row = oTable.row(tr);

    if (row.child.isShown()) {
      // This row is already open - close it
      row.child.hide();
      tr.removeClass('shown');
    } else {

      var html = '';
      var d = row.data();
      var data;
      var approveAt = '';
      var status = '';
      var i = 0;

      html += '<div class="container" style="width:1000px;height:400px;overflow-y:scroll">' +
      '<table class="table display table-bordered table-striped" id="tableprogress">' +
      '<thead>' +
      '<tr>' +
      '<th>No</th>' +
      '<th>Type</th>' +
      '<th>Bureau</th>' +
      '<th>Date</th>' +
      '<th>By</th>' +
      '<th style="width:100px">Note</th>' +
      '</tr>' +
      '</thead>' +
      '<tbody>';

      //paraf
      $.ajax({
        url: 'dok_eng/documents/get_paraf',
        data: {
          id: d.ID
        },
        dataType: 'json',
        type: 'POST',
        beforeSend: function() {},
        success: function(result) {
          data = result;
          $.map(data, function(item, index) {
            var date = item.UPDATE_AT;
            if (!date) {
              date = '-';
            }
            html += '<tr>' +
              '<td>' + (index + 1) + '</td>' +
              '<td>Paraf</td>' +
              '<td>'+ item.APP_JAB+ '</td>' +
              //if(item.UPDATE_AT != null) {
              '<td>'+ date +  '</td>' +
              // }else {
              //   '<td>-</td>' +
              // }
              '<td>' + item.APP_NAME + '</td>' +
              '<td style="width:280px">'+item.note+'</td>' +
              '</tr>';
              i++;
          });
          return html;
        },
        error: function() {

        },
        complete: function() {
          //TTD
          $.ajax({
            url: 'dok_eng/documents/get_ttd',
            data: {
              id: d.ID
            },
            dataType: 'json',
            type: 'POST',
            beforeSend: function() {},
            success: function(result) {
              data = result;
              $.map(data, function(item, index) {
                if (item.UPDATE_AT == null) {
                  item.UPDATE_AT = '-';
                }
                html += '<tr>' +
                  '<td>' + (i + index + 1) + '</td>' +
                  '<td>TTD</td>' +
                  '<td>'+ item.APP_JAB+ '</td>' +
                  //if (item.UPDATE_AT != null) {
                    '<td>'+ item.UPDATE_AT +  '</td>' +
                  //}else {
                  //  '<td>-</td>' +
                  //}
                  '<td>' + item.APP_NAME + '</td>' +
                  '<td style="width:280px">'+item.note+'</td>' +
                  '</tr>';
              });
              return html;
            },
            error: function() {

            },
            complete: function() {
              $.ajax({
                url: 'dok_eng/documents/get_ttd_smgm',
                data: {
                  id: d.ID
                },
                dataType: 'json',
                type: 'POST',
                beforeSend: function() {},
                success: function(result) {
                  data = result;
                  $.map(data, function(item, index) {
                    if (item.APPROVE1_AT == null) {
                      item.APPROVE1_AT = '-';
                    }
                    if (item.APPROVE2_AT == null) {
                      item.APPROVE2_AT = '-';
                    }
                    if (item.APPROVE3_AT == null) {
                      item.APPROVE3_AT = '-';
                    }
                    html += '<tr>' +
                      '<td>' + (i + index + 1) + '</td>' +
                      '<td>TTD Manager</td>' +
                      '<td>'+ item.APPROVE1_JAB + ' Design & Engineering</td>' +
                      //if (item.APPROVE1_AT != null) {
                        '<td>'+ item.APPROVE1_AT +  '</td>' +
                      //}else {
                      //'<td>-</td>' +
                      //}
                      '<td>' + item.APPROVE1_BY + '</td>' +
                      '<td style="width:280px">'+item.NOTE1+'</td>' +
                      '</tr>';

                    html += '<tr>' +
                      '<td>' + (i + index + 1) + '</td>' +
                      '<td>TTD BIRO</td>' +
                      '<td>'+ item.APPROVE2_JAB+ '</td>' +
                      //if (item.APPROVE2_AT != null) {
                        '<td>'+ item.APPROVE2_AT +  '</td>' +
                      //}else {
                      //  '<td>-</td>' +
                      //}
                      //'<td>'+ item.APPROVE2_AT +  '</td>' +
                      '<td>' + item.APPROVE2_BY + '</td>' +
                      '<td style="width:280px">'+item.NOTE2+'</td>' +
                      '</tr>';

                    html += '<tr>' +
                      '<td>' + (i + index + 1) + '</td>' +
                      '<td>TTD Department</td>' +
                      '<td>'+ item.APPROVE3_JAB+ '</td>' +
                      //if (item.APPROVE3_AT != null) {
                        '<td>'+ item.APPROVE3_AT +  '</td>' +
                      //}else {
                      //  '<td>-</td>' +
                      //}
                      '<td>' + item.APPROVE3_BY + '</td>' +
                      '<td style="width:280px">'+item.NOTE3+'</td>' +
                      '</tr>';

                  });
                  return html;
                },
                error: function() {

                },
                complete: function() {
                  html +=
                    '</tbody>' +
                    '</table>' +
                    '</div>';
                  // console.log(html);
                  row.child(html).show();
                  tr.addClass('shown');
                  return html;
                }
              });
            }
          });
        }
      });

    }
  });
}

function getForeign() {
  $.ajax({
    type: 'POST',
    url: 'dok_eng/documents/foreign',
    success: function(json) {
      json = $.parseJSON(json);
      $('#ID_PENUGASAN').html('');
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          foreigns = ('<option value >Please select...</option>');
          $.each(json['data'], function(i, data) {
            foreigns += ('<option value="' + data['ID_PENUGASAN'] + '" title="' + data['NO_PENUGASAN'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENUGASAN'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" >' + data['NO_PENUGASAN'] + ' - ' + data['PENGAJUAN'] + '</option>');
          });
        } else {
          foreigns = ('<option value >Empty...</option>');
        }
        $('#ID_PENUGASAN').html(foreigns);
        onForeign();
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function getForeignDtl(f_id = null) {
  // var form_data = new FormData();
  // form_data.append('F_ID', f_id);
  $.ajax({
    type: 'POST',
    url: 'dok_eng/documents/foreign_dtl/'+f_id,
    success: function(json) {
      json = $.parseJSON(json);
      $('#ID_PACKET').html('');
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          foreigns = ('<option value >Please select...</option>');
          $.each(json['data'], function(i, data) {
            foreigns += ('<option value="' + data['ID'] + '" >' + data['DESKRIPSI'] + '</option>');
            if (data['TIPE'] == 'Capex') {
              var e = document.getElementById("kajian");
              var f = document.getElementById("capex");
              //console.log(e);
              e.style.display = "none";
              f.style.display = "block";
            }else {
              var e = document.getElementById("capex");
              var f = document.getElementById("kajian");
              //console.log(e);
              e.style.display = "none";
              f.style.display = "block";
            }
          });
        } else {
          foreigns = ('<option value >Empty...</option>');
        }
        $('#ID_PACKET').html(foreigns);
        // onForeign();
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function select_content_change(selecttag, urlselect, placeholder, data) {
  $(''+selecttag).select2({
    minimumInputLength: 3,
    allowClear: true,
    placeholder: placeholder,
    theme: 'bootstrap',
    ajax: {
      dataType: 'json',
      url: urlselect,
      data: data,
      delay: 50,
      type: 'POST',
      data: function(params) {
        return {
          search: params.term
          // search: params.term,
          // additional_data: data
        }
      },
      processResults: function(data, page) {
        console.log(data);
        return {
          results: $.map(data['data'], function(obj) {
              console.log(obj);
              if (obj.ID) {
                // var objectcode = '000'+obj.ID_DATA+'';
                // var stdcode = objectcode.slice(-3);

                return {
                    id: obj.ID,
                    text: obj.DESKRIPSI,
                    // photo: obj.photo
                };
              } else {
                return {
                    id: obj.ID,
                    text: obj.DESKRIPSI,
                    // photo: obj.photo
                };
              }

          })
        };
      },
    }
  });
}

function load_selected_option(key, id, text) {
  $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
  $(key).select2('data', {
    id: id,
    label: text
  });
  $(key).trigger('change'); // Notify any JS components that the value changed
}

function onForeign() {
  $('#NO_PENUGASAN').val($('#ID_PENUGASAN :selected').attr('data-no'));
  $('#NOTIFIKASI').val($('#ID_PENUGASAN :selected').attr('data-notif'));
  $('#PENGAJUAN').val($('#ID_PENUGASAN :selected').attr('data-pengajuan'));
  $('#OBJECTIVE').val($('#ID_PENUGASAN :selected').attr('data-object'));
  // data['F_ID'] = $('#ID_PENUGASAN :selected').val();
  // select_content_change('#ID_PACKET', "dok_eng/documents/foreign_dtl", 'Pilih Packet ...',data);
  // $('.select2').select2();
}

$('#ID_PENUGASAN').on('change', function() {
  onForeign();
  getForeignDtl($('#ID_PENUGASAN :selected').val());
  var no_dok = $("#NO_DOK_ENG").val();
  var arr = no_dok.split("/");
  var pengajuan = $("#NO_PENUGASAN").val();
  console.log(pengajuan);
  var arr2 = pengajuan.split("/");
  $("#NO_DOK_ENG").val(arr[0]+"/"+arr2[1]+"/"+arr2[2]+"/"+arr[3]+"/"+arr[4]);
})

function reset_form(reset = null) {
  $('#ID').val('');
  $('#NO_DOK_ENG').val('');
  $('#ID_PENUGASAN').html('');
  $('#NO_PENUGASAN').val('');
  $('#NOTIFIKASI').val('');
  $('#PENGAJUAN').val('');
  $('#OBJECTIVE').val('');
  $('#ID_PACKET').html('');
  $('#PACKET_TEXT').val('');
  $('#PROGRESS').val('');

  // RKS
  $('#RKS_FILE').val('');
  $('#rks_uri').attr('href', '');
  $('#rks_name').removeClass('fa fa-file-pdf-o');
  $('#rks_name').text('');

  // BQ
  $('#BQ_FILE').val('');
  $('#bq_uri').attr('href', '');
  $('#bq_name').text('');
  $('#bq_name').removeClass('fa fa-file-pdf-o');

  // Drawing
  $('#DRAW_FILE').val('');
  $('#draw_uri').attr('href', '');
  $('#draw_name').text('');
  $('#draw_name').removeClass('fa fa-file-pdf-o');

  // ECE
  $('#ECE_FILE').val('');
  $('#ece_uri').attr('href', '');
  $('#ece_name').removeClass('fa fa-file-pdf-o');
  $('#ece_name').text('');

  getForeign();

  $("#APP_LIST").val('');
  $("#APP_LIST").removeAttr('disabled');
  $("#PARAF_LIST").val('');
  $("#PARAF_LIST").removeAttr('disabled');
  $('.select2').select2();

  $("#btn-save").removeClass("disabled");
  $("#btn-save").html(' <i class="fa fa-check"></i> Save');
  $('.selectpicker').selectpicker('refresh');
  $('#ul-new').text('New Engineering Task');
  $('#btn-save').show();

  if (reset) {
    setActiveTab('ul-list', 'list-form');
  }

  generateDoc();

}

$("#btn-cancel").on("click", function() {
  reset_form();
});

// export
$(document).on('click', ".btnExport", function() {
  var data = oTable.row($(this).parents('tr')).data();
  console.log($(this).attr('data-tipe'));
  sAdd='';
  if ($(this).attr('data-tipe')!='All') {
    sAdd = '/'+$(this).attr('data-tipe');
    window.location.assign('eng/filing/export_pdf/' + data['ID']+sAdd);
  }else {
    window.location.assign('dok_eng/documents/export_pdf/' + data['ID']);
  }
  // window.location.assign('eng/docs/export_pdf/' + data['ID']+sAdd);
});

// export transmital
$(document).on('click', ".btnExportBast", function() {
  var data = oTable.row($(this).parents('tr')).data();
  window.location.assign('bast/handover/export_pdf/' + data['ID_BAST']);
});


// event resend mail
$(document).on('click', ".btnResend", function() {
  $('.title_ukur').text('Resend Email');
  var data = oTable.row($(this).parents('tr')).data();


  var form_data = new FormData();
  form_data.append('ID', data['ID']);

  $.ajax({
    url: 'dok_eng/documents/resend', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Resend Email : NOT Success! " + error,
        type: "error"
      });
      // $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    },
    success: function(json) {
      if (json['status'] == 200) {
        // tb_request();
        // tb_erf2();
        swal({
          title: "Email was sent.",
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
      // $("#btn-save").val("Save");
    }
  });
});

// view
$(document).on('click', ".btnEdit", function() {
  var data = oTable.row($(this).parents('tr')).data();
  $('#ID').val(data['ID']);
  $('#NO_DOK_ENG').val(data['NO_DOK_ENG']);
  if (data['ID_PENUGASAN']) {
    $('#ID_PENUGASAN').html('<option value="' + data['ID_PENUGASAN'] + '" title="' + data['NO_PENUGASAN'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENUGASAN'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" >' + data['NO_PENUGASAN'] + '</option>');
  }
  $('#NO_PENUGASAN').val(data['NO_PENUGASAN']);
  $('#NOTIFIKASI').val(data['NOTIFIKASI']);
  $('#PENGAJUAN').val(data['PENGAJUAN']);
  $('#OBJECTIVE').val(data['OBJECTIVE']);
  if (data['ID_PACKET']) {
    $('#ID_PACKET').html('<option value="' + data['ID_PACKET'] + '" >' + data['PACKET_TEXT'] + '</option>');
  }
  $('#PACKET_TEXT').val(data['PACKET_TEXT']);
  $('#PROGRESS').val(data['PROGRESS']);

  // LIST APP
  if (data['LIST_APP']) {
    var aApp = data['LIST_APP'].split(':');
    console.log(aApp);
    console.log(aApp.length);
    $('#APP_LIST').val(aApp);
    $('#APP_LIST').attr('disabled','disabled');;
  }
  // LIST Paraf
  if (data['LIST_PARAF']) {
    var aPar = data['LIST_PARAF'].split(':');
    console.log(aPar);

    $('#PARAF_LIST').val(aPar);
    $('#PARAF_LIST').attr('disabled','disabled');;
  }

  // RKS
  if (data['RKS_FILE']) {
    var aFiles = data['RKS_FILE'].split('/');

    $('#rks_uri').attr('href', data['RKS_FILE']);
    $('#rks_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#rks_name').addClass('fa fa-file-pdf-o');
  }
  // BQ
  if (data['BQ_FILE']) {
    var aFiles = data['BQ_FILE'].split('/');

    $('#bq_uri').attr('href', data['BQ_FILE']);
    $('#bq_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#bq_name').addClass('fa fa-file-pdf-o');
  }
  // Drawing
  if (data['DRAW_FILE']) {
    var aFiles = data['DRAW_FILE'].split('/');

    $('#draw_uri').attr('href', data['DRAW_FILE']);
    $('#draw_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#draw_name').addClass('fa fa-file-pdf-o');
  }
  // ECE
  if (data['ECE_FILE']) {
    var aFiles = data['ECE_FILE'].split('/');

    $('#ece_uri').attr('href', data['ECE_FILE']);
    $('#ece_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#ece_name').addClass('fa fa-file-pdf-o');
  }
  // Kajian
  if (data['KAJIAN_FILE']) {
    var aFiles = data['KAJIAN_FILE'].split('/');

    $('#kajian_uri').attr('href', data['KAJIAN_FILE']);
    $('#kajian_name').text(' ' + aFiles[(aFiles.length - 1)]);
    $('#kajian_name').addClass('fa fa-file-pdf-o');
  }

  if (data['STATUS']) {
    $('#btn-save').hide();
  }


  $('.select2').select2();
  $('.selectpicker').selectpicker('refresh');
  setActiveTab('ul-new', 'new-form');
  $('#btn-save').html(' <i class="fa fa-check"></i> Edit');
  $('#ul-new').text('Edit Engineering Task');
  if (page_state.indexOf(page_short+'-modify=1') == -1) {
    $('.'+page_short+'-modify').hide();
  }
});

// save
$("#fm-new").submit(function(e) {
  e.preventDefault();
  $("#btn-save").addClass("disabled");

  id = $('#ID').val();
  no_dok_eng = $('#NO_DOK_ENG').val();
  tipe = 'All';
  id_penugasan = $('#ID_PENUGASAN').val();
  // no_penugasan = $('#NO_PENUGASAN').val();
  notifikasi = $('#NOTIFIKASI').val();
  id_packet = $('#ID_PACKET').val();
  packet_text = $('#ID_PACKET :selected').text();
  progress = $('#PROGRESS').val();
  rkspath = document.getElementById('RKS_FILE').files[0];
  rksfile = $('#RKS_FILE').attr('data-url');
  bqpath = document.getElementById('BQ_FILE').files[0];
  bqfile = $('#BQ_FILE').attr('data-url');
  drawpath = document.getElementById('DRAW_FILE').files[0];
  drawfile = $('#DRAW_FILE').attr('data-url');
  ecepath = document.getElementById('ECE_FILE').files[0];
  ecefile = $('#ECE_FILE').attr('data-url');
  kajianpath = document.getElementById('KAJIAN_FILE').files[0];
  kajianfile = $('#KAJIAN_FILE').attr('data-url');
  tmp_app = $('#APP_LIST').val();
  tmp_paraf = $('#PARAF_LIST').val();

  var formURL = "";

  var form_data = new FormData();
  form_data.append('ID', id);
  form_data.append('NO_DOK_ENG', no_dok_eng);
  form_data.append('TIPE', tipe);
  form_data.append('ID_PENUGASAN', id_penugasan);
  // form_data.append('NO_PENUGASAN',no_penugasan);
  form_data.append('NOTIFIKASI', notifikasi);
  form_data.append('ID_PACKET', id_packet);
  form_data.append('PACKET_TEXT', packet_text);
  form_data.append('PROGRESS', progress);
  // RKS
  if (rkspath) {
    form_data.append('RKSPATH', rkspath);
  } else {
    if (rksfile) {
      form_data.append('RKSFILE', rksfile);
    }
  }
  // BQ
  if (bqpath) {
    form_data.append('BQPATH', bqpath);
  } else {
    if (bqfile) {
      form_data.append('BQFILE', bqfile);
    }
  }
  // Drawing
  if (drawpath) {
    form_data.append('DRAWPATH', drawpath);
  } else {
    if (drawfile) {
      form_data.append('DRAWFILE', drawfile);
    }
  }
  // ECE
  if (ecepath) {
    form_data.append('ECEPATH', ecepath);
  } else {
    if (ecefile) {
      form_data.append('ECEFILE', ecefile);
    }
  }
  // Kajian
  if (kajianpath) {
    form_data.append('KAJIANPATH', kajianpath);
  } else {
    if (ecefile) {
      form_data.append('KAJIANFILE', kajianfile);
    }
  }
  var list_app = [];
  var index = 1;
  $('#APP_LIST :selected').each(function(i, selectedElement) {
    tmpApp = $(selectedElement).val().split(' - ');
    if (i<8) {
      form_data.append('APPROVE'+index+'_BY', tmpApp[1]);
      form_data.append('APPROVE'+index+'_JAB', tmpApp[3]);
    }
    index++;
  });
  form_data.append('LIST_APP', $('#APP_LIST').val().join(':'));
  form_data.append('LIST_PARAF', $('#PARAF_LIST').val().join(':'));


  var uri = 'create';
  if (id) {
    uri = 'update';
  }

  $.ajax({
    url: 'dok_eng/documents/' + uri, // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      swal({
        title: "Data Save : NOT Success! " + error,
        type: "error"
      });
      $("#btn-save").removeClass("disabled");
    },
    success: function(json) {
      if (json['status'] == 200) {
        tb_docs();
        swal({
          title: "Data Saved!\n" + no_dok_eng,
          type: "success"
        });
        reset_form('yes');
      }
      $("#btn-save").removeClass("disabled");
    }
  });
  return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function() {
  var data = oTable.row($(this).parents('tr')).data();
  swal({
      title: "Are you sure?",
      text: "Data (" + data['NO_DOK_ENG'] + ") will be deleted!",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Delete",
      confirmButtonClass: "btn-danger",
      cancelButtonText: "Cancel",
      closeOnConfirm: true,
      closeOnCancel: true
    },
    function(isConfirm) {
      if (isConfirm) {
        url = 'dok_eng/documents/delete/' + data['ID'];
        $.ajax({
          url: url, // point to server-side PHP script
          dataType: 'json', // what to expect back from the PHP script, if anything
          cache: false,
          contentType: false,
          processData: false,
          type: 'get',
          error: function(xhr, status, error) {
            swal("Error", "Your data failed to delete!", "error");
          },
          success: function(json) {
            if (json['status'] == 200) {
              tb_docs();
              swal("Deleted!", "Your data has been deleted.", "success");
            }
          }
        });
      } else {
        swal("Cancelled", "Deletion of data canceled", "error");
      }
    });
});
