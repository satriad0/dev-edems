var dtTable;
var base_url_api = "<?= base_url(); ?>";
var base_url = window.location.origin + '/' + window.location.pathname.split ('/') [1] + '/';;
var base_host = window.location.host;

var editUserNopeg, editUserName;
var tmpSwerk, tmpIwerk;
var modState = 0;
var tmpSplant, tmpPgrp, tmpSpgrp;
var files1, files2, files3;

$(document).ready(function () {
    tb_erf2();
    
    // Setup - add a text input to each footer cell
    $('#tb_erf2_tr_search th').each( function () {
        var title = $(this).text();
        $(this).html( '<input style="width:100%;" type="text" placeholder="Search '+title+'" />' );
    } ); 
    
    $("#DESSTDATE, #DESENDDATE").datepicker({
        autoclose: true,
        format:'dd/mm/yyyy'
    });

    // $("#PLANGROUP option[value='499']").remove();
    

    tmpSplant = $('#PLANPLANT').val();
    tmpPgrp = $('#PLANGROUP').val();
    tmpSpgrp = $('#PLANGROUP option:selected').text();
    locOnChange();
    areaOnChange();
    disiplinOnChange();
    pgroupOnChange();
    $('.select2').select2();
    $('input[name^=CONSTRUCT_COST],input[name^=ENG_COST]').priceFormat({
        prefix: '',
        centsSeparator: '.',
        centsLimit: 0,
        thousandsSeparator: ',',
        clearOnEmpty: false,
    });
    select_content_app('#APP_LIST', "user/search", 'Please type some word ...', '<= 20');
    select_content_change('#FUNCT_LOC', "erf/request/view_funcloc", 'Select Functional Location ...');

    if (/detail/i.test($("#JENIS").val())) {
        $("#CUST_REQ").addClass("disabled");
        $("#CUST_REQ").prop("disabled", true);
    } else {
        $("#CUST_REQ").removeClass("disabled");
        $("#CUST_REQ").prop("disabled", false);
    }
    $('.opt-lain').change(function () {
        if (/detail/i.test($("#JENIS").val())) {
            if (this.checked) {
                $("#CUST_REQ").removeClass("disabled");
                $("#CUST_REQ").prop("disabled", false);
            } else {
                $("#CUST_REQ").addClass("disabled");
                $("#CUST_REQ").prop("disabled", true);
            }
        }
    });
});

function setActiveTab(ul_id = null, content_id = null) {
    $(".nav-link").removeClass("active show");
    $('#' + ul_id).addClass('active show');
    $(".tab-pane").removeClass("active show");
    $('#' + content_id).addClass('active show');
}

function openModal(id = null) {
    $('#' + id).modal('show');
    $('#' + id).show();
    $('.modal-backdrop').show();
}

function closeModal(id = null) {
    $('#' + id).hide();
    $('.modal-backdrop').hide();
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

function tb_erf2() {
    var view = '';
    var shideres = '';
    if (page_state.indexOf(page_short + '-child=1') != -1) {
        view = 'child';
        // shideres = ' style="display:none;"';
    }
    
    //console.log(view);
    
    var oTable = $('#tb_erf2').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: 'erf/request/data_list',
            type: "POST",
            data: function (d) {
                d.view = view;
                // d.custom = $('#myInput').val();
                // etc
            }
        },
        initComplete: function () {
            
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
        },
        columns: [{
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "CREATE_AT",
                "width": 75
            },
            {
                "data": "NO_PENGAJUAN"
            },
            {
                "data": "NOTIFIKASI"
            },
            {
                "data": "NAMA_PEKERJAAN"
            },
            {
                "data": "PRIOR_TEXT",
                "mRender": function (row, data, index) {
                    if (index['PRIOR_TEXT'] == 'High') {
                        return '<center><p style="color: red">High  <img src="assets/images/high.png" style="vertical-align: text-top;"></p></center> '
                    } else if (index['PRIOR_TEXT'] == 'Medium') {
                        return '<center><p style="color: #f1c40f">Medium  <img src="assets/images/medium.png" style="vertical-align: text-top;"></p></center>'
                    } else {
                        return '<center><p style="color: #1F9E0E">Low  <img src="assets/images/low.png" style="vertical-align: text-top;"></p></center>'
                    }
                }
            },
            {
                "data": "STATUS",
                "mRender": function (row, data, index) {
                    if (index['STATUS'] == 'Approved') {
                        return '<center><p style="color: green">Approved </p></center> ';
                    } else if (index['STATUS'] == 'Open') {
                        return '<center><p style="color: blue">Open </p></center>';
                    } else if (index['STATUS'] == 'Rejected') {
                        return '<center><p style="color: red">Rejected  </p></center>';
                    } else {
                        return '<center><p style="color: yellow">' + index['STATUS'] + '  </p></center>';
                    }
                }
            },
            {
                "data": "DESCPSECTION"
            },
            {
                "data": "DESC"
            },
            {
                "data": "COMP_TEXT"
            },
            {
                "data": "CREATE_BY"
            },
            {
                "data": "ID_MPE",
                "width": 100,
                "mRender": function (row, data, index) {
                    sdis = '';
                    if ((page_state.indexOf(page_short + '-delete=1') == -1) || index['STATUS'].indexOf('Open') == -1) {
                        sdis = 'disabled style="display:none;" '; 
                        //penggantian button atribute disabled menjadi hide [Task no.20]
                    }
                    srej = '';
                    if (page_state.indexOf(page_short + '-child=1') != -1 && index['DISABLING'] == '0' && index['STATUS'] == 'Approved') {
                        srej = '<button class="btnReject btn btn-xs btn-warning ' + page_short + '-reject" data-toggle="tooltip" title="Reject" ><i class="fa fa-remove"></i></button>';
                        sdis = '';
                    }
                    if (page_state.indexOf('special_access=false') == -1 && index['DISABLING'] == '0' && index['STATUS'] == 'Approved' && page_state.indexOf(page_short + '-delete=1') != -1) {
                        sdis = '';
                    }
                    if (page_state.indexOf(page_short + '-spc-del=1') != -1 && index['DISABLING'] == '0' && index['STATUS'] == 'Approved') {
                        sdis = '';
                    }
                    sexp = '';
                    if (page_state.indexOf(page_short + '-export=1') == -1) {
                        sexp = ' style="display:none;"';
                    }
                    if (index['STATUS'] == 'Approved') {
                        return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download"' + sexp + ' ' + sexp + ' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete' + ' ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button>' + srej + '</div></center>';
                    } else if (index['STATUS'] == 'Open') {
                        return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download (Download dengan Lampiran)"' + sexp + ' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnResend btn btn-xs btn-success" ' + shideres + ' data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button><button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button></div></center>';
                    } else {
                        return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download"' + sexp + ' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-info" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button><button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button>' + srej + '</div></center>';
                    }

                }
            },
        ],
        "order": [
            [0, "desc"]
        ],
        "createdRow": function (row, data, dataIndex) {
            if (page_state.indexOf('-child=1') != -1) {
                // if (Number(data['PENDING']) < 1 && data['STATUS'] == 'Approved') {
                if (data['STATUS'] == 'Open') {
                    $('td', row).addClass("color-danger text-danger");
                }
            }

        }
    });
    
   

    dtTable = oTable;

    // Add event listener for opening and closing details
    $('#tb_erf2 tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = oTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            var html = '';
            var d = row.data();
            //console.log(d);
            var data;
            var approveAt = '';
            var status = '';
            html += '<div class="container">' +
                    '<ul class="progressbar">' +
                    '<li data-toggle="tooltip" data-placement="top" title="' + status + '">Request Form</li>' +
                    '<li>Assign Task</li>' +
                    '<li>Documents</li>' +
                    '<li>Finish</li>' +
                    '</ul>';

            var dokeng;

            $.ajax({
                url: 'erf/request/getApproval',
                data: {
                    id: d.ID_MPE
                },
                dataType: 'json',
                type: 'POST',
                success: function (result) {
                    data = result;
                    statuspenugasan = '';
                    statusdok = '';
                    statusbast = '';
                    fprogress = 0;
                    $.map(data, function (item, index) {
                        pengajuan = item.STATUS1;
                        penugasan = item.STATUS2;
                        if (item.STATUS1 === 'Open') {
                            status = 'Pending';
                        } else if (item.STATUS1 === 'Approved') {
                            status = 'active';
                            fprogress = 12.5;
                        } else {
                            status = 'reject';
                        }
                        if (item.STATUS2 === 'In Approval') {
                            statuspenugasan = 'pending';
                        } else if (item.STATUS2 === 'Approved Biro.' || item.STATUS2 === 'SM Approved') {
                            statuspenugasan = 'active';
                            fprogress = 25;
                        } else if (item.STATUS2 === 'Approved Mgr.' || item.STATUS2 === 'MGR Approved') {
                            statuspenugasan = 'active';
                            fprogress = 25;
                        } else if (item.STATUS2 === 'Rejected') {
                            statuspenugasan = 'reject';
                        } else {
                            statuspenugasan = '';
                        }
                        if (item.DOKSTATUS === 'ada') {
                            statusdok = 'active';
                            statuspenugasan = 'active';
                            fprogress = parseFloat(item.STS_PRGS);
                        } else {
                            statusdok = '';
                        }
                        if (item.STATUSBAST === 'Closed') {
                            statusbast = 'active';
                        } else if (item.STATUSBAST === 'Rejected') {
                            statuspenugasan = 'reject';
                        } else {
                            statusbast = '';
                        }
                    });

                    html = '<div class="container">' +
                            '<ul class="progressbar">'; 

                    if (status === 'Pending') {
                        html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
                    } else if (status === 'reject') {
                        html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" title="Pending">Request Form</li>';
                    } else {
                        html += '<li class="' + status + '" data-toggle="tooltip" data-placement="top" >Request Form</li>';
                    }
                    html += '<li class="' + statuspenugasan + '">Assign Task</li>';
                    html += '<li class="' + statusdok + '">Engineering Documents</li>';
                    html += '<li class="' + statusbast + '">Finish</li>' +
                            '</ul>' +
                            '<button type="button" id="btn_progres_req" dt_id_mpe="'+d.ID_MPE+'" dt_no="'+d.NO_PENGAJUAN+'" dt_np="'+d.NAMA_PEKERJAAN+'" class="btn btn-dark btn-outline btn-rounded btn-sm" style="cursor:default;position: absolute;">' + parseFloat(fprogress).toFixed(2) + ' %</button>' +
                            '</div>' +
                            '<div class="container" style="height:400px;width:1000px;overflow-y:scroll">' +
                            '<table class="table display table-bordered table-striped" id="tableprogress">' +
                            '<thead>' +
                            '<tr>' +
                            '<th>No</th>' +
                            '<th>Type</th>' +
                            '<th>Status</th>' +
                            '<th>Work Unit</th>' +
                            '<th>Date</th>' +
                            '<th>By</th>' +
                            '<th style="width:100px">Note</th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>';



                    $.ajax({
                        url: 'erf/request/getLogPengajuan',
                        data: {
                            id: d.ID_MPE
                        },
                        dataType: 'json',
                        type: 'POST',
                        beforeSend: function () {},
                        success: function (result) {
                            data = result;
                            //console.log(d.ID_MPE);
                            $.map(data, function (item, index) {
                                dokeng = item.ID;
                                html += '<tr>' +
                                        '<td>' + (index + 1) + '</td>' +
                                        '<td>ERF Create</td>' +
                                        '<td>Created</td>' +
                                        '<td>' + item.UK_TEXT + '</td>' +
                                        '<td>' + item.CREATE_AT + '</td>' +
                                        '<td>' + item.CREATE_BY + '</td>' +
                                        '<td style="width:280px">-</td>' +
                                        '</tr>';
                                        
                                if (item.APPROVE_AT != null && item.APPROVE_BY != null) {
                                    html += '<tr>' +
                                            '<td>' + (index + 2) + '</td>' +
                                            '<td>ERF Approval</td>' +
                                            '<td>Approved</td>' +
                                            '<td>' + item.APPROVE_JAB + '</td>' +
                                            '<td>' + item.APPROVE_AT + '</td>' +
                                            '<td>' + item.APPROVE_BY + '</td>' +
                                            '<td style="width:280px">' + item.NOTE + '</td>' +
                                            '</tr>';
                                } 
                                else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                                    html += '<tr>' +
                                            '<td>' + (index + 2) + '</td>' +
                                            '<td>ERF Approval</td>' +
                                            '<td>Rejected</td>' +
                                            '<td>' + item.APPROVE_JAB + '</td>' +
                                            '<td>' + item.REJECT_DATE + '</td>' +
                                            '<td>' + item.REJECT_BY + '</td>' +
                                            '<td style="width:280px">' + item.NOTE + '</td>' +
                                            '</tr>';
                                } 
                                else {
                                    html += '<tr>' +
                                            '<td>' + (index + 2) + '</td>' +
                                            '<td>ERF Approval</td>' +
                                            '<td>-</td>' +
                                            '<td>' + item.APPROVE_JAB + '</td>' +
                                            '<td>-</td>' +
                                            '<td>' + item.APPROVE_BY + '</td>' +
                                            '<td style="width:280px">-</td>' +
                                            '</tr>';
                                }  
                                
                                if (item.REJECT_FILE != null) {
                                    index++;
                                    arFile = item.REJECT_FILE.split('/');
                                    html += '<tr>' +
                                            '<td>' + (index + 2) + '</td>' +
                                            '<td>ERF Approval</td>' +
                                            '<td>Rejected</td>' +
                                            '<td>' + item.APPROVE_JAB + '</td>' +
                                            '<td>' + item.REJECT_DATE + '</td>' +
                                            '<td>' + item.REJECT_BY + '</td>' +
                                            '<td style="width:280px"><a href="' + item.REJECT_FILE + '" rel="noopener noreferrer" target="_blank" ><i class="fa fa-file-pdf-o"></i> ' + arFile[(arFile.length - 1)] + '</a></td>' +
                                            '</tr>';
                                }
                                
                                if (item.DELETE_AT != null) {
                                    index++;
                                    html += '<tr>' +
                                            '<td>' + (index + 2) + '</td>' +
                                            '<td>ERF Approval</td>' +
                                            '<td>Deleted</td>' +
                                            '<td>' + item.APPROVE_JAB + '</td>' +
                                            '<td>' + item.DELETE_AT + '</td>' +
                                            '<td>' + item.DELETE_BY + '</td>' +
                                            '<td style="width:280px">-</td>' +
                                            '</tr>';
                                }
                                
                                
                                if (item.CREATEAT2 != null) {
                                    html += '<tr>' +
                                            '<td>' + (index + 3) + '</td>' +
                                            '<td>EAT Create</td>' +
                                            '<td>Created</td>' +
                                            '<td>' + item.UKTEXT2 + '</td>' +
                                            '<td>' + item.CREATEAT2 + '</td>' +
                                            '<td>' + item.CREATEBY2 + '</td>' +
                                            '<td style="width:280px">-</td>' +
                                            '</tr>';
                                } else {
                                    html += '<tr>' +
                                            '<td>' + (index + 3) + '</td>' +
                                            '<td>EAT Create</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td style="width:280px">-</td>' +
                                            '</tr>';
                                }
                                
                                if (item.APPROVE0_BY != null && item.APPROVE0_AT != null) {
                                    html += '<tr>' +
                                            '<td>' + (index + 4) + '</td>' +
                                            '<td>EAT Approval MGR</td>' +
                                            '<td>Approved</td>' +
                                            '<td>' + item.APPROVE0_JAB + '</td>' +
                                            '<td>' + item.APPROVE0_AT + '</td>' +
                                            '<td>' + item.APPROVE0_BY + '</td>' +
                                            '<td style="width:280px">' + item.NOTE0 + '</td>' +
                                            '</tr>';
                                } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
                                    html += '<tr>' +
                                            '<td>' + (index + 4) + '</td>' +
                                            '<td>EAT Approval MGR</td>' +
                                            '<td>Approved</td>' +
                                            '<td>' + item.APPROVE0_JAB + '</td>' +
                                            '<td>' + item.REJECTAT2 + '</td>' +
                                            '<td>' + item.REJECTBY2 + '</td>' +
                                            '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                                            '</tr>';
                                } else {
                                    html += '<tr>' +
                                            '<td>' + (index + 4) + '</td>' +
                                            '<td>EAT Approval MGR</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td style="width:280px">-</td>' +
                                            '</tr>';
                                }
                                
                                if (item.APPROVE1_BY != null && item.APPROVE1_AT != null) {
                                    html += '<tr>' +
                                            '<td>' + (index + 5) + '</td>' +
                                            '<td>EAT Approval SM</td>' +
                                            '<td>Approved</td>' +
                                            '<td>' + item.APPROVE1_JAB + '</td>' +
                                            '<td>' + item.APPROVE1_AT + '</td>' +
                                            '<td>' + item.APPROVE1_BY + '</td>' +
                                            '<td style="width:280px">' + item.NOTE1 + '</td>' +
                                            '</tr>';
                                } else if (item.REJECTAT2 != null && item.REJECTBY2 != null && item.APPROVE1_BY == item.REJECTBY2) {
                                    html += '<tr>' +
                                            '<td>' + (index + 5) + '</td>' +
                                            '<td>EAT Approval SM</td>' +
                                            '<td>Approved</td>' +
                                            '<td>' + item.APPROVE1_JAB + '</td>' +
                                            '<td>' + item.REJECTAT2 + '</td>' +
                                            '<td>' + item.REJECTBY2 + '</td>' +
                                            '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                                            '</tr>';
                                } else {
                                    html += '<tr>' +
                                            '<td>' + (index + 5) + '</td>' +
                                            '<td>EAT Approval SM</td>' +
                                            '<td>-</td>' +
                                            '<td>-</td>' + 
                                            '<td>-</td>' +
                                            '<td>-</td>' +
                                            '<td style="width:280px">-</td>' +
                                            '</tr>';
                                }
                                
                            });
                            return html;
                        },
                        error: function () {

                        },
                        complete: function () {
                            var i = 6;
                            //paraf
                            $.ajax({
                                url: 'erf/request/get_doc_erf',
                                data: {
                                    id: d.ID_MPE
                                },
                                dataType: 'json',
                                type: 'POST',
                                beforeSend: function () {},
                                success: function (result) {
                                    data = result.data;
                                    $.map(data, function (item, index) {

                                        html += '<tr>' +
                                                '<td>' + (index + i) + '</td>' +
                                                '<td>'+(item.TIPE == "CRT" ? "Created" : item.TIPE )+' Document Engineering : '+item.NO_DOK_ENG+'</td>' +
                                                '<td>'+item.STATE+'</td>' +
                                                '<td>' + item.APP_JAB + '</td>' +
                                                '<td>' + item.UPDATE_AT + '</td>' +
                                                '<td>' + item.APP_NAME + '</td>' +
                                                '<td style="width:280px"> ' + item.NOTE + '</td>' +
                                                '</tr>';

                                        

                                        // if (item.APPROVE1_AT != null) {
                                        //   html += '<tr>' +
                                        //     '<td>' + (index + i) + '</td>' +
                                        //     '<td>TTD Manager</td>' +
                                        //     '<td>Approved</td>' +
                                        //     '<td>' + item.APPROVE1_AT + '</td>' +
                                        //     '<td>' + item.APPROVE1_BY + '</td>' +
                                        //
                                        //     '<td style="width:280px"> ' + item.NOTE1 + '</td>' +
                                        //     '</tr>';
                                        // } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                                        //   html += '<tr>' +
                                        //     '<td>' + (index + i) + '</td>' +
                                        //     '<td>TTD Manager</td>' +
                                        //     '<td>Rejected</td>' +
                                        //     '<td>' + item.REJECT_DATE + '</td>' +
                                        //     '<td>' + item.REJECT_BY + '</td>' +
                                        //     '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                                        //     '</tr>';
                                        // } else {
                                        //   html += '<tr>' +
                                        //     '<td>' + (index + i) + '</td>' +
                                        //     '<td>TTD Manager</td>' +
                                        //     '<td>-</td>' +
                                        //     '<td>-</td>' +
                                        //     '<td>' + item.APPROVE1_BY + '</td>' +
                                        //     '<td style="width:280px">-</td>' +
                                        //     '</tr>';
                                        // }
                                        // i++;
                                        //
                                        // if (item.APPROVE2_AT != null) {
                                        //   html += '<tr>' +
                                        //     '<td>' + (index + i) + '</td>' +
                                        //     '<td>TTD SM</td>' +
                                        //     '<td>Approved</td>' +
                                        //     '<td>' + item.APPROVE2_AT + '</td>' +
                                        //     '<td>' + item.APPROVE2_BY + '</td>' +
                                        //
                                        //     '<td style="width:280px"> ' + item.NOTE2 + '</td>' +
                                        //     '</tr>';
                                        // } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                                        //   html += '<tr>' +
                                        //     '<td>' + (index + i) + '</td>' +
                                        //     '<td>TTD SM</td>' +
                                        //     '<td>Rejected</td>' +
                                        //     '<td>' + item.REJECT_DATE + '</td>' +
                                        //     '<td>' + item.REJECT_BY + '</td>' +
                                        //     '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                                        //     '</tr>';
                                        // } else {
                                        //   html += '<tr>' +
                                        //     '<td>' + (index + i) + '</td>' +
                                        //     '<td>TTD SM</td>' +
                                        //     '<td>-</td>' +
                                        //     '<td>-</td>' +
                                        //     '<td>' + item.APPROVE2_BY + '</td>' +
                                        //     '<td style="width:280px">-</td>' +
                                        //     '</tr>';
                                        // }
                                        //
                                        // i++

                                        // if (item.APPROVE3_AT != null) {
                                        //     html += '<tr>' +
                                        //             '<td>' + (index + i) + '</td>' +
                                        //             '<td>TTD GM</td>' +
                                        //             '<td>Approved</td>' +
                                        //             '<td>' + item.APPROVE3_AT + '</td>' +
                                        //             '<td>' + item.APPROVE3_BY + '</td>' +
                                        //             '<td style="width:280px"> ' + item.NOTE3 + '</td>' +
                                        //             '</tr>';
                                        // } else if (item.REJECT_DATE != null && item.REJECT_BY != null) {
                                        //     html += '<tr>' +
                                        //             '<td>' + (index + i) + '</td>' +
                                        //             '<td>TTD GM</td>' +
                                        //             '<td>Rejected</td>' +
                                        //             '<td>' + item.REJECT_DATE + '</td>' +
                                        //             '<td>' + item.REJECT_BY + '</td>' +
                                        //             '<td style="width:280px">' + item.REJECT_REASON + '</td>' +
                                        //             '</tr>';
                                        // } else {
                                        //     html += '<tr>' +
                                        //             '<td>' + (index + i) + '</td>' +
                                        //             '<td>TTD GM</td>' +
                                        //             '<td>-</td>' +
                                        //             '<td>-</td>' +
                                        //             '<td>' + item.APPROVE3_BY + '</td>' +
                                        //             '<td style="width:280px">-</td>' +
                                        //             '</tr>';
                                        // }
                                        // i++;

                                        // if (item.BASTAT != null && item.BASTBY != null) {
                                        //     html += '<tr>' +
                                        //             '<td>' + (index + i) + '</td>' +
                                        //             '<td>Transmital</td>' +
                                        //             '<td>Created</td>' +
                                        //             '<td>' + item.BASTAT + '</td>' +
                                        //             '<td>' + item.BASTBY + '</td>' +
                                        //             '<td style="width:280px"> Transmital Created ' + item.NO_BAST + '</td>' +
                                        //             '</tr>';
                                        //     i++;

                                        // } else {

                                        // }

                                    });
                                    i++;
                                    return html;
                                },
                                error: function () {

                                },
                                complete: function (iii) {
                                    html +=
                                            '</tbody>' +
                                            '</table>' +
                                            '</div>';
                                    // console.log(html);
                                    row.child(html).show();
                                    tr.addClass('shown');
                                    return html;
                                }
                            });
                        }
                    });
                    return html;
                },
                error: function () {
                    alert("Error");
                },
                complete: function () {
                    return html;
                }
            });


        }
    });

}

function scroll() {
    $('#tableprogress').DataTable({
        "scrollY": 100,
        "paging": false
    });
}

function generateDoc() {
    var tahun;
    var lokasi;
    var Nomor;
    $.ajax({
        type: 'POST',
        url: 'erf/request/generateNoDoc',
        //data:{'id':id},
        success: function (data) {
            // console.log("test " + data);
            try {
                data = JSON.parse(data);
                Nomor = data.nomor;
                tahun = data.tahun;

                if (!($('#IDMPE').val())) {
                    $("#NOPENGAJUAN").val("/" + "/" + "/" + "EM/" + Nomor + "/" + "/" + tahun);
                    if ($('#KDLOC').val()) {
                        locOnChange();
                    }
                    if ($('#KDAREA').val()) {
                        areaOnChange();
                    }
                }

            } catch (e) {
                alert(e);
            } finally {

            }

        },
        error: function () {
            console.log("error");
        }
    });

}

function locOnChange() {
    var no_pengajuan = $("#NOPENGAJUAN").val();
    var arr = no_pengajuan.split("/");
    kdlc = $("#KDLOC").val();
    $("#NOPENGAJUAN").val(kdlc + "/" + arr[1] + "/" + arr[2] + "/" + arr[3] + "/" + arr[4] + "/" + arr[5] + "/" + arr[6]);
}

$('#KDLOC').change(function () {
    if (!($('#IDMPE').val())) {
        locOnChange();
    }
});

function areaOnChange() {
    var no_pengajuan = $("#NOPENGAJUAN").val();
    var arr = no_pengajuan.split("/");
    kdarea = $("#KDAREA").val();
    $("#NOPENGAJUAN").val(arr[0] + "/" + kdarea + "/" + arr[2] + "/" + arr[3] + "/" + arr[4] + "/" + arr[5] + "/" + arr[6]);
}

$('#KDAREA').change(function () {
    if (!($('#IDMPE').val())) {
        areaOnChange();
    }
});

function disiplinOnChange() {
    var no_pengajuan = $("#NOPENGAJUAN").val();
    var arr = no_pengajuan.split("/");
    kddis = '';
    if ($("#KDDIS option:selected").data("tipe") == $('#JENIS').val()) {
        kddis = $("#KDDIS").val() + $("#KDDIS option:selected").data("par1");
    } else {
        kddis = $("#KDDIS").val() + $("#KDDIS option:selected").data("par2");
    }

    $("#NOPENGAJUAN").val(arr[0] + "/" + arr[1] + "/" + kddis + "/" + arr[3] + "/" + arr[4] + "/" + arr[5] + "/" + arr[6]);
}

$('#KDDIS').change(function () {
    if (!($('#IDMPE').val())) {
        disiplinOnChange();
    }
});

function pgroupOnChange() {
    var no_pengajuan = $("#NOPENGAJUAN").val();
    var arr = no_pengajuan.split("/");
    kdpgroup = $("#PLANGROUP option:selected").data("kode");

    $("#NOPENGAJUAN").val(arr[0] + "/" + arr[1] + "/" + arr[2] + "/" + arr[3] + "/" + arr[4] + "/" + kdpgroup + "/" + arr[6]);
}

$('#PLANGROUP').change(function () {
    if (!($('#IDMPE').val())) {
        pgroupOnChange();
    }
});

function wbsChange() {
    if (/detail/i.test($("#JENIS").val())) {
        $(".wbs-view").show();
        $(".opt-view").show();
    } else {
        $(".wbs-view").hide();
        $(".opt-view").hide();
        $("#CUST_REQ").removeClass("disabled");
        $("#CUST_REQ").prop("disabled", false);
    }
}

function pGroupChange() {
    var tipe = $('#JENIS').val();
    if (tipe.indexOf('Kajian') != -1) {
        $.ajax({
            type: 'POST',
            url: 'erf/request/get_pgroup_kajian',
            data: {
                "TIPE": tipe
            },
            success: function (json) {
                json = $.parseJSON(json);
                if (json) {
                    if (json['status'] == 200) {
                        var item = json['data']
                        $('#PLANPLANT').val(item['GEN_PAR1'] + ' - ' + item['GEN_PAR7']);
                        $('#PLANGROUP').html('<option selected value="' + item['GEN_CODE'] + '">' + item['GEN_CODE'] + ' - ' + item['GEN_DESC'] + '</option>');
                    }
                    $('.selectpicker').selectpicker('refresh');
                }
            },
            error: function (xhr, status, error) {
                alert('Server Error ...');
            }
        });
    } else {
        $('#PLANPLANT').val(tmpSplant);
        $('#PLANGROUP').html('<option selected value="' + tmpPgrp + '">' + tmpSpgrp + '</option>');
        $('.selectpicker').selectpicker('refresh');
    }

}

function defPgroup() {
    var tipe = $('#JENIS').val();
    if (tipe.indexOf('Kajian') != -1) {
        $('#PLANPLANT').val($('#DEF_PLANT').val());
        $('#PLANGROUP').html('<option selected value="' + $('#DEF_PGROUP').val() + '">' + $('#DEF_PGROUP option:selected').text() + '</option>');
        $('.selectpicker').selectpicker('refresh');
    } else {
        $('#PLANPLANT').val(tmpSplant);
        $('#PLANGROUP').html('<option selected value="' + tmpPgrp + '">' + tmpSpgrp + '</option>');
        $('.selectpicker').selectpicker('refresh');
    }
}

$('#JENIS').change(function () {
    if (!($('#IDMPE').val())) {
        disiplinOnChange();
    }
    wbsChange();

    if($(this).val() == "Kajian Teknis"){
        $('select[name*="PLANGROUP"] option[value^="499"]').attr("hide");
    } else {
        $('select[name*="PLANGROUP"] option[value="499"]').add();
    }
});

function refreshFunloc(str = null) {
    var qq;
    if (str) {
        qq = str;
    } else {
        qq = "{{{q}}}"
    }
    $(".selectpicker").selectpicker().filter('#FUNCT_LOC').ajaxSelectPicker({
        "ajax": {
            "url": 'erf/request/view_funcloc',
            "type": "GET",
            "dataType": "json",
            "data": {
                token: localStorage.token,
                q: qq
            }
        },
        "log": false,
        "preprocessData": function (json) {
            return_data = [];
            if (json) {
                $.each(json, function (i, item) {
                    return_data.push({
                        "value": item['TPLNR'],
                        "text": item['DESC'],
                        "data": {
                            "subtext": item['TPLNR'],
                            "tplnr": item['TPLNR'],
                            // "strno": item['STRNO'],
                            "objnr": item['OBJNR'],
                            "pltxt": item['DESC'],
                            "swerk": item['MPLANT'],
                            "descmplant": item['AS_DESCMPLANT'],
                            "iwerk": item['IWERK']
                        }
                    });
                });
            }

            return return_data;
            $('.selectpicker').selectpicker('refresh');
        }
    });
}

function select_content_change(selecttag, urlselect, placeholder) {
    $('' + selecttag).select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: placeholder,
        theme: 'bootstrap',
        ajax: {
            dataType: 'json',
            url: urlselect,
            delay: 50,
            type: 'POST',
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data, function (obj) {
                        if (obj.OBJNR) {

                            return {
                                id: obj.OBJNR + ' - ' + obj.TPLNR + ' - ' + obj.MPLANT + ' - ' + obj.AS_DESCMPLANT + ' - ' + obj.IWERK,
                                text: '[ ' + obj.TPLNR + ' ] ' + obj.DESC,
                            };
                        } else {
                            return {
                                id: obj.OBJNR + ' - ' + obj.TPLNR + ' - ' + obj.MPLANT + ' - ' + obj.AS_DESCMPLANT + ' - ' + obj.IWERK,
                                text: '[ ' + obj.NOBADGE + ' ] ' + obj.DESC,
                            };
                        }

                    })
                };
            },
        }
    });
    $('' + selecttag).trigger('change');
}

function select_content_app(selecttag, urlselect, placeholder, addition = '') {
    $('' + selecttag).select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: placeholder,
        theme: 'bootstrap',
        ajax: {
            dataType: 'json',
            url: urlselect,
            // data: data,
            delay: 50,
            type: 'POST',
            data: function (params) {
                return {
                    search: params.term,
                    additional: addition
                }
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data, function (obj) {
                        // console.log(obj);
                        if (obj.NOBADGE) {
                            return {
                                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.EMAIL + ' - ' + obj.JAB_TEXT,
                                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                                // photo: obj.photo
                            };
                        } else {
                            return {
                                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.EMAIL + ' - ' + obj.JAB_TEXT,
                                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                                // photo: obj.photo
                            };
                        }

                    })
                };
            },
        }
    });
    $('' + selecttag).trigger('change');
}

function load_selected_option(key, id, text) {
    $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
    $(key).select2('data', {
        id: id,
        label: text
    });
    $(key).trigger('change'); // Notify any JS components that the value changed
}

$('#FUNCT_LOC').on('change', function () {
    if ($('#FUNCT_LOC').val()) {
        var arFuncloc = $('#FUNCT_LOC').val().split(' - ');
        $('#SWERK').val(arFuncloc[2] + ' - ' + arFuncloc[3]);
        if (($('#SWERK').val() && tmpSwerk != $('#SWERK').val()) || !$('#SWERK').val()) {
            $('#BEBER').html('');
            getPSection(arFuncloc[2]);
            tmpSwerk = $('#SWERK').val();
        }
        $('#LOKASI').val(arFuncloc[3]);
    }
})

$('#CONSTRUCT_COST').change(function () {
    if ($('#CONSTRUCT_COST').val()) {
        $('#ENG_COST').val(toCurrency(parseInt(parseInt(toNumber($('#CONSTRUCT_COST').val())) * parseFloat($('#ENG_FEE').val()))));
    }
});

function getPSection(mplant, psection = null) {
    $.ajax({
        type: 'POST',
        url: 'erf/request/view_psection',
        data: {
            "MPLANT": mplant
        },
        success: function (json) {
            json = $.parseJSON(json);
            var result = [];
            if (json) {
                if (json['status'] == 200) {
                    $.each(json['data'], function (i, item) {
                        var tmpdata = {};
                        tmpdata.id = item['PSECTION'];
                        tmpdata.text = item['PSECTION'] + ' - ' + item['DESCPSECTION'];
                        result.push(tmpdata);
                    });
                }
                $('#BEBER').select2({
                    'data': result
                });
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

function getPGroup(pplant, pgroup = null) {
    $.ajax({
        type: 'POST',
        url: 'erf/request/view_pgroup',
        data: {
            "PPLANT": pplant
        },
        success: function (json) {
            json = $.parseJSON(json);
            $('#PLANGROUP').html('');
            pgroups = '';
            if (json) {
                if (json['status'] == 200) {
                    $.each(json['data'], function (i, item) {
                        if (pgroup) {
                            pgroups += ('<option selected value="' + item['PGROUP'] + '">' + item['PGROUP'] + ' - ' + item['DESCPGROUP'] + '</option>');
                        } else {
                            pgroups += ('<option value="' + item['PGROUP'] + '">' + item['PGROUP'] + ' - ' + item['DESCPGROUP'] + '</option>');
                        }
                    });
                }
                $('#PLANGROUP').html(pgroups);
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

function reset_form(reset = null) {
    $('#IDMPE').val('');
    $('#REV_NO').val('0');
    $('#NOPENGAJUAN').val('');
    $('#col-pengajuan').hide();
    generateDoc();
    $('#KDLOC').val('ALL0').trigger('change');
    $('#KDAREA').val('00').trigger('change');
    $('#NOTIFIKASI').val('');
    $('#SHORT_TEXT').val('');
    $('#FUNCT_LOC').val('').trigger('change');
    $('#SWERK').val('');
    $('#BEBER').val('').trigger('change');
    $('#PLANPLANT').val(tmpSplant);
    $('#PLANGROUP').val(tmpPgrp);
    $('#LOKASI').val('');
    $('#PRIORITY').val('3');
    $('#WBS_CODE').val('');
    $('#WBS_TEXT').val('');
    $('#CONSTRUCT_COST').val('');
    $('#ENG_COST').val('');
    $('#DESSTDATE').val('');
    $('#DESENDDATE').val('');
    $('#LATAR_BELAKANG').val('');
    $(':checkbox ').each(function (i) {
        $(this).removeAttr('checked');
    });
    $('#CUST_REQ').val('');
    $('#TECH_INFO').val('');
    $('#RES_MIT').val('');
    $('#STATUS').val('');
    
    $('#ASP0').val('');
    $('#ASP1').val('');
    $('#ASP2').val('');
    $('#ASP3').val('');
    $('#ASP_OTHER').val('');

    $("#APP_LIST").val('');
    select_content_app('#APP_LIST', "user/search", 'Please type some word ...', '<= 20');

    $(".notif-el").removeClass("disabled");
    $(".notif-el").prop("disabled", false);
    $('#FUNCT_LOC').attr('required');
    $('#FUNCT_LOC').prop('required', true);
    $('#FILES').val('');
    $('#file_name').removeClass('fa fa-file-pdf-o');
    $('#file_uri').attr('href', '');
    $('#file_name').text('');
    $('#FILES2').val('');
    $('#file_name2').removeClass('fa fa-file-pdf-o');
    $('#file_uri2').attr('href', '');
    $('#file_name2').text('');
    $('#FILES3').val('');
    $('#file_name3').removeClass('fa fa-file-pdf-o');
    $('#file_uri3').attr('href', '');
    $('#file_name3').text('');
    $("#btn-save").removeClass("disabled");
    $("#btn-save").prop("disabled", false);
    $("#btn-save").val("Save");
    $('.selectpicker').selectpicker('refresh');

    $('#ul-new').text('Create Engineering Request');
    $("#btn-save").show();
    if (reset) {
        setActiveTab('ul-list', 'list-form');
}
}

// Cancel
$("#btn-cancel").on("click", function () {
    reset_form();
});

// export
$(document).on('click', ".btnExport", function (e) {
    e.preventDefault();
    var data = dtTable.row($(this).parents('tr')).data();
    //console.log(data);
    
    window.location.assign('erf/request/export_pdf/' + data['ID_MPE']);
    
    // action download lampiran
    setTimeout( download_lampiran_erf(data['FILES'], 'Lampiran-FS_'), 5000);
    setTimeout( download_lampiran_erf(data['FILES2'], 'Lampiran-Notulen_'), 5000);
    setTimeout( download_lampiran_erf(data['FILES3'], 'Lampiran-Pendukung_'), 5000);
});

function download_lampiran_erf(filePath , name){
    var link = document.createElement('a');
    link.href = filePath;
    link.download = name+filePath.substr(filePath.lastIndexOf('/') + 1);
    link.click(); 
}

// event view
$(document).on('click', ".btnResend", function () {
    $('.title_ukur').text('Resend Email');
    var data = dtTable.row($(this).parents('tr')).data();

    var form_data = new FormData();
    form_data.append('ID_MPE', data['ID_MPE']);

    $.ajax({
        url: 'erf/request/resend', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Resend Email : NOT Success! " + error,
                type: "error"
            });
        },
        success: function (json) {
            if (json['status'] == 200) {
                swal({
                    title: "Email was sent.",
                    type: "success"
                });
                reset_form('yes');
            }
            $("#btn-save").removeClass("disabled");
        }
    });
});

// event view
$(document).on('click', ".btnEdit", function () {
    $('.title_ukur').text('Data Request');
    var data = dtTable.row($(this).parents('tr')).data();
    files1 = data.FILES;
    files2 = data.FILES2;
    files3 = data.FILES3;
    
    reset_form();

    $('#IDMPE').val(data['ID_MPE']);
    $('#KDLOC').val(data['KDLOC']).trigger('change');
    $('#KDAREA').val(data['KDAREA']).trigger('change');
    if (data['KDDIS']) {
        $('#KDDIS').val(data['KDDIS']).trigger('change');
    }
    $('#NOPENGAJUAN').val(data['NO_PENGAJUAN']);
    $('#col-pengajuan').show();
    $('#NOTIFIKASI').val(data['NOTIFIKASI']);
    $('#SHORT_TEXT').val(data['NAMA_PEKERJAAN']);
    $('#JENIS').val(data['TIPE']).trigger('change');
    
    $('#ASP0').val(data['ASPECT1']).trigger('change');
    $('#ASP1').val(data['ASPECT2']).trigger('change');
    $('#ASP2').val(data['ASPECT3']).trigger('change');
    $('#ASP3').val(data['ASPECT4']).trigger('change');
    $('#ASP_OTHER').val(data['ASPECT_OTHER']);

    $('#LOKASI').val(data['LOKASI']);

    if (data['FUNCT_LOC']) {
        load_selected_option('#FUNCT_LOC', data['OBJNR'] + " - " + data['FUNCT_LOC'] + " - " + data['MPLANT'] + " - " + data['DESCMPLANT'] + " - " + data['PPLANT'], data['FUNCT_LOC']);
        ;
        
        // data['FL_TEXT']  tak ada
    }
    if (data['MPLANT']) {
        $('#SWERK').val(data['MPLANT'] + ' - ' + data['DESCMPLANT']);
    }
    if (data['PLANT_SECTION']) {
        load_selected_option('#BEBER', data['PLANT_SECTION'], data['PLANT_SECTION'] + ' - ' + data['DESCPSECTION']);
    }
    $('#PLANPLANT').val(data['PPLANT'] + ' - ' + data['DESCPLANT2']);
    if (data['PLANNER_GROUP']) {
        $('#PLANGROUP').val(data['PLANNER_GROUP']).trigger('change');
    }
    $('#PRIORITY').val(data['PRIORITY']);
    $('#WBS_CODE').val(data['WBS_CODE']);
    $('#WBS_TEXT').val(data['WBS_TEXT']);
    var desstdate = new Date(data['DESSTDATE']);
    $('#DESSTDATE').val(desstdate.getDate()+"/"+desstdate.getMonth()+"/"+desstdate.getFullYear());
    var desenddate = new Date(data['DESENDDATE']);
    $('#DESENDDATE').val(desenddate.getDate()+"/"+desenddate.getMonth()+"/"+desenddate.getFullYear());
    $('#CONSTRUCT_COST').val(toCurrency(data['CONSTRUCT_COST']));
    $('#ENG_COST').val(toCurrency(data['ENG_COST']));
    $('#LATAR_BELAKANG').val(data['LATAR_BELAKANG']);
    // LIST APP
    load_selected_option('#APP_LIST', data['APPROVE_BADGE'] + " - " + data['APPROVE_BY'] + " - " + data['APPROVE_EMAIL'] + " - " + data['APPROVE_JAB'], "[" + data['APPROVE_BADGE'] + "] " + data['APPROVE_BY'] + " - " + data['APPROVE_JAB']);
    if (/detail/i.test($("#JENIS").val())) {
        favorite = data['CUST_REQ'];
        $(':checkbox ').each(function (i) {
            if (favorite.indexOf($(this).val()) != -1) {
                $(this).attr('checked', 'checked');
                favorite = favorite.replace($(this).val() + "\n", "");
                favorite = favorite.replace($(this).val(), "");
            }
        });
        if (favorite) {
            $('.opt-lain').attr('checked', 'checked');
            $("#CUST_REQ").removeClass("disabled");
            $("#CUST_REQ").prop("disabled", false);
            $('#CUST_REQ').val(favorite);
        } else {
            $("#CUST_REQ").addClass("disabled");
            $("#CUST_REQ").prop("disabled", true);
        }
    } else {
        $(".wbs-view").hide();
        $(".opt-view").hide();
        $('#CUST_REQ').val(data['CUST_REQ']);
        $("#CUST_REQ").removeClass("disabled");
        $("#CUST_REQ").prop("disabled", false);
    }
    $('#TECH_INFO').val(data['TECH_INFO']);
    $('#RES_MIT').val(data['RES_MIT']);
    if (data['FILES']) {
        var aFiles = data['FILES'].split('/');

        $('#file_uri').attr('href', data['FILES']);
        $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#file_name').addClass('fa fa-file-pdf-o');
    }
    if (data['FILES2']) {
        var aFiles = data['FILES2'].split('/');

        $('#file_uri2').attr('href', data['FILES2']);
        $('#file_name2').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#file_name2').addClass('fa fa-file-pdf-o');
    }
    if (data['FILES3']) {
        var aFiles = data['FILES3'].split('/');

        $('#file_uri3').attr('href', data['FILES3']);
        $('#file_name3').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#file_name3').addClass('fa fa-file-pdf-o');
    }

    $('#STATUS').val(data['STATUS']);
    if (data['STATUS'] == 'Open') {
        $('#btn-save').show();
    } else {
        $('#btn-save').hide();
    }

    $(".notif-el").addClass("disabled");
    $(".notif-el").prop("disabled", true);
    $('#ul-new').text('View Engineering Request');
    $('.selectpicker').selectpicker('refresh');
    setActiveTab('ul-new', 'new-form');
    if (page_state.indexOf(page_short + '-modify=1') == -1) {
        $('.' + page_short + '-modify').hide();
    }
});

// event revision
$(document).on('click', ".btnRevisi", function () {
    $('.title_ukur').text('Data Request');
    var data = dtTable.row($(this).parents('tr')).data();

    $('#IDMPE').val(data['ID_MPE']);
    $('#REV_NO').val(parseInt(data['REV_NO']) + 1);
    $('#KDLOC').val(data['KDLOC']);
    $('#KDAREA').val(data['KDAREA']);
    $('#NOPENGAJUAN').val(data['NO_PENGAJUAN']);
    $('#NOTIFIKASI').val(data['NOTIFIKASI']);
    $('#SHORT_TEXT').val(data['NAMA_PEKERJAAN']);
    if (data['FUNCT_LOC']) {
        $('#FL_CODE').val(data['FUNCT_LOC']);
        load_selected_option('#FUNCT_LOC', data['OBJNR'] + " - " + data['FUNCT_LOC'] + " - " + data['MPLANT'] + " - " + data['DESCMPLANT'] + " - " + data['PPLANT'], data['FL_TEXT']);
        $('#FUNCT_LOC').html('<option selected value="' + data['FUNCT_LOC'] + '" title="' + data['FL_TEXT'] + '" data-subtext="' + data['FUNCT_LOC'] + '" data-tplnr="' + data['FUNCT_LOC'] + '" data-pltxt="' + data['FL_TEXT'] + '" data-swerk="' + data['MPLANT'] + '" data-descmplant="' + data['DESCMPLANT'] + '" data-iwerk="' + data['PLANT_SECTION'] + ' >' + data['FL_TEXT'] + '</option>');
    }
    if (data['MPLANT']) {
        $('#SWERK').val(data['MPLANT'] + ' - ' + data['DESCMPLANT']);
    }
    if (data['PLANT_SECTION']) {
        $('#BEBER').html('<option selected value="' + data['PLANT_SECTION'] + '">' + data['PLANT_SECTION'] + ' - ' + data['DESCPSECTION'] + '</option>');
    }
    $('#PLANPLANT').val(data['PPLANT']);
    if (data['PLANNER_GROUP']) {
        $('#PLANGROUP').val(data['PLANNER_GROUP']);
    }
    $('#LOKASI').val(data['LOKASI']);
    $('#PRIORITY').val(data['PRIORITY']);
    $('#WBS_CODE').val(data['WBS_CODE']);
    $('#WBS_TEXT').val(data['WBS_TEXT']);
    $('#DESSTDATE').val(data['DESSTDATE']);
    $('#DESENDDATE').val(data['DESENDDATE']);
    $('#LATAR_BELAKANG').val(data['LATAR_BELAKANG']);
    $('#CUST_REQ').val(data['CUST_REQ']);
    $('#TECH_INFO').val(data['TECH_INFO']);
    $('#RES_MIT').val(data['RES_MIT']);
    $('#ASP0').val(data['ASPECT1']);
    $('#ASP1').val(data['ASPECT2']);
    $('#ASP2').val(data['ASPECT3']);
    $('#ASP3').val(data['ASPECT4']);
    if (data['FILES']) {
        var aFiles = data['FILES'].split('/');

        $('#file_uri').attr('href', data['FILES']);
        $('#file_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#file_name').addClass('fa fa-file-pdf-o');
    }

    $('#FUNCT_LOC').removeAttr('required');
    $('#ul-new').text('Revision Engineering Request');
    $('.selectpicker').selectpicker('refresh');
    setActiveTab('ul-new', 'new-form');
});

// event save
$("#fm-new").submit(function (e) {
    e.preventDefault();
    $("#btn-save").addClass("disabled");
    $("#btn-save").prop("disabled", true);
    if ($('#STATUS').val() == 'Open') {
        update_request($('#IDMPE').val());
    } else {
        // disable return notifikasi sap
        // create_request(Math.floor((Math.random() * 100) + 1), 3000106206+Math.floor((Math.random() * 100) + 1), 700021+Math.floor((Math.random() * 100) + 1));

        // using sap notifikation
        if (parseInt($('#REV_NO').val()) > 0) {
            create_request($('#IDMPE').val(), $('#NOTIFIKASI').val(), 'revision');
        } else {
            create_notif_sap();
        }
    }

    return false;
});

//
function create_notif_sap() {
    $("#btn-save").prop("disabled", true);

    var arFuncloc = $('#FUNCT_LOC').val().split(' - ');
    code_funloc = arFuncloc[0];
    var objnr = code_funloc.split('IF');
    idmpe = $('#IDMPE').val();
    nopengajuan = $('#NOPENGAJUAN').val();
    short_text = $('#SHORT_TEXT').val();
    swerk = $('#SWERK').val();
    var mplant = swerk.split(' - ');
    beber = $('#BEBER').val();
    planplant = $('#PLANPLANT').val().split(' - ');
    plangroup = $('#PLANGROUP').val();
    funcloc = '';
    funcloc = arFuncloc[1];
    lokasi = $('#LOKASI').val();
    priority = $('#PRIORITY').val();
    wbs_code = $('#WBS_CODE').val();
    wbs_text = $('#WBS_TEXT').val();
    desstdate = $('#DESSTDATE').val();
    desenddate = $('#DESENDDATE').val();
    latar_belakang = $('#LATAR_BELAKANG').val();
    cust_req = $('#CUST_REQ').val();
    tech_info = $('#TECH_INFO').val();
    res_mit = $('#RES_MIT').val();
    aspect0 = $('#ASP0').val();
    aspect1 = $('#ASP1').val();
    aspect2 = $('#ASP2').val();
    aspect3 = $('#ASP3').val();

    var form_data = new FormData();
    form_data.append('LOGINSAP', 'BNURFAHRI');
    form_data.append('NOTIFTYPE', '01');
    form_data.append('FUNCT_LOC', objnr[1]);
    form_data.append('SHORT_TEXT', short_text);
    form_data.append('PRIORITY', priority);
    tglmulai = desstdate.split("-");
    form_data.append('START_DATE', tglmulai[0] + tglmulai[1] + tglmulai[2]);
    tglselesai = desenddate.split("-");
    form_data.append('END_DATE', tglselesai[0] + tglselesai[1] + tglselesai[2]);
    form_data.append('PLANPLANT', planplant[0]);
    form_data.append('PLANGROUP', plangroup);
    form_data.append('SWERK', mplant[0]);
    form_data.append('BEBER', beber);
    form_data.append('LATAR_BELAKANG', latar_belakang);
    form_data.append('CUST_REQ', cust_req);
    form_data.append('TECH_INFO', tech_info);
    form_data.append('RES_MIT', res_mit);
    form_data.append('ASPECT1', aspect0);
    form_data.append('ASPECT2', aspect1);
    form_data.append('ASPECT3', aspect2);
    form_data.append('ASPECT4', aspect3);

    $.ajax({
        url: 'notifikasi/create_notif', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Data Save : NOT Success! " + error,
                type: "error"
            });
            $("#btn-save").removeClass("disabled");
            $("#btn-save").prop("disabled", false);
        },
        success: function (cekdata) {
            if (cekdata['status'] == 200) {
                if (cekdata['data']['NOTIF_NO'] || cekdata['data']['NOTIF_NO'] != "") {
                    create_request(cekdata['data']['ID_SAP'], cekdata['data']['NOTIF_NO'], cekdata['data']['COSTCENTER']);
                } else {
                    swal("Cancelled", "Your Request Cannot process by system", "error");
                }
            } else {
                swal("Cancelled", "Your Request Cannot process by system", "error");
            }
        }
    });
}
//
function create_request(id_mpe, no_notif, cost_center, stat = 'create') {
    var arFuncloc = $('#FUNCT_LOC').val().split(' - ');
    rev_no = $('#REV_NO').val();
    kdloc = $('#KDLOC').val();
    kdarea = $('#KDAREA').val();
    kddis = $('#KDDIS').val();
    nopengajuan = $('#NOPENGAJUAN').val();
    notifikasi = $('#NOTIFIKASI').val();
    short_text = $('#SHORT_TEXT').val();
    swerk = $('#SWERK').val();
    var mplant = swerk.split(' - ');
    beber = $('#BEBER').val();
    planplant = $('#PLANPLANT').val().split(' - ');
    plangroup = $('#PLANGROUP').val();
    if (!$('#FL_CODE').val()) {
        funcloc = $('#FUNCT_LOC').val();
    } else {
        funcloc = $('#FL_CODE').val();
    }
    funcloc = arFuncloc[1];
    lokasi = $('#LOKASI').val();
    priority = $('#PRIORITY').val();
    wbs_code = $('#WBS_CODE').val();
    wbs_text = $('#WBS_TEXT').val();
    desstdate = $('#DESSTDATE').val();
    desenddate = $('#DESENDDATE').val();
    latar_belakang = $('#LATAR_BELAKANG').val();
    aspect0 = $('#ASP0').val();
    aspect1 = $('#ASP1').val();
    aspect2 = $('#ASP2').val();
    aspect3 = $('#ASP3').val();
    aspect_other = $('#ASP_OTHER').val();
    
    var favorite = '';
    $.each($("input[name='OPT_CUST']:checked"), function () {
        if (favorite) {
            favorite += '\n';
        }
        favorite += $(this).val();
    });

    if ($('.opt-lain').attr('checked', 'checked')) {
        if (favorite) {
            favorite += '\n';
        }
        favorite += $('#CUST_REQ').val();
    }
    if (/detail/i.test($("#JENIS").val())) {
        cust_req = favorite;
    } else {
        cust_req = $('#CUST_REQ').val();
    }
    tech_info = $('#TECH_INFO').val();
    res_mit = $('#RES_MIT').val();
    jenis = $('#JENIS').val();
    construct_cost = toNumber($('#CONSTRUCT_COST').val());
    eng_cost = toNumber($('#ENG_COST').val());
    strpath = document.getElementById('FILES').files[0];
    strfile = $('#FILES').attr('data-url');
    strpath2 = document.getElementById('FILES2').files[0];
    strfile2 = $('#FILES2').attr('data-url');
    strpath3 = document.getElementById('FILES3').files[0];
    strfile3 = $('#FILES3').attr('data-url');

    var formURL = "";

    var form_data = new FormData();
    form_data.append('ID_MPE', id_mpe);
    if (stat != 'create') {
        form_data.append('REV_NO', rev_no);
        form_data.append('NOTIFIKASI', notifikasi);
    }
    form_data.append('KDLOC', kdloc);
    form_data.append('KDAREA', kdarea);
    form_data.append('KDDIS', kddis);
    form_data.append('NO_PENGAJUAN', nopengajuan);
    form_data.append('NOTIFIKASI', no_notif);
    form_data.append('NAMA_PEKERJAAN', short_text);
    form_data.append('MPLANT', mplant[0]);
    form_data.append('PLANT_SECTION', beber);
    form_data.append('PPLANT', planplant[0]);
    form_data.append('PLANNER_GROUP', plangroup);
    form_data.append('FUNCT_LOC', funcloc);
    form_data.append('LOKASI', lokasi);
    form_data.append('PRIORITY', priority);
    form_data.append('WBS_CODE', wbs_code);
    form_data.append('WBS_TEXT', wbs_text);
    form_data.append('DESSTDATE', desstdate);
    form_data.append('DESENDDATE', desenddate);
    form_data.append('LATAR_BELAKANG', latar_belakang);
    form_data.append('CUST_REQ', cust_req);
    form_data.append('TECH_INFO', tech_info);
    form_data.append('RES_MIT', res_mit);
    form_data.append('TIPE', jenis);
    form_data.append('CONSTRUCT_COST', construct_cost);
    form_data.append('ENG_COST', eng_cost);
    form_data.append('COSTCENTER', cost_center);
    form_data.append('ASPECT1', aspect0);
    form_data.append('ASPECT2', aspect1);
    form_data.append('ASPECT3', aspect2);
    form_data.append('ASPECT4', aspect3);
    form_data.append('ASPECT_OTHER', aspect_other);
    form_data.append('LIST_APP', $('#APP_LIST').val());
    if (strpath) {
        form_data.append('STRPATH', strpath);
    } else {
        if (strfile) {
            form_data.append('FILES', strfile);
        }
    }
    if (strpath2) {
        form_data.append('STRPATH2', strpath2);
    } else {
        if (strfile2) {
            form_data.append('FILES2', strfile2);
        }
    }
    if (strpath3) {
        form_data.append('STRPATH3', strpath3);
    } else {
        if (strfile3) {
            form_data.append('FILES3', strfile3);
        }
    }

    $.ajax({
        url: 'erf/request/' + stat, // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Data Save : NOT Success! " + error,
                type: "error"
            });
            $("#btn-save").removeClass("disabled");
            $("#btn-save").prop("disabled", false);
        },
        success: function (json) {
            if (json['status'] == 200) {
                tb_erf2();
                swal({
                    title: "Data Saved!\n" + no_notif + "\n" + nopengajuan,
                    type: "success"
                });
                reset_form('yes');
            }
            $("#btn-save").removeClass("disabled");
            $("#btn-save").prop("disabled", false);
        }
    });
}

// event Update
function update_request(id_mpe) {
    idmpe = $('#IDMPE').val();
    nopengajuan = $('#NOPENGAJUAN').val();
    notifikasi = $('#NOTIFIKASI').val();
    lokasi = $('#LOKASI').val();
    wbs_code = $('#WBS_CODE').val();
    wbs_text = $('#WBS_TEXT').val();
    latar_belakang = $('#LATAR_BELAKANG').val();
    aspect0 = $('#ASP0').val();
    aspect1 = $('#ASP1').val();
    aspect2 = $('#ASP2').val();
    aspect3 = $('#ASP3').val();
    aspect_other = $('#ASP_OTHER').val();
    var favorite = '';
    $.each($("input[name='OPT_CUST']:checked"), function () {
        if (favorite) {
            favorite += '\n';
        }
        favorite += $(this).val();
    });

    if ($('.opt-lain').attr('checked', 'checked')) {
        if (favorite) {
            favorite += '\n';
        }
        favorite += $('#CUST_REQ').val();
    }
    if (/detail/i.test($("#JENIS").val())) {
        cust_req = favorite;
    } else {
        cust_req = $('#CUST_REQ').val();
    }
    tech_info = $('#TECH_INFO').val();
    res_mit = $('#RES_MIT').val();
    jenis = $('#JENIS').val();
    construct_cost = toNumber($('#CONSTRUCT_COST').val());
    eng_cost = toNumber($('#ENG_COST').val());
    strpath = document.getElementById('FILES').files[0];
    strfile = $('#FILES').attr('data-url');

    var formURL = "";

    var form_data = new FormData();
    form_data.append('ID_MPE', id_mpe);
    form_data.append('LOKASI', lokasi);
    form_data.append('WBS_CODE', wbs_code);
    form_data.append('WBS_TEXT', wbs_text);
    form_data.append('LATAR_BELAKANG', latar_belakang);
    form_data.append('CUST_REQ', cust_req);
    form_data.append('TECH_INFO', tech_info);
    form_data.append('RES_MIT', res_mit);
    form_data.append('TIPE', jenis);
    form_data.append('CONSTRUCT_COST', construct_cost);
    form_data.append('ENG_COST', eng_cost);
    form_data.append('ASPECT1', aspect0);
    form_data.append('ASPECT2', aspect1);
    form_data.append('ASPECT3', aspect2);
    form_data.append('ASPECT4', aspect3);
    form_data.append('ASPECT_OTHER', aspect_other);
    form_data.append('LIST_APP', $('#APP_LIST').val());
    if (strpath) {
        form_data.append('STRPATH', strpath);
    } else {
        if (strfile) {
            form_data.append('FILES', strfile);
        }
    }

    $.ajax({
        url: 'erf/request/update', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Data Save : NOT Success! " + error,
                type: "error"
            });
            $("#btn-save").removeClass("disabled");
            $("#btn-save").prop("disabled", false);
        },
        success: function (json) {
            if (json['status'] == 200) {
                tb_erf2();
                swal({
                    title: "Data Saved!\n" + notifikasi + "\n" + nopengajuan,
                    type: "success"
                });
                reset_form('yes');
            }
            $("#btn-save").removeClass("disabled");
            $("#btn-save").prop("disabled", false);
        }
    });
}

// event delete
$(document).on('click', ".btnDel", function () {
    var data = dtTable.row($(this).parents('tr')).data();
    swal({
        title: "Are you sure?",
        text: "Data (" + data['NOTIFIKASI'] + ") will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Delete",
        confirmButtonClass: "btn-danger",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    url = 'erf/request/delete/' + data['ID_MPE'] + '/' + data['NOTIFIKASI'];
                    $.ajax({
                        url: url, // point to server-side PHP script
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'get',
                        error: function (xhr, status, error) {
                            swal("Error", "Your data failed to delete!", "error");
                        },
                        success: function (json) {
                            if (json['status'] == 200) {
                                tb_erf2();
                                swal("Deleted!", "Your data has been deleted.", "success");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Deletion of data canceled", "error");
                }
            });
});

// event reject
$(document).on('click', ".btnReject", function () {
    if (modState < 1) {
        $('#reject_modal').modal('show');
    } else {
        hideShowModal('reject_modal');
    }
    modState += 1;

    var data = dtTable.row($(this).parents('tr')).data();

    $('#tmpID').val(data['ID_MPE'] + '-' + data['NOTIFIKASI']);
});

$('#preview_close').click(function(){
    hideShowModal('preview_modal');
});

$(document).on('click', ".btnPrevFiles1", function () {
    if (modState < 1) {
        var pdf = '<iframe style="width: 100%; height: 100%" id="pdf" class="pdf" src="assets/pdfviewer/web/viewer.html?file=' + base_url + files1 + '" scrolling="no"></iframe>';
        $('#preview_modal').modal('show');
        $('#preview_body').html(pdf);
    } else {
        hideShowModal('preview_modal');
    }
    modState += 1;
});

$(document).on('click', ".btnPrevFiles2", function () {
    if (modState < 1) {
        var pdf = '<iframe style="width: 100%; height: 100%" id="pdf" class="pdf" src="assets/pdfviewer/web/viewer.html?file=' + base_url + files2 + '" scrolling="no"></iframe>';
        $('#preview_modal').modal('show');
        $('#preview_body').html(pdf);
    } else {
        hideShowModal('preview_modal');
    }
    modState += 1;
});

$(document).on('click', ".btnPrevFiles3", function () {
    if (modState < 1) {
        var pdf = '<iframe style="width: 100%; height: 100%" id="pdf" class="pdf" src="assets/pdfviewer/web/viewer.html?file=' + base_url + files3 + '" scrolling="no"></iframe>';
        $('#preview_modal').modal('show');
        $('#preview_body').html(pdf);
    } else {
        hideShowModal('preview_modal');
    }
    modState += 1;
});

$(document).on('click', ".btn-canc-reject", function () {
    hideShowModal('reject_modal');
});

// event save
$("#fm-reject").submit(function (e) {
    e.preventDefault();
    $("#btn-save").addClass("disabled");
    a_id = $('#tmpID').val().split('-');
    strpath = document.getElementById('REJECT_FILE').files[0];
    strfile = $('#REJECT_FILE').attr('data-url');

    var form_data = new FormData();
    form_data.append('ID_MPE', a_id[0]);
    form_data.append('NOTIFIKASI', a_id[1]);
    if (strpath) {
        form_data.append('STRPATH', strpath);
    } else {
        if (strfile) {
            form_data.append('REJECT_FILE', strfile);
        }
    }

    $.ajax({
        url: 'erf/request/de_reject', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Reject NOT Success! " + error,
                type: "error"
            });
        },
        success: function (json) {
            if (json['status'] == 200) {
                tb_erf2();
                swal({
                    title: "Data is Rejected!\n" + a_id[1],
                    type: "success"
                });
                document.getElementById("fm-reject").reset();
            }
        }
    });
    hideShowModal('reject_modal');

    return false;
});

$('#KDLOC').on('change', function () {
    if ($('#FUNCT_LOC').val()) {
        $('#NOPENGAJUAN').val();
    }
});


$(document).on('click', "#btn_progres_req", function () {
    var dt_id_mpe = $(this).attr("dt_id_mpe"); 
    var dt_no = $(this).attr("dt_no"); 
    var dt_np = $(this).attr("dt_np"); 
     
    var title = "("+dt_no+") "+dt_np;
    $('#title_mod_engreq').text(title);
    
    var setHtml = '';
    
    $.ajax({
        url: 'erf/request/getProgresReport', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        type: 'POST',
        data: {
            'ID_MPE': dt_id_mpe
        },
        success: function (json) {
            //console.log(json);
            $.each(json['data'], function (i, item) {
                var label = "tb_log"+i;
                //console.log(label);
                
                setHtml += "<p> "+(i+1)+". "+item['DESKRIPSI']+"<span style='float: right;'>Progress: "+item['PROGRESS']+" %</span></p>";
                setHtml += '<div class="table-responsive"> ' +
                                '<table id="'+label+'" class="table display compact table-bordered" style="width:100%;"> '+
                                    '<thead> '+
                                        '<tr> '+
                                            '<th style="text-align: center;" >No</th> '+
                                            '<th style="text-align: center;" >Progress</th> '+
                                            '<th style="text-align: center;" >Last Update</th> '+
                                            '<th style="text-align: center;" >Remarks</th> '+
                                        '</tr> '+
                                    '</thead> '+
                                    '<tbody> ';
                                    //console.log(item['DT_PROGRESS']);
                                    $.each(item['DT_PROGRESS'], function (j, item2) {
                                        setHtml += '<tr >'+
                                                        '<td style="text-align: center;">'+(j+1)+'</td>'+
                                                        '<td style="text-align: center;">'+item2['LOG_RES1']+'% </td>'+
                                                        '<td style="text-align: center;">'+item2['CREATE_AT']+'</td>'+
                                                        '<td style="text-align: left;"> &nbsp;'+item2['NOTE']+'</td>'+
                                                    '</tr>';      
                                    });
                                    
                                    
                setHtml +=          '</tbody> '+
                                '</table> '+
                            '</div><br>';
                //tb_log(item['ID'], label);
            });
            
            $('#prog_request_bd').html(setHtml);
            //$('#'+label).dataTable(); 
        }
    });
    openModal('preview_modal_progres_req');
    
});



$(document).on('click', "#modal_progres_req_close", function () {
     hideShowModal('preview_modal_progres_req');
     //tb_erf2();
     location.reload();
});



function aspectChange() {
    
}

function aspect1Change() {
    
}

function aspect2Change() {
    
}

function aspect3Change() {
    
}

function aspect3Change() {
    
}

$('#preview_close').click(function(){
    // hideShowModal('preview_modal');
    $('#preview_modal').modal('show');
});

var modState = 0;
function previewDocument(input){
    var file = input.files[0];
    var mime_types = [ 'application/pdf' ];
    if(mime_types.indexOf(file.type) == -1) {
        alert('Error : Incorrect file type');
        return;
    }
    _OBJECT_URL = URL.createObjectURL(file)
    var pdf = '<iframe style="width: 100%; height: 500px" id="pdf" class="pdf" src="assets/pdfviewer/web/viewer.html?file=' + _OBJECT_URL+ '" scrolling="no"></iframe>';
    $('#preview_body').html(pdf);

    if (modState < 1) {
        $('#preview_modal').modal('show');   
    } else {
        hideShowModal('preview_modal');
    }
    modState += 1;
}

$(document).on("change", "#PLANGROUP", function(e){
    var idUnit = $(this).val();
    // if(idUnit == 499){
        $("#APP_LIST").select2({
            allowClear: true,
            theme: 'bootstrap',
            placeholder:'Please type some word ...',
            ajax: {
                dataType: 'json',
                url: "erf/request/getApproverErfTech",
                delay: 50,
                type: 'POST',
                data: function (params) {
                    return {
                        id: idUnit
                    }
                },
                processResults: function (data, page) {
                    var i = 0;
                    var datas = data.data;
                    if(datas){
                        return {
                            results: $.map(data.data, function (obj) {
                                return {
                                    id: obj.GEN_CODE + ' - ' + obj.GEN_DESC + ' - ' + obj.GEN_PAR4 + ' - ' + obj.GEN_PAR1,
                                    text: '[ ' + obj.GEN_CODE + ' ] ' + obj.GEN_DESC + ' - ' + obj.GEN_PAR1,
                                };
                            })
                        };
                    } else {
                        select_content_app('#APP_LIST', "user/search", 'Please type some word ...', '<= 20');
                    }
                    
                },
            }
        });
    // } else {
        // select_content_app('#APP_LIST', "user/search", 'Please type some word ...', '<= 20');
    // }
});

function search_approver(){
    select_content_app('#APP_LIST', "user/search", 'Please type some word ...', '<= 20');
}