var username_logged_in = '';

$(function() {
  if (localStorage.dems_me) {
        logged_in_dems();
        // $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
    }
  // if (localStorage.token_user) {
  //       console.log('session ON');
  //       console.log(localStorage.user);
  //       console.log(localStorage.token_user);
  //       home();
  //       // $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
  //   } else{
  //   	console.log('session OFF');
  //   	// $('.error').addClass('alert alert-danger').html("Invalid Username / Password Combination");
  //   	// $('.error').addClass('alert alert-danger').html("Invalid email/password combination");
  //   }
});
//
// function runScript(e) {
//   if (e.keyCode == 13) {
//     login_mpe();
//   }
// }

function clearStorage() {
  if (localStorage.dems_me) {
    localStorage.removeItem('dems_me');
    // location.href = "login";
  }
}

$("#fm-login").submit(function(e) {
  e.preventDefault();
  login_dems();
  return false;
});

function login_dems() {
  var usnm = document.getElementById('username').value;
  var pswd = document.getElementById('password').value;
  var rmb = document.getElementById('remember').checked ;
  var them = '';
  if (($('input[name=themes]:checked').val())) {
    them = $('input[name=themes]:checked').val();
  }else them = 'none';
  usnm = usnm.toUpperCase();
  // pswd = pswd.toLowerCase();
  var form_data = new FormData();
  form_data.append('username', usnm);
  form_data.append('password', pswd);
  form_data.append('remember', rmb);
  form_data.append('themes', them);
  $.ajax({
    url: 'Auth/login', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {

      faillogin();
    },
    success: function(loginn) {
      if (loginn.response_code == 200) {
        // console.log('session ON');
        if (loginn.rem_me) {
          localStorage.dems_me = loginn.rem_me;
        }
        home();

      } else {
        // console.log(loginn);
        clearStorage();
        faillogin(loginn['message']);
      }
    }
  });

}

function logged_in_dems() {
  var form_data = new FormData();
  form_data.append('rem_me', localStorage.dems_me);
  $.ajax({
    url: 'Auth/logged_in', // point to server-side PHP script
    dataType: 'json', // what to expect back from the PHP script, if anything
    cache: false,
    contentType: false,
    processData: false,
    data: form_data,
    type: 'post',
    error: function(xhr, status, error) {
      faillogin();
    },
    success: function(loginn) {
      // console.log(loginn);
      if (loginn.response_code == 200) {
        if (loginn.rem_me) {
          localStorage.dems_me = loginn.rem_me;
        }

        home();
      } else {
        // console.log(loginn);
        clearStorage();
        faillogin(loginn['message']);
      }
    }
  });

}

function home() {
  if (localStorage.url != null) {
    location.href = localStorage.url;
  } else {
    location.href = "welcome";
  }
  localStorage.removeItem('url')
}

function reload() {
  logout();
}

function faillogin(message = 'Invalid Username / Password') {

  $('.error').addClass('alert alert-danger').html('<small>' + message + '</small>');
  document.getElementById('username').value = '';
  document.getElementById('password').value = '';
  // stop_waitMe('.wrapperloading');
}

function logout() {
  // localStorage.clear();
  sessionStorage.clear();
  localStorage.removeItem('url')
  // console.log('session OFF');
  location.href = "login";
}
