var oTable;

$(document).ready(function() {
  tb_list();
  // getChart();
});

function tb_list(start = null, end = null) {
  // var form_data = new FormData();
  // if (start) {
  //   form_data.append('START_DATE', start);
  // }
  // if (end) {
  //   form_data.append('END_DATE', end);
  // }

  oTable = $('#tb_list').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'report/revenue/data_list',
      data: {
        'start_date': start,
        'end_date': end
      },
      type: "POST"
    },
    columns: [{
        "data": "UK_TEXT"
      },
      {
        "data": "NOTIFIKASI"
      },
      {
        "data": "NO_PENGAJUAN"
      },
      {
        "data": "NAMA_PEKERJAAN"
      },
      {
        "className": "text-right",
        "data": "CONSTRUCT_COST",
        "mRender": function(row, data, index) {
          return toCurrency(index['CONSTRUCT_COST']);
        }
      },
      {
        "className": "text-right",
        "data": "ENG_COST",
        "mRender": function(row, data, index) {
          return toCurrency(index['ENG_COST']);
        }
      },
      {
        "className": "text-right",
        "data": "DE_CONSTRUCT_COST",
        "mRender": function(row, data, index) {
          return toCurrency(index['DE_CONSTRUCT_COST']);
        }
      },
      {
        "className": "text-right",
        "data": "DE_ENG_COST",
        "mRender": function(row, data, index) {
          return toCurrency(index['DE_ENG_COST']);
        }
      }
    ],
    columnDefs: [{
      "visible": false,
      "targets": 0
    }],
    drawCallback: function(settings) {
      var api = this.api();
      var rows = api.rows({
        page: 'current'
      }).nodes();
      var last = null;

      api.column(0, {
        page: 'current'
      }).data().each(function(group, i) {

        if (last !== group) {

          $(rows).eq(i).before(
            '<tr class="group"><td colspan="7" style="/*background-color:rgba(116, 96, 238,0.2);*/font-weight:700;color:#006232;text-align:left;">' + ' ' + group + '</td></tr>'
          );

          last = group;
        }
      });
    }
  });
}

function getChart() {
  $.ajax({
    type: 'POST',
    url: 'report/revenue/count_report',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          graph = json['data'][0];
          // foreigns += ('<option value >Please select...</option>');
          // $.each(json['data'], function(i, data) {
          //   foreigns += ('<option value="' + data['ID_PENGAJUAN'] + '" title="' + data['NO_PENGAJUAN'] + ' - ' + data['NOTIF_NO'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_PENGAJUAN'] + '" data-notif="' + data['NOTIF_NO'] + '" data-flcode="' + data['FUNCT_LOC'] + '" data-fltext="' + data['FL_TEXT'] + '" data-shorttext="' + data['SHORT_TEXT'] + '" data-uktext="' + data['UK_TEXT'] + '" >' + data['SHORT_TEXT'] + '</option>');
          // });

          // Morris bar chart
          Morris.Bar({
            element: 'morris-bar-chart',
            data: [{
              y: 'ERF',
              a: graph['J_ERF']
            }, {
              y: 'EAT',
              a: graph['J_EAT']
            }, {
              y: 'Progress',
              a: graph['J_PROG']
            }, {
              y: 'Documents',
              a: graph['J_DOK']
            }, {
              y: 'BAST',
              a: graph['J_BAST']
            }],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Jml '],
            barColors: ['#26DAD2'],
            hideHover: 'auto',
            gridLineColor: '#eef0f2',
            resize: true
          });

        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

// event date
function changeDate() {
  var start = null;
  var end = null;
  if ($('#START_DATE').val()) {
    start = $('#START_DATE').val();
  }
  if ($('#END_DATE').val()) {
    end = $('#END_DATE').val();
  }
  tb_list(start, end);
}

$('#START_DATE').change(function() {
  changeDate();
});

$('#END_DATE').change(function() {
  changeDate();
});

// export
$("#toExcel").on("click", function() {
  var start = '';
  var end = '';
  if ($('#START_DATE').val()) {
    start = $('#START_DATE').val();
  }
  if ($('#END_DATE').val()) {
    end = $('#END_DATE').val();
  }
  $.redirect('report/revenue/excel', {
    start: start,
    end: end
  }, 'POST', '_blank');
  // window.location.assign('report/revenue/excel?&start=' + (new Date()).getFullYear() + start + end);
  // window.location.assign('report/revenue/excel?&start=' + (new Date()).getFullYear());
});
