var oTable;

$(document).ready(function () {
    // getForeign();
    if ($('#ID_DOK_ENG').val()) {
        onForeign();
        onChange();
    }
    tb_dok_tend();
    $('.select2').select2();
    // generateDoc();
    select_content_change('#CRT_LIST', "user/search", 'Please type some word ...');
    select_content_change('#PARAF_LIST', "user/search", 'Please type some word ...');
    select_content_change('#APP_LIST', "user/search", 'Please type some word ...');
    select_content_change('#GM_LIST', "user/search", 'Please type some word ...');

    $('.trn-no').hide();

    $('input[name^=NOMINAL]').priceFormat({
        prefix: '',
        centsSeparator: '.',
        centsLimit: 0,
        thousandsSeparator: ',',
        clearOnEmpty: false,
    });
    // setAtasan();
    // reloadtabel();
});

function generateDoc() {
    var tahun;
    var lokasi;
    var Nomor;
    $.ajax({
        type: 'POST',
        url: 'scm/tender/generateNoDoc',
        //data:{'id':id},
        success: function (data) {
            try {
                data = JSON.parse(data);
                Nomor = data.nomor;
                tahun = data.tahun;

                $("#NO_DOK_TEND").val("/" + "/" + "/" + "TD/" + Nomor + "/" + "/" + tahun);

            } catch (e) {
                alert(e);
            } finally {

            }

        },
        error: function () {
            console.log("error");
        }
    });
}

function select_content_change(selecttag, urlselect, placeholder) {
    $('' + selecttag).select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: placeholder,
        theme: 'bootstrap',
        ajax: {
            dataType: 'json',
            url: urlselect,
            // data: data,
            delay: 50,
            type: 'POST',
            data: function (params) {
                return {
                    search: params.term
                }
            },
            processResults: function (data, page) {
                return {
                    results: $.map(data, function (obj) {
                        // console.log(obj);
                        if (obj.NOBADGE) {
                            return {
                                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.EMAIL + ' - ' + obj.JAB_TEXT,
                                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                                // photo: obj.photo
                            };
                        } else {
                            return {
                                id: obj.NOBADGE + ' - ' + obj.NAMA + ' - ' + obj.EMAIL + ' - ' + obj.JAB_TEXT,
                                text: '[ ' + obj.NOBADGE + ' ] ' + obj.NAMA + ' - ' + obj.JAB_TEXT,
                                // photo: obj.photo
                            };
                        }

                    })
                };
            },
        }
    });
    $('' + selecttag).trigger('change');
}

function load_selected_option(key, id, text) {
    $(key).empty().append('<option selected value="' + id + '">' + text + '</option>');
    $(key).select2('data', {
        id: id,
        label: text
    });
    $(key).trigger('change'); // Notify any JS components that the value changed
}

function setActiveTab(ul_id = null, content_id = null) {
    $(".nav-link").removeClass("active show");
    $('#' + ul_id).addClass('active show');
    $(".tab-pane").removeClass("active show");
    $('#' + content_id).addClass('active show');
}

function tb_dok_tend() {
    vGroup = 'false';
    if (page_state.indexOf(page_short + '-group=1') != -1) {
        vGroup = 'true';
    }
    oTable = $('#tb_dok_tend').DataTable({
        destroy: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: 'scm/tender/data_list',
            data: function (d) {
                d.vGroup = vGroup;
                // d.custom = $('#myInput').val();
                // etc
            },
            type: "POST"
        },
        columns: [{
                "className": 'details-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                "data": "CREATE_AT",
                "width": 75
            },
            {
                "data": "NO_DOK_TEND"
            },
            {
                "data": "NOTIFIKASI"
            },
            {
                "data": "NO_PENGAJUAN"
            },
            {
                "data": "NO_DOK_ENG"
            },
            {
                "data": "PACKET_TEXT"
            },
            // {
            //   "data": "PROGRESS_TEXT",
            //   "width": 50,
            //   "className": "text-right"
            // },
            {
                "data": "STATUS",
                "mRender": function (row, data, index) {
                    if (index['STATUS'].indexOf('Approved') != -1) {
                        return '<center><p style="color: green">' + index['STATUS'] + ' </p></center> ';
                    } else if (index['STATUS'].indexOf('Rejected') != -1) {
                        return '<center><p style="color: red">' + index['STATUS'] + ' </p></center>';
                    } else {
                        return '<center><p style="color: blue">' + index['STATUS'] + ' </p></center>';
                        // return '';
                    }
                }
            },
            // {
            //   "data": "CREATE_BY"
            // },
            {
                "data": "ID",
                "width": 70,
                "mRender": function (row, data, index) {
                    sresend = '';
                    // console.log(index);
                    if (!index['APPROVE3_AT'] && index['STATUS'] != 'Rejected') {
                        // if (!index['APPROVE'+index['JML_APP']+'_AT'] || index['STATUS'] !== 'Rejected') {
                        sresend = '<button class="btnResend btn btn-xs btn-success" data-toggle="tooltip" title="Resend Email"><i class="fa fa-envelope-square"></i></button>';
                    }
                    srks = '';
                    // if (index['ECE_FILE'] && page_state.indexOf(page_short+'-exp-ece=1') != -1 && !index['KAJIAN_FILE']) {
                    if (index['RKS_FILE'] && page_state.indexOf(page_short + '-exp-tr=1') != -1 && !index['KAJIAN_FILE']) {
                        srks = '<button class="btnExpPc btn btn-xs btn-success" data-toggle="tooltip" data-tipe="TR" style="background-color: #00a69c;" title="Download TOR" ><i class="fa fa-download"></i> T </button>';
                    }
                    sbq = '';
                    if (index['BQ_FILE'] && page_state.indexOf(page_short + '-exp-bq=1') != -1 && !index['KAJIAN_FILE']) {
                        sbq = '<button class="btnExpPc btn btn-xs btn-warning" data-toggle="tooltip" data-tipe="BQ" title="Download BQ" ><i class="fa fa-download"></i> B </button>';
                    }
                    sdraw = '';
                    if (index['DRAW_FILE'] && page_state.indexOf(page_short + '-exp-draw=1') != -1 && !index['KAJIAN_FILE']) {
                        sdraw = '<button class="btnExpPc btn btn-xs btn-info" data-toggle="tooltip" data-tipe="DRW" style="background-color: #288cff;" title="Download Drawing" ><i class="fa fa-download"></i> D </button>';
                    }
                    sece = '';
                    if (index['ECE_FILE'] && page_state.indexOf(page_short + '-exp-ece=1') != -1 && !index['KAJIAN_FILE']) {
                        sece = '<button class="btnExpPc btn btn-xs btn-danger" data-toggle="tooltip" data-tipe="EC" style="background-color: #c00c16;" title="Download ECE" ><i class="fa fa-download"></i> E </button>';
                    }
                    sothers = '';
                    if (index['OTHERS_FILE'] && page_state.indexOf(page_short + '-exp-oth=1') != -1 && !index['KAJIAN_FILE']) {
                        sothers = '<button class="btnExpPc btn btn-xs btn-danger" data-toggle="tooltip" data-tipe="OT" style="background-color: #fcdb03;" title="Download Others" ><i class="fa fa-download"></i> O </button>';
                    }
                    sdis = '';
                    if (page_state.indexOf(page_short + '-delete=1') == -1 || index['STATUS'].indexOf('In Approval') == -1) {
                        sdis = 'disabled';
                    }
                    if (page_state.indexOf('special_access=false') == -1 && page_state.indexOf(page_short + '-delete=1') != -1) {
                        sdis = '';
                    }
                    if (page_state.indexOf(page_short + '-spc-del=1') == -1) {
                        sdis = '';
                    }
                    strm = '';
                    if (index['ID_BAST']) {
                        // strm = '<li class="btnExportBast" style="height: 20px;padding-top: 0px !important;font-size: 10px;cursor: pointer;" ><a>Transmital</a></li>';
                        strm = '<button class="btnExportBast btn btn-xs btn-white" data-toggle="tooltip" data-tipe="TF" title="Download Transmital" ><i class="fa fa-download"></i> TF </button>';
                    }
                    sexp = '';
                    if (page_state.indexOf(page_short + '-export=1') == -1) {
                        sexp = ' style="display:none;"';
                    }
                    sbgroup2 = '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download"' + sexp + ' ><i class="fa fa-download"></i></button>' + srks + sdraw + sbq + sece + sothers + strm + '</div></center>';
                    sbgroup = '';

                    return '<center><div style="float: none;" class="btn-group">' + sbgroup + '<button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></button>' + sresend + '<button class="btnDel btn btn-xs btn-danger ' + page_short + '-delete ' + sdis + '" data-toggle="tooltip" title="Delete" ' + sdis + '><i class="fa fa-trash"></i></button></div></center>' + sbgroup2;
                    // return '<center><div style="float: none;" class="btn-group"><button class="btnExport btn btn-xs btn-dark" data-toggle="tooltip" title="Download"'+sexp+' ><i class="fa fa-download"></i></button><button class="btnEdit btn btn-xs btn-primary" data-toggle="tooltip" title="View" ><i class="fa fa-eye"></i></button>' + sresend + '<button class="btnDel btn btn-xs btn-danger '+page_short+'-delete '+sdis+'" data-toggle="tooltip" title="Delete" '+sdis+'><i class="fa fa-trash"></i></button></div></center>';
                }
            },
        ],
        "order": [
            [0, "desc"]
        ]
    });

    $('#tb_dok_tend tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = oTable.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {

            var html = '';
            var d = row.data();
            var data;
            var approveAt = '';
            var status = '';
            var i = 0;

            html += '<div class="container" style="width:1000px;height:400px;overflow-y:scroll">' +
                    '<table class="table display table-bordered table-striped" id="tableprogress">' +
                    '<thead>' +
                    '<tr>' +
                    '<th>No</th>' +
                    '<th>Type</th>' +
                    '<th>Status</th>' +
                    '<th>Bureau</th>' +
                    '<th>Date</th>' +
                    '<th>By</th>' +
                    '<th style="width:100px">Note</th>' +
                    '</tr>' +
                    '</thead>' +
                    '<tbody>';

            //paraf
            $.ajax({
                url: 'scm/tender/get_app',
                data: {
                    id: d.ID
                },
                dataType: 'json',
                type: 'POST',
                beforeSend: function () {},
                success: function (result) {
                    data = result;
                    // console.log(data);
                    $.map(data, function (item, index) {
                        var date = item.UPDATE_AT;
                        if (!date) {
                            date = '-';
                        }
                        if (item.NOTE == null) {
                            item.NOTE = '-';
                        }
                        html += '<tr>' +
                                '<td>' + (i + 1) + '</td>' +
                                '<td>' + item.TIPE + '</td>' +
                                '<td>' + item.STATUS + '</td>' +
                                '<td>' + item.APP_JAB + '</td>' +
                                //if(item.UPDATE_AT != null) {
                                '<td>' + date + '</td>' +
                                '<td>' + item.APP_NAME + '</td>' +
                                '<td style="width:280px">' + item.NOTE + '</td>' +
                                '</tr>';
                        i++;
                    });
                    return html;
                },
                error: function () {

                },
                complete: function () {
                    html +=
                            '</tbody>' +
                            '</table>' +
                            '</div>';
                    // console.log(html);
                    row.child(html).show();
                    tr.addClass('shown');
                    return html;
                }
            });

        }
    });
}

function getForeign() {
    $.ajax({
        type: 'POST',
        url: 'scm/tender/foreign',
        success: function (json) {
            json = $.parseJSON(json);
            $('#ID_DOK_ENG').html('');
            foreigns = '';
            if (json) {
                if (json['status'] == 200) {
                    // foreigns = ('<option value >Please select...</option>');
                    $.each(json['data'], function (i, data) {
                        sAdd = '';
                        if (i == 0) {
                            sAdd = 'Selected';
                        }
                        foreigns += ("<option value='" + data['ID'] + "' title='" + data['NO_DOK_ENG'] + ' - ' + data['PACKET_TEXT'] + "' data-subtext='" + data['NO_DOK_ENG'] + "' data-no='" + data['NO_DOK_ENG'] + "' data-notif='" + data['NOTIFIKASI'] + "' data-packet='" + data['PACKET_TEXT'] + "' data-rks='" + data['RKS_FILE'] + "' data-bq='" + data['BQ_FILE'] + "' data-draw='" + data['DRAW_FILE'] + "' data-ece='" + data['ECE_FILE'] + "' data-company='" + data['COMPANY'] + "' data-dept_code='" + data['DEPT_CODE'] + "' data-dept_text='" + data['DEPT_TEXT'] + "' data-uk_code='" + data['UK_CODE'] + "' data-uk_text='" + data['UK_TEXT'] + "' >" + data['NO_DOK_ENG'] + " - " + data['PACKET_TEXT'] + "</option>");
                    });
                } else {
                    foreigns = ('<option value >Empty...</option>');
                }
                $('#ID_DOK_ENG').html(foreigns);
                $('#ID_DOK_ENG').select2();
                onForeign();
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

function getForeignDtl(f_id = null) {
    // var form_data = new FormData();
    // form_data.append('F_ID', f_id);
    $.ajax({
        type: 'POST',
        url: 'scm/tender/foreign_dtl/' + f_id,
        success: function (json) {
            json = $.parseJSON(json);
            $('#ID_PACKET').html('');
            foreigns = '';
            if (json) {
                if (json['status'] == 200) {
                    foreigns = ('<option value >Please select...</option>');
                    $.each(json['data'], function (i, data) {
                        foreigns += ('<option value="' + data['ID'] + '" >' + data['DESKRIPSI'] + '</option>');
                        if (/detail/i.test(data['TIPE'])) {
                            var e = document.getElementById("kajian");
                            var f = document.getElementById("capex");
                            //console.log(e);
                            e.style.display = "none";
                            f.style.display = "block";
                        } else {
                            var e = document.getElementById("capex");
                            var f = document.getElementById("kajian");
                            //console.log(e);
                            e.style.display = "none";
                            f.style.display = "block";
                        }
                    });
                } else {
                    foreigns = ('<option value >Empty...</option>');
                }
                $('#ID_PACKET').html(foreigns);
                // onForeign();
            }
        },
        error: function (xhr, status, error) {
            alert('Server Error ...');
        }
    });
}

function onForeign() {
    $('#NO_DOK_ENG').val($('#ID_DOK_ENG :selected').attr('data-no'));
    $('#NOTIFIKASI').val($('#ID_DOK_ENG :selected').attr('data-notif'));
    $('#ID_PACKET').val($('#ID_DOK_ENG :selected').attr('data-idpacket'));
    $('#PACKET_TEXT').val($('#ID_DOK_ENG :selected').attr('data-packet'));
    // RKS
    var pfiles = $('#ID_DOK_ENG :selected').attr('data-rks');
    if (pfiles) {
        var aFiles = pfiles.split('/');

        $('#rks_uri').attr('href', pfiles);
        $('#rks_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#rks_name').addClass('fa fa-file-pdf-o');
    }
    // BQ
    pfiles = $('#ID_DOK_ENG :selected').attr('data-bq');
    if (pfiles) {
        var aFiles = pfiles.split('/');

        $('#bq_uri').attr('href', pfiles);
        $('#bq_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#bq_name').addClass('fa fa-file-pdf-o');
    }
    // DRW
    pfiles = $('#ID_DOK_ENG :selected').attr('data-draw');
    if (pfiles) {
        var aFiles = pfiles.split('/');

        $('#draw_uri').attr('href', pfiles);
        $('#draw_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#draw_name').addClass('fa fa-file-pdf-o');
    }
    // ECE
    pfiles = $('#ID_DOK_ENG :selected').attr('data-ece');
    if (pfiles) {
        var aFiles = pfiles.split('/');

        $('#ece_uri').attr('href', pfiles);
        $('#ece_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#ece_name').addClass('fa fa-file-pdf-o');
    }
    $('#COMPANY').val($('#ID_DOK_ENG :selected').attr('data-company'));
    $('#DEPT_CODE').val($('#ID_DOK_ENG :selected').attr('data-dept_code'));
    $('#DEPT_TEXT').val($('#ID_DOK_ENG :selected').attr('data-dept_text'));
    $('#UK_CODE').val($('#ID_DOK_ENG :selected').attr('data-uk_code'));
    $('#UK_TEXT').val($('#ID_DOK_ENG :selected').attr('data-uk_text'));
    onChange();
}

function onChange() {
    var no_dok = $("#NO_DOK_TEND").val();
    var arr = no_dok.split("/");
    var pengajuan = $("#NO_DOK_ENG").val();
    // console.log(pengajuan);
    var arr2 = pengajuan.split("/");
    $("#NO_DOK_TEND").val(arr2[0] + "/" + arr2[1] + "/" + arr2[2] + "/" + arr[3] + "/" + arr[4] + "/" + arr2[5] + "/" + arr[6]);
}

$('#ID_DOK_ENG').on('change', function () {
    onForeign();
    // getForeignDtl($('#ID_DOK_ENG :selected').val());
})

function tipeChange() {
    if ($('#TIPE').val() == 'Engineering') {
        $('.browse-file').hide();
        $('.trn-foreign').show();
        $('#notif_scm_tender').hide();
        $('#PACKET_TEXT').attr("readonly", true);
        onForeign();
    } else {
        $('#notif_scm_tender').show();
        $('.browse-file').show();
        $('.trn-foreign').hide();
        $('#PACKET_TEXT').attr("readonly", false);
        clearAttach();
    }
}

$('#TIPE').change(function () {
    // if (!$('#ID').val()) {
    reset_form();
    tipeChange();
    // }
});


// $('#NOMINAL').on('change', function() {
//   if (parseInt(toNumber($('#NOMINAL').val()))<10000000000) {
//     $(".app-gm").hide();
//   }else {
//     $(".app-gm").show();
//   }
// })

function clearAttach() {
    // RKS
    $('#RKS_FILE').val('');
    $('#rks_uri').attr('href', '');
    $('#rks_name').removeClass('fa fa-file-pdf-o');
    $('#rks_name').text('');

    // BQ
    $('#BQ_FILE').val('');
    $('#bq_uri').attr('href', '');
    $('#bq_name').text('');
    $('#bq_name').removeClass('fa fa-file-pdf-o');

    // Drawing
    $('#DRAW_FILE').val('');
    $('#draw_uri').attr('href', '');
    $('#draw_name').text('');
    $('#draw_name').removeClass('fa fa-file-pdf-o');

    // ECE
    $('#ECE_FILE').val('');
    $('#ece_uri').attr('href', '');
    $('#ece_name').removeClass('fa fa-file-pdf-o');
    $('#ece_name').text('');

    // Others
    $('#OTHERS_FILE').val('');
    $('#others_uri').attr('href', '');
    $('#others_name').removeClass('fa fa-file-pdf-o');
    $('#others_name').text('');


    $('#COMPANY').val('');
    $('#DEPT_CODE').val('');
    $('#DEPT_TEXT').val('');
    $('#UK_CODE').val('');
}

function reset_form(reset = null) {
    $('#ID').val('');
    $('#NO_DOK_TEND').val('');
    $('.trn-no').hide();
    // $('#ID_DOK_ENG').html('');
    $('#NO_DOK_ENG').val('');
    $('#NOTIFIKASI').val('');
    $('#NOTIFIKASI_IW21').val('');
    $('#PACKET_TEXT').val('');
    $('#NOMINAL').val('');

    clearAttach();
    generateDoc();
    // getForeign();
    // tipeChange();

    $("#GM_LIST").val('');
    $("#GM_LIST").removeAttr('disabled');
    $("#APP_LIST").val('');
    $("#APP_LIST").removeAttr('disabled');
    $("#PARAF_LIST").val('');
    $("#PARAF_LIST").removeAttr('disabled');
    $("#CRT_LIST").val('');
    $("#CRT_LIST").removeAttr('disabled');
    $('#ID_DOK_ENG').select2();
    // $('#TIPE').select2();

    $("#btn-save").removeClass("disabled");
    $("#btn-save").prop("disabled", false);
    $("#btn-save").html(' <i class="fa fa-check"></i> Save');
    $('.selectpicker').selectpicker('refresh');
    $('#ul-new').text('Create SCM Tender');
    $('#btn-save').show();
    select_content_change('#CRT_LIST', "user/search", 'Please type some word ...');
    select_content_change('#PARAF_LIST', "user/search", 'Please type some word ...');
    select_content_change('#APP_LIST', "user/search", 'Please type some word ...');
    select_content_change('#GM_LIST', "user/search", 'Please type some word ...');

    if (reset) {
        setActiveTab('ul-list', 'list-form');
        getForeign();
}

}

$("#btn-cancel").on("click", function () {
    $('#TIPE').val('Engineering').trigger('change');
    reset_form();
});

// export
$(document).on('click', ".btnExport", function () {
    var data = oTable.row($(this).parents('tr')).data();

    window.location.assign('scm/tender/export_pdf/' + data['ID']);
});

// export
$(document).on('click', ".btnExpPc", function () {
    var data = oTable.row($(this).parents('tr')).data();
    console.log($(this).attr('data-tipe'));
    sAdd = '';
    if ($(this).attr('data-tipe') != 'All') {
        sAdd = '/' + $(this).attr('data-tipe');
    }
    window.location.assign('scm/tender/export_pdf/' + data['ID'] + sAdd);
});

// export transmital
$(document).on('click', ".btnExportBast", function () {
    var data = oTable.row($(this).parents('tr')).data();
    window.location.assign('bast/handover/export_pdf/' + data['ID_BAST']);
});

// event resend mail
$(document).on('click', ".btnResend", function () {
    $('.title_ukur').text('Resend Email');
    var data = oTable.row($(this).parents('tr')).data();


    var form_data = new FormData();
    form_data.append('ID', data['ID']);

    $.ajax({
        url: 'scm/tender/resend', // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Resend Email : NOT Success! " + error,
                type: "error"
            });
            // $("#btn-save").removeClass("disabled");
            // $("#btn-save").val("Save");
        },
        success: function (json) {
            if (json['status'] == 200) {
                // tb_request();
                // tb_erf2();
                swal({
                    title: "Email was sent.",
                    type: "success"
                });
                reset_form('yes');
            }
            $("#btn-save").removeClass("disabled");
            // $("#btn-save").val("Save");
        }
    });
});

// view
$(document).on('click', ".btnEdit", function () {
    var data = oTable.row($(this).parents('tr')).data();
    clearAttach();
    $('#notif_scm_tender').show();
    console.log(data);
    $('#ID').val(data['ID']);
    $('#TIPE').val(data['TIPE']);
    $('#NO_DOK_TEND').val(data['NO_DOK_TEND']);
    if (data['ID_DOK_ENG']) {
        $('#ID_DOK_ENG').html('<option value="' + data['ID_DOK_ENG'] + '" title="' + data['NO_DOK_ENG'] + ' - ' + data['OBJECTIVE'] + '" data-subtext="' + data['NO_PENGAJUAN'] + '" data-no="' + data['NO_DOK_ENG'] + '" data-notif="' + data['NOTIFIKASI'] + '" data-pengajuan="' + data['PENGAJUAN'] + '" data-object="' + data['OBJECTIVE'] + '" >' + data['NO_DOK_ENG'] + '</option>');
    }
    $('#NO_DOK_ENG').val(data['NO_DOK_ENG']);
    $('#NOTIFIKASI').val(data['NOTIFIKASI']);
    $('#PENGAJUAN').val(data['PENGAJUAN']);
    $('#OBJECTIVE').val(data['OBJECTIVE']);
    if (data['ID_PACKET']) {
        $('#ID_PACKET').html('<option value="' + data['ID_PACKET'] + '" >' + data['PACKET_TEXT'] + '</option>');
    }
    $('#PACKET_TEXT').val(data['PACKET_TEXT']);
    $('#NOTIFIKASI_IW21').val(data['NOTIFIKASI']);
    $('#NOMINAL').val(toCurrency(data['NOMINAL']));
    $('.trn-no').show();

    // LIST GM
    if (data['LIST_GM']) {
        var aGm = data['LIST_GM'].split(' - ');
        load_selected_option('#GM_LIST', data['LIST_GM'], "[" + aGm[0] + "] " + aGm[1] + " - " + aGm[3]);
    }
    // LIST APP
    if (data['LIST_APP']) {
        var aApp = data['LIST_APP'].split(' - ');
        load_selected_option('#APP_LIST', data['LIST_APP'], "[" + aApp[0] + "] " + aApp[1] + " - " + aApp[3]);
    }
    // LIST Paraf
    if (data['LIST_PARAF']) {
        var aPar = data['LIST_PARAF'].split(' - ');
        load_selected_option('#PARAF_LIST', data['LIST_PARAF'], "[" + aPar[0] + "] " + aPar[1] + " - " + aPar[3]);
    }

    // LIST Created
    if (data['LIST_CRT']) {
        var aCrt = data['LIST_CRT'].split(' - ');
        load_selected_option('#CRT_LIST', data['LIST_CRT'], "[" + aCrt[0] + "] " + aCrt[1] + " - " + aCrt[3]);
    }

    // RKS
    if (data['RKS_FILE']) {
        var aFiles = data['RKS_FILE'].split('/');

        $('#rks_uri').attr('href', data['RKS_FILE']);
        $('#rks_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#rks_name').addClass('fa fa-file-pdf-o');
    }
    // BQ
    if (data['BQ_FILE']) {
        var aFiles = data['BQ_FILE'].split('/');

        $('#bq_uri').attr('href', data['BQ_FILE']);
        $('#bq_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#bq_name').addClass('fa fa-file-pdf-o');
    }
    // Drawing
    if (data['DRAW_FILE']) {
        var aFiles = data['DRAW_FILE'].split('/');

        $('#draw_uri').attr('href', data['DRAW_FILE']);
        $('#draw_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#draw_name').addClass('fa fa-file-pdf-o');
    }
    // ECE
    if (data['ECE_FILE']) {
        var aFiles = data['ECE_FILE'].split('/');

        $('#ece_uri').attr('href', data['ECE_FILE']);
        $('#ece_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#ece_name').addClass('fa fa-file-pdf-o');
    }
    // Kajian
    if (data['KAJIAN_FILE']) {
        var aFiles = data['KAJIAN_FILE'].split('/');

        $('#kajian_uri').attr('href', data['KAJIAN_FILE']);
        $('#kajian_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#kajian_name').addClass('fa fa-file-pdf-o');
    }
    // Kajian
    if (data['OTHERS_FILE']) {
        var aFiles = data['OTHERS_FILE'].split('/');

        $('#others_uri').attr('href', data['OTHERS_FILE']);
        $('#others_name').text(' ' + aFiles[(aFiles.length - 1)]);
        $('#others_name').addClass('fa fa-file-pdf-o');
    }

    if (data['STT_APP'] > 0) {
        $('#btn-save').hide();
    }
    if (data['TIPE'] != 'Non-Engineering') {
        if (/detail/i.test(data['JENIS'])) {
            var e = document.getElementById("kajian");
            var f = document.getElementById("capex");
            //console.log(e);
            e.style.display = "none";
            f.style.display = "block";
        } else {
            var e = document.getElementById("capex");
            var f = document.getElementById("kajian");
            //console.log(e);
            e.style.display = "none";
            f.style.display = "block";
        }
    } else {
        if (data['STATUS'] == 'In Approval') {
            $('.browse-file').show();
        }
        $('.trn-foreign').hide();
        $('#PACKET_TEXT').attr("readonly", false);
    }

    $('#ID_DOK_ENG').select2();
    $('#TIPE').select2();
    $('.selectpicker').selectpicker('refresh');
    setActiveTab('ul-new', 'new-form');
    // $('#btn-save').html(' <i class="fa fa-check"></i> Edit');
    $('#ul-new').text('View Engineering Task');
    if (page_state.indexOf(page_short + '-modify=1') == -1) {
        $('.' + page_short + '-modify').hide();
    }
});

// save
$("#fm-new").submit(function (e) {
    e.preventDefault();
    $("#btn-save").prop("disabled", true);
    $("#btn-save").addClass("disabled");

    id = $('#ID').val();
    no_dok_tend = $('#NO_DOK_TEND').val();
    tipe = $('#TIPE').val();
    id_dok_eng = $('#ID_DOK_ENG').val();
    if (tipe == 'Non-Engineering') {
        id_dok_eng = '0';
    }
    // NO_DOK_ENG = $('#NO_DOK_ENG').val();
    notifikasi = $('#NOTIFIKASI').val();
    notifikasi_iw21 = $('#NOTIFIKASI_IW21').val();
    if (tipe == 'Non-Engineering') {
        notifikasi = '-';
        notifikasi_iw21
    }
    id_packet = $('#ID_PACKET').val();
    packet_text = $('#PACKET_TEXT').val();
    nominal = toNumber($('#NOMINAL').val());
    rkspath = document.getElementById('RKS_FILE').files[0];
    rksfile = $('#rks_uri').attr('href');
    bqpath = document.getElementById('BQ_FILE').files[0];
    bqfile = $('#bq_uri').attr('href');
    drawpath = document.getElementById('DRAW_FILE').files[0];
    drawfile = $('#draw_uri').attr('href');
    ecepath = document.getElementById('ECE_FILE').files[0];
    ecefile = $('#ece_uri').attr('href');
    otherspath = document.getElementById('OTHERS_FILE').files[0];
    othersfile = $('#others_uri').attr('href');
    company = $('#COMPANY').val();
    dept_code = $('#DEPT_CODE').val();
    dept_text = $('#DEPT_TEXT').val();
    uk_code = $('#UK_CODE').val();
    uk_text = $('#UK_TEXT').val();
    // tmp_crt = $('#APP_CRT').val();
    // tmp_app = $('#APP_LIST').val();
    // tmp_paraf = $('#PARAF_LIST').val();

    var formURL = "";

    var form_data = new FormData();
    form_data.append('ID', id);
    form_data.append('NO_DOK_TEND', no_dok_tend);
    form_data.append('TIPE', tipe);
    form_data.append('ID_DOK_ENG', id_dok_eng);
    // form_data.append('NO_DOK_ENG',NO_DOK_ENG);
    form_data.append('NOTIFIKASI', notifikasi_iw21);
    form_data.append('ID_PACKET', id_packet);
    form_data.append('PACKET_TEXT', packet_text);
    form_data.append('NOMINAL', nominal);
    // RKS
    if (rkspath) {
        form_data.append('RKSPATH', rkspath);
    } else {
        if (rksfile && !id) {
            form_data.append('RKS_FILE', rksfile);
        }
    }
    // BQ
    if (bqpath && !id) {
        form_data.append('BQPATH', bqpath);
    } else {
        if (bqfile && !id) {
            form_data.append('BQ_FILE', bqfile);
        }
    }
    // Drawing
    if (drawpath) {
        form_data.append('DRAWPATH', drawpath);
    } else {
        if (drawfile && !id) {
            form_data.append('DRAW_FILE', drawfile);
        }
    }
    // ECE
    if (ecepath) {
        form_data.append('ECEPATH', ecepath);
    } else {
        if (ecefile && !id) {
            form_data.append('ECE_FILE', ecefile);
        }
    }
    // Others
    if (otherspath) {
        form_data.append('OTHERSPATH', otherspath);
    } else {
        if (ecefile && !id) {
            form_data.append('OTHERS_FILE', othersfile);
        }
    }

    if (company) {
        form_data.append('COMPANY', company);
    }
    if (dept_code) {
        form_data.append('DEPT_CODE', dept_code);
    }
    if (dept_text) {
        form_data.append('DEPT_TEXT', dept_text);
    }
    if (uk_code) {
        form_data.append('UK_CODE', uk_code);
    }
    if (uk_text) {
        form_data.append('UK_TEXT', uk_text);
    }

    // if (parseInt(toNumber($('#NOMINAL').val()))>=10000000000) {
    //   form_data.append('LIST_GM', $('#GM_LIST').val());
    // }

    if ($('#GM_LIST').val()) {
        form_data.append('LIST_GM', $('#GM_LIST').val());
    }
    form_data.append('LIST_APP', $('#APP_LIST').val());
    form_data.append('LIST_PARAF', $('#PARAF_LIST').val());
    form_data.append('LIST_CRT', $('#CRT_LIST').val());

    var uri = 'create';
    if (id) {
        uri = 'update';
    }

    $.ajax({
        url: 'scm/tender/' + uri, // point to server-side PHP script
        dataType: 'json', // what to expect back from the PHP script, if anything
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        error: function (xhr, status, error) {
            swal({
                title: "Data Save : NOT Success! " + error,
                type: "error"
            });
        },
        success: function (json) {
            if (json['status'] == 200) {
                tb_dok_tend();

                if (id_dok_eng == '0') {
                    no_dok_tend = '';
                }
                swal({
                    title: "Data Saved!\n" + no_dok_tend,
                    type: "success"
                });
                reset_form('yes');
            }
        }
    });
    $("#btn-save").removeClass("disabled");
    $("#btn-save").prop("disabled", false);
    return false;
});

// edit

// delete
$(document).on('click', ".btnDel", function () {
    var data = oTable.row($(this).parents('tr')).data();
    swal({
        title: "Are you sure?",
        text: "Data (" + data['NO_DOK_TEND'] + ") will be deleted!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Delete",
        confirmButtonClass: "btn-danger",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    url = 'scm/tender/delete/' + data['ID'];
                    $.ajax({
                        url: url, // point to server-side PHP script
                        dataType: 'json', // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        type: 'get',
                        error: function (xhr, status, error) {
                            swal("Error", "Your data failed to delete!", "error");
                        },
                        success: function (json) {
                            if (json['status'] == 200) {
                                tb_dok_tend();
                                swal("Deleted!", "Your data has been deleted.", "success");
                            }
                        }
                    });
                } else {
                    swal("Cancelled", "Deletion of data canceled", "error");
                }
            });
});
