var oTable;

$(document).ready(function() {
  // roles
  // console.log(page_state);
  if (page_state.indexOf(page_short + '-slide=1') == -1) {
    $('.' + page_short + '-slide').hide();
  } else {
    // vmc slider
    // $('#vmc-slide').vmcSimpleSlide({
    //   // width/height
    //   width: 'auto',
    //   height: 'auto',
    //   // min width
    //   minWidth: 0,
    //   // min height
    //   minHeight: 0,
    //   // autoplay
    //   auto: true,
    //   // 'right' or 'left'
    //   autoPlayDirection: 'right',
    //   // duration of animation
    //   duration: 4000,
    //   // animation speed
    //   speed: 600,
    //   // shows navigation buttons
    //   showSideButton: true,
    //   // shows pagination bullets
    //   showBottomButton: true,
    //   // shows image caption
    //   showSummary: 'auto',
    //   // allows HTML content
    //   summaryIsHtml: false,
    //   // pause on hover
    //   hoverStop: true
    // });
    // $('#vmc-slide').vmcSimpleSlide();
    // $('#vmc-slide').show();

    // oka slider
    $('.slider_model').oka_slider_model({
      // slider type. 0~9
      'type': 4,
      // animation speed
      'animateSpeed': 500,
      'speed': 5000
    });
    // $('.slider_model').oka_slider_model();
  }
  if (page_state.indexOf(page_short + '-widget-erf=1') == -1) {
    $('.' + page_short + '-widget-erf').hide();
  } else counterf();
  if (page_state.indexOf(page_short + '-widget-eat=1') == -1) {
    $('.' + page_short + '-widget-eat').hide();
  } else counteat();
  if (page_state.indexOf(page_short + '-widget-docs=1') == -1) {
    $('.' + page_short + '-widget-docs').hide();
  } else countdoc();
  if (page_state.indexOf(page_short + '-widget-bast=1') == -1) {
    $('.' + page_short + '-widget-bast').hide();
  } else countBAST();
  if (page_state.indexOf(page_short + '-graph-date=1') == -1) {
    $('.' + page_short + '-graph-date').hide();
  } else chart_date();
  if (page_state.indexOf(page_short + '-graph-notif=1') == -1) {
    $('.' + page_short + '-graph-notif').hide();
  } else chart_notif();
  if (page_state.indexOf(page_short + '-graph-erf=1') == -1) {
    $('.' + page_short + '-graph-erf').hide();
  } else chart_erf();
  if (page_state.indexOf(page_short + '-graph-stat=1') == -1) {
    $('.' + page_short + '-graph-stat').hide();
  } else chart_stat();
  if (page_state.indexOf(page_short + '-work-doc=1') == -1) {
    $('.' + page_short + '-work-doc').hide();
  } else chart_all();
  if (page_state.indexOf(page_short + '-work-stat=1') == -1) {
    $('.' + page_short + '-work-stat').hide();
  } else getchart3();
  if (page_state.indexOf(page_short + '-work-prog=1') == -1) {
    $('.' + page_short + '-work-prog').hide();
  } else tb_prog();
});

function tb_prog() {
  oTable = $('#tb_prog').DataTable({
    destroy: true,
    processing: true,
    serverSide: true,
    ajax: {
      url: 'eng/progress/data_list/all',
      type: "POST"
    },
    columns: [
      {
        "data": "PROGRESS",
        "width": 100,
        "className": "text-left",
        "mRender": function(row, data, index) {
          if (!index['REMARKS']) {
            index['REMARKS'] = '-';
          }
          state = '<div class="media"> <div class="media-left"></div> <div class="media-body"> <h4 class="media-heading">' + index['DESKRIPSI'] + '</h4> <p><small>note : ' + index['REMARKS'] + '<br /><small class="pull-right">' + index['CREATE_AT'] + '</small><small></p> </div> </div>';
          return state;
        }
      },
    ],
    "order": [
      [0, "desc"]
    ]

  });
}

function counterf() {
  $.ajax({
    type: 'POST',
    url: 'welcome/countall',
    data: {
      from: 'MPE_PENGAJUAN',
    },
    success: function(json) {
      json = $.parseJSON(json);
      $('#erfall').append(json);
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    },
    complete: function() {
      $.ajax({
        type: 'POST',
        url: 'welcome/count',
        data: {
          from: 'MPE_PENGAJUAN',
          status: "STATUS = 'Open' AND (STATE = 'Active' OR STATE IS NULL)",
        },
        success: function(json) {
          json = $.parseJSON(json);
          $('#erfopen').append(json);
        },
        error: function(xhr, status, error) {
          alert('Server Error ...');
        },
        complete: function() {
          $.ajax({
            type: 'POST',
            url: 'welcome/count',
            data: {
              from: 'MPE_PENGAJUAN',
              status: "(STATUS = 'In Progress' OR STATUS = 'Approved') AND ID_MPE NOT IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID_PENGAJUAN=MPE_PENGAJUAN.ID_MPE AND DELETE_AT IS NULL) AND (STATE = 'Active' OR STATE IS NULL)",
            },
            success: function(json) {
              json = $.parseJSON(json);
              $('#erfprog').append(json);
            },
            error: function(xhr, status, error) {
              alert('Server Error ...');
            },
            complete: function() {
              $.ajax({
                type: 'POST',
                url: 'welcome/count',
                data: {
                  from: 'MPE_PENGAJUAN',
                  status: "ID_MPE IN (SELECT ID_PENGAJUAN FROM MPE_PENUGASAN WHERE ID_PENGAJUAN=MPE_PENGAJUAN.ID_MPE AND DELETE_AT IS NULL)",
                },
                success: function(json) {
                  json = $.parseJSON(json);

                  $('#erfclose').append(json);

                },
                error: function(xhr, status, error) {
                  alert('Server Error ...');
                },
                complete: function() {
                  $.ajax({
                    type: 'POST',
                    url: 'welcome/count',
                    data: {
                      from: 'MPE_PENGAJUAN',
                      status: "STATUS = 'Rejected' AND (STATE = 'Active' OR STATE IS NULL)",
                    },
                    success: function(json) {
                      json = $.parseJSON(json);


                      $('#erfreject').append(json);


                    },
                    error: function(xhr, status, error) {
                      alert('Server Error ...');
                    },
                    complete: function() {

                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function counteat() {
  $.ajax({
    type: 'POST',
    url: 'welcome/countall',
    data: {
      from: 'MPE_PENUGASAN',
    },
    success: function(json) {
      json = $.parseJSON(json);

      $('#eattotal').append(json);

    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    },
    complete: function() {
      $.ajax({
        type: 'POST',
        url: 'welcome/count',
        data: {
          from: 'MPE_PENUGASAN',
          status: "STATUS = 'For Approval'",
        },
        success: function(json) {
          json = $.parseJSON(json);


          $('#eatopen').append(json);


        },
        error: function(xhr, status, error) {
          alert('Server Error ...');
        },
        complete: function() {
          $.ajax({
            type: 'POST',
            url: 'welcome/count',
            data: {
              from: 'MPE_PENUGASAN',
              status: "(STATUS = 'In Approval' OR STATUS = 'Approved Biro' OR STATUS = 'SM Approved')",
            },
            success: function(json) {
              json = $.parseJSON(json);

              $('#eatprog').append(json);


            },
            error: function(xhr, status, error) {
              alert('Server Error ...');
            },
            complete: function() {
              $.ajax({
                type: 'POST',
                url: 'welcome/count',
                data: {
                  from: 'MPE_PENUGASAN',
                  status: "STATUS = 'GM Approved'",
                },
                success: function(json) {
                  json = $.parseJSON(json);

                  $('#eatclose').append(json);

                },
                error: function(xhr, status, error) {
                  alert('Server Error ...');
                },
                complete: function() {
                  $.ajax({
                    type: 'POST',
                    url: 'welcome/count',
                    data: {
                      from: 'MPE_PENUGASAN',
                      status: "STATUS = 'Rejected'",
                    },
                    success: function(json) {
                      json = $.parseJSON(json);

                      $('#eatreject').append(json);

                    },
                    error: function(xhr, status, error) {
                      alert('Server Error ...');
                    },
                    complete: function() {

                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function countdoc() {
  $.ajax({
    type: 'POST',
    url: 'welcome/countall',
    data: {
      from: 'MPE_DOK_ENG',
    },
    success: function(json) {
      json = $.parseJSON(json);

      $('#doctotal').append(json);

    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    },
    complete: function() {
      $.ajax({
        type: 'POST',
        url: 'welcome/count',
        data: {
          from: 'MPE_DOK_ENG',
          status: "(SELECT DISTINCT COUNT(*) FROM MPE_APPROVAL WHERE REF_ID = MPE_DOK_ENG.ID AND TIPE = 'TTD' AND STATE='Approved') = 0",
        },
        success: function(json) {
          json = $.parseJSON(json);

          $('#docopen').append(json);

        },
        error: function(xhr, status, error) {
          alert('Server Error ...');
        },
        complete: function() {
          $.ajax({
            type: 'POST',
            url: 'welcome/count',
            data: {
              from: 'MPE_DOK_ENG',
              status: "(STATUS = 'Mgr Pengkaji Approved' OR STATUS = 'SM Pengkaji Approved' OR STATUS = 'Mgr Approved' OR STATUS = 'SM Approved')",
            },
            success: function(json) {
              json = $.parseJSON(json);

              $('#docprog').append(json);

            },
            error: function(xhr, status, error) {
              alert('Server Error ...');
            },
            complete: function() {
              $.ajax({
                type: 'POST',
                url: 'welcome/count',
                data: {
                  from: 'MPE_DOK_ENG',
                  status: "STATUS = 'GM Approved'",
                },
                success: function(json) {
                  json = $.parseJSON(json);

                  $('#docclose').append(json);

                },
                error: function(xhr, status, error) {
                  alert('Server Error ...');
                },
                complete: function() {
                  $.ajax({
                    type: 'POST',
                    url: 'welcome/count',
                    data: {
                      from: 'MPE_DOK_ENG',
                      status: "STATUS = 'Rejected'",
                    },
                    success: function(json) {
                      json = $.parseJSON(json);

                      $('#docreject').append(json);

                    },
                    error: function(xhr, status, error) {
                      alert('Server Error ...');
                    },
                    complete: function() {

                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function countBAST() {
  $.ajax({
    type: 'POST',
    url: 'welcome/countall',
    data: {
      from: 'MPE_BAST',
    },
    success: function(json) {
      json = $.parseJSON(json);

      $('#basttotal').append(json);

    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    },
    complete: function() {
      $.ajax({
        type: 'POST',
        url: 'welcome/count',
        data: {
          from: 'MPE_BAST',
          status: "STATUS IS NULL",
        },
        success: function(json) {
          json = $.parseJSON(json);

          $('#bastopen').append(json);

        },
        error: function(xhr, status, error) {
          alert('Server Error ...');
        },
        complete: function() {
          $.ajax({
            type: 'POST',
            url: 'welcome/count',
            data: {
              from: 'MPE_BAST',
              status: "STATUS = 'Approved SM Cust.' OR STATUS = 'Approved GM Cust.' OR STATUS = 'Approved BIRO'",
            },
            success: function(json) {
              json = $.parseJSON(json);
              $('#bastprog').append(json);

            },
            error: function(xhr, status, error) {
              alert('Server Error ...');
            },
            complete: function() {
              $.ajax({
                type: 'POST',
                url: 'welcome/count',
                data: {
                  from: 'MPE_BAST',
                  status: "STATUS = 'Closed'",
                },
                success: function(json) {
                  json = $.parseJSON(json);

                  $('#bastclose').append(json);

                },
                error: function(xhr, status, error) {
                  alert('Server Error ...');
                },
                complete: function() {
                  $.ajax({
                    type: 'POST',
                    url: 'welcome/count',
                    data: {
                      from: 'MPE_BAST',
                      status: "STATUS = 'Rejected'",
                    },
                    success: function(json) {
                      json = $.parseJSON(json);

                      $('#bastreject').append(json);

                    },
                    error: function(xhr, status, error) {
                      alert('Server Error ...');
                    },
                    complete: function() {

                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

function getChart() {
  $.ajax({
    type: 'POST',
    url: 'report/monitor/count_report',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          graph = json['data'][0];

          // Morris bar chart
          Morris.Bar({
            element: 'morris-bar-chart',
            data: [{
              y: 'ERF',
              a: graph['J_ERF']
            }, {
              y: 'EAT',
              a: graph['J_EAT']
            }, {
              y: 'Documents',
              a: graph['J_DOK']
            }, {
              y: 'BAST',
              a: graph['J_BAST']
            }],
            xkey: 'y',
            ykeys: ['a'],
            labels: ['Total '],
            barColors: ['#448d8d'],
            hideHover: 'auto',
            gridLineColor: '#eef0f2',
            resize: true
          });

        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function getChart2() {
  $.ajax({
    type: 'POST',
    url: 'report/monitor/count_report',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          graph = json['data'][0];

          // Morris bar chart
          Morris.Donut({
            element: 'morris-donut-chart',
            data: [{
              label: 'ERF',
              value: graph['J_ERF']
            }, {
              label: 'EAT',
              value: graph['J_EAT']
            }, {
              label: 'Documents',
              value: graph['J_DOK']
            }, {
              label: 'BAST',
              value: graph['J_BAST']
            }],
            formatter: function(y) {
              return y + "%"
            }

          });

        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });
}

function getchart3() {
  $.ajax({
    type: 'POST',
    url: 'report/monitor/count_report',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          graph = json['data'][0];
          // console.log(graph);
          // Morris bar chart
          Highcharts.chart('hg-chart', {
            chart: {
              type: 'spline'
            },
            title: {
              text: ''
            },
            xAxis: {
              categories: ['ERF', 'EAT', 'PROGRESS', 'DOC', 'BAST']
            },
            yAxis: {
              title: {
                text: 'Total'
              },
              labels: {
                formatter: function() {
                  return this.value;
                }
              }
            },
            tooltip: {
              crosshairs: true,
              shared: true
            },
            plotOptions: {
              spline: {
                marker: {
                  radius: 4,
                  lineColor: '#666666',
                  lineWidth: 1
                }
              }
            },
            series: [{
              name: 'Total',
              marker: {
                symbol: 'square'
              },
              data: [parseInt(graph['J_ERF']), parseInt(graph['J_EAT']),
                parseInt(graph['J_PROG']), parseInt(graph['J_DOK']), parseInt(graph['J_BAST'])
              ]

            }]
          });

        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });

}

function chart_all() {
  $.ajax({
    type: 'POST',
    url: 'welcome/grafik',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          graph = json['data'];
          // console.log(graph);

          var str_series = ' [';
          var rnum = 1;
          $.each(graph, function(index, value) {
            if (rnum > 1) {
              str_series += ',';
            }
            str_series += "{name: '" + index + "', data: [" + value[0] + ", " + value[1] + ", " + value[2] + ", " + value[3] + ", " + value[4] + ", " + value[5] + ", " + value[6] + ", " + value[7] + ", " + value[8] + ", " + value[9] + ", " + value[10] + ", " + value[11] + "]}";
            rnum++;
          });
          str_series += ']';
          // console.log(str_series);
          Highcharts.setOptions({
            colors: ['#ED561B', '#DDDF00', '#64E572', '#FFF263', '#058DC7', '#50B432', '#FF9655', '#24CBE5', '#6AF9C4']
          });
          Highcharts.chart('chart-all', {
            chart: {
              // type: 'spline',
              // backgroundColor: {
              //   linearGradient: [0, 0, 500, 500],
              //   stops: [
              //     [0, 'rgb(255, 255, 255)'],
              //     [1, 'rgb(200, 200, 255)']
              //   ]
              // },
              // polar: true,
              type: 'line'
            },
            title: {
              text: 'Design Engineering Status ' + (new Date()).getFullYear()
            },
            subtitle: {
              text: 'Jumlah \'valid\' dokumen berdasar periode bulan dalam satu tahun.'
            },
            xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
              title: {
                text: 'Jumlah'
              }
            },
            credits: {
              enabled: false
            },
            plotOptions: {
              line: {
                dataLabels: {
                  enabled: true
                },
                enableMouseTracking: false
              }
            },
            series: eval(str_series)
          });

        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });

}

function chart_notif() {
  $.ajax({
    type: 'POST',
    url: 'welcome/grafik_notif',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        var str_series = ' [';
        if (json['status'] == 200) {
          graph = json['data'];
          // console.log(graph);

          var rnum = 1;
          $.each(graph, function(index, value) {
            if (rnum > 1) {
              str_series += ',';
            }
            str_series += "{name: '" + index + "', data: [" + value[0] + ", " + value[1] + ", " + value[2] + ", " + value[3] + ", " + value[4] + ", " + value[5] + ", " + value[6] + ", " + value[7] + ", " + value[8] + ", " + value[9] + ", " + value[10] + ", " + value[11] + "]}";
            rnum++;
          });
        }

        str_series += ']';
        // console.log(str_series);
        Highcharts.setOptions({
          colors: ['#64E572', '#FF9655', '#FFF263', '#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#6AF9C4']
        });
        Highcharts.chart('chart-notif', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'Chart of Amount Notification ' + (new Date()).getFullYear()
          },
          subtitle: {
            text: 'Jumlah \'notifikasi\' aktif berdasar periode bulan dalam satu tahun.'
          },
          xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
          },
          yAxis: {
            title: {
              text: 'Jumlah'
            }
          },
          credits: {
            enabled: false
          },
          plotOptions: {
            line: {
              dataLabels: {
                enabled: true
              },
              enableMouseTracking: false
            }
          },
          series: eval(str_series)
        });
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });

}

function chart_erf() {
  $.ajax({
    type: 'POST',
    url: 'welcome/grafik_erf',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        var str_hdr = ' [';
        var str_dtl = ' [';
        if (json['status'] == 200) {
          graph = json['data'];

          var dt_dtl = '';
          var scomp = '';
          var j = Object.keys(graph).length;
          $.each(graph, function(index, value) {
            // insert header
            if (scomp != value['COMPANY']) {
              if (str_hdr != ' [') {
                str_hdr += ',';
              }
              str_hdr += "{name: '" + value['GEN_PAR5'] + "', y: " + value['JML_CMP'] + ", drilldown: '" + value['GEN_PAR5'] + "'}";
              scomp = value['COMPANY'];
            }

            // insert detail
            if (dt_dtl != '') {
              dt_dtl += ',';
            }
            dt_dtl += "['" + value['DEPT_TEXT'] + "', " + value['JML'] + "]";
            if (j != (index + 1)) {
              if (graph[index + 1]['COMPANY'] != value['COMPANY']) {
                if (str_dtl.indexOf('name') != -1) {
                  str_dtl += ',';
                }
                str_dtl += "{name: '" + value['GEN_PAR5'] + "', id: '" + value['GEN_PAR5'] + "', data: [" + dt_dtl + "]}";
                dt_dtl = '';
              }
            } else {
              if (str_dtl.indexOf('name') != -1) {
                str_dtl += ',';
              }
              str_dtl += "{name: '" + value['GEN_PAR5'] + "', id: '" + value['GEN_PAR5'] + "', data: [" + dt_dtl + "]}";
            }
          });

        }
        str_hdr += ']';
        str_dtl += ']';
        // console.log(eval(str_hdr));
        // console.log(eval(str_dtl));

        str_series = "'series': [{'name': 'Plants', 'colorByPoint': true, 'data': " + str_hdr + " }], 'drilldown': { 'series': " + str_dtl + "}";

        // Create the chart
        Highcharts.setOptions({
          colors: ['#64E572', '#FF9655', '#FFF263', '#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#6AF9C4']
        });
        Highcharts.chart('chart-erf', {
          chart: {
            type: 'pie'
          },
          title: {
            text: 'Chart of Request Amount ' + (new Date()).getFullYear()
          },
          subtitle: {
            text: 'Klik untuk lebih lanjut melihat data unit kerja.'
          },
          credits: {
            enabled: false
          },
          plotOptions: {
            series: {
              dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y}'
              }
            }
          },

          tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
          },

          "series": [{
            "name": "Plants",
            "colorByPoint": true,
            "data": eval(str_hdr)
          }],
          "drilldown": {
            "series": eval(str_dtl)
          }
        });

      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });

}

function chart_date() {
  $.ajax({
    type: 'POST',
    url: 'welcome/grafik_date',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        var str_srs = ' [';
        var str_hdr = ' [';
        var str_dtl = ' [';
        if (json['status'] == 200) {
          graph = json['data'];

          var dt_dtl = '';
          var scomp = '';
          var rno = 1;
          var j = Object.keys(graph).length;
          $.each(graph, function(index, value) {
            // insert header
            if (scomp != value['ID']) {
              if (str_hdr != ' [') {
                str_hdr += ',';
              }
              var dStart = new Date(value['START_DATE']);
              var dEnd = new Date(value['END_DATE']);
              str_hdr += "{name: '" + value['ID'] + "', sno:'" + value['NO_PENUGASAN'] + "', snam:'" + value['ERF_TEXT'] + "', x:" + index + ", low: Date.UTC(" + dStart.getFullYear() + ", " + dStart.getMonth() + ", " + dStart.getDate() + " ), high: Date.UTC(" + dEnd.getFullYear() + ", " + dEnd.getMonth() + ", " + dEnd.getDate() + " ), sdate:'" + value['START_DATE'] + "', edate:'" + value['END_DATE'] + "', drilldown: '" + value['ID'] + "'}";
              scomp = value['ID'];
            }

            // insert detail
            if (dt_dtl != '') {
              dt_dtl += ',';
              rno++;
            }
            var dStart = new Date(value['START_DATE']);
            var dEnd = new Date(value['BAST_DATE']);
            dt_dtl += "{x: " + index + ", sno:'P" + rno + "', snam:'" + value['DESKRIPSI'] + "', low: Date.UTC(" + dStart.getFullYear() + ", " + dStart.getMonth() + ", " + dStart.getDate() + " ), high: Date.UTC(" + dEnd.getFullYear() + ", " + dEnd.getMonth() + "), sdate:'" + value['START_DATE'] + "', edate:'" + value['BAST_DATE'] + "'}";
            if (j != (index + 1)) {
              if (graph[index + 1]['ID'] != value['ID']) {
                if (str_dtl.indexOf('id') != -1) {
                  str_dtl += ',';
                  rno++;
                }
                str_dtl += "{id: '" + value['ID'] + "', data: [" + dt_dtl + "]}";
                str_srs += dt_dtl;
                dt_dtl = '';
                rno = 1;
              }
            } else {
              if (str_dtl.indexOf('id') != -1) {
                str_dtl += ',';
                rno++;
              }
              str_dtl += "{id: '" + value['ID'] + "', data: [" + dt_dtl + "]}";
              str_srs += dt_dtl;
            }
          });
          // str_hdr += ']';
          // str_dtl += ']';
          // str_srs += ']';


        }

        str_hdr += ']';
        str_dtl += ']';
        str_srs += ']';

        // Create the chart
        Highcharts.setOptions({
          colors: ['#058DC7', '#ED561B', '#DDDF00', '#64E572', '#50B432', '#FF9655', '#24CBE5', '#FFF263', '#6AF9C4']
        });
        Highcharts.chart('chart-date', {
          chart: {
            type: 'columnrange',
            inverted: true
          },
          title: {
            text: 'Chart of Planning Engineering Assign Task ' + (new Date()).getFullYear()
          },
          // subtitle: {
          //   text: 'Klik untuk melihat grafik realisasi paket.'
          // },
          xAxis: {
            labels: {
              enabled: true,
              format: '{point.sno}'
            }
          },
          yAxis: {
            type: 'datetime',
            dateTimeLabelFormats: {
              day: '%e %b %Y'
            }
          },
          plotOptions: {
            columnrange: {
              dataLabels: {
                enabled: true,
                format: '{point.sno}'
              }
            }
          },
          tooltip: {
            headerFormat: '<span style="font-size:11px">{point.sno}</span><br>',
            pointFormat: '<span style="font-size:11px">{point.snam}</span><br><span style="color:{point.color}">{point.sdate} : {point.edate}</span><br/>'
          },
          legend: {
            enabled: false
          },
          credits: {
            enabled: false
          },
          series: [{
            name: 'Planning',
            colorByPoint: true,
            data: eval(str_hdr)
          }],
          // drilldown: {
          //   series: eval(str_dtl)
          // }

        });
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });

}

function chart_stat() {
  $.ajax({
    type: 'POST',
    url: 'welcome/grafik_stat',
    success: function(json) {
      json = $.parseJSON(json);
      foreigns = '';
      if (json) {
        if (json['status'] == 200) {
          graph = json['data']['STS'];
          graph2 = json['data']['TOT'];
          // console.log(graph);

          var str_series = ' [';
          var rnum = 1;
          $.each(graph, function(index, value) {
            if (rnum > 1) {
              str_series += ',';
            }
            str_series += "{type: 'column', name: '" + index + "', data: [" + value[0] + ", " + value[1] + ", " + value[2] + ", " + value[3] + ", " + value[4] + ", " + value[5] + ", " + value[6] + ", " + value[7] + ", " + value[8] + ", " + value[9] + ", " + value[10] + ", " + value[11] + "]}";
            rnum++;
          });
          $.each(graph2, function(index, value) {
            if (rnum > 1) {
              str_series += ',';
            }
            str_series += "{type: 'spline', name: '" + index + "', data: [" + value[0] + ", " + value[1] + ", " + value[2] + ", " + value[3] + ", " + value[4] + ", " + value[5] + ", " + value[6] + ", " + value[7] + ", " + value[8] + ", " + value[9] + ", " + value[10] + ", " + value[11] + "]}";
            rnum++;
          });
          str_series += ']';
          // console.log(str_series);
          Highcharts.setOptions({
            colors: ['#A134EB', '#DDDF00', '#6AF9C4', '#ED561B', '#FF9655', '#64E572', '#FFF263', '#058DC7', '#24CBE5']
          });
          Highcharts.chart('chart-stat', {
            title: {
              text: 'Progress Evaluation Chart of Design Engineering ' + (new Date()).getFullYear()
            },
            subtitle: {
              text: 'Engineering, Order dan CLosed'
            },
            xAxis: {
              categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            },
            yAxis: {
              title: {
                text: 'Jumlah'
              }
            },
            credits: {
              enabled: false
            },
            plotOptions: {
              series: {
                dataLabels: {
                  enabled: true
                },
                enableMouseTracking: true
              }
            },
            series: eval(str_series)
          });

        }
      }
    },
    error: function(xhr, status, error) {
      alert('Server Error ...');
    }
  });

}
